<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>


<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Fix Lane : Adv/Exp/Diesel</h3>
              </div>
				
<div class="card-body">
	
	<div class="row">
		<div class="col-md-12">	
			<div class="form-group">
				<button type="button" data-toggle="modal" data-target="#AddForm" class="btn btn-sm btn-success">Add New Record</button>
			</div>	   
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-12 table-responsive" style="overflow:auto">
		
<table id="example" class="table table-bordered" style="font-size:12px;">
	<thead>
	<tr>    
		<th>#</th>
		<th>From</th>
		<th>To</th>
		<th>Vehicle_no.</th>
		<th>Consignor</th>
		<th>Consignee</th>
		<th>Empty<br>Loaded</th>
		<th>Wheeler</th>
		<th>Is_active</th>
		<th>Timestamp</th>
		<th>#</th>
		<th>#</th>
		<th>#</th>
	</tr>
	</thead>
	<tbody>
<?php
$get_record = Qry($conn,"SELECT l.id,l.is_active,l.tno,l.empty_loaded,l.truck_type,l.timestamp,s1.name as from_loc,s2.name as to_loc,
c.name as consignor,c2.name as consignee 
FROM dairy.fix_lane AS l 
LEFT OUTER JOIN station as s1 ON s1.id=l.from_id 
LEFT OUTER JOIN station as s2 ON s2.id=l.to_id 
LEFT OUTER JOIN consignor as c ON c.id=l.consignor 
LEFT OUTER JOIN consignor as c2 ON c2.id=l.consignee 
ORDER BY l.id ASC");

if(!$get_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn=1;
while($row = fetchArray($get_record))
{
	if($row['is_active']=="1"){
		$is_active="<font color='green'>Active</font>";
		$disable_active="disabled";
		$disable_disable="";
	}
	else{
		$is_active="<font color='red'>In-active</font>";
		$disable_active="";
		$disable_disable="disabled";
	}
	
	if($row['empty_loaded']=="1"){
		$empty_loaded="<font color='green'>Loaded</font>";
	}
	else if($row['empty_loaded']=="2"){
		$empty_loaded="<font color='red'>Loaded+Empty</font>";
	}
	else{
		$empty_loaded="<font color='blue'>Empty</font>";
	}
	
	echo '<tr> 
		<td>'.$sn.'</td>
		<td>'.$row["from_loc"].'</td>	
		<td>'.$row["to_loc"].'</td>	
		<td>'.$row["tno"].'</td>	
		<td>'.$row["consignor"].'</td>	
		<td>'.$row["consignee"].'</td>	
		<td>'.$empty_loaded.'</td>	
		<td>'.$row["truck_type"].'</td>	
		<td>'.$is_active.'</td>	
		<input type="hidden" value="'.$row["from_loc"].'" id="from_loc_'.$row["id"].'">
		<input type="hidden" value="'.$row["to_loc"].'" id="to_loc_'.$row["id"].'">
        <td>'.date("d-m-y H:i A",strtotime($row['timestamp'])).'</td>
        <td><button type="button" class="btn btn-primary btn-sm" id="view_btn_'.$row["id"].'" onclick="ViewDetails('.$row["id"].')"><i class="fa fa-eye" aria-hidden="true"></i> View</button></td>
        <td><button type="button" '.$disable_disable.' id="disable_btn_'.$row["id"].'" onclick="Disable('.$row["id"].')" class="btn btn-danger btn-sm"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Disable</button></td>
        <td><button type="button" '.$disable_active.' id="active_btn_'.$row["id"].'" onclick="Active('.$row["id"].')" class="btn btn-success btn-sm"><i class="fa fa-check-square" aria-hidden="true"></i> Active</button></td>
     </tr>';
$sn++; 
}
echo '
</tbody>
</table>';
?>			
		</div>
	</div>
</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	</div>
			
		</div>
		
		  <div id="func_result2"></div>
		  <div id="func_result33"></div>
		  
        </div>
      </div>
    </section>
  </div>
    
<script>
$(function(){  
$("#consignor").autocomplete({
	source: function(request, response) { 
	 $.ajax({
     url: 'autofill/get_consignor.php',
     type: 'post',
     dataType: "json",
     data: {
     search: request.term
    },
    success: function( data ) {
       response( data );
    }
   });
 },
select: function (event, ui) { 
  $('#consignor').val(ui.item.value);   
  $('#consignor_id').val(ui.item.id);   
  return false;
  },
   change: function (event, ui) {  
if(!ui.item){
    $(event.target).val(""); 
    $(event.target).focus();
	$("#consignor").val('');
	$("#consignor_id").val('');
	alert('Consignor not exist !'); 
}},});}); 


$(function(){  
$("#consignee").autocomplete({
	source: function(request, response) { 
	 $.ajax({
     url: 'autofill/get_consignee2.php',
     type: 'post',
     dataType: "json",
     data: {
     search: request.term
    },
    success: function( data ) {
       response( data );
    }
   });
 },
select: function (event, ui) { 
  $('#consignee').val(ui.item.value);   
  $('#consignee_id').val(ui.item.id);   
  return false;
  },
   change: function (event, ui) {  
if(!ui.item){
    $(event.target).val(""); 
    $(event.target).focus();
	$("#consignee").val('');
	$("#consignee_id").val('');
	alert('Consignee not exist !'); 
}},});}); 

$(function() {
		$("#from_loc").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
            $('#from_loc').val(ui.item.value);   
            $('#from_loc_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from_loc').val("");   
			$('#from_loc_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to_loc").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
            $('#to_loc').val(ui.item.value);   
            $('#to_loc_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_loc').val("");   
			$('#to_loc_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(document).ready(function (e) {
$("#AddForm1").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#add_new_button").attr("disabled", true);
	$.ajax({
	url: "./save_fix_lane.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
	
function Active(id)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "fix_lane_toggle.php",
		data: 'id=' + id + '&status=' + '1',
		type: "POST",
		success: function(data) {
			$("#func_result33").html(data);
		},
		error: function() {}
	});
}	
 
function Disable(id)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "fix_lane_toggle.php",
		data: 'id=' + id + '&status=' + '0',
		type: "POST",
		success: function(data) {
			$("#func_result33").html(data);
		},
		error: function() {}
	});
}	

function ViewDetails(id)
{
	$("#view_btn_"+id).attr('disabled',true);
	$('#loadicon').show();
	jQuery.ajax({
		url: "_load_fix_lane_details.php",
		data: 'id=' + id + '&from_loc=' +$('#from_loc_'+id).val() + '&to_loc=' +$('#to_loc_'+id).val(),
		type: "POST",
		success: function(data) {
			$("#func_result2").html(data);
		},
		error: function() {}
	});
}	

function closeModal(id)
{
	$('#view_btn_'+id).attr('disabled',false);
}

function DltExp(id)
{
	$("#dlt_exp_btn_"+id).attr('disabled',true);
	$('#loadicon').show();
	jQuery.ajax({
		url: "fix_lane_delete_exp.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#result_modal2").html(data);
		},
		error: function() {}
	});
}

function UpdateExp(id)
{
	var amount = $('#exp_amount_'+id).val();
	
	if(amount<=0 || amount=="")
	{
		alert('Enter Valid amount !!');
	}
	else
	{
		$("#update_exp_btn_"+id).attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
			url: "fix_lane_update_exp.php",
			data: 'id=' + id + '&amount=' + amount,
			type: "POST",
			success: function(data) {
				$("#result_modal2").html(data);
			},
			error: function() {}
		});
	}
}	

function EditAdvAmt(id)
{
	if($('#adv_amount_1').val()<0 || $('#adv_amount_1').val()=="")
	{
		alert('Enter Valid amount !!');
	}
	else
	{
		$('#edit_adv_btn').attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
			url: "fix_lane_edit_save_adv.php",
			data: 'id=' + id + '&amount=' + $('#adv_amount_1').val(),
			type: "POST",
			success: function(data) {
				$("#result_modal2").html(data);
			},
			error: function() {}
		});
	}	
}	

function EditDieselQty(id)
{
	if($('#diesel_qty_1').val()<0 || $('#diesel_qty_1').val()=="")
	{
		alert('Enter Valid amount !!');
	}
	else
	{
		$('#edit_diesel_btn').attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
			url: "fix_lane_edit_save_diesel.php",
			data: 'id=' + id + '&qty=' + $('#diesel_qty_1').val(),
			type: "POST",
			success: function(data) {
				$("#result_modal2").html(data);
			},
			error: function() {}
		});
	}	
}

function AddnewExp(id)
{
	var exp_name = $('#exp_head_name').val();
	var amount = $('#exp_amount_new').val();
	
	if(exp_name=='')
	{
		alert('select expense first !');
	}
	else if(amount<=0)
	{
		alert('enter valid amount !');
	}
	else
	{
		$('#add_exp_btn').attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
			url: "fix_lane_add_new_exp.php",
			data: 'exp_name=' + exp_name + '&amount=' + amount + '&id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_modal2").html(data);
			},
			error: function() {}
		});
	}
}

$(function() {
		$("#truck_no").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#truck_no').val(ui.item.value);   
            $('#wheeler_db').val(ui.item.wheeler);  
            $('#wheeler').val(ui.item.wheeler);  
            $('#wheeler').attr('disabled',true);  
			return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#truck_no').val("");   
			$('#wheeler_db').val("");   
			$('#wheeler').val("");   
			$('#wheeler').attr('disabled',false);  
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});	
</script> 

<form id="AddForm1" autocomplete="off"> 
<div class="modal fade" id="AddForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
	   
	  <div class="bg-primary modal-header">
			<h5 class="modal-title">Add new Fix Lane :</h5>
      </div>
	
	<div class="modal-body">
			
		<div class="row">
			
			<div class="col-md-6">
				<div class="form-group">
					<label>From Location <font color="red"><sup>required</sup></font></label>
                     <input type="text" id="from_loc" name="from_loc" class="form-control" required>
                     <input type="hidden" id="from_loc_id" name="from_loc_id" required>
                 </div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label>To Location <font color="red"><sup>required</sup></font></label>
                     <input type="text" id="to_loc" name="to_loc" class="form-control" required>
                     <input type="hidden" id="to_loc_id" name="to_loc_id" required>
                 </div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label>Wheeler <font color="red"><sup>required</sup></font></label>
                     <select id="wheeler" name="wheeler" class="form-control" required="required">
						<option value="">--select--</option>
						<option value="4">4</option>
						<option value="6">6</option>
						<option value="10">10</option>
						<option value="12">12</option>
						<option value="14">14</option>
						<option value="16">16</option>
						<option value="18">18</option>
						<option value="22">22</option>
					 </select>
                 </div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label>Empty/Loaded <font color="red"><sup>required</sup></font></label>
                     <select id="empty_loaded" onchange="EmptyLoaded(this.value)" name="empty_loaded" class="form-control" required="required">
						<option value="">--select--</option>
						<option value="0">Empty</option>
						<option value="1">Loaded</option>
						<option value="2">Both</option>
					 </select>
                 </div>
			</div>
			
			<script>
			function EmptyLoaded(elem)
			{
				$('#consignor_id').val('')
				$('#consignee_id').val('')
				$('#consignor').val('')
				$('#consignee').val('')
				
				if(elem=='0'){
					$('#consignor').attr('readonly',true);
					$('#consignee').attr('readonly',true);
				}
				else{
					$('#consignor').attr('readonly',false);
					$('#consignee').attr('readonly',false);
				}
			}
			</script>
			
			<div class="col-md-4">
				<div class="form-group">
					<label>Truck No </label>
                    <input onblur="if(this.value)==''{$('#wheeler').attr('disaled',false;)}" type="text" id="truck_no" name="truck_no" class="form-control">
                 </div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Consignor </label>
                     <input type="text" id="consignor" name="consignor" class="form-control">
					 <input type="hidden" id="consignor_id" name="consignor_id">
                 </div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Consignee </label>
                     <input type="text" id="consignee" name="consignee" class="form-control">
					 <input type="hidden" id="consignee_id" name="consignee_id">
					 <input type="hidden" id="wheeler_db" name="wheeler_db">
                 </div>
			</div>
				
			</div>
		</div>
	<div class="modal-footer">
		<button type="button" id="add_close_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="add_new_button" class="btn btn-primary">ADD Lane</button>
	</div>
		</div>	
     </div>
 </div>	
</form>
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>

<script>
$('#example').dataTable( {
    paging: true,
    searching: true,
	"autoWidth": true,
} );
</script>