<?php
require_once("./connect.php");

$branch=escapeString($conn,$_POST['branch']);

if($branch=='')
{
	echo "<option value=''>-select-</option>
	<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

$qry=Qry($conn,"SELECT name,code FROM emp_attendance WHERE branch='$branch' AND status='3' ORDER BY name ASC");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)>0)
{
	echo "<option value=''>-select-</option>";
	
	while($row = fetchArray($qry))
	{
		echo "<option value='$row[code]'>$row[name]($row[code])</option>";
	}
}
else
{
	echo "<option value=''>-select-</option>";
}

echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
?>