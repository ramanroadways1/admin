<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$amount = sprintf("%.2f",round(escapeString($conn,$_POST['amount'])));
$timestamp = date("Y-m-d H:i:s");

if($id=='')
{
	echo "<script>
		alert('Lane not found !!');
		$('#edit_adv_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}


if($amount=='' || $amount<0)
{
	echo "<script>
		alert('Invalid Advance amount !!');
		$('#edit_adv_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT adv_amount,diesel_qty FROM dairy.fix_lane_adv WHERE lane_id='$id'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_record)==0)
{
	echo "<script>
		alert('No record found !!');
		$('#edit_adv_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_adv = fetchArray($chk_record);

if(sprintf("%.2f",$row_adv['adv_amount'])==$amount)
{
	echo "<script>
		alert('Nothing to update !!');
		$('#edit_adv_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_record = Qry($conn,"UPDATE dairy.fix_lane_adv SET adv_amount='$amount' WHERE lane_id='$id'");

if(!$update_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,edit_by,timestamp) VALUES 
('$id','By Lane Id','FIX_LANE','EDIT_ADVANCE_AMOUNT','Advance amount update $row_adv[adv_amount] to $amount.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
		alert('Updated Successfully !!');
		$('#edit_adv_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fix_lane.php");
	exit();
}
?>