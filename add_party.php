<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#FormParty").on('submit',(function(e) {
$("#loadicon").show();
$("#lr_sub").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./save_add_party.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#lr_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination2(event);" onkeydown = "return disableCtrlKeyCombination2(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add new Party</h3>
              </div>
              
<form role="form" id="FormParty" action="" method="POST" autocomplete="off">
			  
     <div class="card-body">

<div id="lr_result"></div>
		
	<div class="row">	
		
		<div class="col-md-2">	
               <div class="form-group">
                  <label>Party type <font color="red"><sup>*</sup></font></label>
                  <select style="font-size:13px" class="form-control" id="type" name="type" required>
					<option style="font-size:13px" value="">Select an option</option>
					<option style="font-size:13px" value="consignor">Consignor</option>
					<option style="font-size:13px" value="consignee">Consignee</option>
				  </select>
              </div>
		</div>
			
			<div class="col-md-5">	
               <div class="form-group">
                  <label>Trade name <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,()]/,'')" class="form-control" id="trade_name"
				  name="trade_name" required>
              </div>
			</div>
			
			<div class="col-md-5">	
               <div class="form-group">
                  <label>Legal name <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,()]/,'')" class="form-control" id="legal_name"
				  name="legal_name" required>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Display to branch <font color="red"><sup>*</sup></font></label>
                 <select style="font-size:13px" class="form-control" id="display_to_branch" name="display_to_branch" required>
					<option style="font-size:13px" value="">Select an option</option>
					<option style="font-size:13px" value="legal">Legal name</option>
					<option style="font-size:13px" value="trade">Trade name</option>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>State <font color="red"><sup>*</sup></font></label>
                  <select style="font-size:13px" class="form-control" onchange="StateSelect(this.value)" id="state_name" name="state_name" required>
					<option value="">Select an option</option>
					<option style="font-size:13px" value="URP">URP - Unregistered party</option>
					<?php
					$state = Qry($conn,"SELECT name,code FROM state_codes ORDER by name ASC");
					if(numRows($state)>0)
					{
						while($row_s = fetchArray($state))
						{
							echo "<option style='font-size:13px' value='$row_s[name]'>$row_s[name]</option>";
						}
					}
					?>
				  </select>
              </div>
			</div>
			
			<script>
			function StateSelect(elem)
			{
				$('#gst').val('');
				$('#pincode').val('');
				
				if(elem=='URP')
				{
					$('#wo_gst').prop('checked', true);
					$('#pincode').val('000000');
					$('#pincode').attr('readonly',true);
					$('#gst').attr('readonly',true);
					$('#gst').attr('onblur','');
				}
				else
				{
					$('#wo_gst').prop('checked', false);
					$('#pincode').attr('readonly',false);
					$('#gst').attr('readonly',false);
					$('#gst').attr('onblur','ValidateGst()');
				}
			}
			</script>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>GST number <font color="red"><sup>*</sup></font> &nbsp; <input style="width:13px" id="wo_gst" type="checkbox" /> <span style="color:blue;font-size:12px">W/o GST</span></label>
                  <input onblur="ValidateGst()" type="text" maxlength="15" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" id="gst"
				  name="gst" required>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Mobile number </label>
                  <input type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="10" class="form-control" id="mobile"
				  name="mobile">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Pincode <font color="red"><sup>*</sup></font></label>
                  <input type="text" maxlength="6" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" id="pincode"
				  name="pincode" required>
              </div>
			</div>
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>Address <font color="red"><sup>*</sup></font></label>
                  <textarea id="addr" required="required" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/-]/,'')" 
				  name="addr" class="form-control"></textarea>
              </div>
			</div>
		</div>
	</div>
                
       <div class="card-footer">
			<button id="lr_sub" type="submit" class="btn btn-primary">Add Party</button>
	   </div>
				
              </form>
            </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
  </div> 
  
<?php include ("./footer.php"); ?>

</body>
</html>
  
<script>
function ValidateGst()
{
	var gst = $('#gst').val();
	
	if(gst!='')
	{
	   var gstinformat = new RegExp('^([0-9]{2})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1})([1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})$');
	   
	   if(!gstinformat.test(gst))
	   {
			alert('Please Enter Valid GSTIN Number');
			$("#gst").val("");
		}
	}
}

$(document).on('click','#wo_gst',function(){
	
	if($('#state_name').val()=='URP')
	{
		$('#wo_gst').prop('checked', true);
	}
	else
	{
		if($(this).is(':checked'))
		{
			$('#gst').val('');
			$('#gst').attr('readonly',true);
			$('#gst').attr('onblur','');
		}
		else
		{
			$('#gst').val('');
			$('#gst').attr('readonly',false);
			$('#gst').attr('onblur','ValidateGst()');
		}
	}
});
</script>
