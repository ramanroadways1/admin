<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$from_loc = escapeString($conn,$_POST['from_loc']);
$to_loc = escapeString($conn,$_POST['to_loc']);

$get_adv_diesel = Qry($conn,"SELECT adv_amount,diesel_qty FROM dairy.fix_lane_adv WHERE lane_id='$id'");

if(!$get_adv_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_adv_diesel = fetchArray($get_adv_diesel);
?>
<div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-custom" role="document">
       <div class="modal-content">
	   
	   <div id="result_modal2"></div>
	   
	  <div class="bg-primary modal-header">
			<h5 style="font-size:15px" class="modal-title">Fixed Adv/Exp/Diesel : <?php echo $from_loc." to ".$to_loc; ?></h5>
      </div>
	  
		<div class="modal-body">
			
		<div class="row">
			
			<div class="col-md-7" style="max-height:400px;overflow:auto">
				<label>Expenses :</label>
<div class="row">
<div class="col-md-6">
<div class="form-group">				
	<select class="form-control" id="exp_head_name">
	<option value="">--select exp--</option>
	<?php
	$get_exp_list = Qry($conn,"SELECT id,exp_code,name FROM dairy.exp_head");

	if(!$get_exp_list){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($get_exp_list)>0)
	{
		while($row_exp_list = fetchArray($get_exp_list))
		{
			echo "<option value='$row_exp_list[exp_code]_$row_exp_list[name]'>$row_exp_list[name]</option>";
		}
	}
	?>
	</select>	
</div>
</div>

<div class="col-md-4">	
<div class="form-group">
	<input type="number" placeholder="Amount.." id="exp_amount_new" class="form-control" required>
</div>	
</div>	

<div class="col-md-2">	
<div class="form-group">
	<button type="button" id="add_exp_btn" onclick="AddnewExp('<?php echo $id; ?>')" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
</div>	
</div>	
</div>	

<?php
$get_exps = Qry($conn,"SELECT id,exp_name,exp_amount FROM dairy.fix_lane_exp WHERE lane_id='$id'");

if(!$get_exps){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_exps)==0)
{
	echo "<br><font color='red'>No record found..</font>";
}
else
{
	echo "<table class='table-bordered' style='width:100%;font-size:12px'>
		<tr>
			<th style='padding:5px;'>#</th>
			<th style='padding:5px;'>Exp.</th>
			<th style='padding:5px;'>Amount</th>
			<th style='padding:5px;'>#</th>
			<th style='padding:5px;'>#</th>
		</tr>";
	$sn=1;	
	while($row_exp = fetchArray($get_exps))
	{
		echo "<tr>
			<td style='padding:5px;'>$sn</td>
			<td style='padding:5px;'>$row_exp[exp_name]</td>
			<td style='padding:5px;'><input type='number' id='exp_amount_$row_exp[id]' value='$row_exp[exp_amount]'></td>
			<td style='padding:5px;'><button class='btn btn-sm btn-success' id='update_exp_btn_$row_exp[id]' onclick='UpdateExp($row_exp[id])'><i class='fa fa-check-square' aria-hidden='true'></i></button></td>
			<td style='padding:5px;'><button class='btn btn-sm btn-danger' id='dlt_exp_btn_$row_exp[id]' onclick='DltExp($row_exp[id])'><i class='fa fa-trash-o' aria-hidden='true'></i></button></td>
		</tr>";
	$sn++;	
	}		
	echo "</table>";
}
?>              
			</div>
			
			<div class="col-md-5">
				<br />
				<br />
				<div class="form-inline">	
					<div class="col-md-3">
						<label>Advance : </label>
					</div>	
					<div class="col-md-6">
						<input class="form-control" style="width:100%" type="number" value="<?php echo $row_adv_diesel['adv_amount']; ?>" id="adv_amount_1" />
					</div>	
					<div class="col-md-3">
						<button type="button" id="edit_adv_btn" onclick="EditAdvAmt('<?php echo $id; ?>')" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
					</div>	
				</div>
				<br />
				<div class="form-inline">	
					<div class="col-md-3">
						<label>Diesel QTY : </label>
					</div>	
					<div class="col-md-6">
						<input class="form-control" style="width:100%" type="number" value="<?php echo $row_adv_diesel['diesel_qty']; ?>" id="diesel_qty_1" />
					</div>	
					<div class="col-md-3">
						<button type="button" id="edit_diesel_btn" onclick="EditDieselQty('<?php echo $id; ?>')" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
					</div>	
				</div>
				
			</div>
			
		</div>
		</div>
	<div class="modal-footer">
		<button type="button" id="close_modal_btn2" onclick="closeModal('<?php echo $id; ?>');document.getElementById('menu_button2').click();" class="btn btn-danger" data-dismiss="modal">Close</button>
	</div>
		</div>	
     </div>
 </div>	

<?php
echo "<script>
	document.getElementById('menu_button2').click();
	$('#ViewModal').modal();
	$('#loadicon').hide();
</script>";
exit();
?>
