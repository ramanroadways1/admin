<script type="text/javascript">
$(function() {
	$("#to_loc_add_more").autocomplete({
	source: 'autofill/get_location.php',
	// appendTo: '#appenddiv',
	select: function (event, ui) { 
		$('#to_loc_add_more').val(ui.item.value);   
		$('#add_more_to_id').val(ui.item.id);      
		return false;
	},
	change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_loc_add_more').val("");   
			$('#add_more_to_id').val("");   
			alert('Location does not exists !');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(document).ready(function (e) {
$("#MoreLRAddForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#add_btn_add_more_lr").attr("disabled", true);
	$.ajax({
	url: "./add_more_lr_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_AddmoreLRForm").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<button type="button" style="display:none" data-toggle="modal" id="btn_modal_add_more_LR" data-target="#modal_More_LRs"></button>

<form id="MoreLRAddForm" autocomplete="off"> 
<div class="modal fade" id="modal_More_LRs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
	  
	  <div class="modal-header bg-primary">
		ADD LR :
      </div>
	  
	<div class="modal-body">
	
<div class="row">

	<div class="col-md-7">
       <div class="form-group">
           <label class="control-label mb-1">LR No <font color="red"><sup>*</sup></font></label>
		   <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="add_lr_modal_lrno" name="lrno" class="form-control" required>
      </div>
	</div>
	
	<div class="col-md-5" id="lr_add_fetch_btn">
       <div class="form-group">
           <br>
		   <button style="margin-top:3px" type="button" onclick="GetLRinfoToAdd()" class="btn btn-danger">Check !</button>
      </div>
	</div>

	<div class="col-md-6">
       <div class="form-group">
          <label class="control-label mb-1">From Station <font color="red"><sup>*</sup></font></label>
		  <input type="text" readonly id="from_loc_add_more" name="from_loc" class="form-control" required />
      </div>
   </div>
	 
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">To Station <font color="red"><sup>*</sup></font></label>
		   <input type="text" readonly id="to_loc_add_more" name="to_loc" class="form-control" required />
      </div>
   </div>
   
	<div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Act. Weight <font color="red"><sup>*</sup></font></label>
			 <input type="number" readonly min="0.001" step="any" id="act_wt_add_more" name="act_weight" class="form-control" required />
		</div>
   </div>
   
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Chrg. Weight <font color="red"><sup>*</sup></font></label>
			<input type="number" readonly min="0.001" step="any" id="chrg_wt_add_more" name="chrg_weight" class="form-control" required />
		</div>
   </div>
   
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Rate/Fix <font color="red"><sup>*</sup></font></label>
			<input type="text" readonly id="rate_fix_add_more" name="rate_fix" class="form-control" required />
		</div>
   </div>
   
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Freight Amount <font color="red"><sup>*</sup></font></label>
			<input type="number" min="0.001" step="any" id="freight_add_more" name="freight_amount" class="form-control" required />
		</div>
   </div>
   
    <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Eway bill no. <font color="red"><sup>*</sup></font></label>
		   <input type="text" maxlength="12" id="ewb_no_ip_add_more" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="ewb_no" class="form-control" required="required">
		</div>
   </div>
   
	 <div class="col-md-6">
		   <div class="form-group">
			   <label class="control-label mb-1">Further crossing <font color="red"><sup>*</sup></font></label>
			   <select onchange="CrossingSelection2(this.value)" id="further_crossing_add_more" name="crossing" class="form-control" required>
					<option value="">--select--</option>
					<option value="YES">Yes</option>
					<option value="NO">No</option>
			   </select>
			</div>
	  </div>
   
  <script>
   function CrossingSelection2(elem)
   {
	   var to_loc_db = $('#to_loc_add_more_db').val();
	   var to_id_db = $('#to_loc_id_add_more_db').val();
	   $('#add_more_to_id').val('');
	   
	   if(to_loc_db=='')
	   {
		   $('#further_crossing_add_more').val('');
		   $('#add_lr_modal_lrno').focus();
	   }
	   else
	   {
		   if(elem=='YES')
		   {
			   $('#to_loc_add_more').val('');
			   $('#add_more_to_id').val('');
			   $('#to_loc_add_more').attr('readonly',false);
		   }
		   else
		   {
			   $('#to_loc_add_more').val(to_loc_db);
			   $('#add_more_to_id').val(to_id_db);
			   $('#to_loc_add_more').attr('readonly',true);
		   }
	   }
   }
   </script>
   
  </div>
  
</div>
		
	<input type="hidden" name="id" id="add_more_lr_fm_id">
	<input type="hidden" name="frno" id="add_more_lr_vou_no">
	<input type="hidden" name="to_loc_db" id="to_loc_add_more_db">
	<input type="hidden" name="to_loc_id_db" id="to_loc_id_add_more_db">
	<input type="hidden" name="from_id" id="add_more_from_id">
	<input type="hidden" name="to_id" id="add_more_to_id">
	<input type="hidden" name="con1_id" id="add_more_con1_id">
	<input type="hidden" name="item_id" id="add_more_item_id">
	<input type="hidden" name="lr_id" id="add_more_lr_lr_id">
			
 <div id="result_AddmoreLRForm"></div>
 
	<div class="modal-footer">
		<button type="button" id="close_btn_more_lr_add" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="add_btn_add_more_lr" disabled class="btn btn-primary">Add LR</button>
	</div>
		</div>	
     </div>
 </div>
</form>