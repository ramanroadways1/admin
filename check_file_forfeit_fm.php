<?php
require_once("./connect.php");

$record_type = escapeString($conn,($_POST['record_type']));
$timestamp = date("Y-m-d H:i:s");

if($record_type=='FM')
{
	$fm_no = trim(escapeString($conn,strtoupper($_POST['fm_no'])));

	if(!preg_match('/^[a-zA-Z0-9\.]*$/', $fm_no))
	{
		echo "<script>
			alert('Error : Check Freight memo number !');
			$('#loadicon').fadeOut('slow');
			$('#button_chk').attr('disabled', false);
		</script>";
		exit();
	}
	
	$qry = Qry($conn,"SELECT f.id,f.frno,f.date,f.truck_no,f.branch,f.branch_user,f.from1,f.to1,f.totalf,f.totaladv,f.baladv,f.paidto,
	b.name as broker,o.name as owner FROM freight_form as f 
	LEFT OUTER JOIN mk_broker as b ON b.id = f.bid 
	LEFT OUTER JOIN mk_truck as o ON o.id = f.oid 
	WHERE f.frno = '$fm_no'");

	if(!$qry){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$numrows = numRows($qry);

	if($numrows==0)
	{
		echo "<center><font color='red'>Freight memo not found..</font></center>
		<script>
			$('#loadicon').fadeOut('slow');
			$('#button_chk').attr('disabled', false);
		</script>
		";
		exit();
	}

	$row = fetchArray($qry);

	$check_total_lrs = Qry($conn,"SELECT id FROM freight_form_lr WHERE frno='$fm_no'");

	if(!$check_total_lrs){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$total_lrs = numRows($check_total_lrs);

	$check_pod = Qry($conn,"SELECT id FROM rcv_pod WHERE frno='$fm_no'");

	if(!$check_pod){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$numrows_pod = numRows($check_pod);

	if($numrows_pod==0)
	{
		$pod_status = "<font color='red'></font> ALL pending";
	}
	else if($numrows_pod!=$total_lrs)
	{
		$pod_status = "<font color='red'>".($total_lrs - $numrows_pod)."</font> pending";
	}
	else
	{
		$pod_status = "<font color='green'>ALL received</font>";
	}

	echo "<table class='table table-bordered' style='font-size:12px'>
	<tr>
		<th>FM_date</th>
		<th>Branch</th>
		<th>Vehicle_no</th>
		<th>From</th>
		<th>To</th>
		<th>Total_Frt</th>
		<th>Total_Adv</th>
		<th>Balance</th>
		<th>Broker</th>
		<th>Owner</th>
		<th>Pod_status</th>
		<th>#</th>
	</tr>";
	
	echo "<tr>
		<td>$row[date]</td>
		<td>$row[branch]</td>
		<td>$row[truck_no]</td>
		<td>$row[from1]</td>
		<td>$row[to1]</td>
		<td>$row[totalf]</td>
		<td>$row[totaladv]</td>
		<td>$row[baladv]</td>
		<td>$row[broker]</td>
		<td>$row[owner]</td>
		<td>$pod_status</td>";
		if($row['paidto']=='')
		{
		echo "	
		<td><button id='Btn1$row[id]' onclick=Forfeit('$row[id]','$row[frno]') style='font-size:12px;' type='button' 
		class='btn btn-sm btn-danger'>Forfeit Bal</button></td>";
		}
		else
		{
			echo "<td><font color='red'>Balance Paid</font></td>";
		}
		
	echo "</tr>
	</table>";
}
else if($record_type=='Excel')
{
	echo "<script>$('#row_id_form').hide();</script>";
	
	$dlt_cache = Qry($conn,"DELETE FROM cache_forfeit_fm");
	
	if(!$dlt_cache){
		echo "<center><font color='red'>Error while uploading file !</font></center>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>
			$('#loadicon').fadeOut('slow');
			$('#button_chk').attr('disabled', false);
		</script>";
		exit();
	} 
	
	$allowedExtensions = array("csv");
	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
					
	if(!in_array($ext, $allowedExtensions))
	{
		echo "<center><font color='red'>Please upload CSV file !</font></center>
		<script>
			$('#loadicon').fadeOut('slow');
			$('#row_id_form').show();
			$('#button_chk').attr('disabled', false);
		</script>";
		exit();
	}
				
	$fileName = $_FILES["file"]["tmp_name"];

	$vou_nos = array();
	
	if($_FILES["file"]["size"] > 0)
	{
		$file = fopen($fileName, "r");
				
		while(($column = fgetcsv($file, 10000, ",")) !== FALSE)
		{
			if($column[0]!='')
			{
				$vou_nos[] = "'".$column[0]."'";
			}
			
			// if($column[0]!='' AND $column[0]!='Ref No')
			// {
				// $sqlInsert = Qry($conn,"INSERT into cache_forfeit_fm (frno,timestamp) values ('$column[0]','$timestamp')";
				
				// if (!$sqlInsert){
					// echo "<center><font color='red'>Error while uploading file !</font></center>";
					// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					// exit();
				// } 
			// }
		}
	}
	else
	{
		echo "<center><font color='red'>Empty File uploaded !</font></center>
		<script>
			$('#loadicon').fadeOut('slow');
			$('#row_id_form').show();
			$('#button_chk').attr('disabled', false);
		</script>";
		exit();
	}
	
	$vou_nos = implode(",",$vou_nos);
	
	$get_fm_record = Qry($conn,"INSERT INTO cache_forfeit_fm(frno,fm_date,branch,tno,from_loc,to_loc,freight,adv_amount,
	bal_to,lr_count,pod_count,timestamp) 
	SELECT TRIM(f.frno),f.date,f.branch,f.truck_no,f2.fstation,f2.tstation,f.actualf,f.totaladv,f.paidto,count(f2.id),
	(SELECT count(id) FROM rcv_pod where frno=f2.frno GROUP BY f2.frno),'$timestamp' 
	FROM freight_form AS f 
	LEFT JOIN freight_form_lr AS f2 ON f2.frno = f.frno 
	WHERE f.frno in($vou_nos) GROUP BY f.frno");
	
	if(!$get_fm_record){
		echo "<center><font color='red'>Error while uploading file !</font></center>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>
			$('#loadicon').fadeOut('slow');
			$('#button_chk').attr('disabled', false);
		</script>";
		exit();
	} 
	
	$get_records = Qry($conn,"SELECT * FROM cache_forfeit_fm");
	
	if(!$get_records){
		echo "<center><font color='red'>Error while uploading file !</font></center>";
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>
			$('#loadicon').fadeOut('slow');
			$('#button_chk').attr('disabled', false);
		</script>";
		exit();
	} 
	
	// if($numrows_pod==0)
	// {
		// $pod_status = "<font color='red'></font> ALL pending";
	// }
	// else if($numrows_pod!=$total_lrs)
	// {
		// $pod_status = "<font color='red'>".($total_lrs - $numrows_pod)."</font> pending";
	// }
	// else
	// {
		// $pod_status = "<font color='green'>ALL received</font>";
	// }

echo "<button type='button' id='btn_clear_pod' onclick='ClearPOD()' class='btn btn-sm btn-primary'>Forfeit Freight Memos</button>
<table class='table table-bordered' style='font-size:12px'>
	<tr>
		<th>FM_no</th>
		<th>FM_date</th>
		<th>Branch</th>
		<th>Vehicle_no</th>
		<th>Route</th>
		<th>Total_Frt</th>
		<th>Total_Adv</th>
		<th>Balance</th>
		<th>LR Count</th>
		<th>POD Count</th>
		<th>#</th>
	</tr>";
	
while($row = fetchArray($get_records))
{
	echo "<tr id='tr_id_$row[id]'>
		<td>$row[frno]</td>
		<td>$row[fm_date]</td>
		<td>$row[branch]</td>
		<td>$row[tno]</td>
		<td>$row[from_loc] - $row[to_loc]</td>
		<td>$row[freight]</td>
		<td>$row[adv_amount]</td>
		<td>$row[bal_to]</td>
		<td>$row[lr_count]</td>
		<td>$row[pod_count]</td>
		<td><button id='RemoveBtn$row[id]' onclick='RemoveRecord($row[id])' type='button' 
		// class='btn btn-sm btn-danger'>Remove</button></td>";
		// if($row['paidto']=='')
		// {
		// echo "	
		// <td><button id='Btn1$row[id]' onclick=Forfeit('$row[id]','$row[frno]') style='font-size:12px;' type='button' 
		// class='btn btn-sm btn-danger'>Forfeit Bal</button></td>";
		// }
		// else
		// {
			// echo "<td><font color='red'>Balance Paid</font></td>";
		// }
		
	echo "</tr>";
}
	echo "</table>";
	
	echo "<script>
		$('#loadicon').fadeOut('slow');
		$('#button_chk').attr('disabled', true);
	</script>";
	exit();
}
else
{
	echo "<script>
		alert('Invalid option selected !');
		window.location.href='./mark_pod_rcvd.php';
	</script>";
	exit();
}


echo "<script>$('#loadicon').fadeOut('slow');</script>";
?>