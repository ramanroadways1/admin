<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">LR - Breaking</h3>
              </div>
<div class="card-body">
	<div class="row">	
		<div class="col-md-3">	
			<div class="form-group">
               <label>LR Number <font color="red"><sup>*</sup></font></label>
               <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" id="lrno" name="lrno" required>
            </div>
		</div>
		
		<div class="col-md-3">	
			<div class="form-group">
               <label>&nbsp;</label>
			   <br />
               <button onclick="GetBreaking()" id="get_recordBtn" type="button" class="btn btn-md btn-success">Get Records !</button>
            </div>
		</div>
		
	</div>
	
<div class="row">
	<div class="col-md-12">
	<table style="font-size:12.5px;" class="table table-bordered">
		<thead>
            <tr>
                <th>#</th>
                <th>Branch</th>
                <th>Branch_User</th>
                <th>Act_Wt</th>
                <th>Charge_Wt</th>
                <th>Timestamp</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        
		<tbody id="tab_result">
           <tr>
				<td colspan="8">No result found !!</td>
           </tr>
        </tbody>
     </table>
	</div>
</div>
</div>
        </div>
	</div>
  </div>
  </div>
 </section>
</div>
  
<script> 
function GetBreaking()
{
	var lrno = $('#lrno').val();
	
	if(lrno=='')
	{
		alert('Enter LR Number First !');
	}
	else
	{
		$("#lrno").attr('readonly',true);
		$("#loadicon").show();
			jQuery.ajax({
			url: "get_breaking_lr.php",
			data: 'lrno=' + lrno,
			type: "POST",
			success: function(data) {
				$("#tab_result").html(data);
			},
			error: function() {}
		});
	}
}

function Edit(id)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "lr_breaking_get.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data) {
		$("#result_func").html(data);
	},
	error: function() {}
	});
}

function Delete(id)
{
	$("#loadicon").show();
	$('#deleteButton'+id).attr('disabled',true);
	jQuery.ajax({
	url: "lr_breaking_delete.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data) {
		$("#result_func").html(data);
	},
	error: function() {}
	});
}
</script> 
</div> 

<?php include ("./footer.php"); ?>

</body>
</html>

<div id="result_func"></div>

<form id="EditForm" action="#" method="POST">
<div id="EditModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Edit Breaking LR :
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-4">
				<label>LR Number <font color="red"><sup>*</sup></font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="edit_lrno" class="form-control" readonly required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Branch <font color="red"><sup>*</sup></font></label>
				<input id="edit_branch" readonly type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Branch User <font color="red"><sup>*</sup></font></label>
				<input id="edit_branch_user" readonly type="text" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Actual Weight <font color="red"><sup>*</sup></font></label>
				<input id="edit_actual" type="number" step="any" min="0.01" name="actual_weight" class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-4">
				<label>Charge Weight <font color="red"><sup>*</sup></font></label>
				<input id="edit_charge" type="number" step="any" min="0.01" name="charge_weight" class="form-control" required="required">
			</div>
			<input type="hidden" id="edit_id" name="id">
		</div>
      </div>
	  <div class="modal-footer">
        <button type="submit" disabled id="edit_save" class="btn btn-primary">Update</button>
        <button type="button" id="closeBtn" onclick="$('#EditForm')[0].reset();" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</form>	
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#EditForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#edit_save").attr("disabled", true);
	$.ajax({
        	url: "./lr_breaking_save.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_func").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>