<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$diesel_id = escapeString($conn,strtoupper($_POST['id']));

$dlt_cache = Qry($conn,"DELETE FROM diesel_cache WHERE frno='$frno'");
if(!$dlt_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	$insert_cache = Qry($conn,"INSERT INTO diesel_cache(frno,diesel_id,branch,qty,rate,diesel,cash,tno,lrno,dsl_by,card,veh_no,
	mobile_no,dsl_comp,dsl_nrr,no_lr,done,pump_download,pre_req,pre_and_fm,timestamp) SELECT fno,id,branch,qty,rate,disamt,cash,
	tno,lrno,dsl_by,dcard,veh_no,dsl_mobileno,dcom,dsl_nrr,no_lr,done,colset_d,pre_req,pre_and_fm,timestamp 
	FROM diesel_fm WHERE fno='$frno'");

	if(!$insert_cache){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(AffectedRows($conn)==0)
	{
		echo "<script>
			alert('No result found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}


$qry_get_diesel=Qry($conn,"SELECT id,frno,diesel_id,branch,qty,rate,diesel,cash,tno,lrno,dsl_by,card,veh_no,mobile_no,dsl_comp,
	dsl_nrr,no_lr,done,pump_download,pre_req,pre_and_fm,timestamp FROM diesel_cache WHERE frno='$frno'");

if(!$qry_get_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
?>
<script>
function MyFunc1()
{
	$("#req_update_button").attr("disabled", true);
	$('#main_table1').hide();
	$('#change_lrno_div').hide();
	$('#change_tno_div').show();
}

function MyFunc2()
{
	$("#req_update_button").attr("disabled", true);
	
	$('#main_table1').hide();
	$('#change_lrno_div').show();
	$('#change_tno_div').hide();
	
	// jQuery.ajax({
	// url: "fetch_lrno_for_req_update.php",
	// data: 'branch=' + '' + '&tno=' + '' + '&frno=' + '<?php echo $frno; ?>',
	// type: "POST",
	// success: function(data) {
	// $("#lrno_fetch_div").html(data);
	// },
	// error: function() {}
	// });
}

$(function() {
    $("#tno_new_update").autocomplete({
      source: 'autofill/get_market_vehicle.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			 $(event.target).focus();
			 $("#button_change_tno").attr("disabled",true);
			alert('Truck Number doest not exists.');
        }
    }, 
    focus: function (event, ui) {
        $("#button_change_tno").attr("disabled",false);
		return false;
    }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#TnoForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_change_tno").attr("disabled", true);
	$.ajax({
	url: "./update_truck_no_market_request.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_two").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<script type="text/javascript">
$(document).ready(function (e) {
$("#LRNOForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_change_lrno").attr("disabled", true);
	$.ajax({
	url: "./update_lrno_market_request.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_two").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<div id="result_two"></div>

<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th style="font-size:13px;" colspan="16">
	   <?php echo "Vou No : <span id='vou_no_text'></span>, Truck No : <span id='truck_no_text'></span> 
	   <a href='#' onclick='MyFunc1();'><font color='blue'>Change</font></a> 
	   &nbsp; LR No : <span id='lrno_text'></span> <a href='#' onclick='MyFunc2();'><font color='blue'>Change</font></a>"; ?>
	   </th>
    </tr>
	
</table>	

<div class="container-fluid" id="change_tno_div" style="display:none">
   <div class="row">
       <div class="col-md-12">
	
	<form id="TnoForm">
	
		<div class="form-group col-md-4"> 
			<label>Truck No <font color="red"><sup>*</sup></font></label>
			<input type="text" name="tno" id="tno_new_update" class="form-control" required />
		</div>
		
		<input type="hidden" value="<?php echo $frno; ?>" name="frno">
		
		<div class="form-group col-md-4"> 
			<button type="submit" id="button_change_tno" class="btn btn-success">Change Truck Number</button>
		</div>
		</form>	
		</div>
	</div>
</div>
		
<div class="container-fluid" id="change_lrno_div" style="display:none">
        <div class="row">
          <div class="col-md-12">
	 
	<form id="LRNOForm">
	
		<div class="form-group col-md-4"> 
			<label>LR No <font color="red"><sup>*</sup></font></label>
			<input type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" name="lrno" class="form-control" required>
		</div>
		
		<input type="hidden" value="<?php echo $frno; ?>" name="frno">
		
		<div class="form-group col-md-4"> 
			<button type="submit" id="button_change_lrno" class="btn btn-success">Change LR Number</button>
		</div>
		
	</form>	
	
</div>
	</div>
		</div>		
	
<table class="table table-bordered" id="main_table1" style="font-size:12.5px;">
		<tr>    
			<th>Qty</th>
			<th>Rate</th>
			<th>Amount</th>
			<th>Type</th>
			<th>Fuel Comp.</th>
			<th>Card/Pump/Mobile</th>
			<th></th>
			<th></th>
		</tr>	
	
	<?php
	while($row=fetchArray($qry_get_diesel))
	{
		$truck_no_text = $row['tno'];
		$lrno_text = $row['lrno'];
		$vou_no_text = $row['frno'];
		
		if($row['dsl_by']=='PUMP'){
			
		$check_consumer_pump = Qry($conn,"SELECT id,name,consumer_pump FROM diesel_pump WHERE code='$row[card]'");
		if(!$check_consumer_pump){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_check_consumer = fetchArray($check_consumer_pump);
		
		if($row_check_consumer['consumer_pump']=="1"){
			$is_consumer_pump="1";
		}
		else{
			$is_consumer_pump="0";
		}
		
			if($row['pump_download']=="1"){
				$edit_disable="disabled";
				$done_text="Recharge Done";
			}
			else{
				$edit_disable="";
				$done_text="";
			}
		}
		else
		{
			if($row['done']=="1"){
				$edit_disable="disabled";
				$done_text="Recharge Done";
			}
			else{
				$edit_disable="";
				$done_text="";
			}
		}
		
	echo "<tr>
		<td><input type='number' required step='any' max='600' style='width:80%' class='form-control qtyclass' value='$row[qty]' id='qty$row[id]' readonly></td>
		<td><input type='number' required step='any' max='120' style='width:80%' class='form-control rateclass' value='$row[rate]' id='rate$row[id]' readonly></td>
		<td><input type='number' required style='width:80%' class='form-control amtclass' value='$row[diesel]' id='amt$row[id]' readonly></td>
		<td>$row[dsl_by]
		<input type='hidden' value='$row[dsl_by]' id='dsl_by$row[id]'>
	</td>";
	
	if($row['dsl_by']=='PUMP')
	{
	?>
<script type="text/javascript">
$(function() {
$("#qty<?php echo $row['id']; ?>,#rate<?php echo $row['id']; ?>").on("keydown keyup blur change input", CalcDiesel);
function CalcDiesel() {
	if($("#qty<?php echo $row['id']; ?>").val()==''){
		var qty = 0;
	}else{
		var qty = Number($("#qty<?php echo $row['id']; ?>").val());
	}

	if($("#rate<?php echo $row['id']; ?>").val()==''){
		var rate = 0;
	}else{
		var rate = Number($("#rate<?php echo $row['id']; ?>").val());
	}
	
	$("#amt<?php echo $row['id']; ?>").val(Math.round(qty * rate).toFixed(2));
}});
				 
function AmountCall() 
{
	if($("#qty<?php echo $row['id']; ?>").val()=='' && $("#rate<?php echo $row['id']; ?>").val()==''){
		$("#qty<?php echo $row['id']; ?>").focus()
		$("#amt<?php echo $row['id']; ?>").val('');
	}
	else
	{
		if($("#rate<?php echo $row['id']; ?>").val()=='' || $("#rate<?php echo $row['id']; ?>").val()<60)
		{
			$("#rate<?php echo $row['id']; ?>").val((Number($("#amt<?php echo $row['id']; ?>").val()) / Number($("#qty<?php echo $row['id']; ?>").val())).toFixed(2));
		}
		else
		{
			$("#qty<?php echo $row['id']; ?>").val((Number($("#amt<?php echo $row['id']; ?>").val()) / Number($("#rate<?php echo $row['id']; ?>").val())).toFixed(2));
		}
	}	
}

function CheckConsumerPump(id)
{
	
}

// $(document).ready(function(){
    // $("select.pumpselectvalue_<?php echo $row['id']; ?>").change(function(){
        // var selectedCountry = $(this).children("option:selected").val();
        // alert("You have selected the country - " + selectedCountry);
    // });
// });
</script>
	
	<td>
		<select onchange="CheckConsumerPump('<?php echo $row["id"]; ?>')" class="form-control pumpvalueclass pumpselectvalue_<?php echo $row['id']; ?>" id="pumpvalue<?php echo $row['id']; ?>" disabled value="<?php echo $row['card']; ?>" required="required">
		<?php
		$pump_fetch = Qry($conn,"SELECT branch,name,code FROM diesel_pump WHERE code!='' AND branch='$row[branch]' AND active='1' AND consumer_pump!='1'");
		if(!$pump_fetch){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		echo "<option value=''>select pump</option>";
		
		if($is_consumer_pump=="1")
		{
		?>
			<option title="" selected="selected" value="<?php echo $row['card']; ?>"><?php echo $row_check_consumer["name"];?></option>
		<?php
		}
		else
		{
			while($row_pump = fetchArray($pump_fetch))
			{
		?>
			<option title="" <?php if ($row['card'] == $row_pump['code']){ echo ' selected="selected"'; }?> value="<?php echo $row_pump['code']; ?>"><?php echo $row_pump["name"]." - ".$row_pump["branch"];?></option>
		<?php
			}
		}
		?>
		</select>
	</td>
	<td></td>
	<?php
	}
	else if($row['dsl_by']=='CARD')
	{
	?>
<script>
  $(function() {  
      $("#card<?php echo $row['id']; ?>").autocomplete({
			// appendTo: $("#card_no").next()
			source: function(request, response) { 
			if(document.getElementById("fuelcomp<?php echo $row['id']; ?>").value!=''){
                 $.ajax({
                  url: './autofill/get_diesel_cards.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   company: document.getElementById("fuelcomp<?php echo $row['id']; ?>").value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select company first !');
				$("#card<?php echo $row['id']; ?>").val('');
				$("#cardno2<?php echo $row['id']; ?>").val('');
				$("#phy_card_no<?php echo $row['id']; ?>").val('');
              }

              },
              select: function (event, ui) { 
               $("#card<?php echo $row['id']; ?>").val(ui.item.value);   
               $("#cardno2<?php echo $row['id']; ?>").val(ui.item.dbid);     
			   $("#phy_card_no<?php echo $row['id']; ?>").val(ui.item.phy_card_no);
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#cardno2<?php echo $row['id']; ?>").val('');
				$("#phy_card_no<?php echo $row['id']; ?>").val('');
                alert('Card No. does not exist !'); 
              } 
              },
			}); 
      }); 
	  
function ResetCardValue(id)
{
	$('#card'+id).val('');
	$('#cardno2'+id).val('');
	$('#phy_card_no'+id).val('');
}	  
</script> 
	
	<td>
		<select onchange="ResetCardValue('<?php echo $row["id"]; ?>')" class="form-control fuelcompclass" id="fuelcomp<?php echo $row['id']; ?>" required="required" disabled>
			<option value="">Select Company</option>
			<option value="IOCL" <?php if ($row['dsl_comp'] == 'IOCL') echo ' selected="selected"'; ?>>IOCL</option>
			<option value="BPCL" <?php if ($row['dsl_comp'] == 'BPCL') echo ' selected="selected"'; ?>>BPCL</option>
			<!--<option value="BPCL_PVTL" <?php if (strpos($row['card'], 'PVTL') !== false) echo ' selected="selected"'; ?>>BPCL_PVTL</option>
			<option value="HPCL" <?php if ($row['dsl_comp'] == 'HPCL') echo ' selected="selected"'; ?>>HPCL</option>-->
			<option value="RELIANCE" <?php if ($row['dsl_comp'] == 'RELIANCE') echo ' selected="selected"'; ?>>RELIANCE</option>
			<option value="RIL" <?php if ($row['dsl_comp'] == 'RIL') echo ' selected="selected"'; ?>>RIL</option>
		</select>
	</td>
	
	<td>
		<input type="text" style="width:100%" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckCardCompany('<?php echo $row['id']; ?>')" class="form-control cardclass" 
		value="<?php echo $row['card']; ?>" id="card<?php echo $row['id']; ?>" required readonly>
		<input type="hidden" id="cardno2<?php echo $row['id']; ?>" name="cardno2">
		<input type="hidden" value="<?php echo $row['veh_no']; ?>" id="phy_card_no<?php echo $row['id']; ?>" name="phy_card_no">
	</td>
	
<script>
function CheckCardCompany(id)
{
	if($('#fuelcomp'+id).val()=='')
	{
		$('#fuelcomp'+id).focus();
		$('#card'+id).val('');
	}
}
</script>
	
	<?php
	}
	else if($row['dsl_by']=='OTP')
	{
	?>
	<td>
		<select class="form-control fuelcompclass" id="fuelcomp<?php echo $row['id']; ?>" required="required" disabled>
			<option value="">Select Company</option>
			<?php
			$get_dsl_comp = Qry($conn,"SELECT name FROM diesel_api.dsl_company WHERE status='1'");
			
			if(numRows($get_dsl_comp) > 0)
			{
				while($row_d_com = fetchArray($get_dsl_comp))
				{
			?>
				<option value="<?php echo $row_d_com['name']; ?>" <?php if ($row['dsl_comp'] == $row_d_com['name']) echo ' selected="selected"'; ?>><?php echo $row_d_com['name']; ?></option>
			<?php
				}	
			}	
			/*
			?>
			
			<option value="BPCL" <?php if ($row['dsl_comp'] == 'BPCL') echo ' selected="selected"'; ?>>BPCL</option>
			<option value="RELIANCE" <?php if ($row['dsl_comp'] == 'RELIANCE') echo ' selected="selected"'; ?>>RELIANCE</option>
			<option value="RIL" <?php if ($row['dsl_comp'] == 'RIL') echo ' selected="selected"'; ?>>RIL</option>
			<?php
			*/
			?>
		</select>
	</td>
	
	<td>
		<input type="text" style="width:100%" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control cardclass" 
		value="<?php echo $row['mobile_no']; ?>" id="mobile_no<?php echo $row['id']; ?>" required readonly>
	</td>
	<?php	
	}
	else
	{
		echo "<td></td><td></td>";
	}
	
	if($done_text!="")
	{
		echo "<td colspan='2'>$done_text</td>";
	}	
	else
	{
	echo "
		<td>
			<button type='button' $edit_disable id='editbutton1$row[id]' onclick=Edit('$row[id]','$row[dsl_by]','$row[diesel_id]') class='btn btn-sm btn-success'><i class='fa fa-edit'></i></button>
		</td>
		<td id='save_result$row[id]'>	
			<button type='button' $edit_disable id='savebtn$row[id]' disabled onclick=SaveSingle('$row[id]','$row[dsl_by]','$row[diesel_id]') class='btn btn-sm btn-danger savebtnclass'><i class='fa fa-save'></i></button>
		</td>";
	}

echo "</tr>";
			/*<select id='type$row[id]' value='$row[dsl_by]'>
						<option value='CARD'>CARD</option>
						<option value='PUMP'>PUMP</option>
					</select>*/
	}
	echo "
	</table>";
	
	echo "<script>
		$('#truck_no_text').html('$truck_no_text');
		$('#lrno_text').html('$lrno_text');
		$('#vou_no_text').html('$vou_no_text');
		document.getElementById('menu_button2').click();
		$('#loadicon').hide();
	</script>";
?>