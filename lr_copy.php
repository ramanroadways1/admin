<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <span style="font-size:15px" class="card-title">Change LR number (Dummy LR) : ex. 12345 to 12345A</span>
              </div>
              
	<div class="card-body">
				
	
<script>		
function GetLRDetails(lrno)
{
	if(lrno!='')
	{
		$("#loadicon").show();
		$("#get_button").attr('disabled',true);
		$("#create_button").attr('disabled',true);
		$("#lrno").attr('readonly',true);
		jQuery.ajax({
		url: "lr_copy_fetch_lr_by_lrno.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#new_lrno").html(data);
		},
			error: function() {}
		});
	}
}	

function CreateLR()
{
	var lrno_old = $('#lrno').val();
	var new_lrno = $('#new_lrno').val();
	
	$("#loadicon").show();
	$("#create_button").attr('disabled',true);
	jQuery.ajax({
		url: "lr_copy_create_lr.php",
		data: 'lrno_old=' + lrno_old + '&new_lrno=' + new_lrno,
		type: "POST",
		success: function(data) {
			$("#result_div").html(data);
		},
			error: function() {}
		});
}
</script>

<div class="row">	
			
	<div class="col-md-3">	
        <div class="form-group">
           <label>LR Number <font color="red"><sup>*</sup></font></label>
           <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" id="lrno">
        </div>
	</div>
			
	<div class="col-md-2">
		<label>&nbsp;</label>
		<br />
		<button type="button" id="get_button" onclick="GetLRDetails($('#lrno').val())" class="btn btn-danger">Check LR !</button>
	</div>
	
	<div class="col-md-3 lrno_copy_div" style="display:none">	
        <div class="form-group">
           <label>NEW LR Number <font color="red"><sup>*</sup></font></label>
           <select name="new_lrno" id="new_lrno" class="form-control" required="required">
				<option value="">--select postfix--</option>
		   </select>
        </div>
	</div>
	
	<div class="col-md-2 lrno_copy_div" style="display:none">
		<label>&nbsp;</label>
		<br />
		<button type="button" id="create_button" disabled onclick="CreateLR()" class="btn btn-danger">Change LR number !</button>
	</div>
		
</div>

<div id="result_div"></div>


</div>
	
	<div class="card-footer">
	</div>
				
           </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
</div>
 
<?php include ("./footer.php"); ?>
</body>
</html>