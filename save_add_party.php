<?php 
require_once './connect.php';

$type=escapeString($conn,($_POST['type']));
$trade_name=trim(escapeString($conn,strtoupper($_POST['trade_name'])));
$legal_name=trim(escapeString($conn,strtoupper($_POST['legal_name'])));

if($_POST['display_to_branch']=='trade'){
	$name = $trade_name;
}
else{
	$name = $legal_name;
}

$gst=escapeString($conn,strtoupper($_POST['gst']));
$mobile=escapeString($conn,strtoupper($_POST['mobile']));
$addr=trim(escapeString($conn,strtoupper($_POST['addr'])));
$state_name=escapeString($conn,strtoupper($_POST['state_name']));
$pincode=escapeString($conn,strtoupper($_POST['pincode']));
		
$chkGST = Qry($conn,"SELECT name FROM `$type` WHERE gst='$gst' AND gst!=''");
if(!$chkGST){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chkGST)>0)
{
	$rowGST = fetchArray($chkGST);
	echo "<script>
		alert('GST Number: $gst already registered with Party $rowGST[name].');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$insertParty = Qry($conn,"INSERT INTO `$type`(name,legal_name,trade_name,state,gst,mobile,pincode,addr,branch,timestamp) VALUES 
('$name','$legal_name','$trade_name','$state_name','$gst','$mobile','$pincode','$addr','ADMIN','".date("Y-m-d H:i:s")."')");

if(!$insertParty){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
$insert_id = getInsertID($conn);
	
$updateCode = Qry($conn,"UPDATE `$type` SET code='$insert_id' WHERE id='$insert_id'");
if(!$updateCode){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
	
	echo "<script>
		alert('Party : $name registered successfully.');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled',false);
		$('#FormParty')[0].reset();
	</script>";
	
?>