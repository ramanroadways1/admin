<?php
require_once './connect.php'; 

$timestamp=date("Y-m-d H:i:s");
$lrno=escapeString($conn,strtoupper($_POST['lrno'])); // LR No.
$from_loc=escapeString($conn,strtoupper($_POST['from_loc']));
$to_loc=escapeString($conn,strtoupper($_POST['to_loc']));
$act_weight=escapeString($conn,($_POST['act_weight']));
$chrg_weight=escapeString($conn,($_POST['chrg_weight']));
$ewb_no=escapeString($conn,($_POST['ewb_no']));
$crossing=escapeString($conn,($_POST['crossing']));
$rate_fix=escapeString($conn,($_POST['rate_fix']));
$freight=escapeString($conn,($_POST['freight_amount']));
$rate = sprintf("%.2f",($freight/$chrg_weight));


// echo "<script>
		// alert('Hold !');
		// $('#add_btn_add_more_lr').attr('disabled',true);
		// $('#loadicon').hide();
	// </script>";
	// exit();
	
if($crossing=='YES' AND $to_loc==escapeString($conn,($_POST['to_loc_db'])))
{
	echo "<script>
		alert('Check destination location !');
		$('#add_btn_add_more_lr').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($crossing=='NO' AND $to_loc!=escapeString($conn,($_POST['to_loc_db'])))
{
	echo "<script>
		alert('Check destination location !');
		$('#add_btn_add_more_lr').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$fm_id=escapeString($conn,($_POST['id']));
$frno=escapeString($conn,($_POST['frno']));
$from_id=escapeString($conn,($_POST['from_id']));
$to_id=escapeString($conn,($_POST['to_id']));
$con1_id_ip=escapeString($conn,($_POST['con1_id']));
$item_id=escapeString($conn,($_POST['item_id']));
$lr_id=escapeString($conn,($_POST['lr_id']));

if($crossing=='YES')
{
	$cross_to=$to_loc;
	$cross_stn_id=$to_id;
}
else
{
	$cross_to="";
	$cross_stn_id="";
}

$get_vehicle_no = Qry($conn,"SELECT frno,company,branch_user,truck_no,branch,to1,to_id,ptob as adv_to,disadv,rtgsneftamt,paidto as bal_to,colset_adv FROM freight_form WHERE id='$fm_id'");

if(!$get_vehicle_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$get_lr_data= Qry($conn,"SELECT lrno,date as lr_date,company,consignor,consignee,con1_id,con2_id,wt12,weight,truck_no,branch,crossing,
break,diesel_req FROM lr_sample WHERE id='$lr_id'");

if(!$get_lr_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_vehicle_no)==0){
	echo "<script>
		alert('Freight memo not found !');
		$('#add_btn_add_more_lr').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(numRows($get_lr_data)==0){
	echo "<script>
		alert('LR : $lrno not found !');
		$('#add_btn_add_more_lr').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_fm = fetchArray($get_vehicle_no);
$row_lr = fetchArray($get_lr_data);
	
$lr_date = $row_lr['lr_date'];
$consignor = $row_lr['consignor'];
$consignee = $row_lr['consignee'];
$con1_id = $row_lr['con1_id'];
$con2_id = $row_lr['con2_id'];
$wt12 = $row_lr['wt12'];
$weight = $row_lr['weight'];
$branch_user = $row_fm['branch_user'];
$company = $row_fm['company'];
$create_date = $row_fm['newdate'];
$truck_no = $row_fm['truck_no'];

if($row_fm['to_id']!=$to_id)
{
	if($_POST['fm_last_loc']=='')
	{
		echo "<script>
			alert('Select last delivery location !!');
			$('#add_btn_add_more_lr').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	}
	else
	{
		$fm_last_loc = escapeString($conn,($_POST['fm_last_loc']));;
		$fm_last_loc_id = escapeString($conn,($_POST['fm_last_loc_id']));;
	}
}
else
{
	$fm_last_loc = $row_fm['to1'];
	$fm_last_loc_id = $row_fm['to_id'];
}

if($con1_id!=$con1_id_ip)
{
	echo "<script>
		alert('Consignor not verified.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($wt12!=$act_weight)
{
	echo "<script>
		alert('Actual weight not verified.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($weight!=$chrg_weight)
{
	echo "<script>
		alert('Actual weight not verified.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['truck_no']!=$row_lr['truck_no'])
{
	echo "<script>
		alert('Vehicle number not matching with LR.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['company']!=$row_lr['company'])
{
	echo "<script>
		alert('Company not matching.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['branch']!=$row_lr['branch'])
{
	echo "<script>
		alert('LR does not belongs to your branch.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['frno']!=$frno)
{
	echo "<script>
		alert('Voucher number not verified.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['lrno']!=$lrno)
{
	echo "<script>
		alert('LR number not verified.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['crossing']!='')
{
	echo "<script>
		alert('LR : $lrno has been closed.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['break']>0)
{
	echo "<script>
		alert('This is breaking LR.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['diesel_req']>0)
{
	echo "<script>
		alert('Error : Unable to ADD. Diesel request found in LR.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['colset_adv']>0)
{
	echo "<script>
		alert('Error : Voucher downloaded at Head-office.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['bal_to']!='')
{
	echo "<script>
		alert('Error : Unable to Add LR. Balance paid.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if($row_fm['adv_to']!='')
{
	$update_fm = Qry($conn,"UPDATE freight_form SET weight=weight+'$chrg_weight',actualf=actualf+'$freight',totalf=totalf+'$freight',
	baladv=baladv+'$freight' WHERE id='$fm_id'");

	if(!$update_fm){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($row_fm['disadv']>0)
	{
		$update_diesel_table = Qry($conn,"UPDATE diesel_fm SET lrno=CONCAT(lrno,',','$lrno') WHERE fno='$frno'");

		if(!$update_diesel_table){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($row_fm['rtgsneftamt']>0)
	{
		$update_rtgs_freight = Qry($conn,"UPDATE rtgs_fm SET totalamt=totalamt+'$freight',lrno=CONCAT(lrno,',','$lrno') WHERE fno='$frno'");

		if(!$update_rtgs_freight){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
else
{
	$update_fm = Qry($conn,"UPDATE freight_form SET weight=weight+'$chrg_weight' WHERE id='$fm_id'");

	if(!$update_fm){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$close_lr = Qry($conn,"UPDATE lr_sample SET crossing='$crossing' WHERE id='$lr_id'");

if(!$close_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_lr_pending = Qry($conn,"DELETE FROM lr_sample_pending WHERE lr_id='$lr_id'");

if(!$delete_lr_pending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Lr not found in lr_sample_pending table. LR no: $lrno. Id: $lr_id.",$conn,$page_name,__LINE__);
}

$insert_lr = Qry($conn,"INSERT INTO freight_form_lr(frno,company,branch,branch_user,date,create_date,truck_no,lrno,fstation,tstation,
consignor,consignee,from_id,to_id,con1_id,con2_id,wt12,weight,ratepmt,fix_rate,actualf,crossing,cross_to,cross_stn_id,timestamp) VALUES 
('$frno','$company','$row_fm[branch]','$branch_user','$lr_date','$create_date','$truck_no','$lrno','$from_loc','$to_loc','$consignor',
'$consignee','$from_id','$to_id','$con1_id','$con2_id','$wt12','$weight','$rate','$rate_fix','$freight','$crossing','$cross_to',
'$cross_stn_id','$timestamp')");

if(!$insert_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$fm_id','$frno','LR_ADDED_TO_FM','MORE_LR_ADDED','Vou_no: $frno, LRNo: $lrno, Weight: $weight, Rate: $rate','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('LR Added Successfully !!');
		$('#get_button').attr('disabled',false);
		$('#close_btn_more_lr_add')[0].click();
		$('#get_button')[0].click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error while processing request !!');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',true);
	</script>";
	exit();
}	
?>