<?php
require_once('./connect.php');
	
$frno=escapeString($conn,strtoupper($_POST['frno']));
$id=escapeString($conn,strtoupper($_POST['id']));

$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$fetch_vou = Qry($conn,"SELECT vno,user,branch_user,comp,date,newdate,des,asset_voucher,amt,chq,chq_no,chq_bnk_n,neft_bank,neft_acname,neft_acno,
neft_ifsc,pan,narrat,empcode,vehno,colset_d FROM mk_venf WHERE id='$id'");

if(!$fetch_vou){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_vou)==0)
{
	echo "<script>
		alert('Voucher not found !');
		$('#delete_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row=fetchArray($fetch_vou);

if($row['vno']!=$frno)
{
	echo "<script>
		alert('Voucher number not verified !');
		$('#delete_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row['asset_voucher']=="1")
{
	echo "<script>
		alert('This is asset voucher you can\'t delete !');
		$('#delete_btn_$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$company = $row['comp'];
$branch = $row['user'];
$branch_user = $row['branch_user'];
$old_amount = $row['amt'];
$pay_mode = $row['chq'];
$sys_date = $row['date'];
$vou_date = $row['newdate'];
$exp_head = $row['des'];
$cheque_no = $row['chq_no'];
$cheque_bank_name = $row['chq_bnk_n'];

if($pay_mode=='CASH'){
	$extra_field=".";
}
else if($pay_mode=='CHEQUE'){
	$extra_field= ", Cheque_details: $row[chq_no]($row[chq_bnk_n])";
}
else{
	$extra_field= ", Ac_holder: $row[neft_acname], Ac_no: $row[neft_acno], Bank_Ifsc: $row[neft_bank]($row[neft_ifsc]).";
}

$old_data="Vou_id: $frno, Company : $company, Amount : $old_amount, Payment_Mode : $pay_mode, SysDate : $sys_date, VouDate : $vou_date $extra_field";

	if($company=='RRPL')
	{
		$debit="debit";	
		$balance="balance";	
	}
	else
	{
		$debit="debit2";
		$balance="balance2";
	}
	
if($pay_mode=='NEFT')
{
	$fetch_rtgs_req = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
	if(!$fetch_rtgs_req){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($fetch_rtgs_req)>0)
	{
		echo "<script>
			alert('Rtgs Payment Done. You Can\'t Delete !');
			$('#delete_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}	
}
	
StartCommit($conn);
$flag = true;	
	
if($pay_mode=='NEFT')
{		
	$delete_rtgs = Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$frno' AND colset_d!='1'");
	if(!$delete_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Unable to delete rtgs voucher. id : $id. Vou_no : $frno.",$conn,$page_name,__LINE__);
	}
	
	$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
	if(!$delete_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	if($sys_date==$today)
	{
		$update_today_data = Qry($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$old_amount' WHERE branch='$branch' AND date='$today'");
		if(!$delete_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}	
}
else if($pay_mode=='CHEQUE')
{
	$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
	if(!$delete_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$delete_cheque_book = Qry($conn,"DELETE FROM cheque_book WHERE vou_no='$frno'");
	if(!$delete_cheque_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
}
else if($pay_mode=='CASH')
{
	$update_main_balance = Qry($conn,"UPDATE user SET `$balance`=`$balance`+'$old_amount' WHERE username='$branch'");
	if(!$update_main_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$fetch_cash_id = Qry($conn,"SELECT id FROM cashbook WHERE vou_no='$frno'");
	if(!$fetch_cash_id){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($fetch_cash_id)==0){
		$flag = false;
		errorLog("Cash entry not found. Vou_no: $frno",$conn,$page_name,__LINE__);
	}
			
	$row_cash_id = fetchArray($fetch_cash_id);
	$cash_id = $row_cash_id['id'];
			
	$update_cashbook_amount = Qry($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$old_amount' WHERE id>'$cash_id' AND 
	user='$branch' AND comp='$company'");
	if(!$update_cashbook_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
			
	$delete_cashbook = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
	if(!$delete_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
if($sys_date==$today)
{
	$update_today_data = Qry($conn,"UPDATE today_data SET exp_vou=exp_vou-1,exp_vou_amount=exp_vou_amount-'$old_amount' WHERE 
	branch='$branch' AND date='$today'");
	
	if(!$update_today_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$delete_vou=Qry($conn,"DELETE FROM mk_venf WHERE id='$id'");	

if(!$delete_vou){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_log=Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) 
	VALUES ('$frno','Expense_Voucher','Vou_delete','$old_data','$branch','ADMIN','$timestamp')");

if(!$update_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Voucher Deleted Successfully !');
		window.location.href='./expense_voucher.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./expense_voucher.php");
	exit();
}		
?>