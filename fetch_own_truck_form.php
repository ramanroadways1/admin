<?php
require_once './connect.php'; 

$frno = escapeString($conn,strtoupper($_POST['frno']));

$query = Qry($conn,"SELECT id,lrno,date,create_date,truck_no,fstation,tstation,consignor,consignee,wt12,weight,done FROM freight_form_lr 
where frno='$frno' AND frno like '___OLR%'");

if(!$query){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($query)==0)
{
	echo "<script>
		alert('Invalid Vou No entered !');
		window.location.href='./own_truck_form.php';
	</script>";	
	exit();
}
?>
<table class="table table-bordered" style="font-size:12px;width:100%">
	<tr>
       <th class="bg-info" style="font-size:12.5px;" colspan="10">OWN Truck Form LRs : <?php echo $frno; ?></th>
    </tr>
	<tr>    
			<th>LR_No</th>
			<th>LR_Date</th>
			<th>Created_On</th>
			<th>Truck_No</th>
			<th>From</th>
			<th>To</th>
			<th>Consignor & Consignee</th>
			<th>Act_Wt</th>
			<th>Charge_Wt</th>
			<th></th>
	</tr>	
<?php
$awt_sum = 0;
$cwt_sum = 0;

while($row = fetchArray($query))
{
	$vou_id = $row['id'];
	$awt_sum += $row['wt12'];
	$cwt_sum += $row['weight'];
	$create_date = $row['create_date'];
	$truck_no = $row['truck_no'];
	echo '<tr>
		<td>'.$row['lrno'].'</td>
		<td>'.date("d-m-y",strtotime($row['date'])).'</td>
		<td>'.date("d-m-y",strtotime($create_date)).'</td>
		<td>'.$row['truck_no'].'</td>
		<td>'.$row['fstation'].'</td>
		<td>'.$row['tstation'].'</td>
		<td>'.$row['consignor']."<br>".$row['consignee'].'</td>
		<td>'.$row['wt12'].'</td>
		<td>'.$row['weight'].'</td>
		<td>
			<button onclick="EditLR('.$row['id'].')" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
		</td>
	</tr>';
}


	echo'<tr>
			<td colspan=7><b>Total Calculation :</b></td>				
			<td style="letter-spacing:1px;"><b>'.$awt_sum.'</b></td>
			<td style="letter-spacing:1px;"><b>'.$cwt_sum.'</b></td>
			<td></td>
		</tr>
</table>';
		 
	echo "<script>
			$('#fm_no_head').html('$frno');
			$('#fm_id_find').val('$vou_id');
			$('#main_div').show();
			$('#all_button_div').show();
			$('#truck_no_db').val('$truck_no');
			$('#create_date_text').val('$create_date');
			$('#edit_date_vou_id').val('$vou_id');
			$('#truck_no_change_vou_id').val('$vou_id');
			$('#vou_id_block').val('$frno');
			// $('#div_lr_input').hide();
			// $('#search_button_div').hide();
			$('#loadicon').hide();
	</script>";
?>