<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateLRForm").on('submit',(function(e) {
$("#loadicon").show();
$("#lr_sub").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./lr_update_plant_save.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#lr_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update LR Plant</h3>
              </div>
              
<form role="form" id="UpdateLRForm" action="" method="POST" autocomplete="off">
			  
     <div class="card-body">
				
<script>		
function FetchLR(lrno)
{
	$("#lrno").attr('readonly',true);
	$('#loadicon').show();
	jQuery.ajax({
	url: "fetch_lr_for_plant_update.php",
	data: 'lrno=' + lrno,
	type: "POST",
	success: function(data) {
		$("#lr_result").html(data);
	},
		error: function() {}
	});
}
</script>	

<div id="lr_result"></div>
		
		<div class="row">	
		
			<div class="col-md-3">	
               <div class="form-group">
                  <label>LR No <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="FetchLR(this.value)" class="form-control" id="lrno" name="lrno" required>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Plant <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" id="plant" name="plant" required>
					<option value="">Select an option</option>
					<option class="plant_sel" id="plant_AT" value="AT">AT</option>
					<option class="plant_sel" id="plant_AS" value="AS">AS</option>
					<option class="plant_sel" id="plant_OTHER" value="OTHER">OTHER</option>
				  </select>
              </div>
			</div>
			
			<input type="hidden" id="lr_id" name="lr_id">
		
		</div>
	</div>
                
       <div class="card-footer">
			<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update Plant</button>
		</div>
				
              </form>
            </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
 
</div> 

<?php include ("./footer.php"); ?>

</body>
</html>

<script type="text/javascript">
function sum1() 
{
	if($("#chrg_wt").val()=='')
	{
		$("#chrg_wt").focus();
		$("#b_rate").val('');
		$("#b_amt").val('');
	}	
	else
	{
		$("#b_amt").val(Math.round(Number($("#chrg_wt").val()) * Number($("#b_rate").val())).toFixed(2));
	}
}

function sum2() 
{
	if($("#chrg_wt").val()=='')
	{
		$("#chrg_wt").focus();
		$("#b_rate").val('');
		$("#b_amt").val('');
	}	
	else
	{
		$("#b_rate").val(Math.round(Number($("#b_amt").val()) / Number($("#chrg_wt").val())).toFixed(2));
	}	
}

function ResetAll() 
{
	$("#b_amt").val('');
	$("#b_rate").val('');
}
</script> 