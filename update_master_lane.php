<?php
require_once './connect.php'; 

$vou_id=escapeString($conn,strtoupper($_POST['vou_id']));

$from=escapeString($conn,strtoupper($_POST['from']));
$to=escapeString($conn,strtoupper($_POST['to']));
$from_id=escapeString($conn,strtoupper($_POST['from_id']));
$to_id=escapeString($conn,strtoupper($_POST['to_id']));
$timestamp=date("Y-m-d H:i:s");

$get_ext_data = Qry($conn,"SELECT frno,from1,to1,from_id,to_id FROM freight_form WHERE id='$vou_id'");

if(!$get_ext_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_ext_data)==0){
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_fm = fetchArray($get_ext_data);

$update_log=array();
$update_Qry=array();

if($from!=$row_fm['from1'])
{
	$update_log[]="Dispatch_Loc : $row_fm[from1] to $from";
	$update_Qry[]="from1='$from'";
	$update_Qry[]="from_id='$from_id'";
}

if($to!=$row_fm['to1'])
{
	$update_log[]="Last_Del_Loc : $row_fm[to1] to $to";
	$update_Qry[]="to1='$to'";
	$update_Qry[]="to_id='$to_id'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#master_lane_modal_update').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"UPDATE freight_form SET $update_Qry WHERE id='$vou_id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$vou_id','$row_fm[frno]','FM_UPDATE','MASTER_LANE_UPDATE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Master Lane Updated Successfully !');
		$('#get_button').attr('disabled',false);
		$('#master_lane_modal_update').attr('disabled',false);
		$('#master_lane_modal_close').click();
		$('#get_button').click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fm_view.php");
	exit();
}	
?>