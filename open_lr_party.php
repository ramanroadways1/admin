<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Open LR/e-Way Bill Parties</h3>
              </div>
				
<div class="card-body">
	
	<div class="row">
		<div class="col-md-12">	
			<div class="form-group">
				<button type="button" data-toggle="modal" data-target="#AddForm" class="btn btn-sm btn-success">Add New Open LR Party</button>
			</div>	   
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-12 table-responsive" style="overflow:auto">
		
<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="12">Open LR/e-Way Bill Parties :</th>
    </tr>
	
	<tr>    
		<th>#</th>
		<th>Party_Name</th>
		<th>GST_No</th>
	</tr>
<?php
$get_open_lr_party = Qry($conn,"SELECT p.id,p.party,p2.gst FROM open_lr as p 
LEFT OUTER JOIN consignor AS p2 ON p2.id=p.party_id");

if(!$get_open_lr_party){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn=1;
while($row = fetchArray($get_open_lr_party))
{
	echo '<tr>
		<td>'.$sn.'</td>
		<td>'.$row['party'].'</td>	
        <td>'.$row['gst'].'</td>
     </tr>';
$sn++; 
}
echo '</table>';
?>			
		</div>
	</div>
</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	</div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script type="text/javascript">
  $(function() {  
      $("#party_name").autocomplete({
		source: function(request, response) { 
		$.ajax({
                  url: 'autofill/get_consignor.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term
                  },
                  success: function(data) {
                    response(data);
              }
           });
         },
           select: function (event, ui) { 
               $('#party_name').val(ui.item.value);   
               $('#party_id').val(ui.item.id);     
               $('#party_gst').val(ui.item.gst);     
			  return false;
         },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#party_name").val('');
				$("#party_id").val('');
				$("#party_gst").val('');
                alert('Party does not exist !'); 
 }},});}); 
	 
$(document).ready(function (e) {
$("#AddForm1").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#add_new_button").attr("disabled", true);
	$.ajax({
	url: "./open_lr_party_add.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="AddForm1" autocomplete="off"> 
<div class="modal fade" id="AddForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
	   
	  <div class="modal-header">
			<h5 class="modal-title">Add new Open LR Party :</h5>
      </div>
	  
		<div class="modal-body">
			
		<div class="row">
			
			<div class="col-md-12">
				<div class="form-group">
					<label>Party Name <font color="red"><sup>*</sup></font></label>
                     <input type="text" id="party_name" name="party_name" class="form-control" required>
                 </div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label>Party Name <font color="red"><sup>*</sup></font></label>
                     <input type="text" id="party_gst" name="party_gst" class="form-control" required readonly>
                 </div>
			</div>
				
			</div>
		</div>
	<input type="hidden" name="party_id" id="party_id">	
	<div class="modal-footer">
		<button type="button" id="add_close_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="add_new_button" class="btn btn-primary">ADD Party</button>
	</div>
		</div>	
     </div>
 </div>	
</form>
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>