<?php
require_once("connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$search_by = escapeString($conn,strtoupper($_POST['search_by']));

if($search_by!='LR' AND $search_by!='FM')
{
	echo "<script>
		alert('Select search by option first !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').hide();
		$('#button_submit').attr('disabled', true);
		
		$('#button_chk').attr('disabled',false);
	</script>";
	exit();
}

$qry = Qry($conn,"SELECT baladv,paidto,bal_date FROM freight_form WHERE frno='$frno'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').hide();
		$('#button_submit').attr('disabled', true);
		
		$('#button_chk').attr('disabled',false);
	</script>";
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script>
		alert('Freight memo not found !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').hide();
		$('#button_submit').attr('disabled', true);
		
		$('#button_chk').attr('disabled',false);
	</script>";
	exit();
}

$row_fm = fetchArray($qry);

$bal_date = date("d-m-y",strtotime($row_fm['bal_date']));

if($row_fm['paidto']!='')
{
	echo "<script>
		alert('Freight memo balance paid on : $bal_date !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').hide();
		$('#button_submit').attr('disabled', true);
		
		$('#button_chk').attr('disabled',false);
	</script>";
	exit();
}

echo "<script>$('.search_by_option').attr('disabled',true);</script>";
	
if($search_by=='LR')
{
	echo "<script>
		$('#LR_option').attr('disabled',false);
	</script>";
}
else
{
	echo "<script>
		$('#FM_option').attr('disabled',false);
	</script>";
}

echo "<script>
	$('#balance_amount').val('$row_fm[baladv]');
	$('#fm_no').attr('readonly',true);
	$('#button_submit').attr('disabled',false);
	$('#loadicon').fadeOut('slow');
	$('#button_submit').show();
	
	$('#button_chk').attr('disabled',true);
</script>";
?>