<?php
require_once("../_connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));
$branch = escapeString($conn,strtoupper($_POST['branch']));
$fix_rate = escapeString($conn,($_POST['fix_rate']));
$rate = escapeString($conn,($_POST['rate']));

$check_lr = Qry($conn,"SELECT id,branch,company,truck_no,fstation,tstation,wt12 as actual_weight,weight as charge_weight,from_id,to_id,con1_id,crossing,
item_id,break,date(timestamp) as date_created FROM lr_sample WHERE lrno='$lrno'");

if(!$check_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
		$('#add_btn_add_more_lr').attr('disabled',true);
		$('#from_loc_add_more').attr('readonly',true);
		$('#to_loc_add_more').attr('readonly',true);
		$('#from_loc_add_more').val('');
		$('#to_loc_add_more').val('');
		$('#to_loc_add_more_db').val('');
		$('#to_loc_id_add_more_db').val('');
		$('#add_more_from_id').val('');
		$('#add_more_to_id').val('');
		$('#lr_add_fetch_btn').show();
		$('#add_lr_modal_lrno').attr('readonly',false);
		$('#act_wt_add_more').attr('readonly',true);
		$('#chrg_wt_add_more').attr('readonly',true);
		$('#freight_add_more').attr('readonly',true);
		$('#act_wt_add_more').val('');
		$('#freight_add_more').val('');
		$('#rate_fix_add_more').val('');
		$('#chrg_wt_add_more').val('');
		$('#add_more_lr_lr_id').val('');
</script>";

if(numRows($check_lr)==0){
	echo "<script>
		alert('LR not found.');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_check_lr = fetchArray($check_lr);

$lr_id = $row_check_lr['id'];

if($truck_no=='')
{
	echo "<script>
		alert('Error : Vehicle number not found.');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($branch=='')
{
	echo "<script>
		alert('Error : Branch not found.');
		$('#loadicon').hide();
	</script>";
	exit();
}

$diff_days = strtotime(date("Y-m-d")) - strtotime($row_check_lr['date_created']);
$diff_days=round($diff_days / (60 * 60 * 24));	

if($diff_days>10)
{
	echo "<script>
		alert('Error : LR exceed 10 days.');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
if($row_check_lr['truck_no']!=$truck_no)
{
	echo "<script>
		alert('Error : Vehicle number not matching.');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_check_lr['branch']!=$branch)
{
	echo "<script>
		alert('Error : LR not belongs to your branch : $branch.');
		$('#loadicon').hide();
	</script>";
	exit();
}

	
if($row_check_lr['break']>0)
{
	echo "<script>
		alert('Error : This is breaking LR.');
		$('#loadicon').hide();
	</script>";
	exit();	
}

if($row_check_lr['crossing']!='')
{
	echo "<script>
		alert('Error : LR Closed.');
		$('#loadicon').hide();
	</script>";
	exit();	
}
	
$chkEwayBillFree = Qry($conn,"SELECT id FROM _eway_bill_free WHERE item='$row_check_lr[item_id]' || consignor='$row_check_lr[con1_id]' || lrno='$lrno' AND status='1'");
if(!$chkEwayBillFree){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

if(numRows($chkEwayBillFree)>0)
{
	echo "<script>
		$('#ewb_no_ip_add_more').attr('readonly',true);
	</script>";
}
else
{
	echo "<script>
		$('#ewb_no_ip_add_more').attr('readonly',false);
	</script>";
}

$new_freight = sprintf("%.2f",round($row_check_lr['charge_weight']*$rate));


echo "<script>
		$('#from_loc_add_more').attr('readonly',true);
		$('#to_loc_add_more').attr('readonly',true);
		
		$('#from_loc_add_more').val('$row_check_lr[fstation]');
		$('#to_loc_add_more').val('$row_check_lr[tstation]');
		$('#to_loc_add_more_db').val('$row_check_lr[tstation]');
		$('#to_loc_id_add_more_db').val('$row_check_lr[to_id]');
		
		$('#add_more_from_id').val('$row_check_lr[from_id]');
		$('#add_more_to_id').val('$row_check_lr[to_id]');
		$('#add_more_con1_id').val('$row_check_lr[con1_id]');
		$('#add_more_item_id').val('$row_check_lr[item_id]');
		
		$('#add_btn_add_more_lr').attr('disabled',false);
		$('#lr_add_fetch_btn').hide();
		$('#add_lr_modal_lrno').attr('readonly',true);
		
		$('#act_wt_add_more').attr('readonly',true);
		$('#chrg_wt_add_more').attr('readonly',true);
		$('#freight_add_more').attr('readonly',true);
		$('#act_wt_add_more').val('$row_check_lr[actual_weight]');
		$('#chrg_wt_add_more').val('$row_check_lr[charge_weight]');
		$('#freight_add_more').val('$new_freight');
		$('#rate_fix_add_more').val('$fix_rate');
		$('#add_more_lr_lr_id').val('$lr_id');
		$('#loadicon').hide();
</script>";
?>