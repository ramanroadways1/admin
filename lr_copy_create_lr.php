<?php
require_once("./connect.php");

$lrno_old = escapeString($conn,strtoupper($_POST['lrno_old']));
$new_lrno = escapeString($conn,strtoupper($_POST['new_lrno']));

$qry = Qry($conn,"SELECT lr_id FROM lr_sample_pending WHERE lrno = '$lrno_old'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script>
		alert('LR not found !');
		$('#loadicon').fadeOut('slow');
		$('#create_button').attr('disabled',true);
	</script>";	
	exit();
}

$row = fetchArray($qry);

$lr_id = $row['lr_id'];

if($new_lrno != ($lrno_old.'A') AND $new_lrno != ($lrno_old.'B'))
{
	echo "<script>
		alert('LR not verified !');
		$('#loadicon').fadeOut('slow');
		$('#create_button').attr('disabled',false);
	</script>";	
	exit();
}

StartCommit($conn);
$flag = true;

$update_lr_sample_pending = Qry($conn,"UPDATE lr_sample_pending SET lrno = '$new_lrno' WHERE lrno='$lrno_old'");

if(!$update_lr_sample_pending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("LR not updated. lr_sample_pending LR_no: $lrno_old and new lrno: $new_lrno.",$conn,$page_name,__LINE__);
}

$update_Qry = Qry($conn,"UPDATE lr_sample SET lrno = '$new_lrno' WHERE lrno='$lrno_old'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_Qry2 = Qry($conn,"UPDATE lr_check SET lrno = '$new_lrno' WHERE lrno='$lrno_old'");

if(!$update_Qry2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$lr_id','$lrno_old','LR_ENTRY','LR_NO_UPDATE_DUMMY','LR updated $lrno_old to $new_lrno.','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('LR : $lrno_old updated to $new_lrno !');
		window.location.href='./lr_copy.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request !');
		$('#loadicon').fadeOut('slow');
		$('#create_button').attr('disabled',false);
	</script>";	
	exit();
}	
?>