<?php
require_once('../connect.php');

if(isset($_POST['search'])){
 $search = $_POST['search'];
 
 $sql = Qry($conn,"SELECT id,name,gst FROM consignee WHERE name LIKE '%".$search."%' ORDER BY name ASC limit 10");
 
if(!$sql){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$response = array();
while($row = fetchArray($sql))
 {
   $response[] = array("value"=>$row['name'],"label"=>$row['name'],"id"=>$row['id'],"gst"=>$row['gst']);
 }
echo json_encode($response);
}
?>