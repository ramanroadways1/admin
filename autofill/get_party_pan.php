<?php
include ('../connect.php');

if(isset($_POST['search'])){
 $search = $_POST['search'];
 $search_by = $_POST['search_by'];
 
 if($search_by=='BROKER')
 {
	 $sql = Qry($conn,"SELECT id,name,pan FROM mk_broker WHERE pan LIKE '%".$search."%' limit 8");
	 
	if(!$sql){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
 }
 else if($search_by=='OWNER')
 {
	 $sql = Qry($conn,"SELECT id,name,pan FROM mk_truck WHERE pan LIKE '%".$search."%' limit 8");
	 
	if(!$sql){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
 }

 $response = array();
 while($row = fetchArray($sql)){
   $response[] = array("value"=>$row['pan'],"label"=>$row['name'].' ('.$row['pan'].')',"party_name"=>$row['name'],"party_id"=>$row['id']);
 }

 echo json_encode($response);
 }
?>