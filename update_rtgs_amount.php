<?php
require_once("./connect.php");

$frno = escapeString($conn,($_POST['vou_no']));
$vou_id = escapeString($conn,($_POST['vou_id']));
$rtgs_amt = escapeString($conn,$_POST['rtgs_amt']);
$rtgs_id = escapeString($conn,$_POST['rtgs_id']);

$timestamp = date("Y-m-d H:i:s");
	
if(empty($rtgs_amt) || $rtgs_amt<=0)
{
	echo "<script>alert('Error: RTSG amount is not valid..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(empty($frno))
{
	echo "<script>alert('Voucher not found..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(empty($vou_id))
{
	echo "<script>alert('VoucherID not found..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(empty($rtgs_id))
{
	echo "<script>alert('RTGS not found..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_fm_data = Qry($conn,"SELECT f.frno,f.totalf,f.totaladv,f.cashadv,f.chqadv,f.disadv,f.rtgsneftamt,f.baladv,f.paidto 
FROM freight_form AS f 
WHERE f.id='$vou_id'");

if(!$get_fm_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_fm_data)==0){
	echo "<script>alert('Voucher not found..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_fm = fetchArray($get_fm_data);

if($row_fm['paidto']!='')
{
	echo "<script>alert('Error: Balance paid..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$old_rtgs_amt = $row_fm['rtgsneftamt'];

if(sprintf("%.2f",$old_rtgs_amt)==sprintf("%.2f",$rtgs_amt))
{
	echo "<script>alert('Error: Nothing to update.');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_fm['frno']!=$frno)
{
	echo "<script>alert('Error: Voucher not verified..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$new_adv = $row_fm['cashadv']+$row_fm['chqadv']+$row_fm['disadv']+$rtgs_amt;
$new_balance = $row_fm['totalf']-$new_adv;

if($new_balance<0)
{
	echo "<script>alert('Error: Not enough balance left. Balance left is: $row_fm[baladv].');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_fm['totaladv']=='' || $row_fm['totaladv']<=0)
{
	echo "<script>alert('Error: Advance pending or not paid..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_rtgs_data = Qry($conn,"SELECT fno,amount,colset FROM rtgs_fm WHERE id='$rtgs_id'");

if(!$get_rtgs_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_rtgs_data)==0){
	echo "<script>alert('RTGS not found..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_rtgs = fetchArray($get_rtgs_data);

if($row_rtgs['fno']!=$frno)
{
	echo "<script>alert('Error: Voucher not verified..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(sprintf("%.2f",$row_rtgs['amount'])==sprintf("%.2f",$rtgs_amt))
{
	echo "<script>alert('Error: Nothing to update.');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_rtgs['colset']!="")
{
	echo "<script>alert('Error: Voucher downloaded by account-dpt..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}


StartCommit($conn);
$flag = true;

$update_adv_details = Qry($conn,"UPDATE freight_form SET totaladv='$new_adv',rtgsneftamt='$rtgs_amt',baladv='$new_balance' 
WHERE id='$vou_id' AND totaladv>0 AND paidto=''");
	
if(!$update_adv_details){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("FM Not updated. Id: $vou_id.",$conn,$page_name,__LINE__);
}

$update_rtgs = Qry($conn,"UPDATE rtgs_fm SET amount='$rtgs_amt' WHERE id='$rtgs_id' AND colset=''");
	
if(!$update_rtgs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("RTGS Not updated. Id: $rtgs_id.",$conn,$page_name,__LINE__);
}
	
$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$rtgs_id','$frno','RTGS_UPDATE','AMOUNT_UPDATE','Amount $old_rtgs_amt to $rtgs_amt.','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Update Success.');
		$('#loadicon').fadeOut('slow');
		$('#rtgs_amt_update_modal_btn').attr('disabled',true);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error: something went wrong..');$('#rtgs_amt_update_modal_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>