<?php
require_once("./connect.php");

$frno=escapeString($conn,strtoupper($_POST['vou_no']));
$id=escapeString($conn,strtoupper($_POST['vou_id_fm']));
$timestamp = date("Y-m-d H:i:s");

$get_fm = Qry($conn,"SELECT frno,did FROM freight_form WHERE id='$id'");
if(!$get_fm){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_fm)==0){
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_fm = fetchArray($get_fm);

if($row_fm['frno']!=$frno)
{
	echo "<script>
		alert('Freight memo not verified !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$get_ext_driver = Qry($conn,"SELECT name FROM mk_driver WHERE id='$row_fm[did]'");
if(!$get_ext_driver){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_ext_driver)==0){
	echo "<script>
		alert('Driver not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_ext_driver = fetchArray($get_ext_driver);

$ext_driver = $row_ext_driver['name'];

$driver_name=escapeString($conn,strtoupper($_POST['driver_name']));
$driver_id=escapeString($conn,strtoupper($_POST['driver_id']));

$update_log=array();
$update_Qry=array();

if($driver_id!=$row_fm['did'])
{
	$update_log[]="Driver : $ext_driver($row_fm[did]) to $driver_name($driver_id)";
	$update_Qry[]="did='$driver_id'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#chanage_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"UPDATE freight_form SET $update_Qry WHERE id='$id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$row_fm[frno]','FM_UPDATE','DRIVER_UPDATE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Driver Updated Successfully !');
		$('#get_button').attr('disabled',false);
		$('#chanage_driver_button').attr('disabled',false);
		$('#close_driver_button').click();
		$('#get_button').click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fm_view.php");
	exit();
}
?>