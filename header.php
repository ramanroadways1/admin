<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" id="menu_button2" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
</nav>
  
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="font-size:13px;font-family: 'Open Sans', sans-serif !important;">
    
    <a href="./index.php" class="brand-link">
      <img src="logo.png" class="brand-image img-circle elevation-3"
           style="opacity:">
      <span class="brand-text" style="font-size:17px;color:#FFF;font-family:Century Gothic">
		<!--<img src="logo_ummeed.PNG" style="height:100%;width:60%" />-->
		RAMAN_ROADWAYS
	</span>
    </a>
	
  <div class="sidebar">
	<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
		  <li class="nav-item">
            <a href="./index.php" class="nav-link <?php if ($menu=="index.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>Dashboard</p>
            </a>
          </li>
		  
		  <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if ($menu=="lr_entry.php" || $menu=="lr_breaking.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-edit"></i>
              <p>
                LR Section
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
		
			<li class="nav-item">
                <a href="./lr_entry.php" class="nav-link <?php if ($menu=="lr_entry.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>LR Entry</p>
                </a>
             </li>
			 
			 <li class="nav-item">
                <a href="./lr_entry_update_plant.php" class="nav-link <?php if ($menu=="lr_entry_update_plant.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Update Plant (Pune)</p>
                </a>
             </li>
			  
              <li class="nav-item">
                <a href="./lr_breaking.php" class="nav-link <?php if ($menu=="lr_breaking.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>LR Breaking</p>
                </a>
              </li>
			  
			   <li class="nav-item">
                <a href="./lr_copy.php" class="nav-link <?php if ($menu=="lr_copy.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Dummy LR (Jharsuguda)</p>
                </a>
              </li>
            </ul>
         </li>
		 
		  <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if ($menu=="coal_manage_dest.php" || $menu=="coal_dispatch_data.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-ship"></i>
              <p>
                Coal
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
		
			<li class="nav-item">
                <a href="./coal_manage_dest.php" class="nav-link <?php if ($menu=="coal_manage_dest.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-location-arrow nav-icon"></i>
                  <p>Manage destinations</p>
                </a>
             </li>
			 
			 <li class="nav-item">
                <a href="./coal_dispatch_data.php" class="nav-link <?php if ($menu=="coal_dispatch_data.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-location-arrow nav-icon"></i>
                  <p>Dispatch Data</p>
                </a>
             </li>
			
			</ul>
         </li>
		 
		 <li class="nav-item">
		   <a href="./fm_view.php" class="nav-link <?php if ($menu=="fm_view.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-table"></i>
              <p>
                 Freight Memo
              </p>
            </a>
		 </li>	
		 
		 <li class="nav-item">
		   <a href="./gen_ewb_token.php" class="nav-link <?php if ($menu=="gen_ewb_token.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-table"></i>
              <p>
                 Generate Ewb Token
              </p>
            </a>
		 </li>	
		 
		 
		 <li class="nav-item">
		   <a href="./own_truck_form.php" class="nav-link <?php if ($menu=="own_truck_form.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-truck"></i>
              <p>
                Own Truck Form
              </p>
            </a>
		 </li>	

<!--	
 <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if ($menu=="hisab_pay.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-file-text-o"></i>
              <p>
                e-Diary
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
		
			<li class="nav-item">
                <a href="./hisab_pay.php" class="nav-link <?php if ($menu=="hisab_pay.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Hisab Pay</p>
                </a>
             </li>
			</ul>
 </li>-->
		 
		 <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if ($menu=="request_market_truck.php" || $menu=="request_own_truck.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-file-text-o"></i>
              <p>
                Diesel Request
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
		
			<li class="nav-item">
                <a href="./request_market_truck.php" class="nav-link <?php if ($menu=="request_market_truck.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Market Truck</p>
                </a>
             </li>
			  
              <li class="nav-item">
                <a href="./request_own_truck.php" class="nav-link <?php if ($menu=="request_own_truck.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Own Truck</p>
                </a>
              </li>
            </ul>
         </li>
		 
		<li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if ($menu=="truck_voucher.php" || $menu=="expense_voucher.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-inr"></i>
              <p>
                Vouchers
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
		
			<li class="nav-item">
                <a href="./truck_voucher.php" class="nav-link <?php if ($menu=="truck_voucher.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Truck Vouchers</p>
                </a>
             </li>
			  
              <li class="nav-item">
                <a href="./expense_voucher.php" class="nav-link <?php if ($menu=="expense_voucher.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Expense Vouchers</p>
                </a>
              </li>
            </ul>
       </li>
	   
	   <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if ($menu=="add_party.php" || $menu=="add_party_excel.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Party Master
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
		
			<li class="nav-item">
                <a href="./add_party.php" class="nav-link <?php if ($menu=="add_party.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-user nav-icon"></i>
                  <p>Add new party</p>
                </a>
             </li>
			 
			 <li class="nav-item">
                <a href="./add_party_excel.php" class="nav-link <?php if ($menu=="add_party_excel.php") {echo "active"; } else  {echo "";}?>">
                  <i class="fa fa-user nav-icon"></i>
                  <p>Excel upload</p>
                </a>
             </li>
			
			</ul>
       </li>
		 
	<!-- 
	   <li class="nav-item">
            <a href="./add_party.php" class="nav-link <?php if ($menu=="add_party.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-user"></i>
              <p>
               Add New Party
              </p>
            </a>
       </li>
	   -->
		 
		 <li class="nav-item">
		   <a href="./credit.php" class="nav-link <?php if ($menu=="credit.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-plus-square-o"></i>
              <p>
                Credit
              </p>
            </a>
		 </li>	
		 
		 <li class="nav-item">
		   <a href="./debit.php" class="nav-link <?php if ($menu=="debit.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-minus-square-o"></i>
              <p>
                Debit
              </p>
            </a>
		 </li>	
		 
		 <li class="nav-item">
            <a href="late_pod_mgt.php" class="nav-link <?php if ($menu=="late_pod_mgt.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-calendar-times-o"></i>
              <p>
               Late POD Functions
              </p>
            </a>
          </li>
		 
		 <li class="nav-item">
		   <a href="./all_function.php" class="nav-link <?php if ($menu=="all_function.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-database"></i>
              <p>
                Functions
              </p>
            </a>
		 </li>	 
		 
		<li class="nav-item">
            <a href="_ho_manager/" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
               Asset & Employee Mgt
              </p>
            </a>
       </li>
		  
		  <li class="nav-item">
            <a href="open_lr_party.php" class="nav-link <?php if ($menu=="open_lr_party.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-users"></i>
              <p>
               Open LR Parties
              </p>
            </a>
          </li>
		  
		  <li class="nav-item">
            <a href="zero_freight_parties.php" class="nav-link <?php if ($menu=="zero_freight_parties.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-users"></i>
              <p>
               Zero Freight Parties
              </p>
            </a>
          </li>
		  
		   <li class="nav-item">
            <a href="fix_lane.php" class="nav-link <?php if ($menu=="fix_lane.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-road"></i>
              <p>
               Fix Lane (e-diary)
              </p>
            </a>
          </li>

		<li class="nav-item">
            <a href="test_fix_lane.php" class="nav-link <?php if ($menu=="test_fix_lane.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-refresh"></i>
              <p>
               Test Fix Lane
              </p>
            </a>
          </li>
		  
		  <li class="nav-item">
            <a href="e_lr.php" class="nav-link <?php if ($menu=="e_lr.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-print"></i>
              <p>
                e-LR Format
              </p>
            </a>
          </li>
		  
		  <li class="nav-item">
            <a href="./pod.php" class="nav-link <?php if ($menu=="pod.php") {echo "active"; } else  {echo "";}?>">
              <i class="nav-icon fa fa-files-o"></i>
              <p>
                Proof of Delivery
              </p>
            </a>
          </li>
		  
		  <li class="nav-item">
            <a href="./mark_pod_rcvd.php" class="nav-link <?php if ($menu=="mark_pod_rcvd.php") {echo "active"; } else  {echo "";}?>">
             <i class="nav-icon fa fa-file-text-o"></i>
              <p>
                Forfeit FM Balance
              </p>
            </a>
          </li>
		  
		   <li class="nav-item">
            <a href="./forfeit_adv_neft.php" class="nav-link <?php if ($menu=="forfeit_adv_neft.php") {echo "active"; } else  {echo "";}?>">
             <i class="nav-icon fa fa-file-text-o"></i>
              <p>
                Forfeit FM (Adv. Neft)
              </p>
            </a>
          </li>
		
		 <li class="nav-item">
            <a href="./change_ac.php" class="nav-link <?php if ($menu=="change_ac.php") {echo "active"; } else  {echo "";}?>">
             <i class="nav-icon fa fa-university"></i>
              <p>
                Change Account
              </p>
            </a>
          </li>
		  
		<li class="nav-item">
		   <a href="./logout.php" class="nav-link">
              <i class="nav-icon fa fa-sign-out"></i>
              <p>
                Log out
              </p>
            </a>
		 </li>	
         
        </ul>
      </nav>
    </div>
  </aside>