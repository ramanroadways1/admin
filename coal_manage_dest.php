<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Manage coal destinations</h3>
              </div>
				
<div class="card-body">
	
	<div class="row">
		<div class="col-md-12">	
			<div class="form-group">
				<button type="button" data-toggle="modal" data-target="#AddForm" class="btn btn-sm btn-success">Add new record</button>
			</div>	   
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-12 table-responsive" style="overflow:auto">
		
<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="12">Coal destination summary :</th>
    </tr>
	
	<tr>    
		<th>#</th>
		<th>Branch</th>
		<th>Vessel_name</th>
		<th>Destination</th>
		<th>Pincode</th>
		<th>Po_no</th>
		<th>Distance</th>
		<th>Timestamp</th>
	</tr>
<?php
$get_records = Qry($conn,"SELECT d.location,d.pincode,d.po_no,d.distance,d.branch,d.timestamp,v.name as vessel_name 
FROM ship.dest_location as d 
LEFT JOIN ship.shipment as s ON s.unq_id=d.ship_id 
LEFT JOIN ship.vessel_name as v ON v.id=s.vessel_name");

if(!$get_records){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn=1;
while($row = fetchArray($get_records))
{
	$timestamp = date("d/m/y h:i A",strtotime($row['timestamp']));
	echo '<tr>
		<td>'.$sn.'</td>
		<td>'.$row['branch'].'</td>	
		<td>'.$row['vessel_name'].'</td>	
		<td>'.$row['location'].'</td>	
		<td>'.$row['pincode'].'</td>	
		<td>'.$row['po_no'].'</td>	
		<td>'.$row['distance'].'</td>	
		<td>'.$timestamp.'</td>
     </tr>';
$sn++; 
}
echo '</table>';
?>			
		</div>
	</div>
</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	</div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script type="text/javascript">
  $(function() {  
      $("#location_name").autocomplete({
		source: function(request, response) { 
		$.ajax({
                  url: 'autofill/get_location.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term
                  },
                  success: function(data) {
                    response(data);
              }
           });
         },
           select: function (event, ui) { 
               $('#location_name').val(ui.item.value);   
               $('#location_name_id').val(ui.item.id);     
              return false;
         },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#location_name").val('');
				$("#location_name_id").val('');
				alert('Location does not exist !'); 
 }},});}); 
	 
$(document).ready(function (e) {
$("#AddForm1").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#add_new_button").attr("disabled", true);
	$.ajax({
	url: "./save_coal_manage_locations.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="AddForm1" autocomplete="off"> 
<div class="modal fade" id="AddForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
	   
	  <div class="modal-header">
			<h5 class="modal-title">Add new destinatio :</h5>
      </div>
	  
		<div class="modal-body">
			
		<div class="row">
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Ship Name <font color="red"><sup>*</sup></font></label>
                    <select class="form-control" onchange="VesselBranch(this.value)" required="required" id="vessel_name_id" name="vessel_name">
						<option value="">--select--</option>
						<?php
						$qry1 = Qry($conn,"SELECT s.unq_id,s.branch,v.name as v_name 
						FROM ship.shipment AS s 
						LEFT JOIN ship.vessel_name as v ON v.id=s.vessel_name ORDER BY s.id DESC");
						
						if(numRows($qry1)>0)
						{
							while($row_n = fetchArray($qry1))
							{
								echo "<option value='$row_n[unq_id]_#_$row_n[branch]'>$row_n[v_name]</option>";
							}
						}
						?>
					</select>
                 </div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Destination name <font color="red"><sup>*</sup></font></label>
                    <!-- <input type="text" id="location_name" name="location_name" class="form-control" required>-->
					<select class="form-control" onchange="DestinationId(this.value)" required="required" name="destination_name" id="destination_name">
						<option value="">--select--</option>
						<option value="CHANDERIA_#_216_#_312021">CHANDERIA</option>
						<option value="DARIBA_#_273_#_313211">DARIBA</option>
						<option value="ZAWAR_#_1754_#_313901">ZAWAR</option>
					</select>
                 </div>
			</div>
			
			<script>
			function VesselBranch(elem)
			{
				$('#destination_name').val('');
				if(elem!=''){
					var branch = elem.split("_#_")[1];
					var ship_id = elem.split("_#_")[0];
				}
				else{
					var branch="";
					var ship_id="";
				}
				
				$('#branch_name').val(branch);
				$('#ship_id').val(ship_id);
			}
			
			function DestinationId(elem)
			{
				var vessel_name = $('#vessel_name_id').val();
				
				if(vessel_name=='')
				{
					$('#destination_name').val('');
					$('#vessel_name_id').focus();
				}
				else
				{
					var branch = vessel_name.split("_#_")[1];
					
					if(elem!=''){
						var pincode = elem.split("_#_")[2];
						var loc_id = elem.split("_#_")[1];
						
						if(branch=='COAL'){
							if(loc_id=='216'){
								var km_distance="650";
							} 
							else if(loc_id=='273'){
								var km_distance="600";
							}
							else if(loc_id=='1754'){
								var km_distance="560";
							}
							else{
								var km_distance="";
							}
						}
						else if(branch=='DAHEJ'){
							if(loc_id=='216'){
								var km_distance="550";
							} 
							else if(loc_id=='273'){
								var km_distance="545";
							}
							else if(loc_id=='1754'){
								var km_distance="450";
							}
							else{
								var km_distance="";
							}
						}
						else{
							var km_distance="";
						}
					}
					else{
						var pincode="";
						var km_distance="";
					}
					
					$('#pincode').val(pincode);
					$('#km_distance').val(km_distance);
				}
			}
			</script>
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Pincode <font color="red"><sup>*</sup></font></label>
                     <input type="text" id="pincode" name="pincode" class="form-control" readonly required>
                 </div>
			</div>
			
			<div class="col-md-6"> 
				<div class="form-group">
					<label>PO number <font color="red"><sup>*</sup></font></label>
                     <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="po_no" name="po_no" class="form-control" required>
                 </div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Distance <font color="red"><sup>* (in KM)</sup></font></label>
                     <input type="number" step="any" min="1" id="km_distance" name="km_distance" class="form-control" required>
                 </div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Branch <font color="red"><sup>*</sup></font></label>
                     <input type="text" id="branch_name" name="branch_name" class="form-control" readonly required>
                 </div>
			</div>
				 
			</div>
		</div>
		<input type="hidden" name="location_name_id" id="location_name_id">	
		<input type="hidden" name="ship_id" id="ship_id">	
	
	<div class="modal-footer">
		<button type="button" id="add_close_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="add_new_button" class="btn btn-primary">ADD Record</button>
	</div>
		</div>	
     </div>
 </div>	
</form>
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>