<?php
require_once("./connect.php");

$frno=escapeString($conn,strtoupper($_POST['frno']));
$id=escapeString($conn,strtoupper($_POST['id']));
$date=escapeString($conn,strtoupper($_POST['date']));
$timestamp=date("Y-m-d H:i:s");

$get_data = Qry($conn,"SELECT frno,create_date FROM freight_form_lr WHERE id='$id'");
if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)==0){
	echo "<script>
		alert('Invalid Vou No !');
		window.location.href='./own_truck_form.php';
	</script>";	
	exit();
}

$row_data = fetchArray($get_data);

if($row_data['frno']!=$frno)
{
	echo "<script>
		alert('Voucher number not verified !');
		$('#date_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_data['create_date']==$date)
{
	echo "<script>
		alert('Nothing to update !');
		$('#date_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE freight_form_lr SET create_date='$date' WHERE frno='$frno'");
if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$update_log=Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) 
VALUES ('$id','$frno','OWN_TRUCK_FORM','DATE_UPDATE','$row_data[create_date] to $date','','ADMIN','$timestamp')");

if(!$update_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Date Successfully updated !');
		$('#get_button').attr('disabled',false);
		$('#date_update_button').attr('disabled',false);
		$('#close_date_update_button').click();
		$('#get_button').click();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./own_truck_form.php");
	exit();
}	
?>