<?php
require_once('./connect.php');
require_once('./connect2.php');
	exit();
$frno=mysqli_real_escape_string($conn,strtoupper($_POST['frno']));
$vou_type=mysqli_real_escape_string($conn,strtoupper($_POST['vou_type']));

if($vou_type=='')
{
	echo "<script>
			alert('Invalid Voucher Type Defined $vou_type!');
			window.location.href='truck_voucher.php';
		</script>";
	exit();
}

$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$fetch_vou = mysqli_query($conn,"SELECT user,date,truckno,company,amt,mode,colset_d FROM mk_tdv WHERE tdvid='$frno'");
if(!$fetch_vou)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($fetch_vou)==0)
{
	echo "<script>
		alert('Voucher not found !');
	</script>";
	exit();
}

$row=mysqli_fetch_array($fetch_vou);

$company = $row['company'];
$branch = $row['user'];
$old_amount = $row['amt'];
$pay_mode = $row['mode'];
$sys_date = $row['date'];
$truck_no = $row['truckno'];

if($pay_mode=='CASH')
{
	$type11='cash';
}
else if($pay_mode=='CHEQUE')
{
	$type11='cheque';
}
else if($pay_mode=='NEFT')
{
	$type11='rtgs';
}
else
{
	echo "<script>
			alert('Invalid option selected !');
			window.location.href='truck_voucher.php';
		</script>";
		exit();
}

	if($vou_type=='DIARY')
	{
		$qry_fetch_driver = mysqli_query($conn2,"SELECT trip_id,trans_id FROM `$type11` WHERE vou_no='$frno'");
		if(mysqli_num_rows($qry_fetch_driver)==0)
		{
			echo "<script>
			alert('Unable to fetch Transaction Id $frno!');
			$('#loadicon').hide();
			</script>";
			exit();
		}
		
		$row_trans_id22 = mysqli_fetch_array($qry_fetch_driver);
		
		$trans_id_db=$row_trans_id22['trans_id'];
		$trip_id_db=$row_trans_id22['trip_id'];
		
		$chk_driver_book = mysqli_query($conn2,"SELECT driver_code,tno FROM driver_book WHERE trans_id='$trans_id_db'");
		if(mysqli_num_rows($chk_driver_book)==0)
		{
			echo "<script>
			alert('Unable to fetch Driver Code !');
			$('#loadicon').hide();
			</script>";
			exit();
		}
		
		$row_driver_code = mysqli_fetch_array($chk_driver_book);
		
		$tno_db=$row_driver_code['tno'];
		$driver_code_db=$row_driver_code['driver_code'];
		
		$chk_for_active_driver = mysqli_query($conn2,"SELECT id FROM trip WHERE id='$trip_id_db'");
	
		if(mysqli_num_rows($chk_for_active_driver)==0)
		{
			$sel_d_name = mysqli_query($conn2,"SELECT name FROM driver WHERE code='$driver_code_db'");
			if(mysqli_num_rows($sel_d_name)==0)
			{
				echo "<script>
					alert('No Driver Found !');
					window.location.href='./truck_voucher.php';
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$row_d_name = mysqli_fetch_array($sel_d_name);
	
			echo "<script>
				alert('No Running trip found from Truck No $tno_db and Driver $row_d_name[name] or Hisab Already Done !');
				$('#loadicon').hide();
			</script>";
			exit();
		}
	
		if($truck_no!=$tno_db)
		{
			echo "<script>
				alert('Truck Number mis match !');
				window.location.href='truck_voucher.php';
			</script>";
			exit();
		}
	}
	
	if($company=='RRPL')
	{
		$debit="debit";	
		$balance="balance";	
	}
	else
	{
		$debit="debit2";
		$balance="balance2";
	}
	
	if($pay_mode=='NEFT')
	{
		$fetch_rtgs_req = mysqli_query($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
		if(mysqli_num_rows($fetch_rtgs_req)>0)
		{
			echo "<script>
				alert('Rtgs Payment Done. You Can\'t Edit !');
			</script>";
			exit();
		}
		else
		{
			$delete_rtgs = mysqli_query($conn,"DELETE FROM rtgs_fm WHERE fno='$frno' AND colset_d!='1'");
			if(!$delete_rtgs)
			{
				echo mysqli_error($conn);
				exit();
			}
			
			if(mysqli_affected_rows($conn)==0)
			{
				echo "<script>
					alert('Unable to delete RTGS Payment Contact System-ADMIN !');
				</script>";
				exit();
			}
			
			$delete_passbook = mysqli_query($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
			if(!$delete_passbook)
			{
				echo mysqli_error($conn);
				exit();
			}
		}
		
		if($sys_date==$today)
		{
			$update11=mysqli_query($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$old_amount' WHERE branch='$branch' AND date='$today'");
		}	
	}
	else if($pay_mode=='CHEQUE')
	{
		$delete_passbook = mysqli_query($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
		if(!$delete_passbook)
		{
			echo mysqli_error($conn);
			exit();
		}
		
		$delete_chequebook = mysqli_query($conn,"DELETE FROM cheque_book WHERE vou_no='$frno'");
		if(!$delete_chequebook)
		{
			echo mysqli_error($conn);
			exit();
		}
		
	}
	else
	{
		$update_main_balance = mysqli_query($conn,"UPDATE user SET `$balance`=`$balance`+'$old_amount' WHERE username='$branch'");
		if(!$update_main_balance)
		{
			echo mysqli_error($conn);
			exit();
		}
		
			$fetch_cash_id = mysqli_query($conn,"SELECT id FROM cashbook WHERE vou_no='$frno'");
			if(!$fetch_cash_id)
			{
				echo mysqli_error($conn);
				exit();
			}
			
			$row_cash_id = mysqli_fetch_array($fetch_cash_id);
			
			$cash_id = $row_cash_id['id'];
			
			$update_cashbook_amount = mysqli_query($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$old_amount' WHERE id>'$cash_id' AND 
			user='$branch' AND comp='$company'");
			if(!$update_cashbook_amount)
			{
				echo mysqli_error($conn);
				exit();
			}
			
		$delete_cashbook = mysqli_query($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
		if(!$delete_cashbook)
		{
			echo mysqli_error($conn);
			exit();
		}
	}
	
		if($sys_date==$today)
		{
			$update11=mysqli_query($conn,"UPDATE today_data SET truck_vou=truck_vou-1,truck_vou_amount=truck_vou_amount-'$old_amount' WHERE 
			branch='$branch' AND date='$today'");
		}
		
		if($vou_type=='DIARY')
		{
			$delete_diary_vou = mysqli_query($conn2,"DELETE FROM `$type11` WHERE trans_id='$trans_id_db' AND vou_no='$frno'");
			
			if(!$delete_diary_vou)
			{
				echo mysqli_error($conn2);
				exit();
			}
			
			if(mysqli_affected_rows($conn2)==0)
			{
				$error_insert=mysqli_query($conn,"INSERT INTO error_log(vou_no,desct,branch,timestamp) VALUES 
					('$frno','UNABLE TO DELETE EDIARY $type11 VOUCHER','ADMIN','$timestamp')");
			}
			
			$sel_id_driver_book = mysqli_query($conn2,"SELECT id FROM driver_book WHERE trans_id='$trans_id_db'");
				if(!$sel_id_driver_book)
				{
					echo mysqli_error($conn2);
					exit();
				}	
				
				if(mysqli_num_rows($sel_id_driver_book)==0)
				{
					echo "<script>
						alert('Unable to Fetch Driver Book Data !');
					</script>";
				
					$error_insert=mysqli_query($conn,"INSERT INTO error_log(vou_no,desct,branch,timestamp) VALUES 
					('$frno','UNABLE TO FIND VOUCHER ENTRY IN DRIVER BOOK Trans Id : $trans_id_db Truck No : $truck_no','ADMIN',
					'$timestamp')");
				}
				
				$row_driver_book_id=mysqli_fetch_array($sel_id_driver_book);
				
				$d_book_id=$row_driver_book_id['id'];
				$min_id_d_book=$row_driver_book_id['id']-1;
				
				$delete_entry_driver_book = mysqli_query($conn2,"DELETE FROM driver_book WHERE id='$d_book_id' AND trans_id='$trans_id_db'");
				if(!$delete_entry_driver_book)
				{
					echo mysqli_error($conn2);
					exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					$error_insert=mysqli_query($conn,"INSERT INTO error_log(vou_no,desct,branch,timestamp) VALUES 
						('$frno','UNABLE TO DELETE VOUCHER FROM DRIVER BOOK Trans Id : $trans_id_db Truck No : $truck_no','ADMIN','$timestamp')");
				}
				
				$update_driver_book_all = mysqli_query($conn2,"UPDATE driver_book SET balance=balance-'$old_amount' WHERE 
				id>'$min_id_d_book' AND driver_code='$driver_code_db' AND tno='$truck_no'");
				if(!$update_driver_book_all)
				{
					echo mysqli_error($conn2);
					$error_insert=mysqli_query($conn,"INSERT INTO error_log(vou_no,desct,branch,timestamp) VALUES 
						('$frno','UNABLE TO UPDATE New All Row Balance in Driver Book Trans Id : $trans_id_db Truck No : $truck_no','ADMIN','$timestamp')");
					exit();
				}
				
				$update_driver_balance = mysqli_query($conn2,"UPDATE driver_up SET amount_hold=amount_hold+'$old_amount' WHERE 
				code='$driver_code_db' AND tno='$truck_no' AND down=0 ORDER BY id DESC LIMIT 1");
				if(!$update_driver_balance)
				{
					echo mysqli_error($conn2);
					exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					$error_insert=mysqli_query($conn,"INSERT INTO error_log(vou_no,desct,branch,timestamp) VALUES 
						('$frno','VOU DELETE : UNABLE TO UPDATE NEW BALANCE AMOUNT : $old_amount Driver Code : $driver_code_db Trans Id : $trans_id_db Truck No : $truck_no','ADMIN','$timestamp')");
				}
				
				$update_trip_amount = mysqli_query($conn2,"UPDATE trip SET `$type11`=`$type11`-'$old_amount' WHERE id='$trip_id_db'");
				if(!$update_trip_amount)
				{
					echo mysqli_error($conn2);
					exit();
				}
		}
		
	
$delete_vou=mysqli_query($conn,"DELETE FROM mk_tdv WHERE tdvid='$frno'");	

if(!$delete_vou)
{
	echo mysqli_error($conn);
	exit();
}
		
		echo "<script>
			alert('Voucher Deleted Successfully.');
			window.location.href='./truck_voucher.php';
		</script>";
		exit();
?>