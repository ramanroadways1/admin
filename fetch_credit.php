<?php
require_once("./connect.php");

$SearchBy=escapeString($conn,$_POST['SearchBy']);
$branch=escapeString($conn,$_POST['branch']);

	if($_POST['range']=='FULL')
	{
		$from_date="2017-01-01";
		$to_date=date("Y-m-d");
	}
	else
	{
		$from_date=date('Y-m-d', strtotime($_POST['range'], strtotime(date("Y-m-d"))));
		$to_date=date("Y-m-d");
	}

if($SearchBy=='BANK_WDL')
{
	$qry=Qry($conn,"SELECT id,user as branch,date,comp as company,desct as narration,IF(comp='RRPL',credit,credit2) as amount FROM cashbook WHERE 
	date BETWEEN '$from_date' AND '$to_date' AND user='$branch' AND vou_type='CREDIT ADD BALANCE'");
}	
else
{
	$qry=Qry($conn,"SELECT id,credit_by,tno,bilty_no,branch,company,amount,narr as narration,date FROM credit WHERE date 
	BETWEEN '$from_date' AND '$to_date' AND branch='$branch' AND section='$SearchBy'");
}

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)>0)
{
	?>
	<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:13px;" colspan="16">Credit Summary : Branch <?php echo $branch; ?></th>
    </tr>
		<tr>    
			<th>Cash/Chq</th>
			<?php if($SearchBy=='FREIGHT')
			{
			?>	
			<th>Truck No</th>
			<th>Bilty No</th>
			<?php
			}
			?>
			<th>Company</th>
			<th>Amount</th>
			<th>Date</th>
			<th style="width:30%">Narration</th>
			<th></th>
			<th></th>
		</tr>	
	<?php
	while($row=fetchArray($qry))
	{	
		echo "<tr>";
		
			if($SearchBy=='FREIGHT')
			{
				echo "<td>$row[credit_by]</td>
				<td>$row[tno]</td>
				<td>$row[bilty_no]</td>";
			}
			else if($SearchBy=='BANK_WDL')
			{
				echo "<td>CASH</td>";
			}
			else
			{
				echo "<td>$row[credit_by]</td>";
			}
			
				echo "
				<td>$row[company]</td>
				<td>$row[amount]</td>
				<td>".date('d-m-y',strtotime($row['date']))."</td>
				<td>$row[narration]</td>
				<td>
					<button type='button' onclick=Edit('$row[id]','$SearchBy') class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
				</td>
				<td>
					<button type='button' onclick=Delete('$row[id]','$SearchBy') class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button>
				</td>
			</tr>
			";
	}
	echo "</table>";
	
	echo "<script>
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<font color='red'>No record Found !!</font>";
	echo "<script>
		$('#loadicon').hide();
	</script>";
}
?>