<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$status = escapeString($conn,$_POST['status']);

if($id=='')
{
	echo "<script>
		alert('Lane not found !!');
		$('#add_exp_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($status!='1' AND $status!='0')
{
	echo "<script>
		alert('Invalid status !!');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT is_active FROM dairy.fix_lane WHERE id='$id'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_record)==0)
{
	echo "<script>
		alert('Lane not found !!');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_lane = fetchArray($chk_record);

if($row_lane['is_active']=="0")
{
	$new_status="1";
}
else
{
	$new_status="0";
}

if($status!=$new_status)
{
	echo "<script>
		alert('Nothing to update !!');
		$('#loadicon').hide();
	</script>";
	exit();
}
 
$update_status = Qry($conn,"UPDATE dairy.fix_lane SET is_active='$new_status' WHERE id='$id'");

if(!$update_status){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($status=='1')
{
	echo "<script>
		alert('OK : Activated !!');
		$('#active_btn_$id').attr('disabled',true);
		$('#disable_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<script>
		alert('OK : Disabled !!');
		$('#active_btn_$id').attr('disabled',false);
		$('#disable_btn_$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
}
exit();
?>