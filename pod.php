<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">POD Copy View</h3>
              </div>
              
	<div class="card-body">
				
		<script>		
		function FetchRecord()
		{
			var pod_type = $('#pod_type').val();
			var vou_no = $('#vou_no').val();
			
			if(pod_type=='' || vou_no=='')
			{
				alert('select pod type and enter voucher number !');
			}				
			else
			{
					$("#loadicon").show();
					jQuery.ajax({
					url: "fetch_pod_copy.php",
					data: 'vou_no=' + vou_no + '&pod_type=' + pod_type,
					type: "POST",
					success: function(data) {
						$("#main_div").html(data);
					},
						error: function() {}
					});
			}
		}
		</script>	

	<div class="row">	
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>POD Type <font color="red"><sup>*</sup></font></label>
                  <select name="pod_type" id="pod_type" required class="form-control">
					<option value="MARKET">MARKET TRUCK</option>
					<option value="OWN">OWN TRUCK</option>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Voucher Number <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" id="vou_no" name="vou_no">
              </div>
			</div>
			
			<div id="button_div" class="col-md-2">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="button_chk" onclick="FetchRecord()" class="btn pull-right btn-danger">Fetch Record !</button>
			</div>
			
		</div>
		
		<div class="row" id="main_div">	
		</div>
	</div>
	
	 </div>
</div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php include ("./footer.php"); ?>
</body>
</html>

<div id="result_func"></div>

<script>
function RemovePod(frno,lrno,type,pod_id)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "remove_pod_copy.php",
	data: 'frno=' + frno + '&lrno=' + lrno + '&type=' + type + '&pod_id=' + pod_id,
	type: "POST",
	success: function(data) {
		$("#result_func").html(data);
	},
	error: function() {}
	});
}

function ReUpload(frno)
{
	alert('Temporary disabled !');
}
</script>