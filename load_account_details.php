<?php
require_once("./connect.php");

$value1=escapeString($conn,$_POST['SearchBy']);

if($value1=='OWNER')
{
	$id = escapeString($conn,$_POST['owner_id']);
	
	$qry=Qry($conn,"SELECT name,pan,acname,accno,bank,ifsc FROM mk_truck WHERE id='$id'");
}
else if($value1=='BROKER')
{
	$id = escapeString($conn,$_POST['broker_id']);
	
	$qry=Qry($conn,"SELECT name,pan,acname,accno,bank,ifsc FROM mk_broker WHERE id='$id'");
}
else if($value1=='DRIVER')
{
	$id = escapeString($conn,$_POST['driver_id']);
	
	$qry=Qry($conn,"SELECT d.name,'NULL' as pan,a.acname,a.acno as accno,a.bank,a.ifsc FROM dairy.driver as d 
	LEFT OUTER JOIN dairy.driver_ac as a ON a.code=d.code 
	WHERE d.code='$id'");
}
else
{
	echo "<font color='red'>Invalid request !!</font>";
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

$user_code=escapeString($conn,$_POST['user_code']);
$branch_name=escapeString($conn,$_POST['branch_name']);

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<font color='red'>No record Found !!</font>";
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

$row = fetchArray($qry);
?>
	<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:13px;" colspan="16">Ac details <?php echo $value1.": $row[name]"; ?>
	   <button class="btn btn-sm pull-right btn-warning" onclick="ResetData()" type="button">Reset</button>
	   </th>
    </tr>
		<tr>    
			<th>Ac type</th>
			<th>Party name</th>
			<th>Ac_holder</th>
			<th>Ac_no.</th>
			<th>Bank</th>
			<th>IFSC</th>
			<th>PAN</th>
			<th></th>
		</tr>	
	
	<tr>  
		<td><?php echo $value1; ?></td>
		<td><?php echo $row['name']; ?></td>
		<td><?php echo $row['acname']; ?></td>
		<td><?php echo $row['accno']; ?></td>
		<td><?php echo $row['bank']; ?></td>
		<td><?php echo $row['ifsc']; ?></td>
		<td><?php echo $row['pan']; ?></td>
		<td>
			<button type='button' onclick=Edit("<?php echo $value1; ?>","<?php echo $id; ?>") 
			class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
		</td>
	</tr>
		
	</table>
	
	<script>
		$('#party_name1').html('<?php echo $row["name"]; ?>');
		$('#modal_party_name').val('<?php echo $row["name"]; ?>');
		$('#ac_holder_main').val('<?php echo $row["acname"]; ?>');
		$('#ac_no_main').val('<?php echo $row["accno"]; ?>');
		$('#bank_name_main').val('<?php echo $row["bank"]; ?>');
		$('#ifsc_main').val('<?php echo $row["ifsc"]; ?>');
	</script>
	
	<script>
		$('#loadicon').hide();
		$('#row_main1').hide();
		$('#branch_name2').val('<?php echo $branch_name; ?>');
		$('#user_code2').val('<?php echo $user_code; ?>');
	</script>
	
	 <script>
 function ResetData()
 {
	 $('#result_main').html('');
	 $('#AccountForm')[0].reset();
	 $('#row_main1').show();
 }
 </script>
