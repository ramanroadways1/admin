<?php
require_once("./connect.php");

$rtgs_id = escapeString($conn,($_POST['id']));
$vou_no = escapeString($conn,($_POST['vou_no']));
$timestamp = date("Y-m-d H:i:s");

if(empty($rtgs_id) || $rtgs_id==0)
{
	echo "<script>alert('RTGS not found..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$chk_rtgs = Qry($conn,"SELECT fno,branch,colset_d,amount FROM rtgs_fm WHERE id='$rtgs_id'");

if(!$chk_rtgs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($chk_rtgs)==0){
	echo "<script>alert('RTGS Voucher not found..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_rtgs = fetchArray($chk_rtgs);

$rtgs_amount = $row_rtgs['amount'];
$adv_branch = $row_rtgs['branch'];

if($row_rtgs['colset_d']!='')
{
	echo "<script>alert('RTGS Voucher downloaded by accounts dpt..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_rtgs['fno']!=$vou_no)
{
	echo "<script>alert('Voucher number not verified..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_fm_data = Qry($conn,"SELECT id,adv_date,cashadv,chqadv,disadv FROM freight_form WHERE frno='$vou_no'");

if(!$get_fm_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_fm_data)==0){
	echo "<script>alert('Voucher not found..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_fm = fetchArray($get_fm_data);

$vou_id = $row_fm['id'];

$other_advs = $row_fm['cashadv']+$row_fm['chqadv']+$row_fm['disadv'];

if($other_advs<=0)
{
	echo "<script>alert('Error: Cash or diesel or cheque advance not found..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if($row_fm['adv_date']==date("Y-m-d"))
{
	$update_today_data = Qry($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$rtgs_amount' WHERE branch='$adv_branch'");
			
	if(!$update_today_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_fm = Qry($conn,"UPDATE freight_form SET totaladv=ROUND(totaladv-'$rtgs_amount',2),rtgsneftamt=0,
baladv=ROUND(baladv+'$rtgs_amount',2) WHERE id='$vou_id' AND paidto=''");

if(!$update_fm){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__); 
}	

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("RTGS not updated. FmId: $vou_id. Rtgs_amt: $rtgs_amount.",$conn,$page_name,__LINE__);
}

$dlt_rtgs = Qry($conn,"DELETE FROM rtgs_fm WHERE id='$rtgs_id' AND colset_d=''");
	
if(!$dlt_rtgs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to delete rtgs entry. id: $rtgs_id. Vou_no: $vou_no.",$conn,$page_name,__LINE__);
}

$dlt_password_entry = Qry($conn,"DELETE FROM passbook WHERE vou_no='$vou_no' AND desct='Advance RTGS/NEFT'");
	
if(!$dlt_password_entry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('RTGS entry successfully deleted.');
		$('#loadicon').fadeOut('slow');
		$('#get_button').attr('disabled',false);
		FetchFm('$vou_no');
		$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',true);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error: something went wrong..');$('#btn_rtgs_adv_del_$rtgs_id').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>