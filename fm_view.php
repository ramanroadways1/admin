<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<!--<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">-->
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Freight Memo : <span id="fm_no_head"></span></h3>
              </div>
              
	<div class="card-body">
				
	
<script>		
function FetchFm(frno)
{
	var search_by = $('#search_by').val();
	
	if(search_by=='')
	{
		alert('select search_by option first !');
	}
	else
	{
		var frno = frno.toUpperCase();
				
		if(frno=='')
		{
			alert('Enter Voucher number first !');
			$("#all_button_div").hide();
			$("#search_button_div").show();
		}
		else
		{
			$("#loadicon").show();
			$("#frno").attr('disabled',true);
			$("#lrno_find").attr('readonly',true);
			$("#search_by").attr('disabled',true);
			jQuery.ajax({
			url: "fetch_freight_memo.php",
			data: 'frno=' + frno,
			type: "POST",
			success: function(data) {
				$("#load_lr_result").html(data);
			},
				error: function() {}
			});
		}	
	}	
}
</script>
	<div class="row" id="div_lr_input">	
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Search By <font color="red"><sup>*</sup></font></label>
                  <select name="search_by" id="search_by" class="form-control" required onchange="SearchBy(this.value)">
					<option value="">--select option--</option>
					<option value="LR">LR Number</option>
					<option value="FM">FM Number</option>
				  </select>
              </div>
			</div>
			
<script>
function SearchBy(elem)
{
	if(elem=='LR')
	{
		$('#lr_no_div').show();
		$('#fm_no_div').hide();
		$('#get_button').attr('disabled',true);
	}
	else if(elem=='FM')
	{
		$('#lr_no_div').hide();
		$('#fm_no_div').show();
		$('#get_button').attr('disabled',false);
	}
	else
	{
		$('#lr_no_div').hide();
		$('#fm_no_div').hide();
		$('#get_button').attr('disabled',true);
	}
}

function SearchLRNumber(lrno)
{
	if(lrno=='')
	{
		alert('enter LR number first !');
		$('#get_button').attr('disabled',true);
		$('#fm_no_div').hide();
		$('#lr_no_div').show();
	}
	else
	{
		$("#lrno_find").attr('readonly',true);
		$("#get_button").attr('readonly',true);
		$("#search_by").attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
		url: "fm_view_find_lrno.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#fm_no_div").html(data);
		},
			error: function() {}
		});
	}
}
</script>
			
	<div class="col-md-3" style="display:none" id="lr_no_div">	
        <div class="form-group">
           <label>LR Number <font color="red"><sup>*</sup></font></label>
           <input onblur="SearchLRNumber(this.value)" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" id="lrno_find">
        </div>
	</div>
			
	<div class="col-md-3" id="fm_no_div" style="display:none">	
        <div class="form-group">
            <label>Freight_Memo No <font color="red"><sup>*</sup></font></label>
            <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" id="frno" name="frno">
        </div>
	</div>
			
	<div id="search_button_div" class="col-md-2">
		<label>&nbsp;</label>
		<br />
		<button type="button" id="get_button" disabled onclick="FetchFm($('#frno').val())" class="btn pull-right btn-danger">Get details !</button>
	</div>
		
	<!--		
	<div class="col-md-3" id="main_div3" style="display:none">
		<div class="form-group">
			<label><input type="checkbox" class="minimal">Unlock Reset Buttons</label>
		 </div>
	</div>
	-->
			
			<script type="text/javascript">
				// $(document).ready(function(){
					// $('input[type="checkbox"]').click(function(){
						// if($(this).prop("checked") == true){
							// $('#adv_reset_id').attr('disabled',false);
							// $('#bal_reset_id').attr('disabled',false);
						// }
						// else if($(this).prop("checked") == false){
							// $('#adv_reset_id').attr('disabled',true);
							// $('#bal_reset_id').attr('disabled',true);
						// }
					// });
				// });
			</script>
</div>

<div class="row" id="all_button_div" style="display:none">			
	<div class="col-md-12">	
		<div class="form-group">
			<button type="button" id="adv_reset_id" onclick="ResetAdv();" class="btn btn-sm btn-danger">Reset Advance</button>
			<button type="button" onclick="ChangeBroker();" class="btn btn-sm btn-primary">Change Broker</button>
			<button type="button" onclick="ChangeOwner();" class="btn btn-sm btn-warning">Change Owner</button>
			<button type="button" onclick="ChangeDriver();" class="btn btn-sm btn-info">Change Driver</button>
			<button type="button" onclick="ChangeFreight();" class="btn btn-sm btn-success">Change Rate/Freight</button>
			<button type="button" onclick="ChangeMasterLane();" class="btn btn-sm btn-primary">Change Master Lane</button>
			<button type="button" id="bal_reset_id" onclick="ResetBal();" class="btn btn-sm btn-danger">Reset Balance</button>
			<button type="button" id="delete_fm_button" onclick="DeleteFM();" class="btn btn-sm pull-right btn-danger">Delete Freight Memo</button>
		</div>	   
	</div>
</div>
		
<div class="row" id="main_div" style="display:none">	
	<div class="col-md-12 table-responsive" style="overflow:auto">	
		
<div id="load_lr_result"></div>
			
<table class="table table-bordered" style="font-size:12.5px;">
                  
	<tr>
        <th class="bg-gray" style="font-size:13px;color:#FFF" colspan="8">Owner / Broker / Driver :</th>
     </tr>
				 
	<tr>
        <th>Truck No</th> <td id="tno_fm"></td>
        <th>Company</th> <td id="company_fm"></td>
        <th>Branch/User</th> <td id="branch_fm"></td>
    </tr>
	
	<tr>
        <th>Owner</th> <td id="owner_fm"></td>
        <th>PAN</th> <td id="owner_pan_fm"></td>
        <th>Weight</th> <td id="weight_fm"></td>
    </tr>
	
	<tr>
        <th>Broker</th> <td id="broker_fm"></td>
        <th>PAN</th> <td id="broker_pan_fm"></td>
        <th>From</th> <td id="from_fm"></td>
    </tr>
	
	<tr>
        <th>Driver</th> <td id="driver_fm"></td>
        <th>Lic No</th> <td id="driver_lic_fm"></td>
		<th>To</th> <td id="to_fm"></td>
    </tr>
	
</table>
				
<table class="table table-bordered" id="adv_div" style="display:none;font-size:12.5px;">
                  
	<tr>
       <th class="bg-warning" style="font-size:14px;" colspan="8">ADVANCE Details : &nbsp; 
	   <button style="" type="button" id="add_adv_dsl_modal_btn" class="pull-right btn btn-sm btn-default" onclick="AddDieselToFm('ADV')">Add Adv Diesel</button></th>
    </tr>
				 
	<tr>
       <th>Act_Freight</th>
       <th>Loading(+)</th>
       <th>Dsl_Inc(+)</th>
       <th>GPS(-)</th>
       <th>Claim(-)</th>
       <th>Other(-)</th>
       <th>TDS(-)</th>
       <th>Total_Freight</th>
    </tr>
	
	<tr>
       <td id="actual_freight_fm"></td>
       <td id="loading_fm"></td>
       <td id="dsl_inc_fm"></td>
       <td id="gps_fm"></td>
       <td id="claim_fm"></td>
       <td id="other_fm"></td>
       <td id="tds_fm"></td>
       <td id="total_freight_fm"></td>
    </tr>
	
	<tr>
       <th>Cash Adv</th>
       <th>Cheque Adv</th>
       <th colspan="2">Cheque No.</th>
       <th>Diesel Adv</th>
       <th>Rtgs/Neft Adv</th>
	   <th colspan="2">Branch/User</th>
    </tr>
	
	<tr>
       <td id="cash_adv_fm"></td>
       <td id="chq_adv_fm"></td>
       <td colspan="2" id="chq_no_fm"></td>
       <td id="diesel_adv_fm"></td>
       <td id="rtgs_fm"></td>
       <td colspan="2" id="adv_user_fm"></td>
	</tr>
	
	<tr>
       <th>Total_Advance</th>
       <th>Balance</th>
       <th>Advance To</th>
       <th>Advance Date</th>
       <th colspan="4">Narration</th>
    </tr>
	
	<tr>
       <td id="total_adv_fm"></td>
       <td id="balance_left_fm"></td>
       <td id="adv_to_fm"></td>
       <td id="adv_date_fm"></td>
       <td colspan="4" id="adv_narration_fm"></td>
    </tr>
	
</table>
	
<script>	
function AdvDieselLoad(frno)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "_load_diesel.php",
	data: 'frno=' + frno + '&type=' + 'ADVANCE',
	type: "POST",
	success: function(data) {
		$("#adv_diesel_result").html(data);
	},
		error: function() {}
	});
}

function AdvRtgsLoad(frno,freight,adv,rtgs_amt,balance)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "_load_rtgs.php",
	data: 'frno=' + frno + '&type=' + 'ADVANCE' + '&freight=' + freight + '&adv=' + adv + '&rtgs_amt=' + rtgs_amt + '&balance=' + balance,
	type: "POST",
	success: function(data) {
		$("#adv_rtgs_result").html(data);
	},
		error: function() {}
	});
}

function BalDieselLoad(frno)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "_load_diesel.php",
	data: 'frno=' + frno + '&type=' + 'BALANCE',
	type: "POST",
	success: function(data) {
		$("#bal_diesel_result").html(data);
	},
		error: function() {}
	});
}

function BalRtgsLoad(frno)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "_load_rtgs.php",
	data: 'frno=' + frno + '&type=' + 'BALANCE',
	type: "POST",
	success: function(data) {
		$("#bal_rtgs_result").html(data);
	},
		error: function() {}
	});
}
</script>	

	<div id="adv_diesel_result"></div>
	<div id="adv_rtgs_result"></div>

<table class="table table-bordered" id="bal_div" style="display:none;font-size:12.5px;">
				 
	<tr>
        <th class="bg-success" style="color:#FFF;font-size:14px;" colspan="10">BALANCE Details : </th>
	</tr>	
				 
	<tr>
		 <th>Balance_Amt</th>
		 <th>Unloading(+)</th>
		 <th>Detention(+)</th>
		 <th>GPS_Rent(-)</th>
		 <th>Gps_Device(-)</th>
		 <th>TDS(-)</th>
		 <th>Other(-)</th>
		 <th>Claim(-)</th>
		 <th>Late_POD(-)</th>
	 </tr>
	 
	<tr>
		<td id="balance_fm"></td>
		<td id="unloading_fm"></td>
		<td id="detention_fm"></td>
		<td id="gps_rent_fm"></td>
		<td id="gps_device_fm"></td>
		<td id="tds_bal_fm"></td>
		<td id="other_bal_fm"></td>
		<td id="claim_bal_fm"></td>
		<td id="late_pod_fm"></td>
	</tr>
	
	<tr>
       <th>Cash</th>
       <th>Cheque</th>
       <th colspan="2">Cheque No.</th>
       <th>Diesel</th>
       <th>Rtgs/Neft</th>
	   <th colspan="3">Branch/User</th>
    </tr>
	
	<tr>
       <td id="cash_bal_fm"></td>
       <td id="chq_bal_fm"></td>
       <td colspan="2" id="chq_no_bal_fm"></td>
       <td id="diesel_bal_fm"></td>
       <td id="rtgs_bal_fm"></td>
       <td colspan="3" id="bal_user_fm"></td>
	</tr>
	
	<tr>
       <th>Total_Balance</th>
       <th>Balance To</th>
       <th>Balance Date</th>
       <th colspan="6">Narration</th>
    </tr>
	
	<tr>
       <td id="total_bal_fm"></td>
       <td id="bal_to_fm"></td>
       <td id="bal_date_fm"></td>
       <td colspan="6" id="bal_narration_fm"></td>
    </tr>
    
</table>
			   
	<div id="bal_diesel_result"></div>
	<div id="bal_rtgs_result"></div>
		
		</div>
	</div>
</div>
	
	<div class="card-footer">
		<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
		<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
      </div>
				
           </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script> 
function RtgsAmtEdit(id)
{
	var vou_id = $('#vou_id_block').val();
	$('#rtgs_amt_modal_vou_id').val(vou_id);
	$('#ModalRtgsAdvUpdate').modal();
}

function RtgsAdvDelete(id,vou_no)
{
	var result = confirm("Do you really want to delete rtgs advance ?");
	
	if(result){
		$('#btn_rtgs_adv_del_'+id).attr('disabled',true);
	
		$("#loadicon").show();
		jQuery.ajax({
		url: "fm_delete_adv_rtgs.php",
		data: 'id=' + id + '&vou_no=' + vou_no,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
			error: function() {}
		});
	}
}

function AddDieselToFm(adv_bal)
{
	var vou_id = $('#vou_id_block').val();
	var vou_no = $('#vou_number_block').val();
	$('#dsl_modal_vou_no_html').html(vou_no); 
	$('#dsl_modal_vou_no_value').val(vou_no);
	$('#dsl_modal_vou_id_value').val(vou_id);
	$('#DieselAdvAddModal').modal();
}

function AddDiesel()
{
	var frno = $('#frno').val();
	var freight = $('#total_freight_text').val();
	var adv_to = $('#adv_to_text').val();
	var total_adv = $('#adv_amount_text').val();
	var diesel_adv = $('#diesel_adv_text').val();
	var balance = $('#balance_text').val();
	var bal_to = $('#balance_to_text').val();
	
			$('#frno_diesel').val('');
			$('#freight_diesel').val('');
			$('#adv_to_diesel').val('');
			$('#total_adv_diesel').val('');
			$('#diesel_adv_diesel').val('');
			$('#balance_diesel').val('');
			$('#balance_to_diesel').val('');
	
	if(frno=='')
	{
		alert('Unable to fetch LR data !');
	}
	else
	{
		if(adv_to=='')
		{
			alert('Freight Memo Advance Pending !');
		}
		else
		{	
			if(diesel_adv==0)
			{
				alert('No diesel found please reset Advance !');
			}
			else
			{
				$('#frno_diesel').val(frno);
				$('#freight_diesel').val(freight);
				$('#adv_to_diesel').val(adv_to);
				$('#total_adv_diesel').val(total_adv);
				$('#diesel_adv_diesel').val(diesel_adv);
				$('#balance_diesel').val(balance);
				$('#balance_to_diesel').val(bal_to);
				
				document.getElementById("modal_link").click();
			}	
		}	
	}
}
</script> 
    
<script> 
function ChangeMasterLane()
{
	var vou_id = $('#vou_id_block').val();
	
	if(vou_id=='')
	{
		alert('Unable to fetch Voucher Number !');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "fetch_maste_lane.php",
		data: 'vou_id=' + vou_id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
			error: function() {}
		});
	}	
}

function ChangeBroker()
{
	var vou_no = $('#vou_number_block').val();
	var vou_id = $('#vou_id_block').val();
	
	if(vou_no=='')
	{
		alert('Unable to fetch Voucher Number !');
	}
	else
	{
		$('#broker_vou_no').val(vou_no);
		$('#broker_vou_id').val(vou_id);
		$('#BrokerModal').modal();
	}	
}

function ChangeDriver()
{
	var vou_no = $('#vou_number_block').val();
	var vou_id = $('#vou_id_block').val();
	
	if(vou_no=='')
	{
		alert('Unable to fetch Voucher Number !');
	}
	else
	{
		$('#driver_vou_no').val(vou_no);
		$('#driver_vou_id').val(vou_id);
		$('#DriverModal').modal();
	}	
}

function ChangeOwner()
{
	alert('not active !!');
	// var vou_no = $('#vou_number_block').val();
	// var vou_id = $('#vou_id_block').val();
	
	// if(vou_no=='')
	// {
		// alert('Unable to fetch Voucher Number !');
	// }
	// else
	// {
		// $('#owner_vou_no').val(vou_no);
		// $('#owner_vou_id').val(vou_id);
		// $('#OwnerModal').modal();
	// }	
}

function ChangeFreight()
{
	var vou_no = $('#vou_number_block').val();
	var vou_id = $('#vou_id_block').val();
	
	if(vou_no=='')
	{
		alert('Unable to fetch Voucher Number !');
	}
	else
	{
		$('#rate_freight_vou_no').val(vou_no);
		$('#rate_freight_vou_id').val(vou_id);
		$('#FreightModal').modal();
	}	
}

function ResetAdv()
{
	var vou_no = $('#vou_number_block').val();
	var vou_id = $('#vou_id_block').val();
	
	if(vou_no=='')
	{
		alert('Unable to fetch Voucher Number !');
	}
	else
	{
		var result = confirm("Do you really want to reset advance ?");
		if(result){
			$("#loadicon").show();
			$("#adv_reset_id").attr("disabled",true);
			jQuery.ajax({
			url: "reset_advance.php",
			data: 'frno=' + vou_no + '&id=' + vou_id,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
				error: function() {}
			});
		}
	}
}	

function ResetBal()
{
	var vou_no = $('#vou_number_block').val();
	var vou_id = $('#vou_id_block').val();
	
	if(vou_no=='')
	{
		alert('Unable to fetch Voucher Number !');
	}
	else
	{
		var result = confirm("Do you really want to reset balance ?");
		if(result){
			$("#loadicon").show();
			$("#bal_reset_id").attr("disabled",true);
			jQuery.ajax({
			url: "reset_balance.php",
			data: 'frno=' + vou_no + '&id=' + vou_id,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
				error: function() {}
			});
		}
	}
}

function DeleteFM()
{
	var vou_no = $('#vou_number_block').val();
	var vou_id = $('#vou_id_block').val();
	
	if(vou_no=='')
	{
		alert('Unable to fetch Voucher Number !');
	}
	else
	{
		var result = confirm("Do you really want to delete this freight memo ?");
		if(result){
			$("#delete_fm_button").attr('disabled',true);
				jQuery.ajax({
				url: "delete_freight_memo.php",
				data: 'frno=' + vou_no + '&id=' + vou_id,
				type: "POST",
				success: function(data) {
					$("#func_result").html(data);
				},
					error: function() {}
			});
		}	
	}	
}
</script> 

</div>
 
<input type="hidden" id="vou_number_block" name="vou_no"> 
<input type="hidden" id="vou_id_block" name="fm_id"> 
 
<?php include ("./_modal_edit_lr.php"); ?>
<?php include ("./_modal_master_lane.php"); ?>
<?php include ("./_modal_change_broker.php"); ?>
<?php include ("./_modal_change_driver.php"); ?>
<?php include ("./_modal_change_owner.php"); ?>
<?php include ("./_modal_change_rate_freight.php"); ?>

<?php include ("./footer.php"); ?>
</body>
</html>