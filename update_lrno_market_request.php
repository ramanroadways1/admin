<?php
require_once('./connect.php');
	
$frno=escapeString($conn,strtoupper($_POST['frno']));
$lrno=escapeString($conn,strtoupper($_POST['lrno']));

$timestamp = date("Y-m-d H:i:s");

if($frno=='' || $lrno=='')
{
	echo "<script>
		alert('Missing : LR number or token number.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

$fetch_data = Qry($conn,"SELECT GROUP_CONCAT(QUOTE(id) SEPARATOR ',') as diesel_id,SUM(cash) as cash,fno,token_no,branch,branch_user,
com,tno,lrno,type,no_lr,pre_req,pre_and_fm FROM diesel_fm WHERE fno='$frno' GROUP by fno");

if(!$fetch_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_data)==0)
{
	echo "<script>
		alert('diesel not found.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

$row=fetchArray($fetch_data);

$update_log = "Branch and user: $row[branch]($row[branch_user]), LR number updated $row[lrno] to $lrno.";

if($row['cash']>0)
{
	echo "<script>
		alert('Error : cash advance found.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['type']!='ADVANCE')
{
	echo "<script>
		alert('Error : not advance diesel.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['fno']!=$row['token_no'])
{
	echo "<script>
		alert('Error : token number not verified.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['pre_req']=='0')
{
	echo "<script>
		alert('Error : not requested diesel.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['pre_and_fm']=='1')
{
	echo "<script>
			alert('Advance paid. Reset advance first !');
			$('#loadicon').hide();
			$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['no_lr']=='1' AND $row['lrno']!='EMPTY')
{
	echo "<script>
		alert('LR verification failed !');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}
else if($row['no_lr']!='1' AND $row['lrno']=='EMPTY')
{
	echo "<script>
		alert('LR verification failed !');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['lrno']!='EMPTY')
{
	$get_existing_lr = Qry($conn,"SELECT id FROM lr_sample_pending WHERE lrno='$row[lrno]'");
	
	if(!$get_existing_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($get_existing_lr)==0)
	{
		$check_fm_adv = Qry($conn,"SELECT ptob FROM freight_form WHERE frno = (SELECT frno from freight_form_lr WHERE lrno='$row[lrno]')");
	
		if(!$check_fm_adv){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($check_fm_adv)==0)
		{
			errorLog("Freight memo not found. LR number : $row[lrno].",$conn,$page_name,__LINE__);
			echo "<script>
				alert('Freight memo not found !');
				$('#loadicon').hide();
				$('#button_change_lrno').attr('disabled', false);
			</script>";
			exit();
		}
	
		$row_fm_adv = fetchArray($check_fm_adv);
	
		if($row_fm_adv['ptob']!='')
		{
			echo "<script>
				alert('Freight memo advance paid of existing LR. Reset advance first !');
				$('#loadicon').hide();
				$('#button_change_lrno').attr('disabled', false);
			</script>";
			exit();
		}
	}
}
else
{
	$check_for_pending_lr = Qry($conn,"SELECT id FROM diesel_lr_pending WHERE diesel_id IN($row[diesel_id])");

	if(!$check_for_pending_lr){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($check_for_pending_lr)==0)
	{
		echo "<script>
			alert('Diesel not found in pending LR list.');
			$('#loadicon').hide();
			$('#button_change_lrno').attr('disabled', false);
		</script>";
		exit();
	}
}

$check_for_new_lr = Qry($conn,"SELECT id,branch,company,truck_no,crossing FROM lr_sample WHERE lrno='$lrno'");

if(!$check_for_new_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_for_new_lr)==0)
{
	echo "<script>
		alert('invalid lr entered.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}
	
$row_lrno = fetchArray($check_for_new_lr);

if($row_lrno['branch']!=$row['branch'])
{
	echo "<script>
		alert('LR does not belongs to you.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['com']!=$row_lrno['company'])
{
	echo "<script>
		alert('LR company not matching with requested diesel.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['tno']!=$row_lrno['truck_no'])
{
	echo "<script>
		alert('Vehicle number not matching with requested diesel.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row_lrno['crossing']!='')
{
	$check_fm_adv2 = Qry($conn,"SELECT ptob FROM freight_form WHERE frno = (SELECT frno from freight_form_lr WHERE lrno='$lrno')");
	
	if(!$check_fm_adv2){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($check_fm_adv2)==0)
	{
		errorLog("Freight memo not found. LR number : $lrno.",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Freight memo not found !');
			$('#loadicon').hide();
			$('#button_change_lrno').attr('disabled', false);
		</script>";
		exit();
	}
	
	$row_fm_adv2 = fetchArray($check_fm_adv2);
	
	if($row_fm_adv2['ptob']!='')
	{
		echo "<script>
			alert('Freight memo advance paid of LR : $lrno. Reset advance first !');
			$('#loadicon').hide();
			$('#button_change_lrno').attr('disabled', false);
		</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;
	
$update_new_lr = Qry($conn,"UPDATE diesel_fm SET lrno='$lrno',no_lr='0' WHERE fno='$frno' AND fno=token_no");
if(!$update_new_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("LR not updated. lrno : $lrno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}

if($row['no_lr']=="1")
{
	$update_pending_lr = Qry($conn,"DELETE FROM diesel_lr_pending WHERE diesel_id IN($row[diesel_id])");
	if(!$update_pending_lr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Pending LR diesel not deleted. lrno : $lrno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
	}
}

$diesel_id_count = explode(",", $row['diesel_id']);
$diesel_id_count = count($diesel_id_count);

$update_pending_diesel = Qry($conn,"UPDATE _pending_diesel SET lrno='$lrno' WHERE fno='$frno'");
if(!$update_pending_diesel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$updated_rows = AffectedRows($conn);	
	
if($updated_rows==0)
{
	$flag = false;
	errorLog("LR not updated in _pending_diesel. lrno : $lrno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}

if($updated_rows!=$diesel_id_count)
{
	$flag = false;
	errorLog("_pending_diesel update count: $updated_rows not verified with diesel ids count : $diesel_id_count. LR: $lrno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}
	
if($row['no_lr']!="1" || $row['lrno']!='EMPTY')
{	
	$update_olr_lr=Qry($conn,"UPDATE lr_sample SET diesel_req=diesel_req-1 WHERE lrno='$row[lrno]'");
	if(!$update_olr_lr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Diesel entry not updated in old LR. lrno : $lrno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
	}
}

$update_olr_lr_pending_lr_list =Qry($conn,"UPDATE lr_sample_pending SET diesel_req=diesel_req-1 WHERE lrno='$row[lrno]'");
if(!$update_olr_lr_pending_lr_list){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}


$update_new_lr_data=Qry($conn,"UPDATE lr_sample SET diesel_req=diesel_req+1 WHERE lrno='$lrno'");
if(!$update_new_lr_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Diesel entry not updated in new LR. lrno : $lrno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}

$update_new_lr_pending_data=Qry($conn,"UPDATE lr_sample_pending SET diesel_req=diesel_req+1 WHERE lrno='$lrno'");
if(!$update_new_lr_pending_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$frno','MARKET_DIESEL_REQ','EDIT_LR_NO','$update_log','$row[branch]','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('LR Number Updated Successfully !');
		window.location.href='./request_market_truck.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./request_market_truck.php");
	exit();
}	
		
?>