<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#FormParty").on('submit',(function(e) {
$("#loadicon").show();
$("#lr_sub").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./save_add_party_excel.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#lr_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination2(event);" onkeydown = "return disableCtrlKeyCombination2(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Party in Bulk (without GST) : Excel upload</h3>
              </div>
              
<form role="form" id="FormParty" action="" method="POST" autocomplete="off">
			  
     <div class="card-body">

	<div class="row">	
		
		<div class="col-md-4">	
               <div class="form-group">
                  <label>Party type <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" id="type" name="type" required>
					<option value="">Select an option</option>
					<option disabled value="consignor">Consignor</option>
					<option value="consignee">Consignee</option>
				  </select>
              </div>
		</div>
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>Select file <font color="red"><sup>* (.csv) - name,state,pincode</sup></font></label>
                  <input style="font-size:13px;" accept=".csv" type="file" class="form-control" id="file_name" name="file" required>
              </div>
			</div>
			
			
		</div>
		
		<div class="row">
			<div class="col-md-12" id="func2_res"></div>
		</div>
		
		<div class="row">
			<div class="col-md-12" id="lr_result"></div>
		</div>
	</div>
                
       <div class="card-footer">
			<button id="lr_sub" name="lr_sub" type="submit" class="btn btn-primary">Add Party</button>
	   </div>
				
              </form>
            </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
  </div> 
  
<?php include ("./footer.php"); ?>

</body>
</html>

<script>
function LoadData()
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "./fetch_cache_data_party.php",
	data: 'ok=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#lr_result").html(data);
	},
	error: function() {}
	});
}

function Remove(id)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "./remove_party_by_id.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data) {
		$("#func2_res").html(data);
	},
	error: function() {}
	});
}

function SavePartyFinal()
{
	$('#final_add_btn').attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
	url: "./add_party_final.php",
	data: 'id=' + 'id',
	type: "POST",
	success: function(data) {
		$("#func2_res").html(data);
	},
	error: function() {}
	});
}
</script>
  