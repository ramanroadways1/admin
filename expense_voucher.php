<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Expense Voucher Summary :</h3>
              </div>
              
	<div class="card-body">
				
		<script>		
		function SearchBy(select)
		{
			$('#result_main').html('');
			$('#frno').val('');
			$('#exp_head').val('');
			$('#date_range').val('-0 days');
			
			if(select=='FM')
			{
				$('#div_frno').show();
				$('#div_head').hide();
				$('#div_range').hide();
			}
			else if(select=='HEAD')
			{
				$('#div_frno').hide();
				$('#div_head').show();
				$('#div_range').show();
			}
			else
			{
				alert('Invalid option selected !');
				$('#result_main').html('');
				
				$('#div_frno').show();
				$('#div_head').hide();
				$('#div_range').hide();
			}
		}
		
		function Fetch(option)
		{
			if(option=='FM')
			{
				if($('#frno').val()=='')
				{
					alert('Enter Voucher No First !');
					$('#result_main').html('');
					$('#div_frno').show();
					$('#div_head').hide();
					$('#div_range').hide();
				}
				else
				{
					var frno = $('#frno').val();
					
					$("#loadicon").show();
					jQuery.ajax({
					url: "fetch_exp_vou.php",
					data: 'option=' + option + '&frno=' + frno,
					type: "POST",
					success: function(data) {
					$("#result_main").html(data);
					},
						error: function() {}
					});
				}
			}
			else if(option=='HEAD')
			{
				if($('#exp_head').val()=='' || $('#date_range').val()=='')
				{
					alert('Select Expense Head and Date Range First !');
					$('#result_main').html('');
					$('#div_frno').show();
					$('#div_head').hide();
					$('#div_range').hide();
				}
				else
				{
					var exp_head = $('#exp_head').val();
					var date_range = $('#date_range').val();
					
					$("#loadicon").show();
					jQuery.ajax({
					url: "fetch_exp_vou.php",
					data: 'option=' + option + '&exp_head=' + exp_head + '&range=' + date_range,
					type: "POST",
					success: function(data) {
					$("#result_main").html(data);
					},
						error: function() {}
					});
				}
			}
			else
			{
				alert('Invalid option selected !');
				$('#result_main').html('');
				
				$('#div_frno').show();
				$('#div_head').hide();
				$('#div_range').hide();
			}
		}		
		</script>	

<div id="func_result"></div>
		
	<div class="row">	
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Search By <font color="red"><sup>*</sup></font></label>
                  <select id="SearchBy" onchange="SearchBy(this.value)" class="form-control" required>
					<option value="FM">Vou No</option>
					<option value="HEAD">Expense Heading</option>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3" id="div_frno">	
               <div class="form-group">
                  <label>Vou No <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" id="frno" name="frno">
              </div>
			</div>
			
			<div class="col-md-3" id="div_head" style="display:none">	
               <div class="form-group">
                  <label>Expense Heading <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" id="exp_head" name="exp_head">
					<option value="">Select an option</option>
					<?php
					$fetch_head = Qry($conn,"SELECT id,exp FROM expenses ORDER BY exp ASC");
					if(!$fetch_head){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while processing Request","./");
						exit();
					}
					
					if(numRows($fetch_head)>0)
					{
						while($row_exp = fetchArray($fetch_head))
						{
							echo "<option value='$row_exp[id]'>$row_exp[exp]</option>";
						}
					}						
					?>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3" id="div_range" style="display:none">	
               <div class="form-group">
                  <label>Days Ago <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" name="range" id="date_range">
					<option value="-0 days">Today's</option>
					<option value="-1 days">Last 2 days</option>
					<option value="-4 days">Last 5 days</option>
					<option value="-6 days">Last 7 days</option>
					<option value="-9 days">Last 10 days</option>
					<option value="-14 days">Last 15 days</option>
					<option value="-29 days">Last 30 days</option>
					<option value="-59 days">Last 60 days</option>
					<option value="-89 days">Last 90 days</option>
					<option value="-119 days">Last 120 days</option>
					<option value="FULL">FULL REPORT</option>
				</select>
              </div>
			</div>
			
			<div id="button_div" class="col-md-2">
				<label></label>
				<br />
				<button type="button" id="button2" onclick="Fetch($('#SearchBy').val())" class="btn pull-right btn-danger">Check !</button>
			</div>
			
		</div>
		
		<div class="row">	
			<div class="col-md-12 table-responsive" style="overflow:auto">	
			
				<div id="result_main"></div>
			
			</div>
		</div>
	</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
		</div>
	</div>
   </div>
 </div>
</section>
</div>
  
<script>  
function EditVou(id,frno)
{
	$('#modal_frno').val(frno);
	$('#modal_id').val(id);
	$("#loadicon").show();
	jQuery.ajax({
	url: "load_exp_vou_for_edit.php",
	data: 'frno=' + frno + '&id=' + id,
	type: "POST",
	success: function(data) {
		$("#result_modal_data").html(data);
		$('#ReqModal').modal();
	},
	error: function() {}
	});
}

function DeleteVou(id,frno)
{
	$('#delete_btn_'+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
	url: "delete_exp_vou.php",
	data: 'frno=' + frno + '&id=' + id,
	type: "POST",
	success: function(data) {
		$("#func_result").html(data);
	},
	error: function() {}
	});
}  
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#VouUpdateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#req_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_exp_voucher.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#exp_update_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="VouUpdateForm" autocomplete="off"> 
<div class="modal fade" id="ReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-xl" role="document">
     <div class="modal-content">
	 
	 <div class="modal-header bg-primary">
		Update Expense Voucher :
    </div>
	
	<div id="exp_update_result"></div>
	
		<div class="modal-body">
			<div class="row">
				<div class="col-md-12 table-responsive" style="overflow:auto">
					<div id="result_modal_data"></div>
				</div>
			</div>
		</div>
		
	<input type="hidden" name="frno" id="modal_frno">
	<input type="hidden" name="id" id="modal_id">
	
	<div class="modal-footer">
		<button type="button" id="req_close_button" onclick="document.getElementById('menu_button2').click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="req_update_button" class="btn btn-primary">Update</button>
	</div>
		</div>	
     </div>
 </div>	
</form>
 
<?php include ("./footer.php"); ?>
</div>
</body>
</html>