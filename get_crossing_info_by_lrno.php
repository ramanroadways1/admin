<?php
require_once("../_connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$check_lr = Qry($conn,"SELECT branch,company,tstation,wt12 as actual_weight,weight as charge_weight,to_id,con1_id,crossing,item_id,break,
date(timestamp) as date_created FROM lr_sample WHERE lrno='$lrno'");

if(!$check_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_lr)==0){
	echo "<script>
		alert('LR not found.');
		$('#add_btn_crossing_lr').attr('disabled',true);
		
		$('#from_loc_crossing').attr('readonly',true);
		$('#to_loc_crossing').attr('readonly',true);
		
		$('#from_loc_crossing').val('');
		$('#to_loc_crossing').val('');
		$('#to_loc_crossing_db').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#act_wt_crossing').attr('readonly',true);
		$('#chrg_wt_crossing').attr('readonly',true);
		$('#act_wt_crossing').val('');
		$('#chrg_wt_crossing').val('');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_check_lr = fetchArray($check_lr);

$diff_days = strtotime(date("Y-m-d")) - strtotime($row_check_lr['date_created']);
$diff_days=round($diff_days / (60 * 60 * 24));	

if($diff_days>10)
{
	echo "<script>
		alert('Error : LR exceed 10 days.');
		$('#add_btn_crossing_lr').attr('disabled',true);
		
		$('#from_loc_crossing').attr('readonly',true);
		$('#to_loc_crossing').attr('readonly',true);
		
		$('#from_loc_crossing').val('');
		$('#to_loc_crossing').val('');
		$('#to_loc_crossing_db').val('');
		$('#to_loc_id_crossing_db').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#act_wt_crossing').attr('readonly',true);
		$('#chrg_wt_crossing').attr('readonly',true);
		$('#act_wt_crossing').val('');
		$('#chrg_wt_crossing').val('');
		$('#loadicon').hide();
	</script>";
	// HideLoadicon();
	exit();
}
	
if($row_check_lr['break']>0)
{
	echo "<script>
		alert('Error : This is breaking LR.');
		$('#add_btn_crossing_lr').attr('disabled',true);
		
		$('#from_loc_crossing').attr('readonly',true);
		$('#to_loc_crossing').attr('readonly',true);
		
		$('#from_loc_crossing').val('');
		$('#to_loc_crossing').val('');
		$('#to_loc_crossing_db').val('');
		$('#to_loc_id_crossing_db').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#act_wt_crossing').attr('readonly',true);
		$('#chrg_wt_crossing').attr('readonly',true);
		$('#act_wt_crossing').val('');
		$('#chrg_wt_crossing').val('');
		$('#loadicon').hide();
	</script>";
	// HideLoadicon();
	exit();	
}
	
if($row_check_lr['crossing']!='YES'){
	echo "<script>
		alert('LR not crossing enabled.');
		$('#add_btn_crossing_lr').attr('disabled',true);
		
		$('#from_loc_crossing').attr('readonly',true);
		$('#to_loc_crossing').attr('readonly',true);
		$('#from_loc_crossing').val('');
		$('#to_loc_crossing').val('');
		$('#to_loc_crossing_db').val('');
		$('#to_loc_id_crossing_db').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#act_wt_crossing').attr('readonly',true);
		$('#chrg_wt_crossing').attr('readonly',true);
		$('#act_wt_crossing').val('');
		$('#chrg_wt_crossing').val('');
		$('#loadicon').hide();
	</script>";
	// HideLoadicon();
	exit();
}

$get_info = Qry($conn,"SELECT tstation,to_id FROM freight_form_lr WHERE lrno='$lrno' ORDER BY id DESC LIMIT 1");
if(!$get_info){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_info)==0){
	echo "<script>
		alert('Previous Records not found. LR Number : $lrno.');
		$('#add_btn_crossing_lr').attr('disabled',true);
		
		$('#from_loc_crossing').attr('readonly',true);
		$('#to_loc_crossing').attr('readonly',true);
		$('#from_loc_crossing').val('');
		$('#to_loc_crossing').val('');
		$('#to_loc_crossing_db').val('');
		$('#to_loc_id_crossing_db').val('');
		$('#cross_loc_from_id').val('');
		$('#cross_loc_to_id').val('');
		$('#crossing_fetch_btn').show();
		$('#lrno_crossing_1').attr('readonly',false);
		
		$('#act_wt_crossing').attr('readonly',true);
		$('#chrg_wt_crossing').attr('readonly',true);
		$('#act_wt_crossing').val('');
		$('#chrg_wt_crossing').val('');
		$('#loadicon').hide();
	</script>";
	// HideLoadicon();
	exit();
}

$row_prev = fetchArray($get_info);

$chkEwayBillFree = Qry($conn,"SELECT id FROM _eway_bill_free WHERE item='$row_check_lr[item_id]' || consignor='$row_check_lr[con1_id]' || lrno='$lrno' AND status='1'");
if(!$chkEwayBillFree){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

if(numRows($chkEwayBillFree)>0)
{
	echo "<script>
		$('#ewb_no_ip').attr('readonly',true);
	</script>";
}
else
{
	echo "<script>
		$('#ewb_no_ip').attr('readonly',false);
	</script>";
}

echo "<script>
		$('#from_loc_crossing').attr('readonly',true);
		$('#to_loc_crossing').attr('readonly',true);
		$('#from_loc_crossing').val('$row_prev[tstation]');
		$('#to_loc_crossing').val('$row_check_lr[tstation]');
		$('#to_loc_crossing_db').val('$row_check_lr[tstation]');
		$('#to_loc_id_crossing_db').val('$row_check_lr[to_id]');
		$('#cross_loc_from_id').val('$row_prev[to_id]');
		$('#cross_loc_to_id').val('$row_check_lr[to_id]');
		$('#cross_modal_con1_id').val('$row_check_lr[con1_id]');
		$('#cross_modal_item_id').val('$row_check_lr[item_id]');
		
		$('#add_btn_crossing_lr').attr('disabled',false);
		$('#crossing_fetch_btn').hide();
		$('#lrno_crossing_1').attr('readonly',true);
		
		$('#act_wt_crossing').attr('readonly',true);
		$('#chrg_wt_crossing').attr('readonly',true);
		$('#act_wt_crossing').val('$row_check_lr[actual_weight]');
		$('#chrg_wt_crossing').val('$row_check_lr[charge_weight]');
		$('#loadicon').hide();
</script>";

if($row_check_lr['branch']=='MUNDRA' AND ($row_check_lr['actual_weight']=='2' || $row_check_lr['actual_weight']=='4'))
{
	echo "<script>
		$('#act_wt_crossing').attr('readonly',false);
		$('#chrg_wt_crossing').attr('readonly',false);
		$('#to_loc_crossing').attr('readonly',false);
		$('#loadicon').hide();
	</script>";
}
	// HideLoadicon();
	exit();
?>