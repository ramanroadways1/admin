<?php
require_once("./connect.php");
require_once("./connect2.php");

$id = mysqli_real_escape_string($conn,($_POST['id']));
$section = mysqli_real_escape_string($conn,($_POST['section']));
$cash_id = mysqli_real_escape_string($conn,($_POST['cash_id']));
$diary_id = mysqli_real_escape_string($conn,($_POST['diary_id']));
$bilty_type = mysqli_real_escape_string($conn,($_POST['bilty_type']));

$section_array = array("FREIGHT", "OTHER", "HO", "CLAIM");

if(!in_array($section, $section_array))
{
	echo "<script>
			alert('Invalid option selected !');
			window.location.href='./credit.php';
		</script>";
	exit();
}

$chk_entry=mysqli_query($conn,"SELECT cash_id,diary_id,section,branch,bilty_no,amount,date FROM credit WHERE id='$id'");

if(!$chk_entry)
{
	echo mysqli_error($conn);exit();
}

if(mysqli_num_rows($chk_entry)==0)
{
	echo "<script>
			alert('Unable to find credit entry !');
			window.location.href='./credit.php';
		</script>";
	exit();
}

$row_chk_entry=mysqli_fetch_array($chk_entry);

if($row_chk_entry['cash_id']!=$cash_id || $row_chk_entry['diary_id']!=$diary_id)
{
	echo "<script>
			alert('Unable to verify Transactions !');
			window.location.href='./credit.php';
		</script>";
	exit();
}

$credit_by = mysqli_real_escape_string($conn,($_POST['credit_by']));
$ediary_credit = mysqli_real_escape_string($conn,($_POST['ediary_credit']));
$tno = mysqli_real_escape_string($conn,strtoupper($_POST['tno']));

if($section=='FREIGHT')
{
	$lr_date = mysqli_real_escape_string($conn,($_POST['lr_date']));
	$from_station = mysqli_real_escape_string($conn,strtoupper($_POST['from_station']));
	$to_station = mysqli_real_escape_string($conn,strtoupper($_POST['to_station']));
	$market_bilty = mysqli_real_escape_string($conn,strtoupper($_POST['market_bilty']));
}

$amount = mysqli_real_escape_string($conn,($_POST['amount']));
$db_amount = $row_chk_entry['amount'];
$db_date = $row_chk_entry['date'];
$bilty_no = $row_chk_entry['bilty_no'];
$db_branch = $row_chk_entry['branch'];
$cheque_number = mysqli_real_escape_string($conn,strtoupper($_POST['cheque_number']));
$narration = mysqli_real_escape_string($conn,strtoupper($_POST['narration']));

$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$old_data="Credit Id : $id, Section  : $section, CashId : $cash_id, DiaryId : $diary_id, BiltyType : $bilty_type, Amount : $db_amount,
 Date : $db_date, BiltyNo : $bilty_no, Branch : $db_branch.";
 
$new_data="Amount : $amount";

if($amount==0 || $amount=='' || $credit_by=='' || $section=='')
{
	echo "<script>
		alert('Unable to fetch details !');
		window.location.href='./credit.php';
	</script>";
	exit();
}

$diff_amount=$db_amount-$amount;

if($credit_by=='CASH')
{
	if($section=='FREIGHT')
	{
		if($ediary_credit=='0')
		{
			echo "<script>
				alert('Incomplete Freight details found in DataBase Contact System Admin !');
				window.location.href='./credit.php';
				$('#loadicon').hide();
			</script>";
			exit();
		}
		else if($ediary_credit=='1')
		{
			$select_freight_rcvd=mysqli_query($conn2,"SELECT trip_id,trans_id,vou_id,tno,amount,branch FROM freight_rcvd WHERE id='$diary_id'");
			if(mysqli_num_rows($select_freight_rcvd)==0)
			{
				echo "<script>
					alert('Unable to fetch Freight Received Entry in e-Diary !');
					window.location.href='./credit.php';
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$row_adv=mysqli_fetch_array($select_freight_rcvd);
			
			$trans_id=$row_adv['trans_id'];
			$trip_id=$row_adv['trip_id'];
			$branch=$row_adv['branch'];
			
			if($branch!=$db_branch)
			{
				echo "<script>
					alert('Unable to verify Branch !');
					window.location.href='./credit.php';
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			if($row['vou_id']!=$bilty_no)
			{
				echo "<script>
					alert('Unable to verify bilty number !');
					window.location.href='./credit.php';
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			if($row['amount']!=$db_amount)
			{
				echo "<script>
					alert('Unable to verify Amount !');
					window.location.href='./credit.php';
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			if($amount==$db_amount)
			{
				echo "<script>
					alert('Nothing to update !');
					window.location.href='./credit.php';
					$('#loadicon').hide();
				</script>";
				exit();
			}
			else
			{
				$chk_trip_id=mysqli_query($conn2,"SELECT freight FROM trip WHERE id='$trip_id'");
				if(mysqli_num_rows($chk_trip_id)==0)
				{
					echo "<script>
							alert('No Running Hisab or Trip Found !');
							window.location.href='./credit.php';
							$('#loadicon').hide();
						</script>";
						exit();
						
					// $chk_trip_id2=mysqli_query($conn2,"SELECT freight FROM trip_final WHERE trip_id='$trip_id'");
					
					// if(mysqli_num_rows($chk_trip_id)==0)
					// {
						// echo "<script>
							// alert('Unable to verify Trip Id !');
							// window.location.href='./credit.php';
							// $('#loadicon').hide();
						// </script>";
						// exit();
					// }
					// else
					// {
						// $trip_table="trip_final";
						// $trip_id_col="trip_id";
					// }
				}
				else
				{
					$trip_table="trip";
					$trip_id_col="id";
				}
				
				$chk_cashbook_entry=mysqli_query($conn,"SELECT comp FROM cashbook WHERE id='$cash_id'");
				if(mysqli_num_rows($chk_cashbook_entry)==0)
				{
					echo "<script>
							alert('Unable to Find CashBook Entry !');
							window.location.href='./credit.php';
							$('#loadicon').hide();
						</script>";
						exit();
				}
				
				$row_cashbook=mysqli_fetch_array($chk_cashbook_entry);
				
				$debit_company=$row_cashbook['comp'];
				
				if($debit_company=='')
				{
					echo "<script>
							alert('Unable to Find Debit Company !');
							window.location.href='./credit.php';
							$('#loadicon').hide();
						</script>";
						exit();
				}
				
				$fetch_branch_balance=mysqli_query($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
				
				$row_branch_balance=mysqli_fetch_array($fetch_branch_balance);
				
				$rrpl_cash=$row_branch_balance['balance'];
				$rr_cash=$row_branch_balance['balance2'];
				
				$poss_amount=abs($diff_amount);
				
				if($diff_amount<0)
				{
					if($debit_company=='RRPL' AND $rrpl_cash<abs($diff_amount))
					{
						echo "<script>
							alert('Insufficient Fund in RRPL. Min. Balance Required is $poss_amount !');
							window.location.href='./credit.php';
						</script>";
						exit();
					}
					else if($debit_company=='RAMAN_ROADWAYS' AND $rr_cash<abs($diff_amount))
					{
						echo "<script>
							alert('Insufficient Fund in RAMAN_ROADWAYS. Min. Balance Required is $poss_amount !');
							window.location.href='./credit.php';
						</script>";
						exit();
					}
				}
				
				$select_driver_book=mysqli_query($conn2,"SELECT id,driver_code,tno,branch FROM driver_book WHERE trans_id='$trans_id'");
				if(mysqli_num_rows($select_driver_book)==0)
				{
					echo "<script>
							alert('Unable to Find Driver Book !');
							window.location.href='./credit.php';
							$('#loadicon').hide();
						</script>";
						exit();
				}
				
				$row_driver_book = mysqli_fetch_array($select_driver_book);
				
				$driver_code=$row_driver_book['driver_code'];
				$db_tno=$row_driver_book['tno'];
				
				if($db_tno!=$tno)
				{
					echo "<script>
						alert('Unable to verify Truck Number with Driver Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				if($branch!=$row_driver_book['branch'])
				{
					echo "<script>
						alert('Unable to verify Branch with Driver Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
			
				$driver_book_id=$row_driver_book['id'];
				
				$fetch_bilty_book=mysqli_query($conn2,"SELECT id,balance FROM bilty_book WHERE bilty_no='$bilty_no' AND trans_id='$trans_id'");
				if(mysqli_num_rows($fetch_bilty_book)==0)
				{
					echo "<script>
						alert('Unable to Find Bilty Number in Bilty Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$row_bilty_book=mysqli_fetch_array($fetch_bilty_book);
				
				$bilty_book_id=$row_bilty_book['id'];
				
				$fetch_bilty_balance=mysqli_query($conn2,"SELECT id,balance FROM bilty_balance WHERE bilty_no='$bilty_no'");
				if(mysqli_num_rows($fetch_bilty_balance)==0)
				{
					echo "<script>
						alert('Unable to Find Bilty Number in Bilty Balance Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$row_bilty_balance=mysqli_fetch_array($fetch_bilty_balance);
				
				$bilty_balance_book_id=$row_bilty_balance['id'];
				
				$update_driver_book=mysqli_query($conn2,"UPDATE driver_book SET debit='$amount',balance=balance+'$diff_amount' WHERE 
				id='$driver_book_id'");
				
				if(!$update_driver_book)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Driver Book Contact SYSTEM ADMIN !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_driver_book_all_entry=mysqli_query($conn2,"UPDATE driver_book SET balance=balance+'$diff_amount' WHERE 
				id>'$driver_book_id' AND tno='$tno' AND driver_code='$driver_code'");
				
				if(!$update_driver_book_all_entry)
				{
					echo mysqli_error($conn2);exit();
				}
				
				$update_bilty_balance=mysqli_query($conn2,"UPDATE bilty_balance SET balance=balance-'$diff_amount' WHERE 
				id='$bilty_balance_book_id'");
			
				if(!$update_bilty_balance)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Bilty Balance Boook !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_bilty_book=mysqli_query($conn2,"UPDATE bilty_book SET debit='$amount',balance=balance-'$diff_amount' WHERE 
				id='$bilty_book_id'");
				
				if(!$update_bilty_book)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Bilty Book Balance Boook !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_bilty_book_all=mysqli_query($conn2,"UPDATE bilty_book SET balance=balance-'$diff_amount' WHERE 
				id>'$bilty_book_id' AND bilty_no='$bilty_no' AND trans_id='$trans_id'");
				
				if(!$update_bilty_book_all)
				{
					echo mysqli_error($conn2);exit();
				}

				$update_freight_entry=mysqli_query($conn2,"UPDATE freight_rcvd SET amount='$amount',narration='$narration' WHERE 
				id='$diary_id'");
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Freight Table !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_driver_balance=mysqli_query($conn2,"UPDATE driver_up SET amount_hold=amount_hold+'$diff_amount' WHERE 
				driver_code='$driver_code' AND tno='$tno' AND down=0 ORDER BY id DESC LIMIT 1");
				
				if(!$update_driver_balance)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Driver Balance !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_trip_table=mysqli_query($conn2,"UPDATE `$trip_table` SET freight_collected=freight_collected+'$diff_amount' 
				WHERE `$trip_id_col`='$trip_id'");
				
				if(!$update_trip_table)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Trip Column !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				if($debit_company=='RRPL')
				{
					$credit_col="credit";
					$balance_col="balance";
				}
				else
				{
					$credit_col="credit2";
					$balance_col="balance2";
				}
				
				$update_cashbook=mysqli_query($conn,"UPDATE cashbook SET `$credit_col`='$amount',
				`$balance_col`=`$balance_col`-'$diff_amount' WHERE id='$cash_id'");
				
				if(!$update_cashbook)
				{
					echo mysqli_error($conn2);exit();
				}
				
				$update_cashbook_all=mysqli_query($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`-'$diff_amount' WHERE 
				id>'$cash_id' AND comp='$debit_company' AND user='$branch'");
				
				if(!$update_cashbook_all)
				{
					echo mysqli_error($conn2);exit();
				}
				
				$update_main_balance=mysqli_query($conn,"UPDATE user SET `$balance_col`=`$balance_col`-'$diff_amount' WHERE user='$branch'");
				if(!$update_main_balance)
				{
					echo mysqli_error($conn2);exit();
				}
				
				
				$update_credit_table=mysqli_query($conn,"UPDATE credit SET amount='$amount',narr='$narration' WHERE id='$id'");
				if(!$update_credit_table)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Credit Table !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
			}
		}
		else // IF NOT EDIARY CREDIT ********************************************************************************
		{
				$chk_cashbook_entry=mysqli_query($conn,"SELECT comp FROM cashbook WHERE id='$cash_id'");
				if(mysqli_num_rows($chk_cashbook_entry)==0)
				{
					echo "<script>
							alert('Unable to Find CashBook Entry !');
							window.location.href='./credit.php';
							$('#loadicon').hide();
						</script>";
						exit();
				}
				
				$row_cashbook=mysqli_fetch_array($chk_cashbook_entry);
				
				$debit_company=$row_cashbook['comp'];
				
				if($debit_company=='')
				{
					echo "<script>
							alert('Unable to Find Debit Company !');
							window.location.href='./credit.php';
							$('#loadicon').hide();
						</script>";
						exit();
				}
				
				$fetch_branch_balance=mysqli_query($conn,"SELECT balance,balance2 FROM user WHERE username='$db_branch'");
				
				$row_branch_balance=mysqli_fetch_array($fetch_branch_balance);
				
				$rrpl_cash=$row_branch_balance['balance'];
				$rr_cash=$row_branch_balance['balance2'];
				
				$poss_amount=abs($diff_amount);
				
				if($diff_amount<0)
				{
					if($debit_company=='RRPL' AND $rrpl_cash<abs($diff_amount))
					{
						echo "<script>
							alert('Insufficient Fund in RRPL. Min. Balance Required is $poss_amount !');
							window.location.href='./credit.php';
						</script>";
						exit();
					}
					else if($debit_company=='RAMAN_ROADWAYS' AND $rr_cash<abs($diff_amount))
					{
						echo "<script>
							alert('Insufficient Fund in RAMAN_ROADWAYS. Min. Balance Required is $poss_amount !');
							window.location.href='./credit.php';
						</script>";
						exit();
					}
				}
				
				$fetch_bilty_book=mysqli_query($conn2,"SELECT id,balance FROM bilty_book WHERE bilty_no='$bilty_no' AND 
				debit='$db_amount' AND branch='$db_branch' AND date='$db_date'");
				
				if(mysqli_num_rows($fetch_bilty_book)==0)
				{
					echo "<script>
						alert('Unable to Find Bilty Number in Bilty Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				if(mysqli_num_rows($fetch_bilty_book)>1)
				{
					echo "<script>
						alert('More than One Entry Found in Bilty Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$row_bilty_book=mysqli_fetch_array($fetch_bilty_book);
				
				$bilty_book_id=$row_bilty_book['id'];
				
				$fetch_bilty_balance=mysqli_query($conn2,"SELECT id,balance FROM bilty_balance WHERE bilty_no='$bilty_no'");
				if(mysqli_num_rows($fetch_bilty_balance)==0)
				{
					echo "<script>
						alert('Unable to Find Bilty Number in Bilty Balance Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$row_bilty_balance=mysqli_fetch_array($fetch_bilty_balance);
				
				$bilty_balance_book_id=$row_bilty_balance['id'];
				
				$diff_amount=$db_amount-$amount;
				
				$update_bilty_balance=mysqli_query($conn2,"UPDATE bilty_balance SET balance=balance-'$diff_amount' WHERE id='$bilty_balance_book_id'");
				if(!$update_bilty_balance)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Bilty Balance Boook !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_bilty_book=mysqli_query($conn2,"UPDATE bilty_book SET debit='$amount',balance=balance-'$diff_amount' WHERE id='$bilty_book_id'");
				if(!$update_bilty_book)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Bilty Book Balance Boook !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_bilty_book_all=mysqli_query($conn2,"UPDATE bilty_book SET balance=balance-'$diff_amount' WHERE 
				id>'$bilty_book_id' AND bilty_no='$bilty_no'");
				if(!$update_bilty_book_all)
				{
					echo mysqli_error($conn2);exit();
				}

				if($debit_company=='RRPL')
				{
					$credit_col="credit";
					$balance_col="balance";
				}
				else
				{
					$credit_col="credit2";
					$balance_col="balance2";
				}
				
				$update_cashbook=mysqli_query($conn,"UPDATE cashbook SET `$credit_col`='$amount',
				`$balance_col`=`$balance_col`-'$diff_amount' WHERE id='$cash_id'");
				if(!$update_cashbook)
				{
					echo mysqli_error($conn2);exit();
				}
				
				$update_cashbook_all=mysqli_query($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`-'$diff_amount' WHERE 
				id>'$cash_id' AND comp='$debit_company' AND user='$db_branch'");
				if(!$update_cashbook_all)
				{
					echo mysqli_error($conn2);exit();
				}
				
				$update_main_balance=mysqli_query($conn,"UPDATE user SET `$balance_col`=`$balance_col`-'$diff_amount' WHERE user='$db_branch'");
				if(!$update_main_balance)
				{
					echo mysqli_error($conn2);exit();
				}
				
				
				$update_credit_table=mysqli_query($conn,"UPDATE credit SET amount='$amount',narr='$narration' WHERE id='$id'");
				if(!$update_credit_table)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Credit Table !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
		}
	}
	else
	{
				$chk_cashbook_entry=mysqli_query($conn,"SELECT comp FROM cashbook WHERE id='$cash_id'");
				if(mysqli_num_rows($chk_cashbook_entry)==0)
				{
					echo "<script>
							alert('Unable to Find CashBook Entry !');
							window.location.href='./credit.php';
							$('#loadicon').hide();
						</script>";
						exit();
				}
				
				$row_cashbook=mysqli_fetch_array($chk_cashbook_entry);
				
				$debit_company=$row_cashbook['comp'];
				
				if($debit_company=='')
				{
					echo "<script>
							alert('Unable to Find Debit Company !');
							window.location.href='./credit.php';
							$('#loadicon').hide();
						</script>";
						exit();
				}
				
				$fetch_branch_balance=mysqli_query($conn,"SELECT balance,balance2 FROM user WHERE username='$db_branch'");
				
				$row_branch_balance=mysqli_fetch_array($fetch_branch_balance);
				
				$rrpl_cash=$row_branch_balance['balance'];
				$rr_cash=$row_branch_balance['balance2'];
				
				$poss_amount=abs($diff_amount);
				
				if($diff_amount<0)
				{
					if($debit_company=='RRPL' AND $rrpl_cash<abs($diff_amount))
					{
						echo "<script>
							alert('Insufficient Fund in RRPL. Min. Balance Required is $poss_amount !');
							window.location.href='./credit.php';
						</script>";
						exit();
					}
					else if($debit_company=='RAMAN_ROADWAYS' AND $rr_cash<abs($diff_amount))
					{
						echo "<script>
							alert('Insufficient Fund in RAMAN_ROADWAYS. Min. Balance Required is $poss_amount !');
							window.location.href='./credit.php';
						</script>";
						exit();
					}
				}
				
				if($debit_company=='RRPL')
				{
					$credit_col="credit";
					$balance_col="balance";
				}
				else
				{
					$credit_col="credit2";
					$balance_col="balance2";
				}
				
				$update_cashbook=mysqli_query($conn,"UPDATE cashbook SET desct='$narration',`$credit_col`='$amount',
				`$balance_col`=`$balance_col`-'$diff_amount' WHERE id='$cash_id'");
				if(!$update_cashbook)
				{
					echo mysqli_error($conn2);exit();
				}
				
				$update_cashbook_all=mysqli_query($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`-'$diff_amount' WHERE 
				id>'$cash_id' AND comp='$debit_company' AND user='$db_branch'");
				if(!$update_cashbook_all)
				{
					echo mysqli_error($conn2);exit();
				}
				
				$update_main_balance=mysqli_query($conn,"UPDATE user SET `$balance_col`=`$balance_col`-'$diff_amount' WHERE user='$db_branch'");
				if(!$update_main_balance)
				{
					echo mysqli_error($conn2);exit();
				}
				
				$update_credit_table=mysqli_query($conn,"UPDATE credit SET tno='$tno',amount='$amount',narr='$narration' WHERE id='$id'");
				if(!$update_credit_table)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Credit Table !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
	}
}
else if($credit_by=='CHEQUE')
{
	if($section=='FREIGHT')
	{
		if($ediary_credit=='0')
		{
			echo "<script>
				alert('Incomplete Freight details found in DataBase Contact System Admin !');
				window.location.href='./credit.php';
				$('#loadicon').hide();
			</script>";
			exit();
		}
		else if($ediary_credit=='1')
		{
			echo "<script>
				alert('User Can not Credit Cheque in eDiary !');
				window.location.href='./credit.php';
				$('#loadicon').hide();
			</script>";
			exit();
		}
		else // IF NOT EDIARY CREDIT ********************************************************************************
		{
			
				$fetch_bilty_book=mysqli_query($conn2,"SELECT id,balance FROM bilty_book WHERE bilty_no='$bilty_no' AND 
				debit='$db_amount' AND branch='$db_branch' AND date='$db_date'");
				
				if(mysqli_num_rows($fetch_bilty_book)==0)
				{
					echo "<script>
						alert('Unable to Find Bilty Number in Bilty Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				if(mysqli_num_rows($fetch_bilty_book)>1)
				{
					echo "<script>
						alert('More than One Entry Found in Bilty Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$row_bilty_book=mysqli_fetch_array($fetch_bilty_book);
				
				$bilty_book_id=$row_bilty_book['id'];
				
				$fetch_bilty_balance=mysqli_query($conn2,"SELECT id,balance FROM bilty_balance WHERE bilty_no='$bilty_no'");
				if(mysqli_num_rows($fetch_bilty_balance)==0)
				{
					echo "<script>
						alert('Unable to Find Bilty Number in Bilty Balance Book !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$row_bilty_balance=mysqli_fetch_array($fetch_bilty_balance);
				
				$bilty_balance_book_id=$row_bilty_balance['id'];
				
				$diff_amount=$db_amount-$amount;
				
				$update_bilty_balance=mysqli_query($conn2,"UPDATE bilty_balance SET balance=balance-'$diff_amount' WHERE id='$bilty_balance_book_id'");
				if(!$update_bilty_balance)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Bilty Balance Boook !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_bilty_book=mysqli_query($conn2,"UPDATE bilty_book SET debit='$amount',balance=balance-'$diff_amount' WHERE id='$bilty_book_id'");
				if(!$update_bilty_book)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Bilty Book Balance Boook !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_bilty_book_all=mysqli_query($conn2,"UPDATE bilty_book SET balance=balance-'$diff_amount' WHERE 
				id>'$bilty_book_id' AND bilty_no='$bilty_no'");
				if(!$update_bilty_book_all)
				{
					echo mysqli_error($conn2);exit();
				}

				$update_credit_table=mysqli_query($conn,"UPDATE credit SET amount='$amount',chq_no='$cheque_number',narr='$narration' WHERE id='$id'");
				if(!$update_credit_table)
				{
					echo mysqli_error($conn2);exit();
				}
				
				if(mysqli_affected_rows($conn2)==0)
				{
					echo "<script>
						alert('Unable to Update Credit Table !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$update_cheque_book=mysqli_query($conn,"UPDATE cheque_book SET cheq_no='$cheque_number' WHERE vou_no='$bilty_no'");
				if(!$update_cheque_book)
				{
					echo mysqli_error($conn2);exit();
				}
		}
	}
	else
	{
			echo "<script>
						alert('Cheque Facility not available for other than Credit Freight !');
						window.location.href='./credit.php';
						$('#loadicon').hide();
					</script>";
			exit();
	}
}
else
{
	echo "<script>
			alert('Invalid Payment Mode selected !');
			window.location.href='./credit.php';
		</script>";
	exit();
}

$update_log=mysqli_query($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
	('$id','Credit','Update_Credit','$old_data - $new_data','$db_branch','ADMIN','$timestamp')");
	
echo "<script>
	alert('Updated Successfully !');
	document.getElementById('button2').click();
	document.getElementById('req_close_button').click();
</script>";
?>
