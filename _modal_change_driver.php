<script type="text/javascript">
$(function() {
	$("#driver_modal_name").autocomplete({
	source: 'autofill/get_driver.php',
	// appendTo: '#appenddiv',
	select: function (event, ui) { 
		$('#driver_modal_name').val(ui.item.value);   
		$('#driver_modal_id').val(ui.item.id);      
		$('#driver_modal_mobile').val(ui.item.mobile);      
		$('#driver_modal_lic').val(ui.item.pan);      
		$('#driver_modal_addr').val(ui.item.addr);      
		return false;
	},
	change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#driver_modal_name').val("");   
			$('#driver_modal_id').val("");   
			$('#driver_modal_mobile').val("");   
			$('#driver_modal_lic').val("");   
			$('#driver_modal_addr').val("");   
			alert('Driver does not exists !');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#ChanageDriverForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#chanage_driver_button").attr("disabled", true);
	$.ajax({
	url: "./update_driver_fm.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="ChanageDriverForm" autocomplete="off"> 

<div class="modal fade" id="DriverModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
		
	<div class="modal-header bg-primary">
		Change Driver :
      </div>
	  
			<div class="modal-body">
			
	<div class="row">
			
			<div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Driver Name <font color="red"><sup>*</sup></font></label>
					   <input type="text" name="driver_name" id="driver_modal_name" class="form-control" required>
                  </div>
              </div>
			  
			 <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Driver's Lic No <font color="red"><sup>*</sup></font></label>
					   <input id="driver_modal_lic" class="form-control" required readonly />
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Driver's Mobile <font color="red"><sup>*</sup></font></label>
					   <input id="driver_modal_mobile" class="form-control" required readonly />
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Driver's Address <font color="red"><sup>*</sup></font></label>
					   <textarea id="driver_modal_addr" class="form-control" required readonly></textarea>
                  </div>
               </div>
			   
			   <input type="hidden" id="driver_modal_id" name="driver_id">
			   <input type="hidden" id="driver_vou_no" name="vou_no">
			  <input type="hidden" id="driver_vou_id" name="vou_id_fm">
			  
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" id="close_driver_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="chanage_driver_button" class="btn btn-primary">Change Driver</button>
	</div>
		</div>	
     </div>
 </div>	
 
 </form>