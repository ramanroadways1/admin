<?php
require_once("./connect.php");

$frno = escapeString($conn,($_POST['vou_no']));
$vou_id = escapeString($conn,($_POST['vou_no_id']));
$amount = escapeString($conn,$_POST['amount']);
$timestamp = date("Y-m-d H:i:s");

if(empty($amount) || $amount<=0)
{
	echo "<script>alert('Amount is not valid..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(empty($frno))
{
	echo "<script>alert('Voucher not found..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(empty($vou_id))
{
	echo "<script>alert('VoucherID not found..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$get_fm_data = Qry($conn,"SELECT f.frno,f.date as vou_date,f.truck_no,f.branch,f.company,f.totaladv,f.adv_branch_user,f.adv_date,f.baladv,f.paidto,
GROUP_CONCAT(l.lrno SEPARATOR ',') as lrno 
FROM freight_form AS f 
LEFT OUTER JOIN freight_form_lr as l ON l.frno=f.frno 
WHERE f.id='$vou_id' GROUP by f.frno");

if(!$get_fm_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if(numRows($get_fm_data)==0){
	echo "<script>alert('Voucher not found..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$row_fm = fetchArray($get_fm_data);

if($row_fm['paidto']!='')
{
	echo "<script>alert('Error: Balance paid..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_fm['frno']!=$frno)
{
	echo "<script>alert('Error: Voucher not verified..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($amount>$row_fm['baladv'])
{
	echo "<script>alert('Error: Not enough balance left. Balance left is: $row_fm[baladv].');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

if($row_fm['totaladv']=='' || $row_fm['totaladv']<=0)
{
	echo "<script>alert('Error: Advance pending or not paid..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}

$truck_no = $row_fm['truck_no'];
$lrnos = $row_fm['lrno'];
$adv_user = $row_fm['adv_branch_user'];
$branch = $row_fm['branch'];
$adv_date = $row_fm['adv_date'];
$vou_company = $row_fm['company'];
$vou_date = $row_fm['vou_date'];

$get_diesel_rate_max = Qry($conn,"SELECT qty,rate,amount FROM _diesel_max_rate WHERE market_own='MARKET'");
if(!$get_diesel_rate_max){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>alert('Error..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
				
$row_dsl_max_rate = fetchArray($get_diesel_rate_max);

$dsl_max_rate=$row_dsl_max_rate['rate'];
$dsl_max_amount=$row_dsl_max_rate['amount'];
$dsl_max_qty=$row_dsl_max_rate['qty'];

$rilpre = escapeString($conn,$_POST['rilpre']);
$pump_card = escapeString($conn,strtoupper($_POST['pump_card']));

if($pump_card=='CARD')
{
	$card_id=escapeString($conn,($_POST['cardno2']));
	
	$get_cardno = Qry($conn,"SELECT cardno,vehicle,company FROM diesel_api.dsl_cards WHERE id='$card_id' AND active='1'");
	if(!$get_cardno){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script>alert('Error..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	if(numRows($get_cardno)==0){
		echo "<script>alert('Card not found..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	$row_card = fetchArray($get_cardno);

	$card = $row_card['cardno'];
	$type='CARD';
	$rate=0;
	$qty=0;
	$phy_card_no = $row_card['vehicle'];
	$fuel_company = $row_card['company'];
	$trans_date = date("Y-m-d");
	$mobile_no="";	
	
	if($fuel_company!=escapeString($conn,strtoupper($_POST['fuel_company'])))
	{
		echo "<script>alert('Company not verified..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
}
else if($pump_card=='OTP')
{
	if(strlen($_POST['mobile_no'])!=10)
	{
		echo "<script>alert('Check mobile number..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
		exit();
	}
	
	$card_id="0";
	$card=escapeString($conn,($_POST['mobile_no']));
	$mobile_no=escapeString($conn,($_POST['mobile_no']));
	$type="OTP";
	$rate=0;
	$qty=0;
	$phy_card_no=$mobile_no;
	$fuel_company=escapeString($conn,($_POST['fuel_company']));
	$trans_date = date("Y-m-d");
}
else
{
	$mobile_no="";	
	$card_id="0";		
	$card=escapeString($conn,strtoupper($_POST['pump_name']));	
	$card=strtok($card,'-');
	$phy_card_no=$card;
	$fuel_company=escapeString($conn,strtoupper($_POST['pump_company']));
	$type='PUMP';
	$rate=escapeString($conn,$_POST['rate']);
	$qty=escapeString($conn,$_POST['qty']);
	$trans_date = escapeString($conn,strtoupper($_POST['trans_date']));
}

$date=date("Y-m-d");

if($rate!=0 AND $rate>$dsl_max_rate)
{
	echo "<script type='text/javascript'>
		alert('Error : Rate is out of Limit.');
		$('#add_dsl_adv_button').attr('disabled', false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($qty!=0 AND $qty>$dsl_max_qty)
{
	echo "<script type='text/javascript'>
		alert('Error : Qty is out of Limit.');
		$('#add_dsl_adv_button').attr('disabled', false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($amount!=0 AND $amount>$dsl_max_amount)
{
	echo "<script type='text/javascript'>
		alert('Error : Amount is out of Limit.');
		$('#add_dsl_adv_button').attr('disabled', false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($type=='PUMP'){
	$done_one="1";
	$done_time=$timestamp;
}
else{
	$done_one="0";
	$done_time="";
}

if($rilpre=="1")
{
	$check_stock = Qry($conn,"SELECT stockid,balance FROM diesel_api.dsl_ril_stock WHERE cardno='$phy_card_no' 
	ORDER BY id DESC LIMIT 1");
	
	if(!$check_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($check_stock)==0){
		errorLog("Ril Stock not found. Card_no : $phy_card_no",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Error : RIL stock not found !');
			$('#DieselAdvAddForm')[0].reset();
			$('#add_dsl_adv_button').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
	
	$row_ril_stock = fetchArray($check_stock);
	
	$ril_stockid = $row_ril_stock['stockid'];
	
	if($row_ril_stock['balance']<$amount)
	{
		echo "<script>
			alert('Card not in stock. Available Balance is : $row_ril_stock[balance].');
			$('#DieselAdvAddForm')[0].reset();
			$('#add_dsl_adv_button').attr('disabled', false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}		
}
else
{
	$ril_stockid="";
}

StartCommit($conn);
$flag = true;

$dsl_nrr = "$fuel_company-$phy_card_no Rs: $amount/-";

$insert_diesel = Qry($conn,"INSERT INTO diesel_fm(branch,fno,com,qty,rate,disamt,tno,lrno,type,dsl_by,dcard,veh_no,dcom,dsl_nrr,
pay_date,fm_date,done,done_time,ril_card,dsl_mobileno,timestamp,stockid) VALUES ('$branch','$frno','$vou_company',
'$qty','$rate','$amount','$truck_no','$lrnos','ADVANCE','$type','$card','$phy_card_no','$fuel_company','$dsl_nrr','$date',
'$vou_date','$done_one','$done_time','$rilpre','$mobile_no','$timestamp','$ril_stockid')");
	
if(!$insert_diesel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dsl_id = getInsertID($conn);

if($rilpre=="1")
{
	$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$amount' WHERE id='$ril_stockid'");
			
	if(!$update_ril_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($type=='CARD')
{
	$card_live_bal = Qry($conn,"select balance from diesel_api.dsl_cards where id='$card_id'");
	if(!$card_live_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
				
	$row_card_bal=fetchArray($card_live_bal); 
	$live_bal = $row_card_bal['balance']; 

	$qry_log = Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,dslcomp,dsltype,
	cardno,vehno,cardbal,amount,mobileno,reqid) VALUES ('$branch','SUCCESS','New Request Added','$timestamp','MARKET',
	'$truck_no','$fuel_company','CARD','$card','$phy_card_no','$live_bal','$amount','$mobile_no','$dsl_id')");
				
	if(!$qry_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
	
$updateTodayDiesel = Qry($conn,"UPDATE today_data SET diesel_qty_market=ROUND(diesel_qty_market+'$qty',2),
diesel_amount_market=diesel_amount_market+'$amount' WHERE branch='$branch'");
	
if(!$updateTodayDiesel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$update_fm = Qry($conn,"UPDATE freight_form SET totaladv=ROUND(totaladv+'$amount',2),disadv=ROUND(disadv+'$amount',2),
baladv=ROUND(baladv-'$amount',2) WHERE id='$vou_id' AND paidto=''");

if(!$update_fm){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__); 
}	

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Diesel not updated. FmId: $vou_id. DieselAmt: $amount.",$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Diesel Entry Success.');
		$('#loadicon').fadeOut('slow');
		$('#add_dsl_adv_button').attr('disabled',true);
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error: something went wrong..');$('#add_dsl_adv_button').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}
?>