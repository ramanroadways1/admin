<?php
require_once("./connect.php");

$vessel_array = escapeString($conn,$_POST['vessel_name']);
$destination_array = escapeString($conn,$_POST['destination_name']);

$ship_id = explode("_#_",$vessel_array)[0]; // shipment no
$branch = explode("_#_",$vessel_array)[1];

$loc_name = explode("_#_",$destination_array)[0];
$loc_id = explode("_#_",$destination_array)[1];
$pincode = explode("_#_",$destination_array)[2];

$po_no = escapeString($conn,$_POST['po_no']);
$km_distance = escapeString($conn,$_POST['km_distance']);

$timestamp = date("Y-m-d H:i:s");

$chk_record = Qry($conn,"SELECT id FROM ship.dest_location WHERE loc_id='$loc_id' AND ship_id='$ship_id'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_record)>0)
{
	echo "<script>
		alert('Duplicate record found !');
		$('#add_new_button').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$insert_location = Qry($conn,"INSERT INTO ship.dest_location(location,loc_id,pincode,po_no,distance,branch,ship_id,timestamp) VALUES 
('$loc_name','$loc_id','$pincode','$po_no','$km_distance','$branch','$ship_id','$timestamp')");

if(!$insert_location){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Location Added Successfully !');
		window.location.href='./coal_manage_dest.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./coal_manage_dest.php");
	exit();
}	
?>