<?php
require_once("./connect.php");

$from_id = escapeString($conn,$_POST['from_loc_id']);
$to_id = escapeString($conn,$_POST['to_loc_id']);
$empty_loaded = escapeString($conn,$_POST['empty_loaded']);
$tno = escapeString($conn,$_POST['truck_no']);
$consignor = escapeString($conn,$_POST['consignor']);
$consignee = escapeString($conn,$_POST['consignor']);
$consignor_id = escapeString($conn,$_POST['consignor_id']);
$consignee_id = escapeString($conn,$_POST['consignor_id']);
$timestamp = date("Y-m-d H:i:s");

if($tno!='')
{
	$wheeler = escapeString($conn,$_POST['wheeler_db']);
}
else
{
	$wheeler = escapeString($conn,$_POST['wheeler']);
}

if($wheeler=='')
{
	echo "<script>
		alert('Wheeler not found !');
		$('#add_new_button').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}

if($empty_loaded=='0')
{
	$consignor=="";
	$consignee=="";
	$consignor_id=="";
	$consignee_id=="";
}

if($consignor=="")
{
	$consignor_id="0";
}
else
{
	if($consignor_id=="" || $consignor_id==0)
	{
		echo "<script>
			alert('Consignor not found !');
			$('#add_new_button').attr('disabled',false);
			$('#loadicon').hide();	
		</script>";
		exit();
	}
}

if($consignee=="")
{
	$consignee_id="0";
}
else
{
	if($consignee_id=="" || $consignee_id==0)
	{
		echo "<script>
			alert('Consignee not found !');
			$('#add_new_button').attr('disabled',false);
			$('#loadicon').hide();	
		</script>";
		exit();
	}
}

if($from_id=="" || $from_id==0 || $to_id=="" || $to_id==0)
{
	echo "<script>
		alert('Invalid Locations !');
		$('#add_new_button').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}	

$chk_duplicate = Qry($conn,"SELECT id FROM dairy.fix_lane WHERE from_id='$from_id' AND to_id='$to_id' AND tno='$tno' AND 
consignor='$consignor_id' AND consignee='$consignee_id' AND empty_loaded='$empty_loaded' AND truck_type='$wheeler'");

if(!$chk_duplicate){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_duplicate)>0)
{
	echo "<script>
		alert('Duplicate Record Found !');
		$('#add_new_button').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}
	
StartCommit($conn);
$flag = true;

$insert_record = Qry($conn,"INSERT INTO dairy.fix_lane(from_id,to_id,tno,is_active,consignor,consignee,empty_loaded,truck_type,
timestamp) VALUES ('$from_id','$to_id','$tno','1','$consignor_id','$consignee_id','$empty_loaded','$wheeler','$timestamp')");

if(!$insert_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$lane_id = getInsertID($conn);

$insert_adv_record = Qry($conn,"INSERT INTO dairy.fix_lane_adv(lane_id,timestamp) VALUES ('$lane_id','$timestamp')");

if(!$insert_adv_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Record Added Successfully !');
		window.location.href='./fix_lane.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fix_lane.php");
	exit();
}	
?>