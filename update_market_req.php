<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$timestamp=date("Y-m-d H:i:s");

if($frno=='')
{
	echo "<script>
		alert('No result found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_diesel_done = Qry($conn,"SELECT diesel_id FROM diesel_cache WHERE frno='$frno' AND save='1'");
if(!$chk_diesel_done){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$total_updated_rows = numRows($chk_diesel_done);

if($total_updated_rows==0)
{
	echo "<script>
		alert('Please Save All Records First !');
		$('#req_update_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$diesel_ids = array();
		
while($row_lrno=fetchArray($chk_diesel_done))
{
	$diesel_ids[] = "'".$row_lrno['diesel_id']."'";
	// $lrnos_store[] = $row_lrno['diesel_id'];
}
		
$diesel_ids=implode(',', $diesel_ids);
// $lrnos_store=implode(',', $lrnos_store);

$chk_done = Qry($conn,"SELECT id FROM `diesel_fm` WHERE id IN($diesel_ids) AND IF(dsl_by='PUMP',colset_d,done)='1'");
if(!$chk_done){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_done)>0)
{
	echo "<script>
		alert('Error : Pump or Card request marked as done !');
		window.location.href='./request_market_truck.php';
	</script>";
	exit();
}

$get_diesel_update = Qry($conn,"SELECT d1.diesel_id,d1.diesel as diesel1,d1.veh_no as card1,d1.dsl_comp as company1,d2.veh_no as card2,
d2.disamt as diesel2,d2.dcom as company2,d2.fno,d2.token_no 
FROM diesel_cache AS d1 
LEFT OUTER JOIN diesel_fm AS d2 ON d2.id=d1.diesel_id 
WHERE d1.frno='$frno'");

if(!$get_diesel_update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_diesel_update)==0)
{
	echo "<script>
		alert('No record found to update !');
		$('#req_update_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$update_log=array();
$is_fm_created = false;

while($row_update = fetchArray($get_diesel_update))
{
	if($row_update['fno']!=$row_update['token_no'])
	{
		$is_fm_created = true;
	}
	
	if($row_update['card1']!=$row_update['card2'])
	{
		$update_log[]="Diesel-Id: $row_update[diesel_id]-> Card/Pump : $row_update[card1] to $row_update[card2]";
	}
	
	if($row_update['company1']!=$row_update['company2'])
	{
		$update_log[]="Diesel-Id: $row_update[diesel_id]-> Company : $row_update[company1] to $row_update[company2]";
	}
	
	if($row_update['diesel1']!=$row_update['diesel2'])
	{
		$update_log[]="Diesel-Id: $row_update[diesel_id]-> Amount : $row_update[diesel1] to $row_update[diesel2]";
	}
}

$update_log = implode(', ',$update_log); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#req_update_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$update_log = "Vou_No: ".$frno.": ".$update_log;

$check_diesel_sum_cache = Qry($conn,"SELECT SUM(qty) as cache_qty,SUM(diesel) as diesel_cache FROM diesel_cache WHERE frno='$frno'");
if(!$check_diesel_sum_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$check_diesel_sum_main = Qry($conn,"SELECT SUM(qty) as main_qty,SUM(disamt) as diesel_main,SUM(cash) as cash FROM diesel_fm WHERE fno='$frno'");
if(!$check_diesel_sum_main){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_cache_sum = fetchArray($check_diesel_sum_cache);
$row_main_sum = fetchArray($check_diesel_sum_main);

if($row_main_sum['cash']>0)
{
	echo "<script>
			alert('Cash found in diesel request !');
			window.location.href='./request_market_truck.php';
	</script>";
	exit();
}

if($row_cache_sum['diesel_cache']!=$row_main_sum['diesel_main'])
{
	$update_diesel_amount="1";
	$diesel_diff_amount = $row_main_sum['diesel_main']-$row_cache_sum['diesel_cache'];
	$diesel_diff_qty = $row_main_sum['main_qty']-$row_cache_sum['cache_qty'];
}
else
{
	$update_diesel_amount="0";
	$diesel_diff_qty = 0;
	$diesel_diff_amount = 0;
}

if($is_fm_created==true AND $update_diesel_amount=="1")
{
	$get_fm_diesel = Qry($conn,"SELECT branch,company,totalf,totaladv,adv_date,cashadv,chqadv,disadv,rtgsneftamt,ptob as adv_to,baladv,
	paidto as bal_to FROM freight_form WHERE frno='$frno'");
	if(!$get_fm_diesel){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($get_fm_diesel)==0)
	{
		echo "<script>
			alert('Freight memo not found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_fm_details = fetchArray($get_fm_diesel);
	
	if($row_fm_details['bal_to']!="")
	{
		echo "<script>
			alert('Freight memo balance paid !');
			window.location.href='./request_market_truck.php';
		</script>";
		exit();
	}
	
	if($row_fm_details['adv_to']=="")
	{
		echo "<script>
			alert('Freight memo advance pending !');
			window.location.href='./request_market_truck.php';
		</script>";
		exit();
	}
	
	if($row_fm_details['disadv']==$row_cache_sum['diesel_cache'])
	{
		echo "<script>
			alert('Something went wrong !');
			window.location.href='./request_market_truck.php';
		</script>";
		exit();
	}
	
	$new_adv = $row_fm_details['cashadv']+$row_fm_details['chqadv']+$row_cache_sum['diesel_cache']+$row_fm_details['rtgsneftamt'];
	$new_balance = $row_fm_details['totalf']-$new_adv;
	
	if($new_balance<0)
	{
		echo "<script>
			alert('Balance amount: $new_balance is negative !');
			$('#req_update_button').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$fm_created = "1";
}
else
{
	$fm_created = "0";
}

StartCommit($conn);
$flag = true;

if($fm_created=="1" AND $update_diesel_amount=="1")
{
	$adv_paid_to = $row_fm_details['adv_to'];
	$bal_paid_to = $row_fm_details['bal_to'];
	$company = $row_fm_details['company'];
	$fm_branch = $row_fm_details['branch'];
	$adv_date = $row_fm_details['adv_date'];
	$total_freight = $row_fm_details['totalf'];
	$total_cash = $row_fm_details['cashadv'];
	$total_cheque = $row_fm_details['chqadv'];
	$total_diesel = $row_cache_sum['diesel_cache'];
	$total_neft = $row_fm_details['rtgsneftamt'];
	
	$new_total_adv = $total_cash+$total_cheque+$total_diesel+$total_neft;
	
	if($adv_paid_to=='')
	{
		$flag = false;
		echo "<script>
			alert('Freight memo Advance Pending !');
		</script>";
	}
	
	if($bal_paid_to!='')
	{
		$flag = false;
		echo "<script>
			alert('Freight memo Balance Paid. Reset Balance First !');
		</script>";
	}
	
	$new_balance_amt = $total_freight - $new_total_adv;
	
	if($new_balance_amt<0)
	{
		$flag = false;
		errorLog("Advance Amount is greater than freight amount !",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Advance Amount is greater than freight amount !');
		</script>";
	}
	
	echo "New Balance: ".$new_balance_amt;
	
	$update_fm = Qry($conn,"UPDATE freight_form SET totaladv='$new_total_adv',disadv='$total_diesel',baladv='$new_balance_amt',
	colset_adv='0',colset_d_adv='0',adv_download='' WHERE frno='$frno'");

	if(!$update_fm){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_request = Qry($conn,"UPDATE diesel_fm t1 
INNER JOIN diesel_cache t2 
ON t2.id = t2.diesel_id
SET 
t1.qty = t2.qty,
t1.rate = t2.rate,
t1.disamt = t2.diesel,
t1.dsl_by = t2.dsl_by,
t1.dcard = t2.card,
t1.veh_no = t2.veh_no,
t1.dcom = t2.dsl_comp,
t1.dsl_nrr = t2.dsl_nrr,
t1.dsl_mobileno = t2.mobile_no
WHERE t2.frno='$frno' AND t2.save='1' AND t1.id = t2.diesel_id AND IF(t1.dsl_by='PUMP',t1.colset_d,t1.done)!='1'");

// $update_request = Qry($conn,"UPDATE diesel_fm
// INNER JOIN diesel_cache ON diesel_cache.diesel_id = diesel_fm.id 
// SET 
// diesel_fm.qty = diesel_cache.qty,
// diesel_fm.rate = diesel_cache.rate,
// diesel_fm.disamt = diesel_cache.diesel,
// diesel_fm.cash = diesel_cache.cash,
// diesel_fm.dsl_by = diesel_cache.dsl_by,
// diesel_fm.dcard = diesel_cache.card,
// diesel_fm.dcom = diesel_cache.dsl_comp,
// diesel_fm.dsl_nrr = diesel_cache.dsl_nrr
// WHERE diesel_cache.frno='$frno'");

if(!$update_request){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updated_diesel_rows = AffectedRows($conn);

if($updated_diesel_rows!=$total_updated_rows)
{
	$flag = false;
	errorLog("Total Diesel Rows to update in cache is: $total_updated_rows and updated in diesel_fm: $updated_diesel_rows.",$conn,$page_name,__LINE__);
}

$delete_cache_data=Qry($conn,"DELETE FROM diesel_cache WHERE frno='$frno'");

if(!$delete_cache_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_log_qry=Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$frno','MARKET_DIESEL_REQ','UPDATE_REQ','$update_log','','ADMIN','$timestamp')");

if(!$update_log_qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Updated Successfully !');
		// window.location.href='./request_market_truck.php';
		document.getElementById('button2').click();
		document.getElementById('req_close_button').click();
		// $('#req_update_button').attr('disabled', false);
		// $('#loadicon').hide();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	// echo "<script>
		// alert('Error While Processing Request !');
		// $('#req_update_button').attr('disabled', false);
		// $('#loadicon').hide();
	// </script>";
	Redirect("Error While Processing Request.","./request_market_truck.php");
	exit();
}
?>
