<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$timestamp = date("Y-m-d H:i:s");

$dlt_cache = Qry($conn,"DELETE FROM dairy.diesel_cache_own WHERE token_no='$frno'");
if(!$dlt_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$insert_cache = Qry($conn,"INSERT INTO dairy.diesel_cache_own(token_no,diesel_id,branch,qty,rate,amount,tno,lrno,card_pump,card_no,veh_no,
dcom,mobile_no,dsl_narr,card_done,pump_done,timestamp) SELECT d1.unq_id,d1.id,d1.branch,d1.qty,d1.rate,d1.amount,d1.tno,d1.lrno,d2.card_pump,d2.card,
d2.veh_no,d2.dsl_company,d2.dsl_mobileno,d1.narration,'d2.done','d2.download','$timestamp' 
FROM dairy.diesel AS d1 
LEFT OUTER JOIN dairy.diesel_entry AS d2 ON d2.id=d1.id 
WHERE d1.unq_id='$frno'");

if(!$insert_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(AffectedRows($conn)==0)
{
	echo "<script>
		alert('No result found !');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
$get_cache = Qry($conn,"SELECT id,token_no,diesel_id,branch,qty,rate,amount,tno,lrno,card_pump,card_no,veh_no,dcom,mobile_no,dsl_narr,
save,card_done,pump_done FROM dairy.diesel_cache_own WHERE token_no='$frno'");

if(!$get_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
?>

<script>
function MyFunc1()
{
	$("#req_update_button").attr("disabled", true);
	$('#main_table1').hide();
	$('#change_tno_div').show();
}

$(function() {
    $("#tno_new_update").autocomplete({
      source: '../diary/autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			 $(event.target).focus();
			 $("#button_change_tno").attr("disabled",true);
			alert('Truck Number doest not exists.');
        }
    }, 
    focus: function (event, ui) {
        $("#button_change_tno").attr("disabled",false);
		return false;
    }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#TnoForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_change_tno").attr("disabled", true);
	$.ajax({
	url: "./update_truck_no_own_truck_request.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_two").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<div id="result_two"></div>

<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th style="font-size:13px;" colspan="16">
	   <?php echo "Truck No : <span id='truck_no_text'></span> <a href='#' onclick='MyFunc1();'><font color='blue'>Change</font></a>"; ?>
	   </th>
    </tr>
	
</table>	

<div class="container-fluid" id="change_tno_div" style="display:none">
        <div class="row">
          <div class="col-md-12">
	
	<form id="TnoForm">
	
		<div class="form-group col-md-4"> 
			<label>Truck No <font color="red"><sup>*</sup></font></label>
			<input type="text" name="tno" id="tno_new_update" class="form-control" required />
		</div>
		
		<input type="hidden" value="<?php echo $frno; ?>" name="frno">
		
		<div class="form-group col-md-4"> 
			<button type="submit" id="button_change_tno" class="btn btn-success">Change Truck Number</button>
		</div>
		
	</form>	
	
</div>
	</div>
		</div>
		
	
<table class="table table-bordered" id="main_table1" style="font-size:12.5px;">
		<tr>    
			<th>Qty</th>
			<th>Rate</th>
			<th>Amount</th>
			<th>Type</th>
			<th>Fuel Comp.</th>
			<th>Card/Pump/Mobile</th>
			<th></th>
			<th></th>
		</tr>	
	
<?php
while($row=fetchArray($get_cache))
{
		$truck_no_text = $row['tno'];
		$lrno_text = $row['lrno'];
		$vou_no_text = $row['token_no'];
		
		if($row['card_pump']=='PUMP'){
			
		$check_consumer_pump = Qry($conn,"SELECT id,name,consumer_pump FROM dairy.diesel_pump_own WHERE code='$row[card_no]'");
		if(!$check_consumer_pump){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_check_consumer = fetchArray($check_consumer_pump);
		
		if($row_check_consumer['consumer_pump']=="1"){
			$is_consumer_pump="1";
		}
		else{
			$is_consumer_pump="0";
		}
		
			if($row['pump_done']=="1"){
				$edit_disable="disabled";
				$done_text="Recharge Done";
			}
			else{
				$edit_disable="";
				$done_text="";
			}
		}
		else
		{
			if($row['card_done']=="1"){
				$edit_disable="disabled";
				$done_text="Recharge Done";
			}
			else{
				$edit_disable="";
				$done_text="";
			}
		}
		
	echo "<tr>
		<td><input type='number' required step='any' max='400' style='width:80%' class='form-control qtyclass' value='$row[qty]' id='qty$row[id]' readonly></td>
		<td><input type='number' required step='any' max='90' style='width:80%' class='form-control rateclass' value='$row[rate]' id='rate$row[id]' readonly></td>
		<td><input type='number' required style='width:80%' class='form-control amtclass' value='$row[amount]' id='amt$row[id]' readonly></td>
		<td>$row[card_pump]
		<input type='hidden' value='$row[card_pump]' id='dsl_by$row[id]'>
	</td>";

if($row['card_pump']=='PUMP')
{
?>
<script type="text/javascript">
$(function() {
$("#qty<?php echo $row['id']; ?>,#rate<?php echo $row['id']; ?>").on("keydown keyup blur change input", CalcDiesel);
function CalcDiesel() {
	if($("#qty<?php echo $row['id']; ?>").val()==''){
		var qty = 0;
	}else{
		var qty = Number($("#qty<?php echo $row['id']; ?>").val());
	}

	if($("#rate<?php echo $row['id']; ?>").val()==''){
		var rate = 0;
	}else{
		var rate = Number($("#rate<?php echo $row['id']; ?>").val());
	}
	
	$("#amt<?php echo $row['id']; ?>").val(Math.round(qty * rate).toFixed(2));
}});
				 
function AmountCall() 
{
	if($("#qty<?php echo $row['id']; ?>").val()=='' && $("#rate<?php echo $row['id']; ?>").val()==''){
		$("#qty<?php echo $row['id']; ?>").focus()
		$("#amt<?php echo $row['id']; ?>").val('');
	}
	else
	{
		if($("#rate<?php echo $row['id']; ?>").val()=='' || $("#rate<?php echo $row['id']; ?>").val()<60)
		{
			$("#rate<?php echo $row['id']; ?>").val((Number($("#amt<?php echo $row['id']; ?>").val()) / Number($("#qty<?php echo $row['id']; ?>").val())).toFixed(2));
		}
		else
		{
			$("#qty<?php echo $row['id']; ?>").val((Number($("#amt<?php echo $row['id']; ?>").val()) / Number($("#rate<?php echo $row['id']; ?>").val())).toFixed(2));
		}
	}	
}

function CheckConsumerPump(id)
{
	
}

// $(document).ready(function(){
    // $("select.pumpselectvalue_<?php echo $row['id']; ?>").change(function(){
        // var selectedCountry = $(this).children("option:selected").val();
        // alert("You have selected the country - " + selectedCountry);
    // });
// });
</script>
	
	<td>
		<select onchange="CheckConsumerPump('<?php echo $row["id"]; ?>')" class="form-control pumpvalueclass pumpselectvalue_<?php echo $row['id']; ?>" id="pumpvalue<?php echo $row['id']; ?>" disabled value="<?php echo $row['card_no']; ?>" required="required">
	<?php
	$pump_fetch = Qry($conn,"SELECT branch,name,code FROM dairy.diesel_pump_own WHERE code!='' AND branch='$row[branch]' AND active='1' AND consumer_pump!='1'");
		if(!$pump_fetch){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		echo "<option value=''>select pump</option>";
		
		if($is_consumer_pump=="1")
		{
		?>
			<option title="" selected="selected" value="<?php echo $row['card_no']; ?>"><?php echo $row_check_consumer["name"];?></option>
		<?php
		}
		else
		{
			while($row_pump = fetchArray($pump_fetch))
			{
		?>
			<option title="" <?php if ($row['card_no'] == $row_pump['code']){ echo ' selected="selected"'; }?> value="<?php echo $row_pump['code']; ?>"><?php echo $row_pump["name"]." - ".$row_pump["branch"];?></option>
		<?php
			}
		}
		?>
		</select>
	</td>
	<td></td>
	<?php
	}
	else if($row['card_pump']=='CARD')
	{
	?>
<script>
  $(function() {  
      $("#card<?php echo $row['id']; ?>").autocomplete({
			// appendTo: $("#card_no").next()
			source: function(request, response) { 
			if(document.getElementById("fuelcomp<?php echo $row['id']; ?>").value!=''){
                 $.ajax({
                  url: './autofill/get_diesel_cards.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   company: document.getElementById("fuelcomp<?php echo $row['id']; ?>").value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select company first !');
				$("#card<?php echo $row['id']; ?>").val('');
				$("#cardno2<?php echo $row['id']; ?>").val('');
				$("#phy_card_no<?php echo $row['id']; ?>").val('');
              }

              },
              select: function (event, ui) { 
               $("#card<?php echo $row['id']; ?>").val(ui.item.value);   
               $("#cardno2<?php echo $row['id']; ?>").val(ui.item.dbid);     
			   $("#phy_card_no<?php echo $row['id']; ?>").val(ui.item.phy_card_no);
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#cardno2<?php echo $row['id']; ?>").val('');
				$("#phy_card_no<?php echo $row['id']; ?>").val('');
                alert('Card No. does not exist !'); 
              } 
              },
			}); 
      }); 
	  
function ResetCardValue(id)
{
	$('#card'+id).val('');
	$('#cardno2'+id).val('');
	$('#phy_card_no'+id).val('');
}	  
</script> 
	
	<td>
		<select onchange="ResetCardValue('<?php echo $row["id"]; ?>')" class="form-control fuelcompclass" id="fuelcomp<?php echo $row['id']; ?>" required="required" disabled>
			<option value="">Select Company</option>
			<option value="IOCL" <?php if ($row['dcom'] == 'IOCL') echo ' selected="selected"'; ?>>IOCL</option>
			<option value="BPCL" <?php if ($row['dcom'] == 'BPCL') echo ' selected="selected"'; ?>>BPCL</option>
			<!--<option value="BPCL_PVTL" <?php if (strpos($row['card_no'], 'PVTL') !== false) echo ' selected="selected"'; ?>>BPCL_PVTL</option>
			<option value="HPCL" <?php if ($row['dcom'] == 'HPCL') echo ' selected="selected"'; ?>>HPCL</option>-->
			<option value="RELIANCE" <?php if ($row['dcom'] == 'RELIANCE') echo ' selected="selected"'; ?>>RELIANCE</option>
			<option value="RIL" <?php if ($row['dcom'] == 'RIL') echo ' selected="selected"'; ?>>RIL</option>
		</select>
	</td>
	
	<td>
		<input type="text" style="width:100%" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckCardCompany('<?php echo $row['id']; ?>')" class="form-control cardclass" 
		value="<?php echo $row['card_no']; ?>" id="card<?php echo $row['id']; ?>" required readonly>
		<input type="hidden" id="cardno2<?php echo $row['id']; ?>" name="cardno2">
		<input type="hidden" value="<?php echo $row['veh_no']; ?>" id="phy_card_no<?php echo $row['id']; ?>" name="phy_card_no">
	</td>
	
<script>
function CheckCardCompany(id)
{
	if($('#fuelcomp'+id).val()=='')
	{
		$('#fuelcomp'+id).focus();
		$('#card'+id).val('');
	}
}
</script>
	
	<?php
	}
	else if($row['card_pump']=='OTP')
	{
	?>
	<td>
		<select class="form-control fuelcompclass" id="fuelcomp<?php echo $row['id']; ?>" required="required" disabled>
			<option value="">Select Company</option>
			<option value="IOCL" <?php if ($row['dcom'] == 'IOCL') echo ' selected="selected"'; ?>>IOCL</option>
			<option value="BPCL" <?php if ($row['dcom'] == 'BPCL') echo ' selected="selected"'; ?>>BPCL</option>
			<option value="RELIANCE" <?php if ($row['dcom'] == 'RELIANCE') echo ' selected="selected"'; ?>>RELIANCE</option>
			<option value="RIL" <?php if ($row['dcom'] == 'RIL') echo ' selected="selected"'; ?>>RIL</option>
		</select>
	</td>
	
	<td>
		<input type="text" style="width:100%" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control cardclass" 
		value="<?php echo $row['mobile_no']; ?>" id="mobile_no<?php echo $row['id']; ?>" required readonly>
	</td>
	<?php	
	}
	else
	{
		echo "<td></td><td></td>";
	}
	
	if($done_text!="")
	{
		echo "<td colspan='2'>$done_text</td>";
	}	
	else
	{
	echo "
		<td>
			<button type='button' $edit_disable id='editbutton1$row[id]' onclick=Edit('$row[id]','$row[card_pump]','$row[diesel_id]') class='btn btn-sm btn-success'><i class='fa fa-edit'></i></button>
		</td>
		<td id='save_result$row[id]'>	
			<button type='button' $edit_disable id='savebtn$row[id]' disabled onclick=SaveSingle('$row[id]','$row[card_pump]','$row[diesel_id]') class='btn btn-sm btn-danger savebtnclass'><i class='fa fa-save'></i></button>
		</td>";
	}
	echo "</tr>";
}
	echo "
	</table>";
	
	echo "<script>
		document.getElementById('menu_button2').click();
		$('#loadicon').hide();
		$('#truck_no_text').html('$truck_no_text');
	</script>";
?>