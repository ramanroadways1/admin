<?php
require_once('./connect.php');
	
$frno=escapeString($conn,strtoupper($_POST['frno']));
$tno=escapeString($conn,strtoupper($_POST['tno']));

$timestamp = date("Y-m-d H:i:s");

if($frno=='' || $tno=='')
{
	echo "<script>
		alert('Missing : LR number or truck number.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

$fetch_data = Qry($conn,"SELECT GROUP_CONCAT(QUOTE(id) SEPARATOR ',') as diesel_id,fno,token_no,branch,branch_user,type,com,tno,lrno,no_lr,
pre_req,pre_and_fm FROM diesel_fm WHERE fno='$frno'");
if(!$fetch_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_data)==0)
{
	echo "<script>
		alert('Diesel not found.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

$row = fetchArray($fetch_data);

if($tno==$row['tno'])
{
	echo "<script>
		alert('Error : Nothing to update.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

$update_log = "Branch and user: $row[branch]($row[branch_user]), Vehicle number updated $row[tno] to $tno.";

if($row['type']!='ADVANCE')
{
	echo "<script>
		alert('Error : not advance diesel.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['fno']!=$row['token_no'])
{
	echo "<script>
		alert('Error : token number not verified.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['pre_req']=='0')
{
	echo "<script>
		alert('Error : not requested diesel.');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['pre_and_fm']=='1')
{
	echo "<script>
			alert('Advance paid. Reset advance first !');
			$('#loadicon').hide();
			$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

if($row['lrno']!='EMPTY')
{
	echo "<script>
		alert('LR mapped with diesel request. You can\'t edit !!');
		$('#loadicon').hide();
		$('#button_change_lrno').attr('disabled', false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_vehicle = Qry($conn,"UPDATE diesel_fm SET tno='$tno' WHERE fno='$frno' AND pre_req='1' AND no_lr='1'");
if(!$update_vehicle){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Vehicle number not updated. Truck_no : $tno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}
	
$diesel_id_count = explode(",", $row['diesel_id']);
$diesel_id_count = count($diesel_id_count);

$update_pending_diesel = Qry($conn,"UPDATE _pending_diesel SET tno='$tno' WHERE fno='$frno'");
if(!$update_pending_diesel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$updated_rows = AffectedRows($conn);	
	
if($updated_rows==0)
{
	$flag = false;
	errorLog("Vehicle no. not updated in _pending_diesel. truck_no : $tno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}

if($updated_rows!=$diesel_id_count)
{
	$flag = false;
	errorLog("_pending_diesel update count: $updated_rows not verified with diesel ids count : $diesel_id_count. Truck_no: $tno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}

$update_pending_lr_diesel = Qry($conn,"UPDATE diesel_lr_pending SET tno='$tno' WHERE diesel_id IN($row[diesel_id])");
if(!$update_pending_lr_diesel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$updated_rows2 = AffectedRows($conn);	
	
if($updated_rows2==0)
{
	$flag = false;
	errorLog("Vehicle no. not updated in diesel_lr_pending. truck_no : $tno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}

if($updated_rows2!=$diesel_id_count)
{
	$flag = false;
	errorLog("_pending_diesel update count: $updated_rows2 not verified with diesel ids count : $diesel_id_count. Truck_no: $tno. Diesel Token : $frno.",$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$frno','MARKET_DIESEL_REQ','EDIT_TRUCK_NO','$update_log','$row[branch]','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Vehicle Number : $tno Updated !');
		window.location.href='./request_market_truck.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./request_market_truck.php");
	exit();
}
?>