<?php
require_once("./connect.php");

$option=escapeString($conn,$_POST['option']);

if($option=='FM')
{
	$frno = escapeString($conn,strtoupper($_POST['frno']));
	$qry=Qry($conn,"SELECT id,user,vno,newdate,date,comp,des,asset_voucher,amt,chq,narrat,colset_d FROM mk_venf WHERE vno='$frno' ORDER BY id DESC");
}
else if($option=='HEAD')
{
	$exp_id = escapeString($conn,strtoupper($_POST['exp_head']));
	
	if($_POST['range']=='FULL'){
		$from_date="2017-01-01";
		$to_date=date("Y-m-d");
	}else{
		$from_date=date('Y-m-d', strtotime($_POST['range'], strtotime(date("Y-m-d"))));
		$to_date=date("Y-m-d");
	}
	
	$qry=Qry($conn,"SELECT id,user,vno,newdate,date,comp,des,asset_voucher,amt,chq,narrat,colset_d FROM mk_venf WHERE date 
	BETWEEN '$from_date' AND '$to_date' AND desid='$exp_id' ORDER BY id DESC");
}
else
{
	echo "<script>
		alert('Invalid option selected !');
		window.location.href='expense_voucher.php';
	</script>";
	exit();
}

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)>0)
{
?>
<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:13px;" colspan="16">Expense Vouchers :</th>
    </tr>
		<tr>    
			<th>Branch</th>
			<th>Vou_Id</th>
			<th>Vou_Date</th>
			<th>Created_On</th>
			<th>Expense</th>
			<th>Amount</th>
			<th>Pay.Mode</th>
			<th>Done</th>
			<th></th>
			<th></th>
		</tr>	
	<?php
	while($row=fetchArray($qry))
	{
		if($row['colset_d']==1){
			$done="<font color='green'>OK</font>";
		}else{
		  $done="";	
		}
		
		echo "<tr>
				<td>$row[user]</td>
				<td>$row[vno]</td>
				<td>".date('d-m-y',strtotime($row['newdate']))."</td>
				<td>".date('d-m-y',strtotime($row['date']))."</td>
				<td>$row[des]</td>
				<td>$row[amt]</td>
				<td>$row[chq]</td>
				<td>$done</td>";
				
			if($row['asset_voucher']=="1")
			{
				echo "
				<td colspan='2'>
					<font color='red'>Asset Voucher</font>
				</td>";
			}	
			else
			{
				echo "
				<td>
					<button type='button' onclick=EditVou('$row[id]','$row[vno]') class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
				</td>
				<td>
					<button type='button' id='delete_btn_$row[id]' onclick=DeleteVou('$row[id]','$row[vno]') class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button>
				</td>";
			}
			
			echo "</tr>";
	}
	echo "</table>";
	
	echo "<script>
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<script>
		alert('No record Found !');
		$('#loadicon').hide();	
	</script>";
	exit();
}
?>