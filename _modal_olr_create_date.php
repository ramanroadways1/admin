<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateDateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#date_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_create_date_own_truck_form.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="UpdateDateForm" autocomplete="off"> 
<div class="modal fade" id="CreateDateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
	   
	   <div class="modal-header bg-primary">
			Update Create Date :
      </div>
	  
<div class="modal-body">
			
		<div class="row">
			
			<div class="col-md-12">
                   <div class="form-group">
                       <label class="control-label mb-1">Select Date <font color="red"><sup>*</sup></font></label>
					   <input type="date" max="<?php echo date('Y-m-d'); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="create_date_text" name="date" class="form-control" required>
                  </div>
              </div>
			<input type="hidden" name="frno" id="edit_date_vou_no">
			<input type="hidden" name="id" id="edit_date_vou_id">
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" id="close_date_update_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="date_update_button" class="btn btn-primary">Update Date</button>
	</div>
		</div>	
     </div>
 </div>	
</form>