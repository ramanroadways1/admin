<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));
$section = escapeString($conn,strtoupper($_POST['section']));

if($section=='BANK_WDL')
{
	$qry=Qry($conn,"SELECT id as cash_id,'CASH' as credit_by,'BANK_WDL' as section,user as branch,comp as company,
	IF(comp='RRPL',credit,credit2) as amount,desct as narration,date FROM cashbook WHERE id='$id'");
}
else
{
	$qry=Qry($conn,"SELECT id,credit_by,adv_bal,cash_id,debit_table_id,diary_id,section,tno,lr_date,bilty_type,ediary_credit,trans_id,
	from_stn,to_stn,branch,company,bilty_no,amount,chq_no,narr as narration,date FROM credit WHERE id='$id'");
}

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('No result found !');
		window.location.href='./credit.php';
	</script>";
	exit();
}

$row=fetchArray($qry);

if($row['section']=='FREIGHT' AND $row['bilty_type']=='')
{
	echo "<script>
		alert('Incomplete freight details !');
		window.location.href='./credit.php';
	</script>";
	exit();
}

if($row['section']=='FREIGHT' AND $row['ediary_credit']=='0')
{
	echo "<script>
		alert('Incomplete freight details  !');
		window.location.href='./credit.php';
		$('#loadicon').hide();
	</script>";
	exit();
}
?>
<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:13px;" colspan="16">
	   <?php echo "Branch : ".$row['branch']; ?>
	   </th>
    </tr>
</table>	

<div class="container-fluid">
    <div class="row">
      
		<div class="form-group col-md-3"> 
			<label>Cash/Cheque <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" name="credit_by" value="<?php echo $row['credit_by']; ?>" readonly required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Transaction Date <font color="red"><sup>*</sup></font></label>
			<input type="date" id="trans_date" name="trans_date" max="<?php echo date('Y-m-d'); ?>" class="form-control" readonly 
			value="<?php echo $row['date']; ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
		</div>
		
		<?php
		if($row['section']=='FREIGHT' || $row['section']=='CLAIM')
		{
		?>
			<div class="form-group col-md-3"> 
				<label>Truck Number <font color="red"><sup>*</sup></font></label>
				<input type="text" class="form-control" name="tno" id="tno" value="<?php echo $row['tno']; ?>" 
				<?php if($row['section']=='FREIGHT') { echo 'readonly'; } ?> required>
			</div>
			
			<div class="form-group col-md-3" id="bilty_div"> 
				<label>Bilty/LR Number <font color="red"><sup>*</sup></font> <!--<a href="#" onclick="">Change</a>--></label>
				<input name="market_bilty" id="market_bilty_no" <?php if($row['section']=='FREIGHT') { echo 'readonly'; } ?> value="<?php echo $row['bilty_no']; ?>" class="form-control" required>
			</div>
		<?php
		}
		else
		{
			echo '<input type="hidden" name="tno" value="">';
			echo '<input type="hidden" name="market_bilty" value="">';
		}
		
		if($row['section']=='FREIGHT')
		{
		?>
		<div class="form-group col-md-3"> 
			<label>LR Date <font color="red"><sup>*</sup></font></label>
			<input type="date" id="lr_date" name="lr_date" max="<?php echo date('Y-m-d'); ?>" class="form-control" readonly value="<?php echo $row['lr_date']; ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>From <font color="red"><sup>*</sup></font></label>
			<input type="text" name="from_station" id="from_station" class="form-control" value="<?php echo $row['from_stn']; ?>" readonly required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>To <font color="red"><sup>*</sup></font></label>
			<input type="text" name="to_station" id="to_station" class="form-control" value="<?php echo $row['to_stn']; ?>" readonly required>
		</div>
		
		<?php
		}
		?>
		
		<div class="form-group col-md-3"> 
			<label>Amount <font color="red"><sup>*</sup></font></label>
			<input type="number" class="form-control" name="amount" value="<?php echo $row['amount']; ?>" min="1" required>
		</div>
		
		<?php
		if($row['credit_by']=='CHEQUE')
		{
		?>
		<div class="form-group col-md-3"> 
			<label>Cheque No <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" name="cheque_number" value="<?php echo $row['chq_no']; ?>" required>
		</div>
		<?php
		}
		else
		{
			echo '<input type="hidden" name="cheque_number" value="">';
		}
		?>
		
		<div class="form-group col-md-6"> 
			<label>Narration <font color="red"><sup>*</sup></font></label>
			<textarea class="form-control" id="narration" name="narration" required><?php echo $row['narration']; ?></textarea>
		</div>
		
		<input type="hidden" value="<?php echo $row['id']; ?>" name="id">
		<input type="hidden" value="<?php echo $row['section']; ?>" name="section">
		<input type="hidden" value="<?php if($row['section']=='BANK_WDL') { echo "0"; } else { echo $row['ediary_credit']; } ?>" name="ediary_credit">
		<input type="hidden" value="<?php echo $row['cash_id']; ?>" name="cash_id">
		<input type="hidden" value="<?php if($row['section']=='BANK_WDL') { echo "0"; } else { echo $row['diary_id']; } ?>" name="diary_id">
		<input type="hidden" value="<?php if($row['section']=='BANK_WDL') { echo "0"; } else { echo $row['bilty_type']; } ?>" name="bilty_type">
	
	</div>
</div>

<script>
		<?php
		if($row['section']=='FREIGHT')
		{
			if($row['bilty_type']=='OWN')
			{
				echo "
				$('#lr_date').attr('readonly',false);
				$('#from_station').attr('readonly',false);
				$('#to_station').attr('readonly',false);
				";
			}
			else
			{
				echo "
				$('#lr_date').attr('readonly',true);
				$('#from_station').attr('readonly',true);
				$('#to_station').attr('readonly',true);
				";
			}
		}
		else if($row['section']=='CLAIM')
		{
			echo "
			$('#bilty_div').hide;
			$('#market_bilty_no').attr('required',false);
			";
		}
		
		echo "
		document.getElementById('menu_button2').click();
		$('#loadicon').hide()
		";
		
		if($row['ediary_credit']==1)
		{
			echo "$('#narration').attr('readonly',true);";
		}
		else
		{
			echo "$('#narration').attr('readonly',false);";
		}
		?>
</script>