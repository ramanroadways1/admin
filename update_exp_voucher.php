<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$id = escapeString($conn,strtoupper($_POST['id']));
$vou_date = escapeString($conn,($_POST['vou_date']));
$amount = escapeString($conn,($_POST['amount']));
$pay_mode = escapeString($conn,strtoupper($_POST['pay_mode']));
$chq_bank = escapeString($conn,strtoupper($_POST['chq_bank']));
$chq_no = escapeString($conn,strtoupper($_POST['chq_no']));
$pan_no = escapeString($conn,strtoupper($_POST['pan_no']));
$narration = escapeString($conn,strtoupper($_POST['narration']));
$exp_array = escapeString($conn,strtoupper($_POST['exp_head']));
$emp_code = escapeString($conn,strtoupper($_POST['empcode']));
$veh_no = escapeString($conn,strtoupper($_POST['vehno']));
$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$exp_head_id = explode("_",$exp_array)[0];
$exp_limit = explode("_",$exp_array)[1];
$exp_comment = explode("_",$exp_array)[2];
$exp_head_name = explode("_",$exp_array)[3];

if($pay_mode=='NEFT')
{
	$fetch_ac_details = Qry($conn,"SELECT name,acno,bank,ifsc FROM exp_ac WHERE pan='$pan_no'");
	if(!$fetch_ac_details){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($fetch_ac_details)==0)
	{
		echo "<script>
			alert('Unable to fetch Account Details !');
			$('#loadicon').hide();
			$('#req_update_button').attr('disabled', false);
		</script>";
		exit();
	}
	
	$row_ac = fetchArray($fetch_ac_details);
	
	$ac_holder = escapeString($conn,strtoupper($row_ac['name']));
	$ac_no = escapeString($conn,strtoupper($row_ac['acno']));
	$bank_name = escapeString($conn,strtoupper($row_ac['bank']));
	$ifsc = escapeString($conn,strtoupper($row_ac['ifsc']));
}
else
{
	$ac_holder = "";
	$ac_no = "";
	$bank_name = "";
	$ifsc = "";
}

	if(count(explode(' ', $pan_no)) > 1)
	{
		echo "<script>
			alert('PAN Card Contains Whitespaces !');
			$('#loadicon').hide();
			$('#req_update_button').attr('disabled', false);
		</script>";
		exit();
	}

	if(count(explode(' ', $ac_no)) > 1)
	{
		echo "<script>
			alert('Account Number Contains Whitespaces !');
			$('#loadicon').hide();
			$('#req_update_button').attr('disabled', false);
		</script>";
		exit();
	}

	if(count(explode(' ', $ifsc)) > 1)
	{
		echo "<script>
			alert('IFSC Code Contains Whitespaces !');
			$('#loadicon').hide();
			$('#req_update_button').attr('disabled', false);
		</script>";
		exit();
	}

if($amount==0 || $amount=='' || $pay_mode=='' || $exp_head_name=='' || $exp_head_id=='')
{
	echo "<script>
		alert('Unable to fetch voucher details !');
		$('#loadicon').hide();
		$('#req_update_button').attr('disabled', false);
	</script>";
	exit();
}

$fetch_vou = Qry($conn,"SELECT user,branch_user,vno,date,newdate,comp,des,asset_voucher,desid,amt,chq,chq_no,chq_bnk_n,neft_bank,neft_acname,neft_acno,
neft_ifsc,pan,narrat,empcode,vehno,colset_d FROM mk_venf WHERE id='$id'");

if(!$fetch_vou){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_vou)==0)
{
	echo "<script>
		alert('Voucher not found !');
		$('#loadicon').hide();
		$('#req_update_button').attr('disabled', false);
	</script>";
	exit();
}

$row=fetchArray($fetch_vou);

if($row['vno']!=$frno)
{
	echo "<script>
		alert('Voucher number not verified !');
		$('#loadicon').hide();
		$('#req_update_button').attr('disabled', false);
	</script>";
	exit();
}

if($row['asset_voucher']=="1")
{
	echo "<script>
		alert('This is asset voucher you can\'t edit !');
		$('#req_update_button').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$branch = $row['user'];
$branch_user = $row['branch_user'];
$vou_date_old = $row['newdate'];
$sys_date = $row['date'];
$company = $row['comp'];
$expense = $row['des'];
$expense_id = $row['desid'];
$old_amount = $row['amt'];
$payment_mode_old = $row['chq'];
$chq_no_old = $row['chq_no'];
$chq_bank_old = $row['chq_bnk_n'];
$acname_old = $row['neft_acname'];
$acno_old = $row['neft_acno'];
$bank_old = $row['neft_bank'];
$ifsc_old = $row['neft_ifsc'];
$pan_old = $row['pan'];
$narration_old = $row['narrat'];
$emp_code_old = $row['empcode'];
$veh_no_old = $row['vehno'];
$downloaded = $row['colset_d'];

$update_log=array();
$update_Qry=array();

if($vou_date!=$vou_date_old)
{
	$update_log[]="Vou_date : $vou_date_old to $vou_date";
	$update_Qry[]="newdate='$vou_date'";
}

if($exp_head_id!=$expense_id)
{
	$update_log[]="Expense_head : $expense to $exp_head_name";
	$update_Qry[]="des='$exp_head_name'";
	$update_Qry[]="desid='$exp_head_id'";
}

if($amount!=$old_amount)
{
	$update_log[]="Amount : $old_amount to $amount";
	$update_Qry[]="amt='$amount'";
}

if($pay_mode!=$payment_mode_old)
{
	$update_log[]="Payment_mode : $payment_mode_old to $pay_mode";
	$update_Qry[]="chq='$pay_mode'";
}

if($chq_no!=$chq_no_old)
{
	$update_log[]="CHQ_no. : $chq_no_old to $chq_no";
	$update_Qry[]="chq_no='$chq_no'";
}

if($chq_bank!=$chq_bank_old)
{
	$update_log[]="CHQ_bank : $chq_bank_old to $chq_bank";
	$update_Qry[]="chq_bnk_n='$chq_bank'";
}

if($ac_holder!=$acname_old)
{
	$update_log[]="Ac_holder : $acname_old to $ac_holder";
	$update_Qry[]="neft_acname='$ac_holder'";
}

if($ac_no!=$acno_old)
{
	$update_log[]="Ac_no : $acno_old to $ac_no";
	$update_Qry[]="neft_acno='$ac_no'";
}

if($bank_name!=$bank_old)
{
	$update_log[]="Bank_name : $bank_old to $bank_name";
	$update_Qry[]="neft_bank='$bank_name'";
}

if($ifsc!=$ifsc_old)
{
	$update_log[]="Ifsc : $ifsc_old to $ifsc";
	$update_Qry[]="neft_ifsc='$ifsc'";
}

if($pan_no!=$pan_old)
{
	$update_log[]="PAN_no : $pan_old to $pan_no";
	$update_Qry[]="pan='$pan_no'";
}

if($narration!=$narration_old)
{
	$update_log[]="Narration : $narration_old to $narration";
	$update_Qry[]="narrat='$narration'";
}

if($emp_code!=$emp_code_old)
{
	$update_log[]="Employee_code : $emp_code_old to $emp_code";
	$update_Qry[]="empcode='$emp_code'";
}

if($veh_no!=$veh_no_old)
{
	$update_log[]="Vehicle_no : $veh_no_old to $veh_no";
	$update_Qry[]="vehno='$veh_no'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#loadicon').hide();
		$('#req_update_button').attr('disabled', false);
	</script>";
	exit();
}

if($pay_mode=='NEFT')
{
	$fetch_rtgs_req = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
	if(!$fetch_rtgs_req){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($fetch_rtgs_req)>0)
	{
		echo "<script>
			alert('Rtgs Payment Done. You Can\'t Edit !');
			$('#loadicon').hide();
			$('#req_update_button').attr('disabled', false);
		</script>";
		exit();
	}
}

$fetch_email = Qry($conn,"SELECT email FROM user WHERE username='$branch'");
if(!$fetch_email){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_email = fetchArray($fetch_email);

$email=$row_email['email'];

if($company=='RRPL')
{
	$debit="debit";
	$balance="balance";
}
else
{
	$debit="debit2";
	$balance="balance2";
}

if($row['chq']!=$pay_mode AND $pay_mode=='CASH')
{
	$fetch_balance= Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
		
	if(!$fetch_balance){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$row_balance=fetchArray($fetch_balance);

	$rrpl_cash=$row_balance['balance'];
	$rr_cash=$row_balance['balance2'];
		
	if($company=='RRPL' && $rrpl_cash>=$amount)
	{
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$amount)
	{
	}
	else
	{
		echo "<script>
			alert('INSUFFICIENT Balance in Company: $company.');
			$('#loadicon').hide();
			$('#req_update_button').attr('disabled', false);
		</script>";
		exit();
	}
}

StartCommit($conn);
$flag = true;

if($row['chq']==$pay_mode)
{
	$diff = $row['amt']-$amount;
	
	if($pay_mode=='NEFT')
	{
		$update_rtgs = Qry($conn,"UPDATE rtgs_fm SET totalamt='$amount',amount='$amount',acname='$ac_holder',acno='$ac_no',
		bank_name='$bank_name',ifsc='$ifsc',pan='$pan_no' WHERE fno='$frno' AND colset_d!='1'");
			
		if(!$update_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Unable to Update RTGS Payment. Vou_no: $frno.",$conn,$page_name,__LINE__);
		}
			
		$update_passbook = Qry($conn,"UPDATE passbook SET `$debit`='$amount' WHERE vou_no='$frno'");
		if(!$update_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else if($pay_mode=='CHEQUE')
	{
		$update_passbook = Qry($conn,"UPDATE passbook SET chq_no='$chq_no',`$debit`='$amount' WHERE vou_no='$frno'");
		if(!$update_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_chequebook = Qry($conn,"UPDATE cheque_book SET amount='$amount',cheq_no='$chq_no' WHERE vou_no='$frno'");
		if(!$update_chequebook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else if($pay_mode=='CASH')
	{
		if(sprintf("%.2f",$row['amt'])!=sprintf("%.2f",$amount))
		{
			$fetch_cash_id = Qry($conn,"SELECT id FROM cashbook WHERE vou_no='$frno'");
			if(!$fetch_cash_id){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($fetch_cash_id)==0)
			{
				$flag = false;
				errorLog("Cash entry not found in cashbook. Vou_no: $frno.",$conn,$page_name,__LINE__);
			}
			
			$row_cash_id = fetchArray($fetch_cash_id);
			$cash_id = $row_cash_id['id'];
			$min_cash_id = $cash_id-1;
			
			$update_cashbook = Qry($conn,"UPDATE cashbook SET `$debit`='$amount' WHERE id='$cash_id'");
			if(!$update_cashbook){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Cashbook not updated. Vou_no: $frno.",$conn,$page_name,__LINE__);
			}
			
			$update_main_balance = Qry($conn,"UPDATE user SET `$balance`=`$balance`+'$diff' WHERE username='$branch'");
			if(!$update_main_balance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Branch balance not updated. $branch.",$conn,$page_name,__LINE__);
			}
			
			$update_cashbook_amount = Qry($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$diff' WHERE id>'$min_cash_id' AND 
			user='$branch' AND comp='$company'");
			if(!$update_cashbook_amount){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}	
	else
	{
		$flag = false;
		errorLog("Invalid option selected. Payment_mode: $pay_mode.",$conn,$page_name,__LINE__);
	}
	
	$update_voucher = Qry($conn,"UPDATE mk_venf SET newdate='$vou_date',des='$exp_head_name',desid='$exp_head_id',amt='$amount',
	chq='$pay_mode',chq_no='$chq_no',chq_bnk_n='$chq_bank',neft_bank='$bank_name',neft_acname='$ac_holder',neft_acno='$ac_no',
	neft_ifsc='$ifsc',pan='$pan_no',narrat='$narration',colset_d='' WHERE vno='$frno'");
	
	if(!$update_voucher){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	if($row['chq']=='NEFT')
	{
		$delete_rtgs = Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$frno' AND colset_d!='1'");
		if(!$delete_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Unable to delete RTGS Payment. Vou_no: $frno.",$conn,$page_name,__LINE__);
		}
			
		$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
		if(!$delete_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($sys_date==$today)
		{
			$update_today_data = Qry($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$old_amount' WHERE 
			branch='$branch' AND date='$today'");
			if(!$update_today_data){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}	
	}
	else if($row['chq']=='CHEQUE')
	{
		$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
		if(!$delete_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$delete_chequebook = Qry($conn,"DELETE FROM cheque_book WHERE vou_no='$frno'");
		if(!$delete_chequebook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$update_main_balance = Qry($conn,"UPDATE user SET `$balance`=`$balance`+'$old_amount' WHERE username='$branch'");
		if(!$update_main_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$fetch_cash_id = Qry($conn,"SELECT id FROM cashbook WHERE vou_no='$frno'");
		if(!$fetch_cash_id){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$row_cash_id = fetchArray($fetch_cash_id);
		$cash_id = $row_cash_id['id'];
			
		$update_cashbook_amount = Qry($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$old_amount' WHERE id>'$cash_id' AND 
		user='$branch' AND comp='$company'");
		
		if(!$update_cashbook_amount){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$delete_cashbook = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
		if(!$delete_cashbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
		if($sys_date==$today)
		{
			$update_today_data1 = Qry($conn,"UPDATE today_data SET exp_vou=exp_vou-1,exp_vou_amount=exp_vou_amount-'$old_amount' WHERE 
			branch='$branch' AND date='$today'");
			if(!$update_today_data1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	
	if($pay_mode=='NEFT')
	{
		if($company=='RRPL')
		{
			$crn_string="RRPL-E";
		}
		else
		{
			$crn_string="RR-E";
		}
		
		$get_crn_qry = GetCRN($crn_string,$conn);
		
		if(!$get_crn_qry || $get_crn_qry==0 || $get_crn_qry=="")
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$crnnew = $get_crn_qry;
		
		$insert_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,`$debit`,timestamp) VALUES 
		('$branch','$frno','$today','$vou_date','$company','Expense_Voucher','$exp_head_name','$amount','$timestamp')");
		
		if(!$insert_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,
			type,email,crn,timestamp) VALUES ('$frno','$branch','$company','$amount','$amount','$ac_holder','$ac_no','$bank_name','$ifsc',
			'$pan_no','$today','$vou_date','EXPENSE_VOU','$email','$crnnew','$timestamp')");			

		if(!$qry_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$update_today_data_rtgs=Qry($conn,"UPDATE today_data SET neft=neft+1,neft_amount=neft_amount+'$amount' WHERE branch='$branch' 
		AND date='$today'");
		if(!$update_today_data_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else if($pay_mode=='CHEQUE')
	{
		$insert_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,chq_no,`$debit`,timestamp) 
		VALUES ('$branch','$frno','$today','$vou_date','$company','Expense_Voucher','$exp_head_name','$chq_no','$amount','$timestamp')");
		
		if(!$insert_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$insert_chequebook=Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,vou_date,amount,cheq_no,date,branch,timestamp) 
		VALUES ('$frno','Expense_Voucher','$vou_date','$amount','$chq_no','$today','$branch','$timestamp')");
		
		if(!$insert_chequebook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$fetch_balance= Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
		if(!$fetch_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$row_balance=fetchArray($fetch_balance);

		$rrpl_cash=$row_balance['balance'];
		$rr_cash=$row_balance['balance2'];
		
		if($company=='RRPL' && $rrpl_cash>=$amount)
		{
			$newbal=$rrpl_cash - $amount;
			
		}
		else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$amount)
		{
			$newbal=$rr_cash - $amount;
		}
		else
		{
			$flag = false;
			errorLog("INSUFFICIENT Balance in $company.",$conn,$page_name,__LINE__);
		}
		
		$query_update_cash = Qry($conn,"update user set `$balance`='$newbal' where username='$branch'");
		
		if(!$query_update_cash){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$cash_entry=Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit`,`$balance`,timestamp) 
		VALUES ('$branch','$today','$vou_date','$company','$frno','Expense_Voucher','$exp_head_name','$amount','$newbal','$timestamp')");
		
		if(!$cash_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	$update_today_data=Qry($conn,"UPDATE today_data SET exp_vou=exp_vou+1,exp_vou_amount=exp_vou_amount+'$amount' WHERE 
	branch='$branch' AND date='$today'");
	
	if(!$update_today_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_voucher = Qry($conn,"UPDATE mk_venf SET newdate='$vou_date',des='$exp_head_name',desid='$exp_head_id',amt='$amount',
	chq='$pay_mode',chq_no='$chq_no',chq_bnk_n='$chq_bank',neft_bank='$bank_name',neft_acname='$ac_holder',neft_acno='$ac_no',
	neft_ifsc='$ifsc',pan='$pan_no',narrat='$narration',colset_d='' WHERE id='$id'");
	
	if(!$update_voucher){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update_log_qry = Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) 
	VALUES ('$frno','Expense_Voucher','Update_voucher','$update_log','$branch','ADMIN','$timestamp')");

if(!$update_log_qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Voucher Updated Successfully !');
		document.getElementById('button2').click();
		document.getElementById('req_close_button').click();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request !');
		$('#loadicon').hide();
	</script>";
	// Redirect("Error While Processing Request.","./request_market_truck.php");
	exit();
}
?>
