<?php
require_once("./connect.php");

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

echo "<script>$('#btn_clear_pod').attr('disabled',true);</script>";

$chk_cache = Qry($conn,"SELECT id FROM cache_forfeit_fm WHERE bal_to!=''");

if(!$chk_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_cache)>0)
{
	echo "<script>
		alert('Balance paid vouchers found. Please delete them first !');
		$('#loadicon').fadeOut('slow');
		$('#btn_clear_pod').attr('disabled',false);
	</script>";
	exit();
}

$chk_cache_pod_rcvd = Qry($conn,"SELECT id FROM cache_forfeit_fm WHERE pod_count>0");

if(!$chk_cache_pod_rcvd){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_cache_pod_rcvd)>0)
{
	$pod_rcvd = "YES";
	
	$chk_claim_pods = Qry($conn,"SELECT frno FROM rcv_pod WHERE frno in(SELECT frno FROM cache_forfeit_fm WHERE pod_count>0) AND (claim_branch=='1' || claim_ho=='1')");
	
	if(!$chk_claim_pods){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_claim_pods)>0)
	{
		$row_claim_pods = fetchArray($chk_claim_pods);
		
		echo "<script>
			alert('Claim found in FM : $row_claim_pods[frno] !');
			$('#btn_clear_pod').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}
else
{
	$pod_rcvd = "NO";
}

StartCommit($conn);
$flag = true;

if($pod_rcvd=='YES')
{
	$dlt_pods = Qry($conn,"DELETE FROM rcv_pod WHERE frno in(SELECT frno FROM cache_forfeit_fm WHERE pod_count>0)");
	
	if(!$dlt_pods){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}

	$copy_pod = Qry($conn,"INSERT INTO rcv_pod(frno,veh_type,lrno,lr_id,consignor_id,branch,fm_date,fm_amount,pod_branch,
	pod_copy,pod_date,del_date,ho_rcvd,timestamp,exdate) SELECT f.frno,'MARKET',f.lrno,l.id,l.con1_id,f.branch,f.create_date,
	f2.actualf,l.branch,'pod_copy/pod_auto_rcvd.jpg','$date','$date','1','$timestamp','$date' 
	FROM freight_form_lr AS f 
	LEFT OUTER JOIN freight_form AS f2 ON f2.frno=f.frno 
	LEFT OUTER JOIN lr_sample AS l ON l.lrno=f.lrno 
	WHERE f.frno IN(SELECT frno FROM cache_forfeit_fm)");
	
	if(!$copy_pod){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
	
	$update_pod_date_fm = Qry($conn,"UPDATE freight_form_lr SET market_pod_date='$date' WHERE frno IN(SELECT frno FROM cache_forfeit_fm)");
	
	if(!$update_pod_date_fm){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}

	$update_fm_balance = Qry($conn,"UPDATE freight_form SET otherfr=baladv,totalbal='0.00',paidto=ptob,bal_date='$date',
	pto_bal_name=pto_adv_name,bal_pan=adv_pan,rtgs_bal='1',narra='Balance Forfeit.',pod='1',branch_bal=branch,forfeit_balance='1' 
	WHERE frno IN(SELECT frno FROM cache_forfeit_fm)");
	
	if(!$update_fm_balance){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
	
	$dlt_cache = Qry($conn,"DELETE FROM cache_forfeit_fm");
	
	if(!$dlt_cache){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Done !');
		window.location.href='./mark_pod_rcvd.php';
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request !');
		$('#btn_clear_pod').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	// Redirect("Error While Processing Request.","./mark_pod_rcvd.php");
	exit();
}	
?>