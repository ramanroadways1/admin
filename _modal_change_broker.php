<script type="text/javascript">
$(function() {
	$("#broker_modal_name").autocomplete({
	source: 'autofill/get_broker.php',
	// appendTo: '#appenddiv',
	select: function (event, ui) { 
		$('#broker_modal_name').val(ui.item.value);   
		$('#broker_modal_id').val(ui.item.id);      
		$('#broker_modal_mobile').val(ui.item.mobile);      
		$('#broker_modal_pan').val(ui.item.pan);      
		$('#broker_modal_addr').val(ui.item.addr);      
		return false;
	},
	change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#broker_modal_name').val("");   
			$('#broker_modal_id').val("");   
			$('#broker_modal_mobile').val("");   
			$('#broker_modal_pan').val("");   
			$('#broker_modal_addr').val("");   
			alert('Broker does not exists !');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#ChanageBrokerForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#chanage_broker_button").attr("disabled", true);
	$.ajax({
	url: "./update_broker_fm.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="ChanageBrokerForm" autocomplete="off"> 

<div class="modal fade" id="BrokerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
		
		<div class="modal-header bg-primary">
		Change Broker :
      </div>
	  
	<div class="modal-body">
			
		<div class="row">
			
			  <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Broker's Name <font color="red"><sup>*</sup></font></label>
					   <input type="text" name="broker_name" id="broker_modal_name" class="form-control" required>
                  </div>
              </div>
			  
			  <input type="hidden" id="broker_modal_id" name="id">
			  <input type="hidden" id="broker_vou_no" name="vou_no">
			  <input type="hidden" id="broker_vou_id" name="vou_id_fm">
			  
			  <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Broker's PAN <font color="red"><sup>*</sup></font></label>
					   <input id="broker_modal_pan" name="broker_pan" class="form-control" required readonly />
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Broker's Mobile <font color="red"><sup>*</sup></font></label>
					   <input id="broker_modal_mobile" class="form-control" required readonly />
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Broker's Address <font color="red"><sup>*</sup></font></label>
					   <textarea id="broker_modal_addr" class="form-control" required readonly></textarea>
                  </div>
               </div>
			   
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" id="close_broker_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="chanage_broker_button" class="btn btn-primary">Change Broker</button>
	</div>
	
		</div>	
     </div>
</div>	
</form>