<?php
require_once("./connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$qry = Qry($conn,"SELECT frno,truck_no,branch FROM freight_form_lr WHERE lrno='$lrno' AND frno like '___F%' GROUP BY frno");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<option value=''>--select an option--</option>";

if(numRows($qry) >0 )
{
	while($row = fetchArray($qry))
	{
		echo "<option value='$row[frno]'>$row[frno] : $row[truck_no]</option>";
	}
	
	echo "<script>
		$('#lrno').attr('readonly',true);
		$('#lrno').attr('onclick','');
		// $('#search_by_option').attr('disabled',true);
	</script>";
}

echo "<script>
	$('#loadicon').hide();
</script>";
exit();
?>