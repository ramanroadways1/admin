<?php
require_once("./connect.php");

$option=escapeString($conn,$_POST['option']);

if($option=='FM')
{
	$frno = escapeString($conn,strtoupper($_POST['search_value']));
	$search_var="unq_id='$frno'";
}
else if($option=='LR')
{
	$lrno = escapeString($conn,strtoupper($_POST['search_value']));
	$search_var="lrno='$lrno'";
}
else if($option=='TRUCK')
{
	$tno = escapeString($conn,strtoupper($_POST['search_value']));
	if($_POST['date_range']=='FULL'){
		$from_date="2017-01-01";
		$to_date=date("Y-m-d");
	}
	else{
		$from_date=date('Y-m-d', strtotime($_POST['date_range'], strtotime(date("Y-m-d"))));
		$to_date=date("Y-m-d");
	}
	$search_var="date BETWEEN '$from_date' AND '$to_date' AND tno='$tno'";
}
else
{
	echo "<script>
		alert('Invalid option selected !');
		window.location.href='request_own_truck.php';
	</script>";
	exit();
}

$qry=Qry($conn,"SELECT id,unq_id,trip_id,tno,lrno,SUM(amount) as diesel,date,GROUP_CONCAT(narration SEPARATOR ', ') as narration,branch 
FROM dairy.diesel WHERE $search_var GROUP BY unq_id");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)>0)
{
?>
	<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:13px;" colspan="16">Own Truck Diesel Summary :</th>
    </tr>
		<tr>    
			<th>Token_No</th>
			<th>Branch</th>
			<th>Amount</th>
			<th>TruckNo</th>
			<th>LR_Number</th>
			<th>DieselNarr.</th>
			<th>Date</th>
			<th></th>
			<th></th>
		</tr>	
	<?php
	while($row=fetchArray($qry))
	{
		echo "<tr>
				<td>$row[unq_id]</td>
				<td>$row[branch]</td>
				<td>$row[diesel]</td>
				<td>$row[tno]</td>
				<td>$row[lrno]</td>
				<td>$row[narration]</td>
				<td>".date('d-m-y',strtotime($row['date']))."</td>";
				
			echo "<input type='hidden' value='$row[unq_id]' id='vou_id$row[id]'>";
			
			if($row['trip_id']=="")
			{
			echo "
				<td>
					<button type='button' onclick=EditReq('$row[id]') class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
				</td>
				<td>
					<button type='button' onclick=DeleteReq('$row[id]') class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button>
				</td>";
			}
			else
			{
				echo "<td colspan='2' style='color:red'>e-Dairy diesel.</td>";
			}
					
			echo "</tr>";
	}
	echo "</table>";
	
	echo "<script>
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<font color='red'>No record found !<font>";
	echo "<script>
		$('#loadicon').hide();	
	</script>";
	exit();
}
?>