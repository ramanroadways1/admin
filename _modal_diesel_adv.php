<script type="text/javascript">
$(function() {  
      $("#adv_dsl_card_no").autocomplete({
			// appendTo: $("#adv_dsl_card_no").next()
			source: function(request, response) { 
			if(document.getElementById('fuel_company_adv_dsl').value!=''){
                 $.ajax({
                  url: '../b5aY6EZzK52NA8F/autofill/get_diesel_cards.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   company: document.getElementById('fuel_company_adv_dsl').value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select company first !');
				$("#adv_dsl_card_no").val('');
				$("#adv_dsl_cardno2").val('');
				$("#adv_dsl_phy_card_no").val('');
				$("#adv_dsl_rilpre").val('');
              }

              },
              select: function (event, ui) { 
               $('#adv_dsl_card_no').val(ui.item.value);   
               $('#adv_dsl_cardno2').val(ui.item.dbid);     
			   $('#adv_dsl_phy_card_no').val(ui.item.phy_card_no);
			   $("#adv_dsl_rilpre").val(ui.item.rilpre);
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#adv_dsl_card_no").val('');
				$("#adv_dsl_cardno2").val('');
				$("#adv_dsl_phy_card_no").val('');
				$("#adv_dsl_rilpre").val('');
                alert('Card number does not exist !'); 
              } 
              },
			}); 
  }); 
</script>

<?php
$get_diesel_rate_max = Qry($conn,"SELECT qty,rate,amount FROM _diesel_max_rate WHERE market_own='MARKET'");
if(!$get_diesel_rate_max){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
				
if(numRows($get_diesel_rate_max)>0)
{
	$row_dsl_max_rate = fetchArray($get_diesel_rate_max);
	$dsl_max_rate=$row_dsl_max_rate['rate'];
	$dsl_max_amount1=$row_dsl_max_rate['amount'];
	$dsl_max_qty1=$row_dsl_max_rate['qty'];
}
else
{
	$dsl_max_rate=95;
	$dsl_max_amount1=76000;
	$dsl_max_qty1=800;
}
?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#DieselAdvAddForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#add_dsl_adv_button").attr("disabled", true);
	$.ajax({
	url: "./add_fm_adv_diesel.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#adv_dsl_form_res").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="DieselAdvAddForm" autocomplete="off"> 

<div class="modal fade" id="DieselAdvAddModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
		
	<div class="modal-header bg-primary">
		Add advance diesel : <span id="dsl_modal_vou_no_html"></span>
      </div>
	  
<div class="modal-body">
	<div class="row">
			<div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Pump/Card. <font color="red"><sup>*</sup></font></label>
					   <select onchange="CardPump(this.value)" id="type1" name="pump_card" class="form-control" required>
							<option value="">Select option</option>
							<option value="CARD">CARD</option>
							<option value="OTP">OTP</option>
							<option value="PUMP">PUMP</option>
						</select>
                  </div>
            </div>
			  
			<div style="display:none" id="qty_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">QTY <font color="red"><sup>*</sup></font></label>
					   <input type="number" min="0" max="<?php echo $dsl_max_qty1; ?>" name="qty" step="any" required id="adv_dsl_qty" class="form-control" />
                  </div>
               </div>
			   
			   <div style="display:none" id="rate_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Rate <font color="red"><sup>*</sup></font></label>
					   <input type="number" max="<?php echo $dsl_max_rate; ?>" min="0" name="rate" step="any" required id="adv_dsl_rate" class="form-control" />
                  </div>
               </div>
			  
			  <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Amount <font color="red"><sup>*</sup></font></label>
					   <input min="70" max="<?php echo $dsl_max_amount1; ?>" type="number" oninput="AmountCall();" oninput="" name="amount" id="dsl_amt2" class="form-control" required />
                  </div>
               </div>
			   
			  <div style="display:none" id="pump_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Pump Name. <font color="red"><sup>*</sup></font></label>
					   <select onchange="SetPumpCode(this.value)" id="pump_name_adv_dsl" name="pump_name" class="form-control" required>
						<option value="">Select option</option>
						<?php
						$q_pump=Qry($conn,"SELECT name,code,comp FROM diesel_pump WHERE code!='' AND branch='$row[branch]' AND active='1' ORDER BY name ASC");
						
						if(numRows($q_pump)>0)
						{
							while($row_p=fetchArray($q_pump))
							{
								echo "<option value='".$row_p['code']."-".$row_p['comp']."'>$row_p[name]</option>";	
							}
						}
						?>
					</select>
                  </div>
               </div>
			   
			 <script>
			function SetPumpCode(code)
			{
				var com = code.split("-").pop();
				$('#pump_company_adv_dsl').val(com);
			}
			</script>
			
			<input type="hidden" name="pump_company" id="pump_company_adv_dsl">
			
			 <div id="fuel_div" style="display:none" class="col-md-6">
                 <div class="form-group">
                    <label class="control-label mb-1">Fuel Company. <font color="red"><sup>*</sup></font></label>
					<select id="fuel_company_adv_dsl" name="fuel_company" onchange="$('#adv_dsl_card_no').val('');$('#adv_dsl_phy_card_no').val('')" class="form-control" required>
						<option value="">Select option</option>
							<?php
							$get_fuel_comp_22 = Qry($conn,"SELECT name FROM diesel_api.dsl_company WHERE status='1' 
							ORDER by name ASC");
							
							if(!$get_fuel_comp_22){
								errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
								Redirect("Error while processing Request","./");
								exit();
							}

							while($r_f_c2 = fetchArray($get_fuel_comp_22))
							{
								echo "<option value='$r_f_c2[name]'>$r_f_c2[name]</option>";
							}
							?>
					</select>
                  </div>
             </div>
			   
			   <script>
			   function ChkPumpInput()
			   {
				  if($('#fuel_company_adv_dsl').val()=='')
				  {
					  alert('Select Fuel Company First');
					  $('#adv_dsl_card_no').val('');
					  $('#adv_dsl_cardno2').val('');
					  $('#dsl_mobile_no').val('');
					  $('#adv_dsl_phy_card_no').val('');
				  }					  
			   }
			   </script>
			   
			  <div style="display:none" id="card_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Card No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" name="card_no" oninput="ChkPumpInput();this.value=this.value.replace(/[^A-Z a-z0-9]/,'')" id="adv_dsl_card_no" class="form-control" required />
					   <input type="hidden" id="adv_dsl_cardno2" name="cardno2">
				 </div>
              </div>
			   
			  <div style="display:none" id="mobile_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Mobile No. <font color="red"><sup>*</sup></font></label>
					   <input type="text" maxlength="10" minlength="10" oninput="ChkPumpInput();this.value=this.value.replace(/[^0-9]/,'')" name="mobile_no" id="dsl_mobile_no" class="form-control" required />
				  </div>
               </div>
			   
			    <div style="display:none" id="phy_card_div" style="display:none" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Virtual Id. <font color="red"><sup>*</sup></font></label>
					   <input type="text" name="phy_card_no" id="adv_dsl_phy_card_no" class="form-control" readonly required />
				  </div>
               </div>
			   
			   <div style="display:none" id="trans_date_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Transaction Date <font color="red"><sup>*</sup></font></label>
					   <input min="<?php echo date('Y-m-d', strtotime('-2 days')) ?>" max="<?php echo date('Y-m-d'); ?>" type="date" name="trans_date" id="trans_date_diesel" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" required />
				  </div>
               </div>
			   
			   <div id="adv_dsl_form_res"></div>
			   
			    <input type="hidden" name="rilpre" id="adv_dsl_rilpre">
				
			    <input type="hidden" name="vou_no" id="dsl_modal_vou_no_value">
			    <input type="hidden" name="vou_no_id" id="dsl_modal_vou_id_value">
	</div>
</div>
		
	<div class="modal-footer">
		<button type="button" onclick="$('#get_button').attr('disabled',false);$('#get_button')[0].click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="add_dsl_adv_button" class="btn btn-primary">Add diesel</button>
	</div>
		</div>	
     </div>
 </div>	
 
 </form>
 
 <script type="text/javascript">
$(function() {
$("#adv_dsl_qty,#adv_dsl_rate").on("keydown keyup blur change input", CalcDiesel);
function CalcDiesel() {
	if($("#adv_dsl_qty").val()==''){
		var qty = 0;
	}else{
		var qty = Number($("#adv_dsl_qty").val());
	}

	if($("#adv_dsl_rate").val()==''){
		var rate = 0;
	}else{
		var rate = Number($("#adv_dsl_rate").val());
	}
	
	$("#dsl_amt2").val(Math.round(qty * rate).toFixed(2));
}});
				 
function AmountCall() 
{
	if($("#adv_dsl_qty").val()=='' && $("#adv_dsl_rate").val()==''){
		$("#adv_dsl_qty").focus()
		$("#dsl_amt2").val('');
	}
	else
	{
		if($("#adv_dsl_rate").val()=='' || $("#adv_dsl_rate").val()<60)
		{
			$("#adv_dsl_rate").val((Number($("#dsl_amt2").val()) / Number($("#adv_dsl_qty").val())).toFixed(2));
		}
		else
		{
			$("#adv_dsl_qty").val((Number($("#dsl_amt2").val()) / Number($("#adv_dsl_rate").val())).toFixed(2));
		}
	}	
}
</script>

<script>
function CardPump(card)
{
	$("#adv_dsl_qty").val('')
	$("#adv_dsl_rate").val('')
	$("#dsl_amt2").val('')
	
	$('#fuel_company_adv_dsl').val('');
	$('#adv_dsl_card_no').val("");
	$('#adv_dsl_cardno2').val("");
	$('#dsl_mobile_no').val("");
	$('#adv_dsl_phy_card_no').val("");
	
	if(card=='')
	{
		$('#pump_div').hide();	
		$('#card_div').hide();	
		$('#mobile_div').hide();
		$('#fuel_div').hide();	
		$('#qty_div').hide();	
		$('#rate_div').hide();	
		$('#phy_card_div').hide();	
		$('#trans_date_div').hide();	
		
		$('#adv_dsl_card_no').attr('required',true);	
		$('#dsl_mobile_no').attr('required',true);	
		$('#pump_name_adv_dsl').attr('required',true);	
		$('#fuel_company_adv_dsl').attr('required',true);	
		$('#pump_company_adv_dsl').attr('required',true);
		$('#adv_dsl_rate').attr('required',true);	
		$('#adv_dsl_qty').attr('required',true);	
		$('#adv_dsl_phy_card_no').attr('required',true);
		$('#trans_date_diesel').attr('required',true);
		
		$('#dsl_amt2').attr('oninput','');
	}
	else if(card=='CARD' || card=='OTP')
	{
		if(card=='CARD')
		{
			$('#mobile_div').hide();
			$('#dsl_mobile_no').attr("required",false);

			$('#card_div').show();
			$('#adv_dsl_card_no').attr("required",true);
			$('#adv_dsl_card_no2').attr("required",true);

			$('#phy_card_div').show();	
			$('#adv_dsl_phy_card_no').attr('required',true);
		}
		else
		{
			$('#mobile_div').show();
			$('#dsl_mobile_no').attr("required",true);

			$('#card_div').hide();
			$('#adv_dsl_card_no').attr("required",false);
			$('#adv_dsl_card_no2').attr("required",false);

			$('#phy_card_div').hide();	
			$('#adv_dsl_phy_card_no').attr('required',false);
		}
		
		$('#fuel_div').show();	
		$('#fuel_company_adv_dsl').attr('required',true);
		
		$('#pump_div').hide();	
		$('#qty_div').hide();	
		$('#rate_div').hide();	
		$('#trans_date_div').hide();	
		
		$('#pump_name_adv_dsl').attr('required',false);	
		$('#pump_company_adv_dsl').attr('required',false);	
		$('#adv_dsl_rate').attr('required',false);	
		$('#adv_dsl_qty').attr('required',false);	
		$('#trans_date_diesel').attr('required',false);		
		
		$('#dsl_amt2').attr('oninput','');
	}
	else if(card=='PUMP')
	{
		$('#pump_div').show();	
		$('#qty_div').show();	
		$('#rate_div').show();	
		$('#trans_date_div').show();	
		
		$('#card_div').hide();	
		$('#fuel_div').hide();	
		$('#mobile_div').hide();
		$('#phy_card_div').hide();	
		
		$('#adv_dsl_rate').attr('required',true);	
		$('#adv_dsl_qty').attr('required',true);	
		$('#pump_name_adv_dsl').attr('required',true);	
		$('#pump_company_adv_dsl').attr('required',true); 
		$('#trans_date_diesel').attr('required',true);
		
		$('#adv_dsl_card_no').attr('required',false);	
		$('#adv_dsl_card_no2').attr('required',false);	
		$('#fuel_company_adv_dsl').attr('required',false);	
		$('#dsl_mobile_no').attr('required',false);	
		$('#adv_dsl_phy_card_no').attr('required',false);
		
		$('#dsl_amt2').attr('oninput','AmountCall()');
	}
}
</script>