<?php
require_once('./connect2.php');
	
$frno=mysqli_real_escape_string($conn2,strtoupper($_POST['frno']));
$tno=mysqli_real_escape_string($conn2,strtoupper($_POST['tno']));

if($frno=='' || $tno=='')
{
	echo "<script>
		alert('Unable to fetch Truck No or Token Number.');
	</script>";
	exit();
}

$fetch_data = mysqli_query($conn2,"SELECT trip_id,tno,trans_id FROM diesel WHERE unq_id='$frno'");
if(!$fetch_data)
{
	echo mysqli_error($conn2);
	exit();
}

if(mysqli_num_rows($fetch_data)==0)
{
	echo "<script>
		alert('No result found.');
	</script>";
	exit();
}

$row=mysqli_fetch_array($fetch_data);

$data_old="VouId : $frno, TruckNo : $row[tno], TransId : $row[trans_id], TripId : $row[trip_id]";

if($row['trip_id']!='' || $row['trans_id']!='')
{
	echo "<script>
		alert('Diesel Reqiested From E-Diary. You Can\'t change Truck Number.');
	</script>";
	exit();
}

	$update=mysqli_query($conn2,"UPDATE diesel SET tno='$tno' WHERE unq_id='$frno' AND trip_id='' AND trans_id=''");
	
	if(!$update)
	{
		echo mysqli_error($conn2);
		exit();
	}
	
	if(mysqli_affected_rows($conn2)==0)
	{
		echo "<script>
			alert('Unable to Update Truck Number !');
		</script>";
		exit();
	}
	else
	{
	
	$update2=mysqli_query($conn2,"UPDATE diesel_entry SET tno='$tno' WHERE unq_id='$frno'");
	if(!$update2)
	{
		echo mysqli_error($conn2);
		exit();
	}
	
	$update_log=mysqli_query($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
		('$frno','OWN_DIESEL_REQ','EDIT_TRUCK_NO','$data_old, New Truck No : $tno','$row[branch]','ADMIN','$timestamp')");
	
		echo "<script>
			alert('Truck Number Updated Successfully !');
			window.location.href='./request_own_truck.php';
		</script>";
		exit();
	}
?>