<?php
require_once("./connect.php"); 

$frno=escapeString($conn,strtoupper($_POST['vou_no']));
$id=escapeString($conn,strtoupper($_POST['vou_id_fm']));
$weight=escapeString($conn,strtoupper($_POST['weight']));
$known=escapeString($conn,strtoupper($_POST['known']));
$rate=escapeString($conn,strtoupper($_POST['rate']));
$freight=escapeString($conn,strtoupper($_POST['actual_freight']));

$timestamp = date("Y-m-d H:i:s");

if($known=='ZERO')
{
	if($rate>0 || $freight>0)
	{
		echo "<script>
			alert('Rate or Freight is not valid !');
			$('#loadicon').hide();
			$('#chanage_freight_button').attr('disabled',false);
		</script>";
		exit();
	}
}
else
{
	if($rate<=0 || $freight<=0)
	{
		echo "<script>
			alert('Rate or Freight is not valid !');
			$('#loadicon').hide();
			$('#chanage_freight_button').attr('disabled',false);
		</script>";
		exit();
	}
}

if($weight<=0)
{
	echo "<script>
		alert('Rate is not valid !');
		$('#loadicon').hide();
		$('#chanage_freight_button').attr('disabled',false);
	</script>";
	exit();
}

$calc_freight = round($rate*$weight);

if($freight!=$calc_freight)
{
	echo "<script>
		alert('Freigh not verified. please check !');
		$('#loadicon').hide();
		$('#chanage_freight_button').attr('disabled',false);
	</script>";
	exit();
}

$get_fm = Qry($conn,"SELECT frno,oid,newdate,newtds as loading,dsl_inc,gps,adv_claim,newother,tds,totalf,totaladv,rtgsneftamt,
ptob as adv_to,paidto as bal_to,adv_pan,totalbal,colset_adv,colset_d_adv,adv_download FROM freight_form WHERE id='$id'");

if(!$get_fm){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_fm)==0){
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_fm = fetchArray($get_fm);

$loading = $row_fm['loading'];
$dsl_inc = $row_fm['dsl_inc'];
$gps = $row_fm['gps'];
$adv_claim = $row_fm['adv_claim'];
$newother = $row_fm['newother'];
$tds = $row_fm['tds'];

$fm_date = $row_fm['newdate'];
$adv_download = $row_fm['colset_d_adv'];
$adv_download_time = $row_fm['adv_download'];

$adv_to = $row_fm['adv_to'];
$bal_to = $row_fm['bal_to'];
$total_advance = $row_fm['totaladv'];
$total_balance = $row_fm['totalbal'];
$rtgs_amount = $row_fm['rtgsneftamt'];

if($row_fm['frno']!=$frno)
{
	echo "<script>
		alert('Freight memo not verified !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$adv_pan = $row_fm['adv_pan'];

if($adv_to!="" AND $tds>0)
{
	$get_tds_value = Qry($conn,"SELECT tds_value FROM tds_value WHERE pan_letter='".substr($adv_pan,3,1)."'");

	if(!$get_tds_value){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($get_tds_value)==0)
	{
		echo "<script>
			alert('Owner not verified !');
			window.location.href='./fm_view.php';
		</script>";
		exit();
	}
	$row_tds_value = fetchArray($get_tds_value);
	$tds_amount = sprintf("%0.2f",ceil($freight*$row_tds_value['tds_value']/100));
}
else
{
	$tds_amount = 0;
}

if($tds>0 AND $adv_download=="1")
{
	// echo "<script>
			// alert('Can\'t update Tds. Voucher downloaded by head-office !');
			// $('#loadicon').hide();
		// </script>";
	// exit();
			
	if($adv_download_time!='' AND $adv_download_time!=0)
	{
		$date_parts = explode('-',$adv_download_time);
		$date_parts_fm = explode('-',$fm_date);
		
		$download_year = $date_parts[0];
		$download_month = $date_parts[1];
		$download_day = $date_parts[2];
		
		// $fm_year = $date_parts_fm[0];
		// $fm_month = $date_parts_fm[1];
		// $fm_day = $date_parts_fm[2];
		
		if(date("Y")==$download_year)
		{
			if($download_month<date("m"))
			{
				if(strtotime(date("Y-m-d"))>strtotime(date("Y-m-")."06"))
				{
					echo "<script>
						alert('Can\'t update Tds. Voucher downloaded by head-office after TDS filling date !');
						$('#loadicon').hide();
					</script>";
					exit();
				}
			}
			else if($download_month>date("m"))
			{
				echo "<script>
					alert('Advance download date error !');
					$('#loadicon').hide();
				</script>";
				exit();
			}
		}
		else
		{
			echo "<script>
				alert('Can\'t update Tds. Voucher downloaded by head-office !');
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	else
	{
		echo "<script>
			alert('Downloaded date not found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

$delete_cache = Qry($conn,"DELETE FROM lr_pre_admin WHERE frno='$frno'");

if(!$delete_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$copy_to_cache = Qry($conn,"INSERT INTO lr_pre_admin(frno,lr_id,wt12,weight,ratepmt,actualf,timestamp) SELECT frno,id,wt12,weight,
ratepmt,actualf,'$timestamp' FROM freight_form_lr WHERE frno='$frno'");

if(!$copy_to_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($bal_to!='')
{
	echo "<script>
		alert('Balance Paid. Reset Balance First !');
		$('#loadicon').hide();
		$('#chanage_freight_button').attr('disabled',false);
	</script>";
	exit();
}

// if($adv_to=='')
// {
	// echo "<script>
		// alert('Advance Pending !');
		// $('#loadicon').hide();
		// $('#chanage_freight_button').attr('disabled',false);
	// </script>";
	// exit();
// }

if($freight<$total_advance)
{
	echo "<script>
		alert('Freight Amount is less than Advance Amount. Please Check !');
		$('#loadicon').hide();
		$('#chanage_freight_button').attr('disabled',false);
	</script>";
	exit();
}

$get_cache = Qry($conn,"SELECT sum(wt12) as total_act,SUM(weight) AS total_charge,SUM(actualf) as freight FROM lr_pre_admin 
WHERE frno='$frno'");

if(!$get_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_cache_sum = fetchArray($get_cache);

$update_log=array();

if(sprintf("%.3f",$row_cache_sum['total_charge'])!=sprintf("%.3f",$weight))
{
	$update_log[]="Weight : $row_cache_sum[total_charge] to $weight";
}

if(sprintf("%.2f",$row_cache_sum['freight'])!=sprintf("%.2f",$freight))
{
	$update_log[]="Freight : $row_cache_sum[freight] to $freight";
}

$update_log = implode(', ',$update_log); 

if($update_log=="")
{
	$delete_cache = Qry($conn,"DELETE FROM lr_pre_admin WHERE frno='$frno'");
	if(!$delete_cache){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	echo "<script>
		alert('Nothing to update !');
		$('#chanage_freight_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$total_aw = $row_cache_sum['total_act'];

StartCommit($conn);
$flag = true;

$fetch_data = Qry($conn,"SELECT id,wt12 FROM lr_pre_admin WHERE frno='$frno'");
if(!$fetch_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
	if(numRows($fetch_data)==0)
	{
		$flag = false;
		errorLog("Unable to fetch LR Data. VouNo : $frno.",$conn,$page_name,__LINE__);
	}
	else if(numRows($fetch_data)==1)
	{
		$update_data=Qry($conn,"UPDATE lr_pre_admin SET weight='$weight',ratepmt='$rate',fix_rate='$known',actualf='$freight' 
		WHERE frno='$frno'");
		if(!$update_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		while($data_row=fetchArray($fetch_data))
		{
			$db_id=$data_row['id'];
			$db_aw=$data_row['wt12'];
			$x=($db_aw*100)/$total_aw;
			$cw_db=sprintf("%.3f",((($weight)*$x)/100));
			$update_frt=sprintf("%.2f",($cw_db*$rate));

			$update_data=Qry($conn,"UPDATE lr_pre_admin SET weight='$cw_db',ratepmt='$rate',fix_rate='$known',actualf='$update_frt' 
			WHERE id='$db_id'");
			if(!$update_data){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}

		$check_sum=Qry($conn,"SELECT SUM(weight) as lr_weight,SUM(actualf) as freight1 FROM lr_pre_admin WHERE frno='$frno'");
		if(!$check_sum){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

		$row_sum=fetchArray($check_sum);

		if(sprintf("%.3f",$weight)!=sprintf("%.3f",$row_sum['lr_weight']))
		{
			$weight_diff=sprintf("%.3f",($weight-$row_sum['lr_weight']));
		}
		else
		{
			$weight_diff=0;
		}
	
		if($freight!=$row_sum['freight1'])
		{
			$freight_diff=sprintf("%.2f",($freight-$row_sum['freight1']));
		}
		else
		{
			$freight_diff=0;
		}

		$update_data_lr=Qry($conn,"UPDATE lr_pre_admin SET weight=weight+'$weight_diff',actualf=actualf+'$freight_diff' WHERE frno='$frno' 
		ORDER BY wt12 DESC, id DESC LIMIT 1");
		
		if(!$update_data_lr){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

$select_weight_sum=Qry($conn,"SELECT sum(weight) as total_charge FROM lr_pre_admin WHERE frno='$frno'");
if(!$select_weight_sum){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row1_sum=fetchArray($select_weight_sum);

if(sprintf("%.3f",$row1_sum['total_charge']) != sprintf("%.3f",$weight))
{
	$weight_from_lr=sprintf("%.3f",($row1_sum['total_charge']));
	$wt_enter=sprintf("%.3f",($weight));
	$flag = false;
	errorLog("Charge weight Error. LR Weight: $weight_from_lr, Entered : $wt_enter.",$conn,$page_name,__LINE__);
}

$update_freight_form_lr = Qry($conn,"UPDATE freight_form_lr t1 
        INNER JOIN lr_pre_admin t2 
             ON t1.id = t2.lr_id
SET t1.weight = t2.weight,t1.fix_rate = t2.fix_rate,t1.ratepmt = t2.ratepmt,t1.actualf = t2.actualf 
WHERE t1.id = t2.lr_id");

if(!$update_freight_form_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_cache_lr = Qry($conn,"DELETE FROM lr_pre_admin WHERE frno='$frno'");

if(!$delete_cache_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($adv_to!='')
{
	$total_freight_new = ($freight+$loading+$dsl_inc)-($gps+$adv_claim+$newother+$tds_amount);
	$balance_new = $total_freight_new-$total_advance;
	
	if($balance_new<0)
	{
		$flag = false;
		errorLog("Balance is negative: $frno.",$conn,$page_name,__LINE__);
	}
	
	$update_Qry = Qry($conn,"UPDATE freight_form SET weight='$weight',actualf='$freight',tds='$tds_amount',totalf='$total_freight_new',
	baladv='$balance_new',colset_adv=0,colset_d_adv=0,adv_download='' WHERE id='$id'");
}
else
{
	$update_Qry = Qry($conn,"UPDATE freight_form SET weight='$weight' WHERE id='$id'");
}

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($rtgs_amount>0)
{
	$update_freight_in_rtgs = Qry($conn,"UPDATE rtgs_fm SET totalamt='$freight' WHERE fno='$row_fm[frno]'");
	
	if(!$update_freight_in_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$row_fm[frno]','FM_UPDATE','FREIGHT_UPDATE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Freight Updated Successfully !');
		$('#get_button').attr('disabled',false);
		$('#chanage_freight_button').attr('disabled',false);
		$('#close_freight_button').click();
		$('#get_button').click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fm_view.php");
	exit();
}
?>