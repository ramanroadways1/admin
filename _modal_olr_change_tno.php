<script type="text/javascript">
$(function() {
	$("#tno_modal").autocomplete({
	source: 'autofill/get_own_vehicle.php',
	// appendTo: '#appenddiv',
	select: function (event, ui) { 
		$('#tno_modal').val(ui.item.value);   
		return false;
	},
	change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_modal').val("");   
			alert('Vehicle number does not exists !');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
	 
$(document).ready(function (e) {
$("#UpdateTruckNumberForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#truck_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_tno_own_truck_form.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="UpdateTruckNumberForm" autocomplete="off"> 
<div class="modal fade" id="TruckModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
	   
	    <div class="modal-header bg-primary">
			Update Vehicle Number :
      </div>
	  
		<div class="modal-body">
	
	<div class="row">
		
		<div class="col-md-12">
             <div class="form-group">
				<label class="control-label mb-1">Truck Number <font color="red"><sup>*</sup></font></label>
				<input type="text" id="tno_modal" name="tno" class="form-control" required>
             </div>
        </div>
	
	</div>
</div>

	<input type="hidden" name="frno" id="frno_modal">
	<input type="hidden" name="id" id="truck_no_change_vou_id">
	
	<div class="modal-footer">
		<button type="button" id="close_truck_update_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="truck_update_button" class="btn btn-primary">Update</button>
	</div>
</div>	
     </div>
 </div>	
</form>