<?php
require_once("./connect.php");

$frno=escapeString($conn,strtoupper($_POST['vou_no']));
$id=escapeString($conn,strtoupper($_POST['vou_id_fm']));
$timestamp = date("Y-m-d H:i:s");

$get_fm = Qry($conn,"SELECT frno,bid,ptob as adv_to,paidto as bal_to FROM freight_form WHERE id='$id'");
if(!$get_fm){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_fm)==0){
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_fm = fetchArray($get_fm);

if($row_fm['frno']!=$frno)
{
	echo "<script>
		alert('Freight memo not verified !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$get_ext_broker = Qry($conn,"SELECT name FROM mk_broker WHERE id='$row_fm[bid]'");
if(!$get_ext_broker){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_ext_broker)==0){
	echo "<script>
		alert('Broker not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_ext_broker = fetchArray($get_ext_broker);

$ext_broker = $row_ext_broker['name'];
$adv_to = $row_fm['adv_to'];
$bal_to = $row_fm['bal_to'];

$broker_name=escapeString($conn,strtoupper($_POST['broker_name']));
$broker_pan=escapeString($conn,strtoupper($_POST['broker_pan']));
$broker_id=escapeString($conn,strtoupper($_POST['id']));

if($bal_to!='' AND $bal_to=='BROKER')
{
	echo "<script>
		alert('Balance Paid to Broker. Reset Balance First !');
		$('#loadicon').hide();
		$('#chanage_broker_button').attr('disabled',false);
	</script>";
	exit();
}

if($adv_to!='' AND $adv_to=='BROKER')
{
	echo "<script>
		alert('Advance Paid to Broker. Reset Advance First !');
		$('#loadicon').hide();
		$('#chanage_broker_button').attr('disabled',false);
	</script>";
	exit();
}

$update_log=array();
$update_Qry=array();

if($broker_id!=$row_fm['bid'])
{
	$update_log[]="Broker : $ext_broker($row_fm[bid]) to $broker_name($broker_id)";
	$update_Qry[]="bid='$broker_id'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#chanage_broker_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"UPDATE freight_form SET $update_Qry WHERE id='$id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$row_fm[frno]','FM_UPDATE','BROKER_UPDATE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Broker Updated Successfully !');
		$('#get_button').attr('disabled',false);
		$('#chanage_broker_button').attr('disabled',false);
		$('#close_broker_button').click();
		$('#get_button').click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fm_view.php");
	exit();
}
?>