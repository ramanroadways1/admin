<?php
require_once("./connect.php");

$frno = $_POST['frno'];

$qry_adv_diesel = mysqli_query($conn,"SELECT SUM(disamt) as adv_diesel,dsl_by,dcard,dcom FROM diesel_fm WHERE fno='$frno' AND 
	type='ADVANCE' GROUP BY dcard");
	
if(!$qry_adv_diesel)
{
	echo mysqli_error($conn);
	echo "<script type='text/javascript'>
			$('#loadicon').hide();
		</script>";	
	exit();	
}	

	if(mysqli_num_rows($qry_adv_diesel)>0)
	{
	?>
		<table id="adv_diesel_table" class="table table-bordered" style="font-family:Verdana;font-size:12px;">
				  
				 <tr>
                    <th class="bg-warning" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="8">Advance Diesel : 
					&nbsp; <button onclick="AddDiesel();" class="btn btn-sm btn-danger" type="button">Add Diesel</button></th>
                 </tr>
		<?php
		echo "
		<tr>
			<th>Card</th>
			<th>Card No</th>
			<th>Amount</th>
			<th>FuelCompany</th>
		</tr>	
		";
		
		while($row_adv_diesel = mysqli_fetch_array($qry_adv_diesel))
		{
		  echo '
		  <tr>
			<td>'.$row_adv_diesel["dsl_by"].'</td>
			<td>'.$row_adv_diesel["dcard"].'</td>
			<td>'.$row_adv_diesel["adv_diesel"].'</td>
			<td>'.$row_adv_diesel["dcom"].'</td>
		 </tr>		
		  ';	
		}
		echo "</table>";
	}
	else
	{
		echo "<script type='text/javascript'>
				alert('Something went wrong unbale to fetch Advance Diesel !');
				window.location.href='./fm_view.php';
		</script>";	
		exit();
	}	
?>