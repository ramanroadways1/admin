<?php
require_once './connect.php'; 

$vou_id=escapeString($conn,strtoupper($_POST['vou_id']));

$get_lane = Qry($conn,"SELECT from1,to1,from_id,to_id FROM freight_form WHERE id='$vou_id'");

if(!$get_lane){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_lane)==0){
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_lane = fetchArray($get_lane);

echo "<script>
	$('#master_lane_from').val('$row_lane[from1]');
	$('#master_lane_to').val('$row_lane[to1]');
	$('#master_lane_from_id').val('$row_lane[from_id]');
	$('#master_lane_to_id').val('$row_lane[to_id]');
	$('#master_lane_vou_id').val('$vou_id');
	$('#modal_Master_Lane').modal();
	$('#loadicon').hide();
</script>";
?>

		