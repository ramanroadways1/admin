<?php
require_once("./connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$qry = Qry($conn,"SELECT id,company,branch,lrno,lr_type,wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignor,
consignee,con2_addr,from_id,to_id,con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,
bill_rate,bill_amt,t_type,do_no,shipno,invno,item,item_id,item_name,plant,articles,goods_desc,goods_value,con1_gst,con2_gst,
crossing,cancel,break,diesel_req,download FROM lr_sample_pending WHERE lrno='$lrno'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script type='text/javascript'>
		alert('Invalid LR No entered or LR has been closed !');
		window.location.href='./lr_entry.php';
	</script>";	
	exit();
}

if($numrows>1)
{
	errorLog("Duplicate LR No Found : $lrno.",$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Duplicate LR No Found !');
		window.location.href='./lr_entry.php';
	</script>";	
	exit();
}

$row = fetchArray($qry);

$lr_branch = $row['branch'];

$chk_coal = Qry($conn,"SELECT z FROM user WHERE username='$lr_branch'");

if(!$chk_coal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numrows($chk_coal)==0)
{
	errorLog("Branch not found !",$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Branch not found !');
		window.location.href='./lr_entry.php';
	</script>";	
	exit();
}

$row_branch_chk = fetchArray($chk_coal);

if($row_branch_chk['z']=="1")
{
	$is_coal_branch = "YES";
}
else
{
	$is_coal_branch = "NO";
}

$chk_open_lr = Qry($conn,"SELECT id FROM open_lr WHERE party_id='$row[con1_id]'");

if(!$chk_open_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_open_lr)>0)
{
	echo "<script>
		$('#company_auto_div').hide();
		$('#company_div').show();
		$('#company').attr('required',true);
		$('#company_auto').attr('required',false);
		
		$('#company').val('$row[company]');
		$('#company_auto').val('');
		$('#open_lr').val('1');
	</script>";
}
else
{
	echo "<script>
		$('#company_auto_div').show();
		$('#company_div').hide();
		$('#company').attr('required',false);
		$('#company_auto').attr('required',true);
		
		$('#company').val('');
		$('#company_auto').val('$row[company]');
		$('#open_lr').val('0');
	</script>";
}

if($row['con1_id']=='10')
{
	echo "<script>
		$('#po_no').attr('readonly',false);
		$('#bank').attr('readonly',false);
		$('#sold_to_pay').attr('readonly',false);
		$('#lr_name').attr('readonly',false);
		$('#con2_addr').attr('readonly',false);
		$('#freight_type').attr('readonly',false);
		$('#f_type1').attr('disabled',false);
		$('#f_type2').attr('disabled',false);
	</script>";
}
else
{
	echo "<script>
		$('#po_no').attr('readonly',true);
		$('#bank').attr('readonly',true);
		$('#sold_to_pay').attr('readonly',true);
		$('#lr_name').attr('readonly',true);
		$('#con2_addr').attr('readonly',true);
		$('#freight_type').attr('readonly',true);
		$('#f_type1').attr('disabled',true);
		$('#f_type2').attr('disabled',true);
	</script>";
}

if($row['con1_id']=='56' || $row['con1_id']=='675')
{
	echo "<script>
		$('#dest_zone').attr('readonly',false);
	</script>";
}
else
{
	echo "<script>
		$('#dest_zone').attr('readonly',true);
	</script>";
}

if($row['lr_type']=='MARKET')
{
	echo "<script>
		$('#div_market').show();
		$('#div_own').hide();
		$('#tno_market').attr('required',true);
		$('#tno_own').attr('required',false);
		$('#tno_market').val('$row[truck_no]');
		$('#wheeler_market_truck').val('$row[wheeler]');
		$('#wheeler_own_truck').val('');
	</script>";
}
else if($row['lr_type']=='OWN')
{
	echo "<script>
		$('#div_market').hide();
		$('#div_own').show();
		$('#tno_market').attr('required',false);
		$('#tno_own').attr('required',true);
		$('#tno_own').val('$row[truck_no]');
		$('#wheeler_own_truck').val('$row[wheeler]');
		$('#wheeler_market_truck').val('');
	</script>";
}
else
{
	echo "<script>
		alert('Invalid LR Type defined !');
		window.location.href='./lr_entry.php';
	</script>";
	exit();
}

echo "<script>
		$('#branch').val('$row[branch]');
		$('#lr_type').val('$row[lr_type]');
		$('#vehile_type_db').val('$row[lr_type]');
		$('#vehile_no_db').val('$row[truck_no]');
		$('#wheeler_db').val('$row[wheeler]');
		$('#lr_date').val('$row[date]');
		$('#sys_date').val('$row[create_date]');
		$('#from').val('$row[fstation]');
		$('#from_id').val('$row[from_id]');
		$('#to').val('$row[tstation]');
		$('#to_id').val('$row[to_id]');
		$('#dest_zone').val('$row[dest_zone]');
		$('#dest_zone_id').val('$row[zone_id]');
		$('#consignor').val('$row[consignor]');
		$('#con1_gst').val('$row[con1_gst]');
		$('#consignor_id').val('$row[con1_id]');
		$('#consignee').val('$row[consignee]');
		$('#con2_gst').val('$row[con2_gst]');
		$('#consignee_id').val('$row[con2_id]');
		$('#do_no').val('$row[do_no]');
		$('#ship_no').val('$row[shipno]');
		$('#inv_no').val('$row[invno]');
		$('#po_no').val('$row[po_no]');
		$('#act_wt').val('$row[wt12]');
		$('#chrg_wt').val('$row[weight]');
		$('#gross_wt').val('$row[gross_wt]');
		$('#item1').val('$row[item]');
		$('#item_id').val('$row[item_id]');
		$('#item_name').val('$row[item_name]');
		
		$('#bank').val('$row[bank]');
		$('#sold_to_pay').val('$row[sold_to_pay]');
		$('#lr_name').val('$row[lr_name]');
		$('#con2_addr').val('$row[con2_addr]');
		
		$('#articles').val('$row[articles]');
		$('#goods_value').val('$row[goods_value]');
		
		$('#b_rate').val('$row[bill_rate]');
		$('#b_amt').val('$row[bill_amt]');
		$('#t_type').val('$row[t_type]');
		$('#freight_type').val('$row[freight_type]');
		$('#goods_desc').val('$row[goods_desc]');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled',false);
		$('#lr_delete_button').attr('disabled',false);
	</script>";
	
$flag = false;
$error_msg = "";
	
if($row['break']>0)
{
	$flag = true;
	$error_msg.="This is breaking LR You Can not Edit !";
}

if($row['cancel']==1)
{
	$flag = true;
	$error_msg.="LR marked as cancelled ! You can not edit.";
}

if($row['crossing']!='')
{
	$flag = true;
	$error_msg.="You can not edit this LR. LR Mapped with FM or OLR.";
}

if($row['diesel_req']>0)
{
	// if(numRows($chk_open_lr)>0)
	// {
		// echo "<script>
			// $('#company_$row[company]').attr('disabled',true);
		// </script>";
	// }
	echo "<script>
		$('#veh_type_$row[lr_type]').attr('disabled',true);
		$('#tno_market').attr('readonly',true);
		$('#tno_own').attr('readonly',true);
	</script>";
}

if($row['download']>0)
{
	$flag = true;
	$error_msg.="You can not edit this LR. LR Downloaded by head-office.";
}

if($is_coal_branch=='YES')
{
	echo "<script>
		$('#lr_sub').html('COAL LR');
		$('#lr_sub').attr('disabled',true);
	</script>";
}

if($flag)
{
	echo "<script>
		$('#error_msg').html('$error_msg');
		$('#error_msg').show();
		$('#lr_sub').attr('disabled',true);
		$('#lr_delete_button').attr('disabled',true);
	</script>";
	exit();	
}	
?>