<?php
require_once("./connect.php");

$party_name = escapeString($conn,$_POST['party_name']);
$id = escapeString($conn,$_POST['party_id']);
$narration = escapeString($conn,$_POST['narration']);
$timestamp = date("Y-m-d H:i:s");

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE _zero_freight_party SET is_active='0',update_narration='$narration',update_timestamp='$timestamp' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_type,section,edit_desc,edit_by,timestamp) VALUES ('$id','Zero_Freight_Party',
'Party_Disabled','$narration','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Party Disabled Successfully !');
		window.location.href='./zero_freight_parties.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./zero_freight_parties.php");
	exit();
}	
?>