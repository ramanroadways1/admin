<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$id = escapeString($conn,strtoupper($_POST['id']));
$timestamp=date("Y-m-d H:i:s");

$get_fm = Qry($conn,"SELECT frno,branch,branch_user,truck_no,bid,did,oid,from1,to1,weight,ptob,paidto,timestamp,gps_id 
FROM freight_form WHERE id='$id'");
if(!$get_fm){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_fm)==0)
{
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_fm = fetchArray($get_fm);

$fm_branch = $row_fm['branch'];

if($row_fm['gps_id']!="0")
{
	echo "<script>
		alert('Can\'t delete voucher mapped with gps device !');
		$('#loadicon').hide();
		$('#delete_fm_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['frno']!=$frno)
{
	echo "<script>
		alert('Freight memo not verified !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

if($row_fm['paidto']!='')
{
	echo "<script>
		alert('Balance Paid. Reset Freight Memo Balance First !');
		$('#loadicon').hide();
		$('#delete_fm_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['ptob']!='')
{
	echo "<script>
		alert('Advance Paid. Reset Freight Memo Advance First !');
		$('#loadicon').hide();
		$('#delete_fm_button').attr('disabled',false);
	</script>";
	exit();
}
	
$fetch_lr_count = Qry($conn,"SELECT id FROM freight_form_lr WHERE lrno IN(SELECT lrno FROM freight_form_lr WHERE frno='$frno') 
AND frno!='$frno'");
if(!$fetch_lr_count){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}	
		
if(numRows($fetch_lr_count)>0)
{
	echo "<script>
		alert('More than One Freight Memo or Own Truck Form Found. Contact Sytem-Admin !');
		$('#loadicon').hide();
		$('#delete_fm_button').attr('disabled',false);
	</script>";
	exit();
}
	
$select_lrno = Qry($conn,"SELECT lrno FROM freight_form_lr WHERE frno='$frno' AND branch='$fm_branch'");
if(!$select_lrno){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$lrnos = array();
$lrnos_store = array();
		
while($row_lrno=fetchArray($select_lrno))
{
	$lrnos[] = "'".$row_lrno['lrno']."'";
	$lrnos_store[] = $row_lrno['lrno'];
}
		
$lrnos=implode(',', $lrnos);
$lrnos_store=implode(',', $lrnos_store);
	
$update_log = "Truck_no: $row_fm[truck_no]($row_fm[oid]), Branch: $row_fm[branch], Branch_User: $row_fm[branch_user], Broker: $row_fm[bid], 
Driver: $row_fm[did], From-To: $row_fm[from1]-$row_fm[to1], Weight: $row_fm[weight], Timestamp: $row_fm[timestamp], LR_Nos: $lrnos_store.";
	
StartCommit($conn);
$flag = true;
	
$update_lr = Qry($conn,"UPDATE lr_sample SET crossing='' WHERE lrno IN($lrnos) AND branch='$fm_branch'");
if(!$update_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Crossing Update failed. $lrnos_store.",$conn,$page_name,__LINE__);
}

$copy_to_pending = Qry($conn,"INSERT INTO lr_sample_pending(lr_id,company,branch,branch_user,lrno,lr_type,wheeler,date,create_date,
truck_no,fstation,tstation,dest_zone,consignor,consignor_before,bill_to,bill_to_id,consignee,con2_addr,from_id,to_id,con1_id,con2_id,
zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,weight2,bill_rate,bill_amt,t_type,do_no,shipno,invno,item,
item_id,item_name,plant,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing,cancel,break,diesel_req,nrr,
downloadid,downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus) SELECT id,company,branch,branch_user,
lrno,lr_type,wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignor,consignor_before,bill_to,bill_to_id,consignee,
con2_addr,from_id,to_id,con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,weight2,bill_rate,
bill_amt,t_type,do_no,shipno,invno,item,item_id,item_name,plant,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,
crossing,cancel,break,diesel_req,nrr,downloadid,downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus 
FROM lr_sample WHERE lrno IN($lrnos) AND branch='$fm_branch'");

if(!$copy_to_pending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("LR copy failed. $lrnos_store.",$conn,$page_name,__LINE__);
}

$delete_fm_lr=Qry($conn,"DELETE FROM freight_form_lr WHERE frno='$frno'");
if(!$delete_fm_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
$delete_fm=Qry($conn,"DELETE FROM freight_form WHERE id='$id'");
if(!$delete_fm){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_pod=Qry($conn,"DELETE FROM rcv_pod WHERE frno='$frno'");
if(!$delete_pod){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
	('$id','$row_fm[frno]','FM_UPDATE','FM_DELETE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
		
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
		
	echo "<script>
		alert('Freight Memo Deleted Successfully !');
		window.location.href='./fm_view.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fm_view.php");
	exit();
}			
?>