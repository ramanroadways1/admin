<?php
require_once("./connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$qry = Qry($conn,"SELECT b.id,b.branch,b.act_wt,b.chrg_wt,b.crossing,b.timestamp,u.name 
FROM lr_break as b 
LEFT OUTER JOIN emp_attendance AS u ON u.code=b.branch_user 
WHERE b.lrno='$lrno'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script type='text/javascript'>
		alert('Invalid LR No entered !');
		window.location.href='./lr_breaking.php';
	</script>";	
	exit();
}

$sn=1;
while($row = fetchArray($qry))
{
	echo "<tr>
		<td>$sn</td>
		<td>$row[branch]</td>
		<td>$row[name]</td>
		<td>$row[act_wt]</td>
		<td>$row[chrg_wt]</td>
		<td>".date("d/m/y H:i A",strtotime($row["timestamp"]))."</td>
		<td><button onclick='Edit($row[id])' style='font-size:12px;' type='button' class='btn btn-sm btn-primary'>Edit</button></td>
		<td><button id='deleteButton$row[id]' onclick='Delete($row[id])' style='font-size:12px;' type='button' class='btn btn-sm btn-danger'>Delete</button></td>
	</tr>";
$sn++;	
}
	echo "<script>
		$('#loadicon').hide();
	</script>";
?>