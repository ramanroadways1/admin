<?php
require_once("./connect.php");

$branch = escapeString($conn,$_POST['branch']);

$qry=Qry($conn,"SELECT id,user,tdvid,newdate,date,truckno,dname,amt,mode,hisab_vou,colset_d FROM mk_tdv WHERE tdvid='$frno' 
ORDER BY id DESC");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('No record Found !');
		$('#loadicon').hide();	
	</script>";
	exit();
}
	
$row=fetchArray($qry);
	
$mode1=$row['mode'];
	
if($mode1=='CASH' || $mode1=='HAPPAY')
{
	$type11="dairy.cash";
}
else if($mode1=='CHEQUE')
{
	$type11="dairy.cheque";
}
else if($mode1=='NEFT')
{
	$type11="dairy.rtgs";
}
else
{
	echo "<script>
		alert('Invalid option selected !');
		window.location.href='truck_voucher.php';
	</script>";
	exit();
}
		
if($vou_in=='MAIN')
{
	$e_diary_chk = Qry($conn,"SELECT id FROM $type11 WHERE vou_no='$frno'");
	if(!$e_diary_chk){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($e_diary_chk)>0)
	{
		echo "<script>
			alert('Voucher Belongs to e-diary Change your option !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
else
{
	$e_diary_chk = Qry($conn,"SELECT id FROM $type11 WHERE vou_no='$frno'");
	if(!$e_diary_chk){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($e_diary_chk)==0)
	{
		echo "<script>
			alert('Voucher not found in d-diary !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

}
else
{
	echo "<script>
		alert('Invalid option selected !');
		window.location.href='truck_voucher.php';
	</script>";
	exit();
}
?>
<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="16">Truck Vouchers :</th>
    </tr>
		<tr>    
			<th>Branch</th>
			<th>Vou_Id</th>
			<th>Vou_Date</th>
			<th>Created_On</th>
			<th>Truck_No</th>
			<th>Driver_Name</th>
			<th>Amount</th>
			<th>Payment_Mode</th>
			<th>Done</th>
			<th></th>
			<th></th>
		</tr>	
	<?php
		if($row['colset_d']==1)
		{
			$done="<font color='green'>OK</font>";
		}
		else
		{
		  $done="";	
		}
		
		echo "<tr>
				<td>$row[user]</td>
				<td>$row[tdvid]</td>
				<td>".date('d-m-y',strtotime($row['newdate']))."</td>
				<td>".date('d-m-y',strtotime($row['date']))."</td>
				<td>$row[truckno]</td>
				<td>$row[dname]</td>
				<td>$row[amt]</td>
				<td>$row[mode]</td>
				<td>$done</td>";
			
			if($row['hisab_vou']=='2')
			{
				echo "<td>
					<button type='button' onclick=EditVou('$row[tdvid]_$vou_in') class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
				</td>
				<td>
					<button type='button' onclick=DeleteVou('$row[tdvid]_$vou_in') class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button>
				</td>";
			}
			else
			{
				echo "<td colspan='2'><font color='red'>Hisab_Vou</font></td>";
			}
		
	echo "</tr></table>
	<script>
			$('#loadicon').hide();
	</script>";
?>