<?php
require_once './connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

if(isset($_POST['type'])) {
		
	$party_type = escapeString($conn,($_POST['type']));	
	$allowedExtensions = array("csv");

	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
					
				if(!in_array($ext, $allowedExtensions))
				{
					echo "<script>
						alert('Please upload .csv format !');
						$('#lr_sub').attr('disabled',false);
						$('#loadicon').hide();
					</script>";
					exit();
				}

$clear_old_data = Qry($conn,"DELETE FROM add_party_temp_table");			
	
if(!$clear_old_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
	
		$fileName = $_FILES["file"]["tmp_name"];
			
			if ($_FILES["file"]["size"] > 0)
			{
				$file = fopen($fileName, "r");
				
StartCommit($conn);
$flag = true;

				while(($column = fgetcsv($file, 10000, ",")) !== FALSE)
				{
					if($column[0]!='' AND strlen($column[0])>10)
					{
						$party_name = strtoupper(trim(quoted_printable_decode(preg_replace("/[[:blank:]]+/"," ",$column[0]))));
						$state_name = strtoupper(trim(quoted_printable_decode(preg_replace("/[[:blank:]]+/"," ",$column[1]))));
						$pincode = strtoupper(trim(quoted_printable_decode(preg_replace("/[[:blank:]]+/"," ",$column[2]))));
						
						$insert = Qry($conn,"INSERT INTO add_party_temp_table(type,name,state,pincode) VALUES ('$party_type','$party_name','$state_name','$pincode')");
						
						if(!$insert){
							$flag = false;
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						}
					}
				}
				
				if($flag)
				{
					MySQLCommit($conn);
					closeConnection($conn);
					
					echo "<script>$('#lr_sub').hide();LoadData();</script>";
					exit();
				}
				else
				{
					MySQLRollBack($conn);
					closeConnection($conn);
					echo "<script>alert('Error..');$('#lr_sub').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
					exit();
				}
			}
		
}

echo "<script>
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled',false);
	</script>";
exit();
?> 