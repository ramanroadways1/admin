<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$actual_weight = escapeString($conn,strtoupper($_POST['actual_weight']));
$charge_weight = escapeString($conn,strtoupper($_POST['charge_weight']));

$qry = Qry($conn,"SELECT lrno,act_wt,chrg_wt,crossing FROM lr_break WHERE id='$id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script type='text/javascript'>
		alert('Record not found !');
		window.location.href='./lr_breaking.php';
	</script>";	
	exit();
}

$row = fetchArray($qry);

if($row['crossing']!='')
{
	echo "<script type='text/javascript'>
		alert('LR attached to freight memo or OLR. Delete Voucher First !');
		$('#edit_save').attr('disabled',true);
	</script>";	
	exit();
}

$update_log=array();
$update_Qry=array();

if($actual_weight!=$row['act_wt'])
{
	$update_log[]="Act_Wt : $row[act_wt] to $actual_weight";
	$update_Qry[]="act_wt='$actual_weight'";
}

if($charge_weight!=$row['chrg_wt'])
{
	$update_log[]="Chrg_Wt : $row[chrg_wt] to $charge_weight";
	$update_Qry[]="chrg_wt='$charge_weight'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#edit_save').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"UPDATE lr_break SET $update_Qry WHERE id='$id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$row[lrno]','LR_BREAKING','LR_EDIT','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Updated Successfully !');
		$('#closeBtn').click();
		$('#get_recordBtn').click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./lr_breaking.php");
	exit();
}	
?>