<?php
require_once("./connect.php");

$vou_no=escapeString($conn,strtoupper($_POST['vou_no']));
$pod_type=escapeString($conn,strtoupper($_POST['pod_type']));

if($pod_type=='MARKET')
{
	$fetch_pods=Qry($conn,"SELECT f.id as fm_id,f.frno,f.branch as fm_branch,f.lrno,f.fstation,f.tstation,f.truck_no,f.crossing,
	f2.actualf as freight_amount,f2.totaladv as freight_advance,l.branch,l.date as lr_date,l.consignor,l.con1_id,l.pod_rcv_date,
	l.id as lr_id,l.item_id,p.pod_copy,p.del_date,p.timestamp 
	FROM freight_form_lr AS f 
	LEFT OUTER JOIN lr_sample AS l ON l.lrno=f.lrno 
	LEFT OUTER JOIN freight_form AS f2 ON f2.frno=f.frno 
	LEFT OUTER JOIN rcv_pod AS p ON p.frno=f.frno AND p.lr_id=l.id 
	WHERE f.frno='$vou_no' GROUP BY f.lrno ORDER BY f.id ASC");
	
	if(numRows($fetch_pods)==0)
	{
		echo "
		<div style='font-family:Verdana;color:red' class='col-md-12'>	
              <div class='form-group'>
				NO RESULT FOUND..
			</div>
		</div>
		<script>
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	?>
	<div class="col-md-12 table-responsive" style="overflow:auto">
		<table class="table table-bordered" style="font-size:12px;">
          
		  <tr>
             <th class="bg-success" style="font-size:13px;" colspan="14">
				Market Vehicle PODs Summary : <?php echo $vou_no; ?>
			 </th>
          </tr>
		   
		   <tr>
			   <th>Vou_no</th>
			   <th>FM_Branch</th>
			   <th>LR_no</th>
			   <th>Truck_no</th>
			   <th>LR_date</th>
			   <th>Locations</th>
			   <th>Pod date</th>
			   <th>Delivery date</th>
			   <th>POD_Branch</th>
			   <th>Pod_Copy</th>
			   <th>TimeStamp</th>
			   <th>Remove</th>
			   <th>Re Upload</th>
	       </tr>	
	<?php
		while($row=fetchArray($fetch_pods))
		{
			if($row['del_date']==0) {
				$del_date = "NULL";
			}
			else{
				$del_date = date('d/m/y',strtotime($row['del_date']));
			}

			if($row['pod_rcv_date']==0){
				$pod_date = "NULL";
			}
			else{
				$pod_date = date('d/m/y',strtotime($row['pod_rcv_date']));
			}
			
			$timestamp = date('d/m/y H:i A',strtotime($row['timestamp']));
			
		echo "<tr>
				<td>$row[frno]</td>
				<td>$row[fm_branch]</td>
				<td>$row[lrno]</td>
				<td>$row[truck_no]</td>
				<td>$row[lr_date]</td>
				<td>$row[fstation] to $row[tstation]</td>
				<td>$pod_date</td>
				<td>$del_date</td>
				<td>$row[branch]</td>
			  <td>";
			  $var=explode(',',$row['pod_copy']);
			   $i = 0;
			   foreach($var as $files)
			   {
				 $i++;
?>
<div class="modal fade" id="ViewCopy_<?php echo $row['fm_id']."_".$i; ?>" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <div class="modal-header"> 
				<h5 class="modal-title">POD Copy : </h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
           <div class="modal-body">
               <iframe height="500px" width="100%" src="<?php echo "https://rrpl.online/b5aY6EZzK52NA8F/$files"; ?>"></iframe> 
           </div> 
		</div>
	</div>
</div>
<?php				 
				 echo "<a id='' data-toggle='modal' style='color:blue' href='#ViewCopy_$row[fm_id]_$i' target='_blank'>File: $i</a><br>";
			   }
				echo "
			   </td>
				<td>$timestamp</td>
				<td><button id='btn_remove_$row[frno]_$row[lrno]' onclick=RemovePod('$row[frno]','$row[lrno]','MARKET','0') class='btn btn-sm btn-danger'>Remove</button></td>
				<td><button onclick=ReUpload('$row[frno]''$row[lrno]','MARKET') class='btn btn-sm btn-warning'>Replace</button></td>
			</tr>";
		}
		
		echo "</table></div>
		
		<script>
			// document.getElementById('menu_button2').click();
			// $('#button_chk').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
}		
else
{
	$fetch_pods=Qry($conn,"SELECT p.id,p.tno,p.trip_no,p.frno,p.lrno,p.branch as pod_branch,p.pod_date,p.del_date,p.pod_copy,
	p.timestamp 
	FROM rcv_pod AS p 
	WHERE p.frno='$vou_no' ORDER BY p.id ASC");
	
	if(numRows($fetch_pods)==0)
	{
		echo "
		<div style='font-family:Verdana;color:red' class='col-md-12'>	
              <div class='form-group'>
				NO RESULT FOUND..
			</div>
		</div>
		<script>
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	?>
	<div class="col-md-12 table-responsive" style="overflow:auto">
		<table class="table table-bordered" style="font-size:12px;">
          
		  <tr>
             <th class="bg-success" style="font-size:13px;" colspan="14">
				OWN Truck POD Summary Vou_No : <?php echo $vou_no; ?>
			 </th>
          </tr>
		   
		   <tr>
			   <th>Vou_no</th>
			   <th>Truck_no</th>
			   <th>Trip_no</th>
			   <th>LR_no</th>
			   <th>Pod_Branch</th>
			   <th>Pod_Date</th>
			   <th>Delivery date</th>
			   <th>POD Copy</th>
			   <th>TimeStamp</th>
			   <th>Remove</th>
			   <th>Re Upload</th>
	       </tr>	
	<?php
		while($row=fetchArray($fetch_pods))
		{
			if($row['del_date']==0) {
				$del_date = "NULL";
			}
			else{
				$del_date = date('d/m/y',strtotime($row['del_date']));
			}

			if($row['pod_date']==0){
				$pod_date = "NULL";
			}
			else{
				$pod_date = date('d/m/y',strtotime($row['pod_date']));
			}
			
			$timestamp = date('d/m/y H:i A',strtotime($row['timestamp']));
			
		echo "<tr>
				<td>$row[frno]</td>
				<td>$row[tno]</td>
				<td>$row[trip_no]</td>
				<td>$row[lrno]</td>
				<td>$row[pod_branch]</td>
				<td>$pod_date</td>
				<td>$del_date</td>
			  <td>";
			  $var=explode(',',$row['pod_copy']);
			   $i = 0;
			   foreach($var as $files)
			   {
				 $i++;
?>
<div class="modal fade" id="ViewCopy_<?php echo $row['id']."_".$i; ?>" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <div class="modal-header"> 
				<h5 class="modal-title">POD Copy : </h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
           <div class="modal-body">
               <iframe height="500px" width="100%" src="<?php echo "https://rrpl.online/diary/close_trip/$files"; ?>"></iframe> 
           </div> 
		</div>
	</div>
</div>
<?php				 
				 echo "<a id='' data-toggle='modal' style='color:blue' href='#ViewCopy_$row[id]_$i' target='_blank'>File: $i</a><br>";
			   }
				echo "
			   </td>
				<td>$timestamp</td>
				<td><button id='btn_remove_$row[frno]_$row[lrno]' onclick=RemovePod('$row[frno]','$row[lrno]','OWN','$row[id]') class='btn btn-sm btn-danger'>Remove</button></td>
				<td><button onclick=ReUpload('$row[frno]''$row[lrno]','OWN') class='btn btn-sm btn-warning'>Replace</button></td>
			</tr>";
		}
		
		echo "</table></div>
		
		<script>
			// document.getElementById('menu_button2').click();
			// $('#button_chk').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
}
?>