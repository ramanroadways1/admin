<?php
require_once('./connect.php');
	
$frno=escapeString($conn,strtoupper($_POST['frno']));
$vou_type=escapeString($conn,strtoupper($_POST['vou_type']));
$id=escapeString($conn,strtoupper($_POST['id']));

if($vou_type=='')
{
	echo "<script>
			alert('Invalid Voucher Type Defined $vou_type!');
			window.location.href='truck_voucher.php';
		</script>";
	exit();
}

$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$fetch_vou = Qry($conn,"SELECT user,date,newdate,truckno,company,amt,mode,chq_no,chq_bank,ac_name,ac_no,bank,ifsc,
hisab_vou,colset_d,timestamp FROM mk_tdv WHERE id='$id'");
if(!$fetch_vou){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_vou)==0)
{
	echo "<script>
		alert('Voucher not found !');
		window.location.href='./truck_voucher.php';
	</script>";
	exit();
}

$row=fetchArray($fetch_vou);

if($row['hisab_vou']=="1")
{
	echo "<script>
		alert('You can\'t delete hisab voucher !');
		// window.location.href='./truck_voucher.php';
		$('#loadicon').hide();
	</script>";
	exit();
}

$company = $row['company'];
$branch = $row['user'];
$old_amount = $row['amt'];
$pay_mode = $row['mode'];
$sys_date = $row['date'];
$truck_no = $row['truckno'];

if($pay_mode=='CASH')
{
	$type11='cash';
	$extra_var=",";
}
else if($pay_mode=='CHEQUE')
{
	$type11='cheque';
	$extra_var=", Chq_no: $row[chq_no], Chq_bank: $row[chq_bank],";
}
else if($pay_mode=='NEFT')
{
	$type11='rtgs';
	$extra_var=", Ac_holder: $row[ac_name], Ac_no: $row[ac_no], Bank_ifsc: $row[bank]-$row[ifsc],";
}
else
{
	echo "<script>
			alert('Invalid payment type : $pay_mode selected !');
			window.location.href='truck_voucher.php';
	</script>";
	exit();
}

$old_data="Table_id : $id, VouId : $frno, TruckNo : $truck_no, Company : $company, Branch : $branch, Amount : $old_amount, 
PaymentMode : $pay_mode $extra_var SystemDate : $sys_date, VouDate : $row[newdate], TimeStamp : $row[timestamp].";

if($vou_type=='DIARY')
{
	$fetch_trip_id = Qry($conn,"SELECT trip_id,trans_id FROM dairy.$type11 WHERE vou_no='$frno'");
	if(!$fetch_trip_id){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($fetch_trip_id)==0)
	{
		echo "<script>
			alert('Unable to fetch Transaction Id $frno!');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_trans_id22 = fetchArray($fetch_trip_id);
		
	$trans_id_db=$row_trans_id22['trans_id'];
	$trip_id_db=$row_trans_id22['trip_id'];
		
	$chk_driver_book = Qry($conn,"SELECT id,driver_code,tno FROM dairy.driver_book WHERE trans_id='$trans_id_db'");
	if(!$chk_driver_book){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_driver_book)==0)
	{
		echo "<script>
			alert('Transaction entry not found in driver book !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$get_running_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id_db'");
	if(!$get_running_trip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($get_running_trip)==0)
	{
		echo "<script>
			alert('Running Trip not found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
		
	$row_driver_code = fetchArray($chk_driver_book);
		
	$tno_db=$row_driver_code['tno'];
	$driver_code_db=$row_driver_code['driver_code'];
	$driver_book_id=$row_driver_code['id'];
		
	if($truck_no!=$tno_db)
	{
		echo "<script>
				alert('Truck Number mis match !');
				window.location.href='truck_voucher.php';
		</script>";
		exit();
	}
}
	
	if($company=='RRPL')
	{
		$debit="debit";	
		$balance="balance";	
	}
	else
	{
		$debit="debit2";
		$balance="balance2";
	}
	
	if($pay_mode=='NEFT')
	{
		$fetch_rtgs_req = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
		if(!$fetch_rtgs_req){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($fetch_rtgs_req)>0)
		{
			echo "<script>
				alert('Rtgs Payment Done. You Can\'t Delete this voucher !');
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	else if($pay_mode=='CASH')
	{
		$fetch_cash_id = Qry($conn,"SELECT id,user FROM cashbook WHERE vou_no='$frno'");
		if(!$fetch_cash_id){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($fetch_cash_id)==0)
		{
			echo "<script>
				alert('Voucher not found in cashbook !');
				$('#loadicon').hide();
			</script>";
			exit();
		}
			
		$row_cash_id = fetchArray($fetch_cash_id);
		
		if($row_cash_id['user']!=$branch)	
		{
			echo "<script>
				alert('Voucher branch not matching with cashbook branch !');
				$('#loadicon').hide();
			</script>";
			exit();
		}			
		
		$cash_id = $row_cash_id['id'];
	}

StartCommit($conn);
$flag = true;
	
if($vou_type=='DIARY')
{
	$delete_diary_vou = Qry($conn,"DELETE FROM dairy.$type11 WHERE trans_id='$trans_id_db' AND vou_no='$frno'");
	if(!$delete_diary_vou){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}		
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Ediary voucher not deleted. $frno",$conn,$page_name,__LINE__);
	}
	
	$min_id_d_book=$driver_book_id-1;
	
	$update_driver_book_all = Qry($conn,"UPDATE dairy.driver_book SET balance=balance-'$old_amount' WHERE id>'$min_id_d_book' AND 
	driver_code='$driver_code_db' AND tno='$truck_no'");
	if(!$update_driver_book_all){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
				
	$delete_entry_driver_book = Qry($conn,"DELETE FROM dairy.driver_book WHERE id='$driver_book_id' AND trans_id='$trans_id_db'");
	if(!$delete_entry_driver_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
				
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Driver book entry not deleted. VoucherNo: $frno, Trans_id: $trans_id_db.",$conn,$page_name,__LINE__);
	}
				
	$update_driver_balance = Qry($conn,"UPDATE driver_up SET amount_hold=amount_hold+'$old_amount' WHERE 
	code='$driver_code_db' AND tno='$truck_no' AND down=0 ORDER BY id DESC LIMIT 1");
	if(!$update_driver_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Driver balance not updated. DriverCode: $driver_code_db, Truck_no: $truck_no.",$conn,$page_name,__LINE__);
	}
				
	$update_trip_amount = Qry($conn,"UPDATE trip SET `$type11`=`$type11`-'$old_amount' WHERE id='$trip_id_db'");
	if(!$update_trip_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Trip not updated. trip_id: $trip_id_db, Truck_no: $truck_no.",$conn,$page_name,__LINE__);
	}
}
	
if($pay_mode=='CASH')
{
	$update_main_balance = Qry($conn,"UPDATE user SET `$balance`=`$balance`+'$old_amount' WHERE username='$branch'");
	if(!$update_main_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Branch balance not updated. $frno",$conn,$page_name,__LINE__);
	}
		
	$update_cashbook_amount = Qry($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$old_amount' WHERE id>'$cash_id' AND 
	user='$branch' AND comp='$company'");
	if(!$update_cashbook_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete_cashbook = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
	if(!$delete_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Unable to delete voucher entry. $frno",$conn,$page_name,__LINE__);
	}
}
else if($pay_mode=='NEFT')
{
	$delete_rtgs = Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$frno' AND colset_d!='1'");
	if(!$delete_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
			
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Rtgs voucher not deleted. $frno",$conn,$page_name,__LINE__);
	}
			
	$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
	if(!$delete_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
			
	if($sys_date==$today)
	{
		$update_today_data_neft=Qry($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$old_amount' WHERE 
		branch='$branch' AND date='$today'");
		if(!$update_today_data_neft){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}
else if($pay_mode=='CHEQUE')
{
	$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
	if(!$delete_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$delete_chequebook = Qry($conn,"DELETE FROM cheque_book WHERE vou_no='$frno'");
	if(!$delete_chequebook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$flag = false;
	errorLog("Invalid payment mode: $pay_mode.--> $frno.",$conn,$page_name,__LINE__);
}
		
if($sys_date==$today)
{
	$update_today_data=Qry($conn,"UPDATE today_data SET truck_vou=truck_vou-1,truck_vou_amount=truck_vou_amount-'$old_amount' WHERE 
	branch='$branch' AND date='$today'");
	if(!$update_today_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
				
$delete_vou = Qry($conn,"DELETE FROM mk_tdv WHERE id='$id' AND hisab_vou!='1'");	
if(!$delete_vou){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_log=Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) 
VALUES ('$id','$frno','Truck_Voucher','Vou_delete','$old_data','$branch','ADMIN','$timestamp')");

if(!$update_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Voucher Deleted Successfully.');
		window.location.href='./truck_voucher.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request !');
		$('#loadicon').hide();
	</script>";
	// Redirect("Error While Processing Request.","./request_market_truck.php");
	exit();
}
	
		
?>