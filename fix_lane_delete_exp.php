<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$timestamp = date("Y-m-d H:i:s");

if($id=='')
{
	echo "<script>
		alert('Expense not found !!');
		$('#dlt_exp_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT lane_id,exp_name,exp_amount FROM dairy.fix_lane_exp WHERE id='$id'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_record)==0)
{
	echo "<script>
		alert('No record found !!');
		$('#dlt_exp_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row = fetchArray($chk_record);

$exp_amount = $row['exp_amount'];
$exp_name = $row['exp_name'];
$lane_id = $row['lane_id'];

StartCommit($conn);
$flag = true;

$dlt_record = Qry($conn,"DELETE FROM dairy.fix_lane_exp WHERE id='$id'");

if(!$dlt_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,edit_by,timestamp) VALUES 
('$lane_id','By Lane Id','FIX_LANE','DELETE_EXP','Exp_name: $exp_name, Exp_amount: $exp_amount.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
		alert('Deleted Successfully !!');
		$('#dlt_exp_btn_$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fix_lane.php");
	exit();
}
?>