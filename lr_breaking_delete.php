<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));

$qry = Qry($conn,"SELECT lrno,crossing FROM lr_break WHERE id='$id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script type='text/javascript'>
		alert('Record not found !');
		window.location.href='./lr_breaking.php';
	</script>";	
	exit();
}

$row = fetchArray($qry);

$lrno = $row['lrno'];

if($row['crossing']!='')
{
	echo "<script type='text/javascript'>
		alert('LR attached to freight memo or OLR. Delete Voucher First !');
		$('#deleteButton$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";	
	exit();
}

$get_values = Qry($conn,"SELECT * FROM lr_break WHERE id='$id'");

if(!$get_values){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$log_record = array();

while($row_data = fetchArray($get_values))
{
    foreach($row_data as $key => $value)
    {
		$log_record[]=$key."->".$value;
    }
}

$log_record = implode(', ',$log_record);

StartCommit($conn);
$flag = true;

$deleteQry = Qry($conn,"DELETE FROM lr_break WHERE id='$id'");

if(!$deleteQry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$lrno','LR_BREAKING','LR_DELETE','$log_record','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Record Deleted Successfully !');
		$('#get_recordBtn').click();
		$('#loadicon').hide();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./lr_breaking.php");
	exit();
}	
?>