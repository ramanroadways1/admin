<?php
require_once("./connect.php");

$frno = $_POST['frno'];

$qry_bal_diesel = mysqli_query($conn,"SELECT SUM(disamt) as bal_diesel,dsl_by,dcard,dcom FROM diesel_fm WHERE fno='$frno' AND 
	type='BALANCE' GROUP BY dcard");
	
if(!$qry_bal_diesel)
{
	echo mysqli_error($conn);
	echo "<script type='text/javascript'>
			$('#loadicon').hide();
		</script>";	
	exit();	
}
	
	if(mysqli_num_rows($qry_bal_diesel)>0)
	{
		?>
	 <table id="bal_diesel_table" class="table table-bordered" style="font-family:Verdana;font-size:12px;">
			<tr>
                 <th class="bg-success" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="8">Balance Diesel :</th>
             </tr>	
		<?php
		echo '
		<tr>
			<th>Card/Pump</th>
			<th>Card No</th>
			<th>Amount</th>
			<th>FuelCompany</th>
		</tr>	
		';
		
		while($row_bal_diesel = mysqli_fetch_array($qry_bal_diesel))
		{
		  echo "
		  <tr>
			<td>$row_bal_diesel[dsl_by]</td>
			<td>$row_bal_diesel[dcard]</td>
			<td>$row_bal_diesel[bal_diesel]</td>
			<td>$row_bal_diesel[dcom]</td>
		 </tr>		
		  ";	
		}
		echo "</table>";
	}
	else
	{
		echo "<script type='text/javascript'>
				alert('Something went wrong unbale to fetch Balance Diesel !');
				window.location.href='./fm_view.php';
		</script>";	
		exit();
	}	
?>