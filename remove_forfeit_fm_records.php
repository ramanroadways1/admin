<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));

echo "<script>$('#RemoveBtn$id').attr('disabled',true);</script>";

$qry = Qry($conn,"DELETE FROM cache_forfeit_fm WHERE id='$id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
		$('#tr_id_$id').attr('class','bg-warning');
		$('#RemoveBtn$id').html('Removed');
		$('#loadicon').fadeOut('slow');
	</script>"; 
exit();
?>