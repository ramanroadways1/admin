<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));

$qry = Qry($conn,"SELECT b.id,b.branch,b.lrno,b.act_wt,b.chrg_wt,b.crossing,b.timestamp,u.name 
FROM lr_break AS b 
LEFT OUTER JOIN emp_attendance AS u ON u.code=b.branch_user 
WHERE b.id='$id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script type='text/javascript'>
		alert('Record not found !');
		window.location.href='./lr_breaking.php';
	</script>";	
	exit();
}

$row = fetchArray($qry);

if($row['crossing']!='')
{
	echo "<script type='text/javascript'>
		alert('LR attached to freight memo or OLR. Delete Voucher First !');
		$('#edit_save').attr('disabled',true);
		$('#loadicon').hide();
	</script>";	
	exit();
}

	echo "<script>
		$('#edit_id').val('$row[id]');
		$('#edit_lrno').val('$row[lrno]');
		$('#edit_branch').val('$row[branch]');
		$('#edit_branch_user').val('$row[name]');
		$('#edit_actual').val('$row[act_wt]');
		$('#edit_charge').val('$row[chrg_wt]');
		$('#edit_save').attr('disabled',false);
		$('#EditModal').modal();
		$('#loadicon').hide();
	</script>";
?>