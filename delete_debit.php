<?php
require_once('./connect.php');
require_once('./connect2.php');

$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$id = mysqli_real_escape_string($conn,($_POST['id']));

$qry=mysqli_query($conn,"SELECT debit_by,section,to_branch,branch,company,amount,date FROM debit WHERE id='$id'");

if(mysqli_num_rows($qry)==0)
{
	echo "<script>
			alert('Unable to fetch record !');
			window.location.href='./debit.php';
		</script>";
		exit();
}

$row=mysqli_fetch_array($qry);

$cr_to_branch = $row['to_branch'];
$branch = $row['branch'];
$company = $row['company'];
$amount_old = $row['amount'];
$pay_mode = $row['debit_by'];
$section = $row['section'];
$trans_date = $row['date'];

if($pay_mode!='CASH')
{
	echo "<script>
			alert('There is an problem with Payment Mode. Contact System Admin !');
			window.location.href='./debit.php';
		</script>";
	exit();
}

if($company=='RRPL')
{
	$debit_col="debit";
	$credit_col="credit";
	$balance_col="balance";
	
	if($section=='ITR')
	{
		$credit_col="credit2";
	}
}
else
{
	$debit_col="debit2";
	$credit_col="credit2";
	$balance_col="balance2";
	
	if($section=='ITR')
	{
		$credit_col="credit";
	}
}

if($section=='HO')
{
	$fetch_row = mysqli_query($conn,"SELECT id FROM cashbook WHERE date='$trans_date' AND vou_type='DEBIT-HO' AND user='$branch' 
	AND `$debit_col`='$amount_old'");

	if(!$fetch_row)
	{
		echo mysqli_error($conn);exit();
	}

	$num_rows=mysqli_num_rows($fetch_row);

	if($num_rows==0)
	{
		echo "<script>
				alert('No entry found in Cashbook. 77 !');
				window.location.href='./debit.php';
			</script>";
		exit();
	}
	else if($num_rows>1)
	{
		echo "<script>
				alert('More than one Cash Entry Found for same result. Contact System Admin. 85 !');
				window.location.href='./debit.php';
			</script>";
		exit();
	}
	else
	{
		$row_fetch_id = mysqli_fetch_array($fetch_row);
		$cash_id = $row_fetch_id['id'];
	}

}
else if($section=='DEBIT-BRANCH')
{
	$dr_col="DEBIT-BRANCH";
	$cr_col="CREDIT-BRANCH";
	
	$dr_col_cash="DEBIT-BRANCH";
	$cr_col_cash="CREDIT-BRANCH";
	
	$cr_branch=$cr_to_branch;
}
else if($section=='ITR')
{
	$dr_col="ITR";
	$cr_col="ITR";
	
	$dr_col_cash="DEBIT-ITR";
	$cr_col_cash="CREDIT-ITR";
	
	$cr_branch=$branch;
}


if($section!='HO')
{
	$fetch_row = mysqli_query($conn,"SELECT id FROM cashbook WHERE date='$trans_date' AND vou_type='$dr_col_cash' AND user='$branch' 
	AND `$debit_col`='$amount_old'");

	if(!$fetch_row)
	{
		echo mysqli_error($conn);exit();
	}

	$num_rows=mysqli_num_rows($fetch_row);

	if($num_rows==0)
	{
		echo "<script>
				alert('No entry found in Cashbook 133 !');
				window.location.href='./debit.php';
			</script>";
		exit();
	}
	else if($num_rows>1)
	{
		echo "<script>
				alert('More than one Cash Entry Found for same result. Contact System Admin. 141 !');
				window.location.href='./debit.php';
			</script>";
		exit();
	}
	else
	{
		$row_fetch_id = mysqli_fetch_array($fetch_row);
		$cash_id = $row_fetch_id['id'];
	}

	$fetch_row_credit = mysqli_query($conn,"SELECT id FROM credit WHERE date='$trans_date' AND section='$cr_col' AND 
	branch='$cr_branch' AND amount='$amount_old'");

	if(!$fetch_row_credit)
	{
		echo mysqli_error($conn);exit();
	}

	$num_rows_credit=mysqli_num_rows($fetch_row_credit);

	if($num_rows_credit==0)
	{
		echo "<script>
				alert('No entry found in Credit. 165 !');
				window.location.href='./debit.php';
			</script>";
		exit();
	}
	else if($num_rows_credit>1)
	{
		echo "<script>
				alert('More than one Entry Found for same result in Credit. Contact System Admin. 173 !');
				window.location.href='./debit.php';
			</script>";
		exit();
	}
	else
	{
		$row_fetch_id_credit = mysqli_fetch_array($fetch_row_credit);
		$credit_id = $row_fetch_id_credit['id'];
	}

	$fetch_row2 = mysqli_query($conn,"SELECT id FROM cashbook WHERE date='$trans_date' AND vou_type='$cr_col_cash' AND user='$cr_branch' 
	AND `$credit_col`='$amount_old'");
	
	if(!$fetch_row2)
	{
		echo mysqli_error($conn);exit();
	}

	$num_rows2=mysqli_num_rows($fetch_row2);

	if($num_rows2==0)
	{
		echo "<script>
				alert('No entry found in Cashbook for Credit to Branch 198 !');
				window.location.href='./debit.php';
			</script>";
		exit();
	}
	else if($num_rows2>1)
	{
		echo "<script>
				alert('More than one Cash Entry Found for same result for Credit to Branch. Contact System Admin 205 !');
				window.location.href='./debit.php';
			</script>";
		exit();
	}
	else
	{
		$row_fetch_id2 = mysqli_fetch_array($fetch_row2);
		$cash_id_to_branch = $row_fetch_id2['id'];
	}
}

if($section=='DEBIT-BRANCH')
{
			$fetch_balance_br_branch = mysqli_query($conn,"SELECT `$balance_col` as balance FROM user WHERE username='$branch'");
			$fetch_balance_cr_branch = mysqli_query($conn,"SELECT `$balance_col` as balance FROM user WHERE username='$cr_to_branch'");
			
			if(!$fetch_balance_br_branch)
			{
				echo mysqli_error($conn);exit();
			}
			
			if(!$fetch_balance_cr_branch)
			{
				echo mysqli_error($conn);exit();
			}
			
			$row_dr_balance = mysqli_fetch_array($fetch_balance_br_branch);
			$row_cr_balance = mysqli_fetch_array($fetch_balance_cr_branch);
			
			$curr_bal_dr_branch=$row_dr_balance['balance'];
			$curr_bal_cr_branch=$row_cr_balance['balance'];
			
			if($curr_bal_cr_branch<$amount_old)
			{
				echo "<script>
					alert('Credit Branch has Insufficient Balance. Min required Balance is $amount_old !');
					window.location.href='./debit.php';
				</script>";
				exit();
			}
			
		$update_dr_branch_bal = mysqli_query($conn,"UPDATE user SET `$balance_col`=`$balance_col`+'$amount_old' WHERE username='$branch'");
		$update_cr_branch_bal = mysqli_query($conn,"UPDATE user SET `$balance_col`=`$balance_col`-'$amount_old' WHERE username='$cr_to_branch'");
			
			if(!$update_dr_branch_bal)
			{
				echo mysqli_error($conn);exit();
			}
			
			if(!$update_cr_branch_bal)
			{
				echo mysqli_error($conn);exit();
			}	

		$update_debit_cashbook_all = mysqli_query($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`+'$amount_old' WHERE id>'$cash_id' 
		AND user='$branch' AND comp='$company'");	
		
		$update_credit_cashbook_all = mysqli_query($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`-'$amount_old' WHERE id>'$cash_id_to_branch' 
		AND user='$cr_to_branch' AND comp='$company'");	
		
		$update_cashbook_dr_branch = mysqli_query($conn,"DELETE FROM cashbook WHERE id='$cash_id'");	
		
		$update_cashbook_cr_branch = mysqli_query($conn,"DELETE FROM cashbook WHERE id='$cash_id_to_branch'");	
		
		$update_dr_table = mysqli_query($conn,"DELETE FROM debit WHERE id='$id'");
			if(!$update_dr_table)
			{
				echo mysqli_error($conn);exit();
			}
			
		$update_cr_table = mysqli_query($conn,"DELETE FROM credit WHERE id='$credit_id'");
			
			if(!$update_cr_table)
			{
				echo mysqli_error($conn);exit();
			}
			
}
else if($section=='ITR')
{
		$fetch_balance_branch = mysqli_query($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
			
			if(!$fetch_balance_branch)
			{
				echo mysqli_error($conn);exit();
			}
			
			$row_balance2 = mysqli_fetch_array($fetch_balance_branch);
			
			$rrpl_cash=$row_balance2['balance'];
			$rr_cash=$row_balance2['balance2'];
			
			if($company=='RRPL')
			{
				if($rr_cash<$amount_old)
				{
					echo "<script>
						alert('Credit Company has Insufficient Balance. Min required Balance is $amount_old !');
						window.location.href='./debit.php';
					</script>";
					exit();
				}
				
				$transfer_company="RAMAN_ROADWAYS";
			}
			else
			{
				if($rrpl_cash<$amount_old)
				{
					echo "<script>
						alert('Credit Company has Insufficient Balance. Min required Balance is $amount_old !');
						window.location.href='./debit.php';
					</script>";
					exit();
				}
				
				$transfer_company="RRPL";
			}
			
			
			if($transfer_company=='RAMAN_ROADWAYS')
			{
				$update_main_balance=mysqli_query($conn,"UPDATE user SET balance=balance+'$amount_old',balance2=balance2-'$amount_old' 
				WHERE username='$branch'");	
				
				$update_cashbook_new_all_dr=mysqli_query($conn,"UPDATE cashbook SET balance=balance+'$amount_old' WHERE id>'$cash_id' AND 
				comp='RRPL' AND user='$branch'");
				
				$update_cashbook_new_all_cr=mysqli_query($conn,"UPDATE cashbook SET balance2=balance2-'$amount_old' WHERE id>'$cash_id_to_branch' 
				AND comp='RAMAN_ROADWAYS' AND user='$branch'");
				
			}
			else
			{
				$update_main_balance=mysqli_query($conn,"UPDATE user SET balance=balance-'$amount_old',balance2=balance2+'$amount_old' 
				WHERE username='$branch'");	
				
				$update_cashbook_new_all_dr=mysqli_query($conn,"UPDATE cashbook SET balance2=balance2+'$amount_old' WHERE id>'$cash_id' AND 
				comp='RAMAN_ROADWAYS' AND user='$branch'");
				
				$update_cashbook_new_all_cr=mysqli_query($conn,"UPDATE cashbook SET balance=balance-'$amount_old' WHERE id>'$cash_id_to_branch' 
				AND comp='RRPL' AND user='$branch'");				
			}
			
			$update_cashbook_new_dr=mysqli_query($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
				
			$update_cashbook_new_cr=mysqli_query($conn,"DELETE FROM cashbook WHERE id='$cash_id_to_branch'");
			
			$update_dr_table = mysqli_query($conn,"DELETE FROM debit WHERE id='$id'");
			
			$update_cr_table = mysqli_query($conn,"DELETE FROM credit WHERE id='$credit_id'");
			
}
else if($section=='HO')
{
		$fetch_balance_branch = mysqli_query($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
			
			if(!$fetch_balance_branch)
			{
				echo mysqli_error($conn);exit();
			}
			
			$row_balance2 = mysqli_fetch_array($fetch_balance_branch);
			
			$rrpl_cash=$row_balance2['balance'];
			$rr_cash=$row_balance2['balance2'];
			
			if($company=='RRPL' AND $rrpl_cash<$amount_old)
			{
				echo "<script>
						alert('Insufficient Balance in RRPL. Min required Balance is $amount_old !');
						window.location.href='./debit.php';
					</script>";
					exit();
			}
			else if($company=='RAMAN_ROADWAYS' AND $rr_cash<$amount_old)
			{
				echo "<script>
						alert('Insufficient Balance in RAMAN_ROADWAYS. Min required Balance is $amount_old !');
						window.location.href='./debit.php';
					</script>";
					exit();
			}
			else if($company=='')
			{
				echo "<script>
						alert('Unable to fetch Company !');
						window.location.href='./debit.php';
					</script>";
				exit();
			}
			
			$update_main_balance=mysqli_query($conn,"UPDATE user SET `$balance_col`=`$balance_col`+'$amount_old' WHERE username='$branch'");	
				
			$update_cashbook_new_all=mysqli_query($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`+'$amount_old' WHERE id>'$cash_id' AND 
				comp='$company' AND user='$branch'");
			
			$update_dr_table = mysqli_query($conn,"DELETE FROM debit WHERE id='$id'");
			
			$update_cashbook_new=mysqli_query($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
}
else
{
	echo "<script>
			alert('Invalid option selected !');
			window.location.href='./debit.php';
		</script>";
	exit();
}	

		echo "<script>
			alert('Voucher Deleted Successfully.');
			window.location.href='./debit.php';
		</script>";
		exit();
?>