<?php
require_once("./connect.php");

$frno=escapeString($conn,strtoupper($_POST['frno']));
$tno=escapeString($conn,strtoupper($_POST['tno']));
$id=escapeString($conn,strtoupper($_POST['id']));
$timestamp=date("Y-m-d H:i:s");

$get_data = Qry($conn,"SELECT frno,branch,truck_no,done FROM freight_form_lr WHERE id='$id'");
if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)==0){
	echo "<script>
		alert('Invalid Vou No !');
		window.location.href='./own_truck_form.php';
	</script>";	
	exit();
}

$row_data = fetchArray($get_data);

$voucher_branch = $row_data['branch'];

if($row_data['frno']!=$frno)
{
	echo "<script>
		alert('Voucher number not verified !');
		$('#date_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_data['truck_no']==$tno)
{
	echo "<script>
		alert('Nothing to update !');
		$('#truck_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_data['done']=="1")
{
	echo "<script>
		alert('Voucher Attached To e-diary Trip. Delete Trip First !');
		$('#truck_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$fetch_lr_count = Qry($conn,"SELECT id FROM freight_form_lr WHERE lrno IN(SELECT lrno FROM freight_form_lr WHERE frno='$frno') 
AND frno!='$frno'");
if(!$fetch_lr_count){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}	
		
if(numRows($fetch_lr_count)>0)
{
	echo "<script>
		alert('More than One Freight Memo or Own Truck Form Found. Contact Sytem-Admin !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$select_lrno = Qry($conn,"SELECT lrno FROM freight_form_lr WHERE frno='$frno' AND branch='$voucher_branch'");
if(!$select_lrno){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$lrnos = array();
$lrnos_store = array();
		
while($row_lrno=fetchArray($select_lrno))
{
	$lrnos[] = "'".$row_lrno['lrno']."'";
	$lrnos_store[] = $row_lrno['lrno'];
}
		
$lrnos=implode(',', $lrnos);
$lrnos_store=implode(',', $lrnos_store);

StartCommit($conn);
$flag = true;

$update_freight_lr = Qry($conn,"UPDATE freight_form_lr SET truck_no='$tno' WHERE frno='$frno' AND done!=1");
if(!$update_freight_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lr = Qry($conn,"UPDATE lr_sample SET truck_no='$tno' WHERE lrno IN($lrnos)");
if(!$update_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_log = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$frno','OWN_TRUCK_FORM','TRUCK_NO_UPDATE','$row_data[truck_no] to $tno. LRs : $lrnos_store.','','ADMIN','$timestamp')");
if(!$update_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Vehicle Number Successfully updated !');
		$('#get_button').attr('disabled',false);
		$('#truck_update_button').attr('disabled',false);
		$('#close_truck_update_button').click();
		$('#get_button').click();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./own_truck_form.php");
	exit();
}
?>