<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$new_company = escapeString($conn,strtoupper($_POST['company']));

$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$fetch_vou = Qry($conn,"SELECT user,comp,amt,chq,colset_d FROM mk_venf WHERE vno='$frno'");
if(!$fetch_vou){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}


if(numRows($fetch_vou)==0)
{
	echo "<script>
		alert('Voucher not found !');
		$('#loadicon').hide();
		$('#button_change_company').attr('disabled', false);
	</script>";
	exit();
}

$row=fetchArray($fetch_vou);

$company = $row['comp'];
$branch = $row['user'];
$vou_amount = $row['amt'];
$pay_mode = $row['chq'];

if($company==$new_company)
{
	echo "<script>
		alert('Nothing to update !');
		$('#loadicon').hide();
		$('#button_change_company').attr('disabled', false);
	</script>";
	exit();
}

$old_data="VouId : $frno-> Company update from $company to $new_company.";

if($pay_mode=='NEFT')
{
	$fetch_rtgs_req = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
	if(!$fetch_rtgs_req){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($fetch_rtgs_req)>0)
	{
		echo "<script>
			alert('Rtgs Payment Done. You Can\'t Edit !');
			$('#loadicon').hide();
			$('#button_change_company').attr('disabled', false);
		</script>";
		exit();
	}
	
	$row_rtgs_vou = fetchArray($fetch_rtgs_req);
	$rtgs_vou_id = $row_rtgs_vou['id'];
}
else if($pay_mode=='CASH')
{
	if($company=='RRPL'){
		$debit="debit";	
		$balance="balance";	
		$debit_new="debit2";
		$balance_new="balance2";
	}
	else{
		$debit="debit2";
		$balance="balance2";
		$debit_new="debit";	
		$balance_new="balance";	
	}
	
	$chk_balance = Qry($conn,"SELECT `$balance_new` as cash_balance FROM user WHERE username='$branch'");
	if(!$chk_balance){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($chk_balance)==0)
	{
		echo "<script>
			alert('Branch not found : $branch !');
			$('#loadicon').hide();
			$('#button_change_company').attr('disabled', false);
		</script>";
		exit();
	}
	
	$row_cash_balance = fetchArray($chk_balance);
	
	if($row_cash_balance['cash_balance']<$vou_amount)
	{
		echo "<script>
			alert('Insufficient balance in company : $new_company-($branch) !');
			$('#loadicon').hide();
			$('#button_change_company').attr('disabled', false);
		</script>";
		exit();
	}
	
	$fetch_cash_id = Qry($conn,"SELECT id,`$debit` as cash,`$balance` as balance FROM cashbook WHERE vou_no='$frno'");
	if(!$fetch_cash_id){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($fetch_cash_id)==0)
	{
		echo "<script>
				alert('Voucher not found in cashbook !');
				$('#loadicon').hide();
				$('#button_change_company').attr('disabled', false);
		</script>";
		exit();
	}
	
	$row_cash_id = fetchArray($fetch_cash_id);
	
	$cash_id = $row_cash_id['id'];
	$cash_amount = $row_cash_id['cash'];
}

	if($new_company=='RRPL'){
		$crn_string ="RRPL-E";
		$pass_col_reset="debit2";
		$pass_col_new="debit";
	}
	else{
		$crn_string="RR-E";
		$pass_col_reset="debit";
		$pass_col_new="debit2";
	}
	
StartCommit($conn);
$flag = true;

if($pay_mode=='NEFT')
{
	$crn_Qry = GetCRN($crn_string,$conn);
			
	if(!$crn_Qry || $crn_Qry==0 || $crn_Qry==""){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
			
	$crnnew = $crn_Qry;
			
	$update_rtgs = Qry($conn,"UPDATE rtgs_fm SET com='$new_company',crn='$crnnew' WHERE id='$rtgs_vou_id' AND colset_d!='1'");
	if(!$update_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
			
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Rtgs Voucher Downloaded. id : $rtgs_vou_id. Vou_No : $frno.",$conn,$page_name,__LINE__);
	}
			
	$update_passbook = Qry($conn,"UPDATE passbook SET comp='$new_company',`$pass_col_reset`='0',`$pass_col_new`='$vou_amount' 
	WHERE vou_no='$frno'");
	
	if(!$update_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($pay_mode=='CHEQUE')
{
	$update_passbook = Qry($conn,"UPDATE passbook SET comp='$new_company',`$pass_col_reset`='0',`$pass_col_new`='$vou_amount' 
	WHERE vou_no='$frno'");
	
	if(!$update_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	// $update_cheque_book = Qry($conn,"UPDATE cheque_book SET comp='$new_company',`$pass_col_reset`='0',`$pass_col_new`='$vou_amount' 
	// WHERE vou_no='$frno'");
	
	// if(!$update_cheque_book){
		// $flag = false;
		// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	// }
}
else if($pay_mode=='CASH')
{
	$last_debit = Qry($conn,"SELECT `$balance_new` as new_bal FROM cashbook WHERE id<'$cash_id' AND user='$branch' AND 
	comp='$new_company' ORDER BY id DESC LIMIT 1");
	
	if(!$last_debit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($last_debit)==0){
		$flag = false;
		errorLog("Last debit not found.",$conn,$page_name,__LINE__);
	}
	
	$row_last = fetchArray($last_debit);
	$last_amount = $row_last['new_bal'];
	
	if(empty($last_amount) || $last_amount=="" || $last_amount==0)
	{
		$flag = false;
		errorLog("Last cash debit is empty !",$conn,$page_name,__LINE__);
	}
	
	$new_balance = $last_amount-$cash_amount;
	
	$update_cashbook = Qry($conn,"UPDATE cashbook SET comp='$new_company',`$debit`='',`$balance`='',`$debit_new`='$cash_amount',
	`$balance_new`='$new_balance' WHERE id='$cash_id'");
	
	if(!$update_cashbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Unable to Fetch Cash entry !",$conn,$page_name,__LINE__);
	}
			
	$update_cash_balance = Qry($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$cash_amount' WHERE id>'$cash_id' AND 
	comp='$company' AND user='$branch'");

	if(!$update_cash_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_cash_balance2 = Qry($conn,"UPDATE cashbook SET `$balance_new`=`$balance_new`-'$cash_amount' WHERE id>'$cash_id' AND 
	comp='$new_company' AND user='$branch'");
	
	if(!$update_cash_balance2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_main_balance = Qry($conn,"UPDATE user SET `$balance`=`$balance`+'$cash_amount',
	`$balance_new`=`$balance_new`-'$cash_amount' WHERE username='$branch'");
	
	if(!$update_main_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$flag = false;
	errorLog("Invalid payment mode: $pay_mode.",$conn,$page_name,__LINE__);
}
	
$update_voucher = Qry($conn,"UPDATE mk_venf SET comp='$new_company',colset_d='' WHERE vno='$frno'");
if(!$update_voucher){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}		
	
$update_log=Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) 
VALUES ('$frno','Expense_Voucher','Company_Change','$old_data','$branch','ADMIN','$timestamp')");

if(!$update_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Company updated to $new_company !');
		document.getElementById('button2').click();
		document.getElementById('req_close_button').click();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	// echo "<script>
		// alert('Error While Processing Request !');
		// $('#req_update_button').attr('disabled', false);
		// $('#loadicon').hide();
	// </script>";
	Redirect("Error While Processing Request.","./expense_voucher.php");
	exit();
}		
?>