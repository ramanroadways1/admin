<?php 
require_once("./connect.php");

$days_from = escapeString($conn,$_POST['days_from']);
$days_to = escapeString($conn,$_POST['days_to']);
$amount = escapeString($conn,$_POST['amount']);
$type_of = escapeString($conn,$_POST['type_of']);
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if($type_of!='DAY' AND $type_of!='FIX')
{
	echo "<script type='text/javascript'>
			$('#add_rule_btn').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid type defined !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($type_of=='FIX')
{
	$amount_from = escapeString($conn,$_POST['amt_from']);
	$amount_to = escapeString($conn,$_POST['amt_to']);
	
	if($amount_from=='' || $amount_from<=0)
	{
		echo "<script type='text/javascript'>
				$('#add_rule_btn').attr('disabled',false);
				$('#loadicon').hide();
				swal({
				title: 'Invalid value of amount from !!',
				type: 'error',
				closeOnConfirm: true
				});
			</script>";
		exit();
	}
	
	if($amount_to=='' || $amount_to<=0)
	{
		echo "<script type='text/javascript'>
				$('#add_rule_btn').attr('disabled',false);
				$('#loadicon').hide();
				swal({
				title: 'Invalid value of amount to !!',
				type: 'error',
				closeOnConfirm: true
				});
			</script>";
		exit();
	}
	
	if($amount_to<=$amount_from)
	{
		echo "<script type='text/javascript'>
				$('#add_rule_btn').attr('disabled',false);
				$('#loadicon').hide();
				swal({
				title: 'Check amount range value !!',
				type: 'error',
				closeOnConfirm: true
				});
			</script>";
		exit();
	}
}

if($days_from=='' || $days_from<=0)
{
	echo "<script type='text/javascript'>
			$('#add_rule_btn').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid value of days from !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($days_to=='' || $days_to<=0)
{
	echo "<script type='text/javascript'>
			$('#add_rule_btn').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid value of till days !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($days_to<=$days_from)
{
	echo "<script type='text/javascript'>
			$('#add_rule_btn').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Check days range value !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($amount=='' || $amount<=0)
{
	echo "<script type='text/javascript'>
			$('#add_rule_btn').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid value of deduction amount !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($type_of=='FIX')
{
	$chk_record = Qry($conn,"SELECT id FROM pod_late_chrg WHERE $days_from>=from1 AND $days_to<=to1");
}
else
{
	$chk_record = Qry($conn,"SELECT id FROM late_pod_rules WHERE $days_from>=days_from AND $days_to<=days_to");
}

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
}

if(numRows($chk_record)>0)
{
	echo "<script type='text/javascript'>
			$('#add_rule_btn').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Duplicate record found !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if($type_of=='FIX')
{
	$insert_new = Qry($conn,"INSERT INTO pod_late_chrg(from1,to1,amt_frm,amt_to,chrg,timestamp) VALUES 
	('$days_from','$days_to','$amount_from','$amount_to','$amount','$timestamp')");
}
else
{
	$insert_new = Qry($conn,"INSERT INTO late_pod_rules(days_from,days_to,charges,timestamp) VALUES ('$days_from','$days_to','$amount','$timestamp')");
}

if(!$insert_new){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	if($type_of=='FIX')
	{
		echo "<script type='text/javascript'>
			$('#amt_from_new').val('');
			$('#amt_to_new').val('');
		</script>";
	}

	echo "<script type='text/javascript'>
			$('#days_from_new').val('');
			$('#days_till_new').val('');
			$('#amount_new').val('');
			$('#loadicon').hide();
			swal({
			  title: 'Rule added successfully !!',
			  type: 'success',
			  closeOnConfirm: true
			},
			function(){
				window.location.href='./late_pod_mgt.php';
			});
	</script>";
	exit();
}
else
{
	echo "<script>
		alert('Error !!');
		$('#add_rule_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
?>