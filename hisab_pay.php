<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">e-Diary Hisab</h3>
              </div>
              
	<div class="card-body">
	
<script>				
function Fetch(trip_no)
{
	if(trip_no=='')
	{
		alert('Enter Trip Number First !');
	}
	else
	{
			$("#loadicon").show();
			jQuery.ajax({
			url: "get_hisab.php",
			data: 'trip_no=' + trip_no,
			type: "POST",
			success: function(data) {
				$("#result_main").html(data);
			},
				error: function() {}
			});
	}
}		
</script>	

	<div class="row">	
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Trip No <font color="red"><sup>*</sup></font></label>
                  <input oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" class="form-control" id="trip_no" name="trip_no">
              </div>
			</div>
			
			<div id="button_div" class="col-md-2">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="button2" onclick="Fetch($('#trip_no').val())" class="btn pull-right btn-danger">Get Records !</button>
			</div>
			
		</div>
		
		<div class="row">	
			<div class="col-md-12 table-responsive" style="overflow:auto">	
			
				<div id="result_main"></div>
			
			</div>
		</div>
	</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	
	<div id="result_main2"></div>
				
           </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script>  
function EditReq(frno)
{
	$('#modal_frno').val(frno);
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "load_market_req_for_edit.php",
	data: 'frno=' + frno,
	type: "POST",
	success: function(data) {
		$("#result_modal_data").html(data);
		document.getElementById("req_modal_button").click();
	},
	error: function() {}
	});
}

function DeleteReq(frno)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "delete_market_req.php",
	data: 'frno=' + frno,
	type: "POST",
	success: function(data) {
	$("#result_main2").html(data);
	$("#loadicon").hide();
	},
	error: function() {}
	});
}  

function SaveSingle(id)
{
	var dsl_by = $('#dsl_by'+id).val();
	
	var qty = $('#qty'+id).val();
	var rate = $('#rate'+id).val();
	var amt = $('#amt'+id).val();
	var cash = $('#cash'+id).val();
	
	if(dsl_by=='CARD')
	{
		var card = $('#card'+id).val();
		var comp = $('#fuelcomp'+id).val();
	}	
	else if(dsl_by=='PUMP')
	{
		var card = $('#pumpvalue'+id).val();
		var comp = '';
	}
	else
	{
		window.location.href='request_market_truck.php';	
	}
	
	if(qty=='' || rate=='' || amt=='' || cash=='' || card=='')
	{
		alert('All field are required !');
	}
	else
	{
		if(qty>400 || rate>90)
		{
			alert('Max value for Qty is : 400 and Rate is : 90');
		}
		else
		{
			$("#loadicon").show();
			
			jQuery.ajax({
			url: "save_market_req_diesel_cache.php",
			data: 'id=' + id + '&dsl_by=' + dsl_by + '&qty=' + qty + '&rate=' + rate + '&amt=' + amt + '&cash=' + cash + '&card=' + card + '&comp=' + comp,
			type: "POST",
			success: function(data)
			{
				$("#save_result"+id).html(data);
				$("#loadicon").hide();
			},
			error: function() {}
			});
		}
	}
} 

function Edit(id)
{
	var parts = id.split('_', 2);
	var id = parts[0];
	var type  = parts[1];
	
	$("#req_update_button").attr("disabled", false);
	
		$('.qtyclass').attr('readonly',true);
		$('.rateclass').attr('readonly',true);
		$('.amtclass').attr('readonly',true);
		$('.cashclass').attr('readonly',true);
		$('.cardclass').attr('readonly',true);
		$('.savebtnclass').attr('disabled',true);
		$('.pumpvalueclass').attr('disabled',true);
		$('.fuelcompclass').attr('disabled',true);
		
		$('.qtyclass').attr('oninput','');
		$('.rateclass').attr('oninput','');
		$('.amtclass').attr('oninput','');
		
	if(type=='CARD')
	{
		$('#amt'+id).attr('readonly',false);
		$('#cash'+id).attr('readonly',false);
		$('#card'+id).attr('readonly',false);
		
		$('#savebtn'+id).attr('disabled',false);
		$('#pumpvalue'+id).attr('disabled',false);
		$('#fuelcomp'+id).attr('disabled',false);
		
		$("#loadicon").show();
			jQuery.ajax({
			url: "mark_as_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data)
			{
				$("#result_main2").html(data);
				$("#loadicon").hide();
			},
			error: function() {}
			});
	}
	else if(type=='PUMP')
	{
		$('#qty'+id).attr("oninput","sum2('"+id+"')");
		$('#rate'+id).attr("oninput","sum1('"+id+"')");
		$('#amt'+id).attr("oninput","sum3('"+id+"')");
		
		$('#qty'+id).attr('readonly',false);
		$('#rate'+id).attr('readonly',false);
		$('#amt'+id).attr('readonly',false);
		$('#cash'+id).attr('readonly',false);
		$('#card'+id).attr('readonly',false);
		
		$('#savebtn'+id).attr('disabled',false);
		$('#pumpvalue'+id).attr('disabled',false);
		$('#fuelcomp'+id).attr('disabled',false);
		
		$("#loadicon").show();
			jQuery.ajax({
			url: "mark_as_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data)
			{
				$("#result_main2").html(data);
				$("#loadicon").hide();
			},
			error: function() {}
			});
	}
	else
	{
		window.location.href='request_market_truck.php';	
	}
}
</script>  

<a style="display:none1" id="req_modal_button" data-toggle="modal" data-target="#ReqModal"></a>

<script type="text/javascript">
$(document).ready(function (e) {
$("#ReqUpdateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#req_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_market_req.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_req_modal").html(data);
		$("#req_update_button").attr("disabled", false);
		$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script> 

<div class="modal fade" id="ReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
		
		<form id="ReqUpdateForm" autocomplete="off"> 
			<div class="modal-body">
			
				<div id="result_req_modal"></div>
				
			<input type="hidden" name="frno" id="modal_frno">
				
			<div class="row">
			
				<div class="col-md-12">
                   <div class="form-group">
                      <h4>Update Diesel Req. : </h4>
                  </div>
               </div>
			   
				<div class="col-md-12 table-responsive" style="overflow:auto">
					<div id="result_modal_data"></div>
				</div>
				
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" id="req_close_button" onclick="document.getElementById('menu_button2').click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="req_update_button" disabled class="btn btn-primary">Update Final</button>
	</div>
	
		</form>
		</div>	
     </div>
 </div>	
  <!-- MODAL -->
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>