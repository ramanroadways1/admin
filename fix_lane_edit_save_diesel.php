<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$qty = sprintf("%.2f",escapeString($conn,$_POST['qty']));
$timestamp = date("Y-m-d H:i:s");

if($id=='')
{
	echo "<script>
		alert('Lane not found !!');
		$('#edit_diesel_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}


if($qty=='' || $qty<0)
{
	echo "<script>
		alert('Invalid QTY !!');
		$('#edit_diesel_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT adv_amount,diesel_qty FROM dairy.fix_lane_adv WHERE lane_id='$id'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_record)==0)
{
	echo "<script>
		alert('No record found !!');
		$('#edit_diesel_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_adv = fetchArray($chk_record);

if(sprintf("%.2f",$row_adv['diesel_qty'])==$qty)
{
	echo "<script>
		alert('Nothing to update !!');
		$('#edit_diesel_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_record = Qry($conn,"UPDATE dairy.fix_lane_adv SET diesel_qty='$qty' WHERE lane_id='$id'");

if(!$update_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,edit_by,timestamp) VALUES 
('$id','By Lane Id','FIX_LANE','EDIT_DIESEL_QTY','Qty update $row_adv[diesel_qty] to $qty.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
		alert('Updated Successfully !!');
		$('#edit_diesel_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fix_lane.php");
	exit();
}
?>