<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Change Account details :</h3>
              </div>
              
	<div class="card-body">
	
<div id="lr_result"></div>
		
<script type="text/javascript">
$(function() {
      $("#market_tno").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_market_vehicle.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   term: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#market_tno').val(ui.item.value);   
               $('#owner_id').val(ui.item.oid);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#market_tno").val('');
				$("#owner_id").val('');
				 alert('Vehicle number does not exist !'); 
              } 
              },
			}); 
});  

$(function() {
      $("#broker_name").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_broker.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   term: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#broker_name').val(ui.item.value);   
               $('#broker_id').val(ui.item.id);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#broker_name").val('');
				$("#broker_id").val('');
				 alert('Broker does not exist !'); 
              } 
              },
			}); 
});  

$(function() {
      $("#driver_name").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_own_veh_driver.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   term: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#driver_name').val(ui.item.value);   
               $('#driver_id').val(ui.item.code);     
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#driver_name").val('');
				$("#driver_id").val('');
				 alert('Driver does not exist !'); 
              } 
              },
			}); 
});  
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#AccountForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
// $("#button2").attr("disabled", true);
	$.ajax({
	url: "./load_account_details.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_main").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="AccountForm">

	<div class="row" id="row_main1">	
		
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Account Type <font color="red"><sup>*</sup></font></label>
                  <select name="SearchBy" id="SearchBy" onchange="SearchByFunc(this.value)" class="form-control" required>
					<option value="">---Select---</option>
					<option value="OWNER">Vehicle owner</option>
					<option value="BROKER">Broker</option>
					<option value="DRIVER">Own Truck Driver</option>
				 </select>
              </div>
			</div>
			
			<script>
			function SearchByFunc(elem)
			{
				if(elem=='OWNER')
				{
					$('#veh_div').show();
					$('#broker_div').hide();
					$('#driver_div').hide();
					$('#market_tno').attr('required',true);
					$('#broker_name').attr('required',false);
					$('#driver_name').attr('required',false);
				}
				else if(elem=='BROKER')
				{
					$('#veh_div').hide();
					$('#broker_div').show();
					$('#driver_div').hide();
					$('#market_tno').attr('required',false);
					$('#broker_name').attr('required',true);
					$('#driver_name').attr('required',false);
				}
				else if(elem=='DRIVER')
				{
					$('#veh_div').hide();
					$('#broker_div').hide();
					$('#driver_div').show();
					$('#market_tno').attr('required',false);
					$('#broker_name').attr('required',false);
					$('#driver_name').attr('required',true);
				}
				else
				{
					$('#veh_div').hide();
					$('#broker_div').hide();
					$('#driver_div').hide();
					$('#market_tno').attr('required',true);
					$('#broker_name').attr('required',true);
					$('#driver_name').attr('required',true);
				}
			}
			</script>
			
			<div class="col-md-3" style="display:none" id="veh_div">	
               <div class="form-group">
                  <label>Select vehicle <font color="red"><sup>*</sup></font></label>
                  <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9,-]/,'')" type="text" id="market_tno" class="form-control" name="market_tno" required />
              </div>
			</div>
			
			<div class="col-md-3" style="display:none" id="broker_div">	
               <div class="form-group">
                  <label>Select broker <font color="red"><sup>*</sup></font></label>
                  <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-]/,'')" type="text" id="broker_name" class="form-control" name="broker_name" required />
              </div>
			</div>
			
			<div class="col-md-3" style="display:none" id="driver_div">	
               <div class="form-group">
                  <label>Select driver <font color="red"><sup>*</sup></font></label>
                  <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-]/,'')" type="text" id="driver_name" class="form-control" name="driver_name" required />
              </div>
			</div>
			
			<input type="hidden" name="owner_id" id="owner_id">
			<input type="hidden" name="broker_id" id="broker_id">
			<input type="hidden" name="driver_id" id="driver_id">
			<input type="hidden" name="branch_name2" id="branch_name2">
			<input type="hidden" name="user_code2" id="user_code2">
			
			<div class="col-md-2">	
               <div class="form-group">
                  <label>By Branch <font color="red"><sup>*</sup></font></label>
                  <select style="font-size:13px" name="branch_name" id="branch_name" onchange="BranchSel(this.value)" class="form-control" required>
					<option value="">---Select---</option>
					<?php
					$get_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username not in('DUMMY','HEAD') 
					ORDER BY username ASC");

					if(!$get_branch){
						ScriptError($conn,$page_name,__LINE__);
						exit();
					}

					if(numRows($get_branch)>0)
					{
						while($row_b = fetchArray($get_branch))
						{
							echo "<option value='$row_b[username]'>$row_b[username]</option>";
						}
					}
					?>
				 </select>
              </div>
			</div>
			
			<script>
			function BranchSel(branch)
			{
				$('#user_code').val('');
				$("#loadicon").show();
				jQuery.ajax({
				url: "load_branch_users.php",
				data: 'branch=' + branch,
				type: "POST",
				success: function(data) {
					$("#user_code").html(data);
				},
				error: function() {}
				});
			}
			
			</script>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>By User <font color="red"><sup>*</sup></font></label>
                  <select style="font-size:13px" name="user_code" id="user_code" onchange="if($('#branch_name').val()==''){ $('#branch_name').focus();$(this).val(''); }" class="form-control" required>
					<option value=''>-select-</option>
				  </select>
              </div>
			</div>
			
			<div id="button_div" class="col-md-1">
				<label>&nbsp;</label>
				<br />
				<button type="submit" id="button2" class="btn btn-sm btn-danger">Check !</button>
			</div>
			
		</div>
		
</form>	
		
		<div class="row">	
			<div class="col-md-12 table-responsive" style="overflow:auto">	
			
				<div id="result_main"></div>
			
			</div>
		</div>
	</div>
	
	<div id="result_main2"></div>
				
           </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script>  
function Edit(section,id)
{
	if(id=='')
	{
		alert('id not found..');
	}
	else if(section=='')
	{
		alert('account type not found..');
	}
	else
	{
		$('#modal_id').val(id);
		$('#modal_section').val(section);
		$('#modal_branch_name').val($('#branch_name2').val());
		$('#modal_user_code').val($('#user_code2').val());
		$("#req_modal_button")[0].click();
	}
}

function GenOTP()
{
	$('#otp').attr('required',true);
	var section = $('#modal_section').val();
	var party_name = $('#modal_party_name').val();
	$("#loadicon").show();
				jQuery.ajax({
				url: "gen_otp.php",
				data: 'section=' + section + '&party_name=' + party_name,
				type: "POST",
				success: function(data) {
					$("#otp_result").html(data);
				},
				error: function() {}
				});
}
</script>  

<a style="display:none1" id="req_modal_button" data-toggle="modal" data-target="#ReqModal"></a>


<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ifsc_main");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
               document.getElementById("ifsc_main").setAttribute("style","background-color:red;color:#fff;");
				document.getElementById("req_update_button").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
				document.getElementById("ifsc_main").setAttribute("style","background-color:green;color:#fff;");
				document.getElementById("req_update_button").removeAttribute("disabled");
           }
        }
		else
		{
			document.getElementById("ifsc_main").setAttribute("style","background-color:white;color:#fff");
			document.getElementById("req_update_button").removeAttribute("disabled");
		}
 } 			
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault(); 
$("#req_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_ac_details.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_req_modal").html(data);
	}, 
	error: function() 
	{} });}));});
</script> 

<div class="modal fade" id="ReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
		
	<div class="modal-header bg-primary">
		Update A/c: <span id="party_name1"></span>
    </div>
	
		<form id="UpdateForm" autocomplete="off"> 
			<div class="modal-body">
			
				<div id="result_req_modal"></div>
				
			<input type="hidden" name="id" id="modal_id">
			<input type="hidden" name="section" id="modal_section">
			<input type="hidden" name="party_name" id="modal_party_name">
			<input type="hidden" name="branch" id="modal_branch_name">
			<input type="hidden" name="user_code" id="modal_user_code">
				
			<div class="row">
			
				<div class="col-md-12">	
				   <div class="form-group">
					  <label>Ac holder <font color="red"><sup>*</sup></font></label>
					  <input oninput="this.value=this.value.replace(/[^a-z A-Z,-]/,'')" type="text" id="ac_holder_main" 
					  class="form-control" name="ac_holder" required />
				  </div>
				</div>
				
				<div class="col-md-12">	
				   <div class="form-group">
					  <label>Ac number <font color="red"><sup>*</sup></font></label>
					  <input oninput="this.value=this.value.replace(/[^0-9,-]/,'')" type="text" id="ac_no_main" 
					  class="form-control" name="ac_no" required />
				  </div>
				</div>
				
				<div class="col-md-6">	
				   <div class="form-group">
					  <label>Bank name <font color="red"><sup>*</sup></font></label>
					  <input oninput="this.value=this.value.replace(/[^a-z A-Z,-]/,'')" type="text" id="bank_name_main" 
					  class="form-control" name="bank_name" required />
				  </div>
				</div>
				
				<div class="col-md-6">	
				   <div class="form-group">
					  <label>IFSC code <font color="red"><sup>*</sup></font></label>
					  <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9,-]/,'');ValidateIFSC()" type="text" id="ifsc_main" 
					  class="form-control" maxlength="11" name="ifsc" required />
				  </div>
				</div>
				
				<div class="col-md-12">	
				   <div class="form-group" id="otp_result">
					  <label>Upload A/c proof <font color="red"><sup>* (cheque/passbook)</sup></font> 
					  &nbsp; <span style="font-size:12px">No copy? <button class="btn btn-sm btn-success" type="button" id="otp_btn" 
					  onclick="GenOTP()">generate OTP</button></span></label>
					  <input type="file" id="ac_proof" class="form-control" required="required" name="ac_proof"
					  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" />
				  </div>
				</div>
				
				<div class="col-md-12" style="display:none" id="otp_div">	
				   <div class="form-group">
					  <label>Enter OTP <font color="red"><sup>*</sup></font></label>
					  <input oninput="this.value=this.value.replace(/[^0-9,-]/,'')" maxlength="6" type="text" 
					  id="otp" class="form-control" name="otp" />
				  </div>
				</div>
				
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" id="req_close_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="req_update_button" class="btn btn-primary">Update Final</button>
	</div>
		</form>
		</div>	
     </div>
 </div>	
  <!-- MODAL -->
  
<?php include ("./footer.php"); ?>

</div>
</body>
</html>