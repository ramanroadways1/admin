<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Zero Freight Parties</h3>
              </div>
				
<div class="card-body">
	
	<div class="row">
		<div class="col-md-12">	
			<div class="form-group">
				<button type="button" data-toggle="modal" data-target="#AddForm" class="btn btn-sm btn-success">Add New Record</button>
			</div>	   
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-12 table-responsive" style="overflow:auto">
		
<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="12">Zero Freight Parties :</th>
    </tr>
	
	<tr>    
		<th>#</th>
		<th>Record Search By</th>
		<th>Party Name</th>
		<th>PAN No</th>
		<th>Timestamp</th>
		<th>Status</th>
		<th>#</th>
	</tr>
<?php
$get_record = Qry($conn,"SELECT id,veh_id,broker_id,pan_no,lrno,timestamp,is_active,IF(is_active='1','Active','Disabled') as active_status 
FROM _zero_freight_party ORDER BY id ASC");

if(!$get_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn=1;
while($row = fetchArray($get_record))
{
	if($row['veh_id']!=0)
	{
		$get_owner_name = Qry($conn,"SELECT name FROM mk_truck WHERE pan='$row[pan_no]'");

		if(!$get_owner_name){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_owner = fetchArray($get_owner_name);
		
		$party_name=$row_owner['name'];
		$record_by="Owner";
	}
	else if($row['broker_id']!=0)
	{
		$get_broker_name = Qry($conn,"SELECT name FROM mk_broker WHERE pan='$row[pan_no]'");

		if(!$get_broker_name){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_broker = fetchArray($get_broker_name);
		
		$party_name=$row_broker['name'];
		$record_by="Broker";
	}
	else
	{
		$party_name="";
		$record_by="LR_No.";
	}
	
	echo '<tr>
		<td>'.$sn.'</td>
		<td>'.$record_by.'</td>	
		<td>'.$party_name.'</td>	
        <td>'.$row['pan_no'].'</td>
        <td>'.date("d-m-y H:i A",strtotime($row['timestamp'])).'</td>
        <td>'.$row["active_status"].'</td>';
		
		if($row['is_active']=='1')
		{
			echo "<td><input type='hidden' id='party_name_$row[id]' value='$party_name'><button type='button' class='btn btn-sm btn-danger' onclick='DisableParty($row[id])'>Disable</button></td>";
		}
		else
		{
			echo "<td></td>";
		}
     echo '</tr>';
$sn++; 
}
echo '</table>';
?>			
		</div>
	</div>
</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	</div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script>
  $(function() {  
      $("#broker_owner").autocomplete({
			// appendTo: $("#broker_owner").next()
			source: function(request, response) { 
			if(document.getElementById('search_by').value!=''){
                 $.ajax({
                  url: 'autofill/get_party_pan.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   search_by: document.getElementById('search_by').value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select search by option first !');
				$("#broker_owner").val('');
				$("#party_name").val('');
				$("#party_id").val('');
			 }

              },
              select: function (event, ui) { 
               $('#broker_owner').val(ui.item.value);   
               $('#party_name').val(ui.item.party_name);   
               $('#party_id').val(ui.item.party_id);   
              return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#broker_owner").val('');
				$("#party_name").val('');
				$("#party_id").val('');
				alert('PAN does not exist !'); 
              } 
              },
			}); 
      }); 
	 
$(document).ready(function (e) {
$("#AddForm1").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#add_new_button").attr("disabled", true);
	$.ajax({
	url: "./save_zero_freight_party.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
	
$(document).ready(function (e) {
$("#DisableForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#update_btn").attr("disabled", true);
	$.ajax({
	url: "./save_zero_freight_party_disable.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});	
	
function DisableParty(id)
{
	$('#party_name_disable').val($('#party_name_'+id).val());
	$('#party_id_disable').val(id);
	$('#disable_modal_btn')[0].click();
}
</script> 

<button type="button" style="display:none" id="disable_modal_btn" data-toggle="modal" data-target="#DisableModal"></button>

<form id="DisableForm" autocomplete="off"> 
<div class="modal fade" id="DisableModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
	   
	  <div class="modal-header">
			<h5 class="modal-title">Disable Zero Freight Party :</h5>
      </div>
	  
		<div class="modal-body">
			
		<div class="row">
			
			<input type="hidden" name="party_id" id="party_id_disable">
			
			<div class="col-md-12">
				<div class="form-group">
					<label>Party Name <font color="red"><sup>*</sup></font></label>
                     <input type="text" name="party_name" id="party_name_disable" class="form-control" required readonly>
                 </div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label>Narration <font color="red"><sup>*</sup></font></label>
                     <textarea id="narration_disable" name="narration" class="form-control" required></textarea>
                 </div>
			</div>
				
			</div>
		</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="update_btn" class="btn btn-primary">Submit</button>
	</div>
		</div>	
     </div>
 </div>	
</form>

<form id="AddForm1" autocomplete="off"> 
<div class="modal fade" id="AddForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
	   
	  <div class="modal-header">
			<h5 class="modal-title">Add new Open LR Party :</h5>
      </div>
	  
		<div class="modal-body">
			
		<div class="row">
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Search By <font color="red"><sup>*</sup></font></label>
                     <select id="search_by" onchange="$('#broker_owner').val('');$('#party_name').val('');$('#party_id').val('')" name="search_by" class="form-control" required="required">
						<option value="">--select--</option>
						<option value="BROKER">BROKER</option>
						<option value="OWNER">OWNER</option>
					 </select>
                 </div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label>Search PAN <font color="red"><sup>*</sup></font></label>
                     <input type="text" id="broker_owner" name="broker_owner" class="form-control" required>
                 </div>
			</div>
			
			<input type="hidden" name="party_id" id="party_id">
			
			<div class="col-md-12">
				<div class="form-group">
					<label>Party Name <font color="red"><sup>*</sup></font></label>
                     <input type="text" id="party_name" name="party_name" class="form-control" required readonly>
                 </div>
			</div>
				
			</div>
		</div>
	<div class="modal-footer">
		<button type="button" id="add_close_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="add_new_button" class="btn btn-primary">ADD Party</button>
	</div>
		</div>	
     </div>
 </div>	
</form>
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>