<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>
<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<script>
$(function() {
    $("#tno_own").autocomplete({
      source: '../diary/autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			 $(event.target).focus();
			 $("#button2").attr("disabled",true);
		alert('Truck Number doest not exists.');
        }
    }, 
    focus: function (event, ui) {
        $("#button2").attr("disabled",false);
		return false;
    }
    });
  });
</script>

<div id="func_result"></div>

<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Truck Voucher Summary :</h3>
              </div>
              
	<div class="card-body">
				
		<script>		
		function SearchBy(select)
		{
			$('#result_main').html('');
			$('#frno').val('');
			$('#tno_own').val('');
			$('#date_range').val('-0 days');
			
			if(select=='FM')
			{
				$('#div_frno').show();
				$('#div_tno').hide();
				$('#div_range').hide();
			}
			else if(select=='TNO')
			{
				$('#div_frno').hide();
				$('#div_tno').show();
				$('#div_range').show();
			}
			else
			{
				alert('Invalid option selected !');
				$('#result_main').html('');
				
				$('#div_frno').show();
				$('#div_tno').hide();
				$('#div_range').hide();
			}
		}
		
		function Fetch(option)
		{
			if(option=='FM')
			{
				if($('#frno').val()=='')
				{
					alert('Enter Voucher No First !');
					$('#result_main').html('');
					$('#div_frno').show();
					$('#div_tno').hide();
					$('#div_range').hide();
				}
				else
				{
					var frno = $('#frno').val();
					var vou_in = $('#vou_in').val();
					
					$("#loadicon").show();
					jQuery.ajax({
					url: "fetch_truck_vou.php",
					data: 'option=' + option + '&frno=' + frno + '&vou_in=' + vou_in,
					type: "POST",
					success: function(data) {
					$("#result_main").html(data);
					},
						error: function() {}
					});
				}
			}
			else if(option=='TNO')
			{
				if($('#tno_own').val()=='' || $('#date_range').val()=='')
				{
					alert('Select Truck Number and Date Range First !');
					$('#result_main').html('');
					$('#div_frno').show();
					$('#div_tno').hide();
					$('#div_range').hide();
				}
				else
				{
					var tno = $('#tno_own').val();
					var date_range = $('#date_range').val();
					var vou_in = $('#vou_in').val();
					
					$("#loadicon").show();
					jQuery.ajax({
					url: "fetch_truck_vou.php",
					data: 'option=' + option + '&tno=' + tno + '&range=' + date_range + '&vou_in=' + vou_in,
					type: "POST",
					success: function(data) {
					$("#result_main").html(data);
					},
						error: function() {}
					});
				}
			}
			else
			{
				alert('Invalid option selected !');
				$('#result_main').html('');
				
				$('#div_frno').show();
				$('#div_tno').hide();
				$('#div_range').hide();
			}
		}		
		</script>	

	<div id="lr_result"></div>
		
	<div class="row">	
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Search By <font color="red"><sup>*</sup></font></label>
                  <select id="SearchBy" onchange="SearchBy(this.value)" class="form-control" required>
					<option value="FM">Vou No</option>
					<!--<option value="TNO">Truck Number</option>-->
				  </select>
              </div>
			</div>
			
			<div class="col-md-3" id="div_frno">	
               <div class="form-group">
                  <label>Vou No <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" id="frno" name="frno">
              </div>
			</div>
			
			<div class="col-md-3" id="div_tno" style="display:none">	
               <div class="form-group">
                  <label>Truck Number <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" id="tno_own">
              </div>
			</div>
			
			<div class="col-md-3" id="div_range" style="display:none">	
               <div class="form-group">
                  <label>Days Ago <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" name="range" id="date_range">
					<option value="-0 days">Today's</option>
					<option value="-1 days">Last 2 days</option>
					<option value="-4 days">Last 5 days</option>
					<option value="-6 days">Last 7 days</option>
					<option value="-9 days">Last 10 days</option>
					<option value="-14 days">Last 15 days</option>
					<option value="-29 days">Last 30 days</option>
					<option value="-59 days">Last 60 days</option>
					<option value="-89 days">Last 90 days</option>
					<option value="-119 days">Last 120 days</option>
					<option value="FULL">FULL REPORT</option>
				</select>
              </div>
			</div>
			
			<div class="col-md-2">
				<label>e-Diary Voucher</label>
				 <select class="form-control" id="vou_in">
					<option value="MAIN">Out of e-Diary</option>
					<option value="DIARY">e-Diary Voucher</option>
				</select>
			</div>
			
			<div class="col-md-1">
				<label></label>
				<br />
				<button type="button" id="button2" onclick="Fetch($('#SearchBy').val())" class="btn pull-right btn-danger">Check !</button>
			</div>
			
		</div>
		
		<div class="row">	
			<div class="col-md-12 table-responsive" style="overflow:auto">	
			
				<div id="result_main"></div>
			
			</div>
		</div>
	</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
		</div>
	</div>
   </div>
  </div>
</section>
</div>
  
<script>  
function EditVou(id,frno,vou_type)
{
	$('#modal_frno').val(frno);
	$('#modal_id').val(id);
	$("#loadicon").show();
	jQuery.ajax({
	url: "load_truck_vou_for_edit.php",
	data: 'frno=' + frno + '&vou_type=' + vou_type + '&id=' + id,
	type: "POST",
	success: function(data) {
		$("#result_modal_data").html(data);
		$('#ReqModal').modal();
	},
	error: function() {}
	});
}

function DeleteVou(id,frno,vou_type)
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "delete_truck_vou.php",
		data: 'frno=' + frno + '&vou_type=' + vou_type + '&id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
		});
}  

</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#VouUpdateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#req_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_truck_voucher.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="VouUpdateForm" autocomplete="off"> 
<div class="modal fade" id="ReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-xl" role="document">
   <div class="modal-content">
   
   <div class="modal-header bg-primary">
		Update Truck Voucher :
    </div>
	
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12 table-responsive" style="overflow:auto">
				<div id="result_modal_data"></div>
			</div>
		</div>
	</div>
	
	<input type="hidden" name="frno" id="modal_frno">	
	<input type="hidden" name="id" id="modal_id">	
		
	<div class="modal-footer">
		<button type="button" id="req_close_button" onclick="document.getElementById('menu_button2').click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="req_update_button" class="btn btn-primary">Update</button>
	</div>
		</div>	
     </div>
 </div>	
</form>
 
<?php include ("./footer.php"); ?>

</div>
</body>
</html>