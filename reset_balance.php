<?php 
require_once("./connect.php");

$frno=escapeString($conn,strtoupper($_POST['frno']));
$id=escapeString($conn,strtoupper($_POST['id']));
$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$get_fm = Qry($conn,"SELECT frno,company,branch,branch_user,adv_branch_user,ptob,baladv,unloadd,detention,gps_rent,gps_device_charge,bal_tds,
otherfr,claim,late_pod,totalbal,bal_branch_user,paidto,bal_date,paycash,paycheq,paydsl,newrtgsamt,branch_bal,gps_id 
FROM freight_form WHERE id='$id'");

if(!$get_fm){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_fm)==0)
{
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_fm = fetchArray($get_fm);

// if($row_fm['gps_id']!="0")
// {
	// echo "<script>
		// alert('Can\'t reset voucher mapped with gps device !');
		// $('#loadicon').hide();
		// $('#bal_reset_id').attr('disabled',false);
	// </script>";
	// exit();
// }

if($row_fm['frno']!=$frno)
{
	echo "<script>
		alert('Freight memo not verified !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$fm_branch=$row_fm['branch_bal'];
$company=$row_fm['company'];

$update_log = "Branch : $row_fm[branch]($row_fm[branch_user]), AdvUser: $row_fm[adv_branch_user], Company: $company, O/B: $row_fm[paidto], 
BalanceAmount: $row_fm[baladv], Unloading: $row_fm[unloadd], Detention: $row_fm[detention], GPS_Rent: $row_fm[gps_rent], 
GPS_Device_Charge: $row_fm[gps_device_charge], TDS: $row_fm[bal_tds], Others: $row_fm[otherfr], Claim: $row_fm[claim], 
Late_POD: $row_fm[late_pod], Total_Bal: $row_fm[totalbal], Bal_date: $row_fm[bal_date], Cash: $row_fm[paycash], Cheq: $row_fm[paycheq], 
Diesel: $row_fm[paydsl], Rtgs: $row_fm[newrtgsamt], Bal_Branch: $row_fm[branch_bal]($row_fm[bal_branch_user]).";
	
if($row_fm['ptob']=='')
{
	echo "<script>
		alert('Freight_Memo Advance Pending !');
		$('#loadicon').hide();
		$('#bal_reset_id').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['paidto']=='')
{
	echo "<script>
		alert('Freight_Memo Balance Pending !');
		$('#loadicon').hide();
		$('#bal_reset_id').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['gps_rent']>0)
{
	echo "<script>
		alert('GPS Rent deducted. Can\'t reset balance !');
		$('#loadicon').hide();
		$('#bal_reset_id').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['gps_device_charge']>0)
{
	echo "<script>
		alert('GPS Device charges deducted. Can\'t reset balance !');
		$('#loadicon').hide();
		$('#bal_reset_id').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['paidto']=='NULL')
{
	if($row_fm['totalbal']>0)
	{
		errorLog("Invalid Balance amount. FM_Id: $id.",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Invalid Balance amount.');
			$('#loadicon').hide();
			// $('#bal_reset_id').attr('disabled',false);
		</script>";
		exit();
	}
}

	$cash=$row_fm['paycash'];
	$cheque=$row_fm['paycheq'];
	$diesel=$row_fm['paydsl'];
	$neft=$row_fm['newrtgsamt'];
		
	$total_balance=$cash+$cheque+$diesel+$neft;
		
	if($neft>0)
	{
		$chk_neft=Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND type='BALANCE' AND colset_d='1'");
		if(!$chk_neft){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($chk_neft)>0)
		{
			echo "<script>
				alert('NEFT Request Download by Accounts !');
				$('#loadicon').hide();
				// $('#bal_reset_id').attr('disabled',false);
			</script>";
			exit();	
		}
	}
		
	if($diesel>0)
	{
		$chk_diesel=Qry($conn,"SELECT id FROM diesel_fm WHERE fno='$frno' AND type='BALANCE' 
		AND IF(dsl_by='PUMP',approval,done)='1'");
		
		if(!$chk_diesel){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($chk_diesel)>0)
		{
			echo "<script>
				alert('Diesel Request done by Diesel Department. Delete Diesel First !');
				$('#loadicon').hide();
				// $('#bal_reset_id').attr('disabled',false);
			</script>";
			exit();	
		}
		
		$chk_diesel_sum = Qry($conn,"SELECT SUM(qty) as total_qty FROM diesel_fm WHERE fno='$frno' AND type='BALANCE'");
		if(!$chk_diesel_sum){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		$row_diesel_qty = fetchArray($chk_diesel_sum);
		$diesel_Qty = $row_diesel_qty['total_qty'];
	}
	else
	{
		$diesel_Qty = 0;
	}
	
StartCommit($conn);
$flag = true;
	
	if($neft>0)
	{
		if($row_fm['bal_date']==$date)
		{	
			$update_today_data_rtgs=Qry($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$neft' WHERE 
			branch='$fm_branch' AND date='$date'");
			
			if(!$update_today_data_rtgs){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
			
		$neft_delete=Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$frno' AND type='BALANCE' AND colset_d!='1'");
		if(!$neft_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		if(AffectedRows($conn)==0){
			$flag = false;
			errorLog("UNABLE TO DELETE RTGS PAYMENT WHEN RTGS AMOUNT IS GRETER THAN 0. Vou_No: $frno.",$conn,$page_name,__LINE__);
		}
				
		$passbook_delete=Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno' AND desct='Balance NEFT/RTGS Amount'");
		if(!$passbook_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($diesel>0)
	{
		if($row_fm['bal_date']==$date)
		{	
			$update_today_data_dsl=Qry($conn,"UPDATE today_data SET diesel_qty_market=diesel_qty_market-'$diesel_Qty',
			diesel_amount_market=diesel_amount_market-'$diesel' WHERE branch='$fm_branch' AND date='$date'");	
			
			if(!$update_today_data_dsl){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
			
		$diesel_delete=Qry($conn,"DELETE FROM diesel_fm WHERE fno='$frno' AND type='BALANCE' AND 
		IF(dsl_by='PUMP',approval,done)!='1'");
		
		if(!$diesel_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0){
			$flag = false;
			errorLog("UNABLE TO DELETE Diesel PAYMENT WHEN Diesel AMOUNT IS GRETER THAN 0. Vou_No: $frno.",$conn,$page_name,__LINE__);
		}
	}

	if($cheque>0)
	{
		$cheque_delete = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno' AND desct='Balance Cheque Amount'");
		if(!$cheque_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$cheque_delete2=Qry($conn,"DELETE FROM cheque_book WHERE vou_no='$frno' AND pay_type='BAL'");
		if(!$cheque_delete2){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	
	if($cash>0)
	{
		$get_cash_id = Qry($conn,"SELECT id,debit,debit2 FROM cashbook WHERE vou_no='$frno' AND desct='Balance Cash Amount'");
		if(!$get_cash_id){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_cash_id)==0)
		{
			$flag = false;
			errorLog("UNABLE TO FETCH CASH ENTRY WHILE CASH AMOUNT IS GREATER THAN 0. Vou_No : $frno.",$conn,$page_name,__LINE__);
		}
				
		$row_cash = fetchArray($get_cash_id);
				
		$cash_id = $row_cash['id'];
		
		$delete_cash_entry = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
		if(!$delete_cash_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($company=='RRPL'){
			$balance_col="balance";
		}
		else{
			$balance_col="balance2";
		}
		
		$update_balance = Qry($conn,"UPDATE user SET `$balance_col`=`$balance_col`+'$cash' WHERE username='$fm_branch'");
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_cashbook=Qry($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`+'$cash' WHERE id>$cash_id AND comp='$company' 
		AND user='$fm_branch'");
		if(!$update_cashbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($row_fm['bal_date']==$date)
	{
		$update_today_data=Qry($conn,"UPDATE today_data SET fm_bal=fm_bal-1,fm_bal_amount=fm_bal_amount-'$total_balance' 
		WHERE branch='$fm_branch' AND date='$date'");
		if(!$update_today_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	
	$reset_balance = Qry($conn,"UPDATE freight_form SET unloadd=0,detention=0,gps_rent=0,gps_device_charge=0,bal_tds=0,otherfr=0,claim=0,
	late_pod=0,totalbal='NA',bal_branch_user='',paidto='',bal_date=0,pto_bal_name='',bal_pan='',paycash=0,paycheq=0,paycheqno='',paydsl=0,
	newrtgsamt=0,rtgs_bal=0,narra='',colset_bal=0,colset_d_bal=0,bal_download='',pod=0,branch_bal='' WHERE id='$id'");
	
	if(!$reset_balance){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}	
	
	$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
	('$id','$row_fm[frno]','FM_UPDATE','BALANCE_RESET','$update_log','','ADMIN','$timestamp')");

	if(!$insertLog){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		echo "<script>
			alert('Freight Memo Balance Reset Success !');
			$('#get_button').attr('disabled',false);
			// $('#bal_reset_id').attr('disabled',false);
			$('#get_button').click();
		</script>";
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		Redirect("Error While Processing Request.","./fm_view.php");
		exit();
	}	

?>