<script type="text/javascript">
$(document).ready(function (e) {
$("#ChanageRateFreight").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#chanage_freight_button").attr("disabled", true);
	$.ajax({
	url: "./update_rate_freight.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="ChanageRateFreight" autocomplete="off"> 
<div class="modal fade" id="FreightModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
	   
	   <div class="modal-header bg-primary">
		Change Rate/Freight :
      </div>
	  
		<div class="modal-body">
			
	<div class="row">
	
		   
<script type="text/javascript">			
function Known(elem){
	
	var chrg1=$("#weight_g").val();
	
	// $("#actual2").val($('#rate_freight_db_freight').val());
	$("#rate2").val($('#rate_freight_db_rate').val());
	$("#actual2").val(Math.round(Number($("#rate2").val()) * Number(chrg1)));
	
	if(chrg1=='')
	{
		alert('Please enter charge weight..');
		$("#known").val('');
	}
	else
	{
		if(elem == "FIX")
		{
			$("#rate2").attr("readonly",true);
			$("#actual2").attr("readonly",false);
			$("#actual2").attr("oninput","calcFix();");
			$("#rate2").attr("oninput",false);
		}
		else if(elem == "RATE")
		{
			$("#actual2").attr("readonly",true);
			$("#rate2").attr("readonly",false);
			$("#rate2").attr("oninput","calcRate();");
			$("#actual2").attr("oninput",false);
		}
		else if(elem == "ZERO")
		{
			$("#actual2").attr("readonly",true);
			$("#rate2").attr("readonly",true);
			$("#actual2").attr("oninput",false);
			$("#rate2").attr("oninput",false);
			
			$("#actual2").val('0');
			$("#rate2").val('0');
		}
		else
		{
			$("#actual2").attr("readonly",true);
			$("#rate2").attr("readonly",true);
			$("#actual2").attr("oninput",false);
			$("#rate2").attr("oninput",false);
		}
	}
}
</script>

<script type="text/javascript"> 
function calcFix(){
$("#rate2").val((Number($("#actual2").val()) / Number($("#weight_g").val())).toFixed(2));	
}

function calcRate(){
	$("#actual2").val(Math.round(Number($("#rate2").val()) * Number($("#weight_g").val())));
	}
	
function CalculateAll(weight){
	$("#actual2").val(Math.round(Number($("#rate2").val()) * Number(weight)));
	}	
</script> 	

			<div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Charge Wt <font color="red"><sup>*</sup></font></label>
					   <input class="form-control" id="weight_g" oninput="CalculateAll(this.value)" name="weight" type="number" step="any" max="90" min="0.05" required />
                  </div>
              </div>
			  
			  <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Rate/FIX <font color="red"><sup>*</sup></font></label>
					   <select id="known" onchange="Known(this.value)" name="known" required="required" class="form-control">
							<option value="">Select an option</option>
							<option value="FIX">Fix Freight (फिक्स भाड़ा)</option>
							<option value="RATE">Rate (रेट)</option>
							<option value="ZERO">ZERO Freight</option>
						</select>
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Rate PMT <font color="red"><sup>*</sup></font></label>
					    <input step="any" min="0" name="rate" type="number" id="rate2" class="form-control" required readonly />
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Freight <font color="red"><sup>*</sup></font></label>
					   <input type="number" min="0" name="actual_freight" id="actual2" class="form-control" readonly required />
                  </div>
               </div>
			 </div>
		</div>
		
		<input type="hidden" id="rate_freight_vou_no" name="vou_no">
		<input type="hidden" id="rate_freight_vou_id" name="vou_id_fm">
		
		<input type="hidden" id="rate_freight_db_freight">
		<input type="hidden" id="rate_freight_db_weight">
		<input type="hidden" id="rate_freight_db_rate">
		
	<div class="modal-footer">
		<button type="button" id="close_freight_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="chanage_freight_button" class="btn btn-primary">Update</button>
	</div>
		
		</div>	
     </div>
 </div>	
 </form>