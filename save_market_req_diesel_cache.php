<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$dsl_by = escapeString($conn,$_POST['dsl_by']);
$qty = escapeString($conn,$_POST['qty']);
$rate = escapeString($conn,$_POST['rate']);
$amt = escapeString($conn,$_POST['amt']);
// $cash = escapeString($conn,$_POST['cash']);
$card = escapeString($conn,$_POST['card_no']);
$diesel_id = escapeString($conn,$_POST['diesel_id']);
$card_id = escapeString($conn,$_POST['card_id']);
$phy_card_no = escapeString($conn,$_POST['phy_card_no']);
$mobile_no = escapeString($conn,$_POST['mobile_no']);

if($qty=='' || $rate=='' || $amt=='' || $card=='')
{
	echo "<script>
			alert('All field are required !');
			$('#loadicon').hide();
		</script>";
	exit();
}

if($qty>600)
{
	echo "<script>
		alert('Max Qty allowed is 400 LTR !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($rate>120)
{
	echo "<script>
			alert('Max Rate allowed is Rs : 90 per Ltr!');
			$('#loadicon').hide();
		</script>";
	exit();
}

if($dsl_by=='OTP' AND strlen($mobile_no)!=10 AND strlen($card)!=10)
{
	echo "<script>
			alert('Invalid mobile number !');
			$('#loadicon').hide();
		</script>";
	exit();	
}

if($dsl_by=='CARD' || $dsl_by=='OTP')
{
	$comp = escapeString($conn,$_POST['comp']);
	$qty="0";
	$rate="0";
}
else
{
	$qry_pump_code = Qry($conn,"SELECT comp,active,consumer_pump FROM diesel_pump WHERE code='$card'");
	if(!$qry_pump_code){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($qry_pump_code)==0)
	{
		echo "<script>
			alert('Invalid Pump Selected !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_pump_code = fetchArray($qry_pump_code);
	
	if($row_pump_code['active']=="0")
	{
		echo "<script>
			alert('Pump is Inactive !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	if($row_pump_code['consumer_pump']=="1")
	{
		echo "<script>
			alert('Consusmer pump is not editable !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	if($amt!=(round($qty*$rate)))
	{
		echo "<script>
			alert('Check Diesel Qty or Rate or Amount !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$comp = escapeString($conn,$row_pump_code['comp']);
}	

if($card=='' || $comp=='')
{
	echo "<script>
		alert('Card or Company not found. Card_No : $card and Company : $comp !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$get_diesel = Qry($conn,"SELECT disamt,dsl_by,dcard,veh_no,dsl_mobileno,done,colset_d FROM diesel_fm WHERE id='$diesel_id'");

if(!$get_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_diesel)==0)
{
	echo "<script>
		alert('Diesel entry not found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_db = fetchArray($get_diesel);

if($row_db['dsl_by']=='PUMP')
{
	if($row_db['colset_d']=="1")
	{
		echo "<script>
			alert('Diesel entry downloaded by diesel pump user !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
else
{
	if($row_db['done']=="1")
	{
		echo "<script>
			alert('Diesel recharge done by diesel dpt. !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

if($row_db['dsl_by']=='CARD' AND $row_db['dcard']==$card AND $row_db['disamt']==$amt AND $row_db['veh_no']==$phy_card_no)
{
	echo "<script>
			alert('Nothing to update !');
			$('#loadicon').hide();
		</script>";
	exit();
}
else if($row_db['dsl_by']=='OTP' AND $row_db['disamt']==$amt AND $row_db['dsl_mobileno']==$mobile_no)
{
	echo "<script>
			alert('Nothing to update !');
			$('#loadicon').hide();
		</script>";
	exit();
}
else if($row_db['dsl_by']=='PUMP' AND $row_db['disamt']==$amt AND $row_db['veh_no']==$phy_card_no)
{
	echo "<script>
			alert('Nothing to update !');
			$('#loadicon').hide();
		</script>";
	exit();
}

$update = Qry($conn,"UPDATE diesel_cache SET qty='$qty',rate='$rate',diesel='$amt',card='$card',veh_no='$phy_card_no',
mobile_no='$mobile_no',dsl_comp='$comp',dsl_nrr='$comp-$card Rs: $amt/-',save='1' WHERE id='$id'");

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(AffectedRows($conn)==0)
{
	echo "<script>
		$('#save_result$id').html('No Update');
	</script>";
	// $result_1 = "<b><font color='red'>Zero</font></b>";
}
else
{
	echo "<script>
		$('#save_result$id').html('Done');
	</script>";
}

echo "<script>
		$('#editbutton1$id').attr('disabled',true);
		$('.qtyclass').attr('readonly',true);
		$('.rateclass').attr('readonly',true);
		$('.amtclass').attr('readonly',true);
		// $('.cashclass').attr('readonly',true);
		$('.cardclass').attr('readonly',true);
		$('.savebtnclass').attr('disabled',true);
		$('.pumpvalueclass').attr('disabled',true);
		$('.fuelcompclass').attr('disabled',true);
		
		$('.qtyclass').attr('oninput','');
		$('.rateclass').attr('oninput','');
		$('.amtclass').attr('oninput','');
		$('#loadicon').hide();
</script>";
?>