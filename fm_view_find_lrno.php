<?php
require_once("./connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$qry = Qry($conn,"SELECT DISTINCT frno,truck_no FROM freight_form_lr WHERE lrno='$lrno'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows=numRows($qry);

?>
<div class="form-group">
    <label>Voucher No <font color="red"><sup>*</sup></font></label>
    <select id="frno" name="frno" class="form-control">
		<option value="">--select voucher--</option>
<?php
if($numrows>0)
{
	while($row = fetchArray($qry))
	{
		echo "<option value='$row[frno]'>$row[frno] - $row[truck_no]</option>";
	}
}
?>
	</select>
</div>

<script>
	$('#fm_no_div').show();
	$('#loadicon').hide();
	$('#search_by').attr('disabled',true);
	$('#get_button').attr('disabled',false);
	$('#lrno_find').attr('onblur','');
</script>