<?php
require_once("./connect.php");

$SearchBy=mysqli_real_escape_string($conn,$_POST['SearchBy']);
$branch=mysqli_real_escape_string($conn,$_POST['branch']);

	if($_POST['range']=='FULL')
	{
		$from_date="2017-01-01";
		$to_date=date("Y-m-d");
	}
	else
	{
		$from_date=date('Y-m-d', strtotime($_POST['range'], strtotime(date("Y-m-d"))));
		$to_date=date("Y-m-d");
	}
	
$qry=mysqli_query($conn,"SELECT id,to_branch,company,amount,narr,date FROM debit WHERE date BETWEEN '$from_date' AND '$to_date' AND 
branch='$branch' AND section='$SearchBy'");

if(!$qry)
{
	echo mysqli_error($conn);
}

if(mysqli_num_rows($qry)>0)
{
	?>
	<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="16">Debit Summary : Branch <?php echo $branch; ?></th>
    </tr>
		<tr>    
			<th>Cr Branch</th>
			<th>Company</th>
			<th>Amount</th>
			<th>Date</th>
			<th>Narration</th>
			<th></th>
			<th></th>
		</tr>	
	<?php
	while($row=mysqli_fetch_array($qry))
	{	
			echo "<tr>
				<td>$row[to_branch]</td>
				<td>$row[company]</td>
				<td>$row[amount]</td>
				<td>".date('d-m-y',strtotime($row['date']))."</td>
				<td>$row[narr]</td>
				<td>
					<button type='button' onclick=Edit('$row[id]') class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
				</td>
				<td>
					<button type='button' onclick=Delete('$row[id]') class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button>
				</td>
			</tr>
			";
	}
	echo "</table>";
	
	echo "<script>
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<script>
		alert('No record Found !');
		window.location.href='debit.php';
	</script>";
	exit();
}
?>