<?php
require_once("./connect.php");

$search_by = escapeString($conn,strtoupper($_POST['search_by']));
$fm_no = escapeString($conn,strtoupper($_POST['fm_no']));
$lrno_fm_no = escapeString($conn,strtoupper($_POST['lrno_fm_no']));
$balance_amount = escapeString($conn,strtoupper($_POST['balance_amount']));
$other = escapeString($conn,strtoupper($_POST['other']));
$unloading = escapeString($conn,strtoupper($_POST['unloading']));
$detention = escapeString($conn,strtoupper($_POST['detention']));
$paid_amount = escapeString($conn,strtoupper($_POST['paid_amount']));
$utr_no = escapeString($conn,strtoupper($_POST['utr_no']));
$utr_date = escapeString($conn,strtoupper($_POST['utr_date']));
$narration = escapeString($conn,strtoupper($_POST['narration']));

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if($search_by!='LR' AND $search_by!='FM')
{
	echo "<script>
		alert('Invalid option selected for search by !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

if($search_by=='LR')
{
	$frno = $lrno_fm_no;
}
else
{
	$frno = $fm_no;
}

if($frno=='')
{
	echo "<script>
		alert('FM number not found !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

$get_fm_balance = Qry($conn,"SELECT branch,id,truck_no,totalf,bid,oid,ptob,date,baladv,paidto,company FROM freight_form WHERE frno='$frno'");

if(!$get_fm_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_fm_balance) == 0)
{
	echo "<script>
		alert('Freight-Memo not found !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

$row = fetchArray($get_fm_balance);

if(numRows($get_fm_balance) == 0)
{
	echo "<script>
		alert('Freight-Memo not found !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

if($row['paidto']!='')
{
	echo "<script>
		alert('Balance paid !!');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

if($paid_amount != ($row['baladv'] + $unloading + $detention - $other))
{
	echo "<script>
		alert('Payment amount not verified !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

if($paid_amount<=0)
{
	echo "<script>
		alert('Invalid Payment amount !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

$get_lrno = Qry($conn,"SELECT GROUP_CONCAT(lrno SEPARATOR ',') as lrno FROM freight_form_lr WHERE frno='$frno' GROUP BY frno");

if(!$get_lrno){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_lrno = fetchArray($get_lrno);

$lrno = $row_lrno['lrno'];
$truck_no = $row['truck_no'];
$fm_date = $row['date'];
$freight_amount = $row['totalf'];
$company = $row['company'];
$fm_branch = $row['branch'];

if($row['ptob']=='OWNER' || $row['ptob']=='NULL')
{
	$get_ac_details = Qry($conn,"SELECT pan,acname,accno,bank,ifsc FROM mk_truck WHERE id='$row[oid]'");

	if(!$get_ac_details){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$ac_type="Owner";
}
else
{
	$get_ac_details = Qry($conn,"SELECT pan,acname,accno,bank,ifsc FROM mk_broker WHERE id='$row[bid]'");

	if(!$get_ac_details){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$ac_type="Broker";
}

if(numRows($get_ac_details) == 0)
{
	echo "<script>
		alert('$ac_type\'s account details not found !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

$row_ac = fetchArray($get_ac_details);

$pan_no = $row_ac['pan'];

if(strlen($pan_no)!=10)
{
	echo "<script>
		alert('$ac_type\'s PAN number is not valid !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

if($row_ac['acname']=='')
{
	echo "<script>
		alert('$ac_type\'s account details not found !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL",$conn);
		
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$get_Crn = GetCRN("RR",$conn);
		
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	
$chk_for_crn = Qry($conn,"SELECT id FROM rtgs_fm WHERE crn='$crnnew'");	

if(!$chk_for_crn){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if(numRows($chk_for_crn)>0)
{
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL",$conn);
		
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$get_Crn = GetCRN("RR",$conn);
		
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		
		$crnnew = $get_Crn;
	}
}	

if($company=='RRPL')
{
	$debit_col='debit';		
	$balance_col='balance';		
}
else if($company=="RAMAN_ROADWAYS")
{
	$debit_col='debit2';		
	$balance_col='balance2';			
}

$insert_rtgs = Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,lrno,tno,
type,crn,colset,colset_d,approval,bank,utr_date,timestamp,narration,timestamp_approve) VALUES ('$frno','$fm_branch','$company','$freight_amount',
'$paid_amount','$row_ac[acname]','$row_ac[accno]','$row_ac[bank]','$row_ac[ifsc]','$pan_no','$date','$fm_date','$lrno','$truck_no','BALANCE',
'$crnnew','1','1','1','$utr_no','$utr_date','$timestamp','$narration','$timestamp')");

if(!$insert_rtgs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	$flag = false;
}

$insert_passbook = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,lrno,`$debit_col`,timestamp) 
VALUES ('$fm_branch','$frno','$date','$fm_date','$company','Freight_Memo','Balance NEFT/RTGS Amount','$lrno','$paid_amount','$timestamp')");
	
if(!$insert_passbook){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_fm_balance = Qry($conn,"UPDATE freight_form SET unloadd='$unloading',detention='$detention',otherfr='$other',totalbal='$paid_amount',
newrtgsamt='$paid_amount',bal_branch_user=adv_branch_user,paidto=ptob,bal_date='$date',pto_bal_name=pto_adv_name,bal_pan=adv_pan,rtgs_bal='1',
narra='Balance Forfeit. : $narration',pod='1',branch_bal=branch,forfeit_balance='1' WHERE id='$row[id]'");
	
if(!$update_fm_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	$flag = false;
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('OK : Success !');
		window.location.href='forfeit_adv_neft.php';
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request !');
		$('#loadicon').fadeOut('slow');
		$('#button_submit').attr('disabled', false);
	</script>";
	exit();
}	
?>