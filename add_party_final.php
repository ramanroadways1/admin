<?php 
require_once './connect.php';

$timestamp = date("Y-m-d H:i:s");

$chk_data = Qry($conn,"SELECT name FROM add_party_temp_table");
if(!$chk_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_data)==0)
{
	echo "<script>
		alert('No record found..');
		$('#final_add_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$flag_duplicate="NO";

$party_names=array();

while($row = fetchArray($chk_data))
{
	$chk_duplicate = Qry($conn,"SELECT id FROM consignee WHERE name='$row[name]'");
	
	if(!$chk_duplicate){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_duplicate)>0)
	{
		$flag_duplicate="YES";
		$party_names[] = $row['name'];
	}
}

if($flag_duplicate=='YES')
{
	$party_names = implode(", ",$party_names);
	
	echo "Duplicate party found : $party_names";
	
	echo "<script>
		$('#final_add_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_data2 = Qry($conn,"SELECT * FROM add_party_temp_table");
if(!$chk_data2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

StartCommit($conn);
$flag = true;

while($row2 = fetchArray($chk_data2))
{
	$insertParty = Qry($conn,"INSERT INTO consignee(name,legal_name,trade_name,state,gst,mobile,pincode,addr,branch,timestamp) VALUES 
	('$row2[name]','$row2[name]','$row2[name]','$row2[state]','','','$row2[pincode]','','ADMIN','$timestamp')");

	if(!$insertParty){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$insert_id = getInsertID($conn);
		
	$updateCode = Qry($conn,"UPDATE consignee SET code='$insert_id' WHERE id='$insert_id'");
	if(!$updateCode){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
}

$dlt_all = Qry($conn,"DELETE FROM add_party_temp_table");
	if(!$dlt_all){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
					
	echo "<script>
		alert('Record added successfully.');
		window.location.href='./add_party_excel.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error..');$('#final_add_btn').attr('disabled',false);$('#loadicon').fadeOut('slow');</script>";
	exit();
}		
?>