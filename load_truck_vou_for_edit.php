<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$vou_type = escapeString($conn,strtoupper($_POST['vou_type']));
$id = escapeString($conn,strtoupper($_POST['id']));

$get_vou = Qry($conn,"SELECT id,newdate,truckno,amt,mode,dname,chq_no,chq_bank,ac_name,ac_no,bank,ifsc,pan,dest,hisab_vou,colset_d FROM 
mk_tdv WHERE id='$id'");

if(!$get_vou){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_vou)==0)
{
	echo "<script>
		alert('Voucher not found !');
		window.location.href='./truck_voucher.php';
	</script>";
	exit();
}

$row=fetchArray($get_vou);

if($row['hisab_vou']=="1")
{
	echo "<script>
		alert('You can\'t edit hisab voucher !');
		// window.location.href='./truck_voucher.php';
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row['mode']=='')
{
	echo "<script>
		alert('Unable to fetch Payment Mode !');
		window.location.href='./truck_voucher.php';
	</script>";
	exit();
}

if($row['mode']=='HAPPAY')
{
	echo "<script>
		alert('You can\'t edit happay transactions !');
		$('#loadicon').hide();
	</script>";
	exit();
}

		if($row['mode']=='CASH' || $row['mode']=='HAPPAY')
		{
			$type11="dairy.cash";
		}
		else if($row['mode']=='CHEQUE')
		{
			$type11="dairy.cheque";
		}
		else if($row['mode']=='NEFT')
		{
			$type11="dairy.rtgs";
		}
		else
		{
			echo "<script>
				alert('Invalid option selected !');
				window.location.href='truck_voucher.php';
			</script>";
			exit();
		}

if($vou_type=='MAIN')
{
	$ac_set=0;
	$driver_name=$row['dname'];
	$tno=$row['truckno'];
	$driver_code="";
	$trip_id="";
}
else
{
	$qry_fetch_driver = Qry($conn,"SELECT trip_id,trans_id FROM $type11 WHERE vou_no='$frno'");
	if(!$qry_fetch_driver){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($qry_fetch_driver)==0)
	{
		echo "<script>
			alert('Unable to fetch Transaction Id !');
			window.location.href='./truck_voucher.php';
		</script>";
		exit();
	}
	
	$row_trans_id = fetchArray($qry_fetch_driver);
	
	$trip_id=$row_trans_id['trip_id'];
	$trans_id=$row_trans_id['trans_id'];
	
	$get_running_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE id='$trip_id'");
	if(!$get_running_trip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($get_running_trip)==0)
	{
		echo "<script>
			alert('Running Trip not found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$chk_driver_book = Qry($conn,"SELECT id,driver_code,tno FROM dairy.driver_book WHERE trans_id='$trans_id'");
	if(!$chk_driver_book){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_driver_book)==0)
	{
		echo "<script>
			alert('Unable to fetch Driver Code !');
			window.location.href='./truck_voucher.php';
		</script>";
		exit();
	}
	
	$row_driver_code = fetchArray($chk_driver_book);
	
	$sel_d_name = Qry($conn,"SELECT name FROM dairy.driver WHERE code='$row_driver_code[driver_code]'");
	if(!$sel_d_name){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($sel_d_name)==0)
	{
		echo "<script>
			alert('No Driver Found !');
			window.location.href='./truck_voucher.php';
		</script>";
		exit();
	}
	
	$row_d_name = fetchArray($sel_d_name);
	
	$chk_ac_details = Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$row_driver_code[driver_code]'");
	if(!$chk_ac_details){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_ac_details)==0)
	{
		echo "<script>
			alert('Unable to fetch Driver Account details !');
			window.location.href='./truck_voucher.php';
		</script>";
		exit();
	}
	
	$row_ac_details = fetchArray($chk_ac_details);
	
	$ac_set=1;
	
	$driver_name=$row_d_name['name'];
	$tno=$row_driver_code['tno'];
	$driver_code=$row_driver_code['driver_code'];
	$driver_book_id=$row_driver_code['id'];
	
	if($row['truckno']!=$tno)
	{
		errorLog("Vou_no: $frno. Vehicle Number not verified or Transaction belongs to another Vehicle or Trip !",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Vehicle Number not verified or Transaction belongs to another Vehicle or Trip !');
			window.location.href='./truck_voucher.php';
		</script>";
		exit();
	}
	
	$chk_driver_code_validation=Qry($conn,"SELECT id,code FROM dairy.driver_up WHERE tno='$row[truck_no]' AND code='$driver_code' AND 
	down=0 ORDER BY id DESC LIMIT 1");
	if(!$chk_driver_code_validation){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_driver_code_validation)==0)
	{
		echo "<script>
			alert('Driver Code not Verified !');
			window.location.href='./truck_voucher.php';
		</script>";
		exit();
	}
}
?>

<script>
function MyFunc1()
{
	$("#req_update_button").attr("disabled", true);
	$('#main_div_main').hide();
	$('#change_tno_div').show();
}
</script>

<script type="text/javascript">
$(function() {
    $("#truck_no").autocomplete({
      source: './autofill/get_own_vehicle.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			 $(event.target).focus();
			 $("#req_update_button").attr("disabled",true);
			alert('Truck Number doest not exists.');
        }
    }, 
    focus: function (event, ui) {
        $("#req_update_button").attr("disabled",false);
		return false;
    }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#TruckNoForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_change_tno").attr("disabled", true);
	$.ajax({
	url: "./update_truck_no_truck_vou.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th style="font-size:13px;" colspan="16">
	   <?php echo "Vou No : ".$frno.", Truck No : ".$row['truckno'];
	  // echo " <button type='button' class='btn btn-sm btn-danger' onclick='MyFunc1();'>Change</button>"; 
	  ?>
	   </th>
    </tr>
	
</table>	

<div class="container-fluid" id="change_tno_div" style="display:none">
        <div class="row">
          <div class="col-md-12">
	
	<form id="TruckNoForm">
	
		<div class="form-group col-md-4"> 
			<label>Truck Number <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" id="truck_no" required name="tno">
		</div>
		
		<input type="hidden" value="<?php echo $frno; ?>" name="frno">
		<input type="hidden" value="<?php echo $vou_type; ?>" name="vou_type">
		<input type="hidden" value="<?php echo $ac_set; ?>" name="random_var">
		
		<input type="hidden" value="<?php echo $driver_name; ?>" name="driver_name">
		<input type="hidden" value="<?php echo $tno; ?>" name="tno">
		<input type="hidden" value="<?php echo $driver_code; ?>" name="driver_code">
		<input type="hidden" value="<?php echo $trip_id; ?>" name="trip_id">
		
		<div class="form-group col-md-4"> 
			<button type="submit" id="button_change_tno" class="btn btn-success">Change Truck Number</button>
		</div>
		
	</form>	
	
		</div>
	</div>
</div>

<div class="container-fluid" id="main_div_main">
    <div class="row">
      
		<div class="form-group col-md-3"> 
			<label>Vou Date <font color="red"><sup>*</sup></font></label>
			<input type="date" name="vou_date" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?php echo $row['newdate']; ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Amount <font color="red"><sup>*</sup></font></label>
			<input type="number" min="1" name="amount" class="form-control" value="<?php echo $row['amt']; ?>" required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Payment Mode <font color="red"><sup>*</sup></font></label>
			<select class="form-control" onchange="PaymentBy(this.value)" name="pay_mode" required>
				<option value="">Select an option</option>
				<option <?php if ($row['mode'] == 'CASH') echo ' selected="selected"'; ?> value="CASH">CASH</option>
				<option <?php if ($row['mode'] == 'CHEQUE') echo ' selected="selected"'; ?> value="CHEQUE">CHEQUE</option>
				<option <?php if ($row['mode'] == 'NEFT') echo ' selected="selected"'; ?> value="NEFT">NEFT</option>
			</select>
		</div>
			
		<div id="chq_div" style="display:none" class="form-group col-md-3"> 
			<label>Bank Name (Cheque Bank) <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" id="chq_bank" oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" name="chq_bank" value="<?php echo $row['chq_bank']; ?>" required>
		</div>	
		
		<div id="chq_div1" style="display:none" class="form-group col-md-3"> 
			<label>Cheque No <font color="red"><sup>*</sup></font></label>
			<input type="number" min="1" id="chq_no" name="chq_no" class="form-control" value="<?php echo $row['chq_no']; ?>" required>
		</div>
		
		<div id="rtgs_div" style="display:none" class="form-group col-md-3"> 
			<label>PAN No </label>
			<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePAN();" maxlength="10" minlength="10" type="text" name="pan_no" id="pan_no" class="form-control" value="<?php echo $row['pan']; ?>">
		</div>
		
		<div id="rtgs_div1" style="display:none" class="form-group col-md-3"> 
			<label>Ac Holder <font color="red"><sup>*</sup></font></label>
			<input type="text" oninput='this.value=this.value.replace(/[^a-z A-Z]/,"")' name="ac_holder" id="ac_holder" class="form-control" readonly value="<?php echo $row['ac_name']; ?>" required>
		</div>
		
		<div id="rtgs_div2" style="display:none" class="form-group col-md-3"> 
			<label>A/c No <font color="red"><sup>*</sup></font></label>
			<input type="text" oninput='this.value=this.value.replace(/[^a-zA-Z0-9]/,"")' name="ac_no" id="ac_no" class="form-control" readonly value="<?php echo $row['ac_no']; ?>" required>
		</div>
		
		<div id="rtgs_div3" style="display:none" class="form-group col-md-3"> 
			<label>Bank Name <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" name="bank_name" id="bank_name" readonly value="<?php echo $row['bank']; ?>" required>
		</div>
		
		<div id="rtgs_div4" style="display:none" class="form-group col-md-3"> 
			<label>IFSC Code <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" maxlength="11" minlength="11" pattern="[a-zA-Z]{4}[0]{1}[a-zA-Z0-9]{6}" oninput='this.value=this.value.replace(/[^a-zA-Z0-9]/,"");ValidateIFSC()' name="ifsc" id="ifsc" readonly value="<?php echo $row['ifsc']; ?>" required>
		</div>
		
		<div class="form-group col-md-6"> 
			<label>Narration <font color="red"><sup>*</sup></font></label>
			<textarea class="form-control" name="narration" required><?php echo $row['dest']; ?></textarea>
		</div>
		
		<input type="hidden" value="<?php echo $vou_type; ?>" name="vou_type">
		<input type="hidden" value="<?php echo $frno; ?>" name="frno">
		<input type="hidden" value="<?php echo $ac_set; ?>" name="random_var">
		
		<input type="hidden" value="<?php echo $driver_name; ?>" name="driver_name">
		<input type="hidden" value="<?php echo $tno; ?>" name="tno">
		<input type="hidden" value="<?php echo $driver_code; ?>" name="driver_code">
		<input type="hidden" value="<?php echo $trip_id; ?>" name="trip_id">

	</div>
</div>

<script>
function ValidatePAN() { 
  var Obj = document.getElementById("pan_no");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                //alert("Invalid Pan No");
				document.getElementById("pan_no").setAttribute(
   "style","background-color:red;text-transform:uppercase;color:#fff;");
   document.getElementById("req_update_button").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
              //alert("Correct Pan No");
			  document.getElementById("pan_no").setAttribute(
   "style","background-color:green;color:#fff;text-transform:uppercase;");
				document.getElementById("req_update_button").removeAttribute("disabled");
				}
        }
		else
		{
			document.getElementById("pan_no").setAttribute(
   "style","background-color:white;color:#fff;text-transform:uppercase;");
	document.getElementById("req_update_button").removeAttribute("disabled");
		}
  } 			
</script>	

<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ifsc");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
                //alert("Invalid Pan No");
				document.getElementById("ifsc").setAttribute(
   "style","background-color:red;text-transform:uppercase;color:#fff;");
   document.getElementById("req_update_button").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
              //alert("Correct Pan No");
			  document.getElementById("ifsc").setAttribute(
   "style","background-color:green;color:#fff;text-transform:uppercase;");
			document.getElementById("req_update_button").removeAttribute("disabled");
              }
        }
		else
		{
			document.getElementById("ifsc").setAttribute(
   "style","background-color:white;color:#fff;text-transform:uppercase;");
	document.getElementById("req_update_button").removeAttribute("disabled");
		//	$("#pan_div").val("1");
		}
  } 			
</script>	

<script>
function PaymentBy(elem)
{
	$("#req_update_button").attr("disabled", false);
	
	if(elem=='CHEQUE')
	{	
		$('#rtgs_div').hide();
		$('#rtgs_div1').hide();
		$('#rtgs_div2').hide();
		$('#rtgs_div3').hide();
		$('#rtgs_div4').hide();
		
		$('#chq_div').show();
		$('#chq_div1').show();
		
		$('#chq_bank').attr('required',true);
		$('#chq_no').attr('required',true);
		
		$('#ac_holder').attr('required',false);
		$('#ac_no').attr('required',false);
		$('#bank_name').attr('required',false);
		$('#ifsc').attr('required',false);
	}
	else if(elem=='NEFT')
	{
		$('#rtgs_div').show();
		$('#rtgs_div1').show();
		$('#rtgs_div2').show();
		$('#rtgs_div3').show();
		$('#rtgs_div4').show();
		
		$('#chq_div').hide();
		$('#chq_div1').hide();
		
		$('#chq_bank').attr('required',false);
		$('#chq_no').attr('required',false);
		
		$('#ac_holder').attr('required',true);
		$('#ac_no').attr('required',true);
		$('#bank_name').attr('required',true);
		$('#ifsc').attr('required',true);
		
		<?php
		if($ac_set==1)
		{
		?>
		$('#ac_holder').attr('readonly',true);
		$('#ac_no').attr('readonly',true);
		$('#bank_name').attr('readonly',true);
		$('#ifsc').attr('readonly',true);
		
		$('#ac_holder').val("<?php echo $row_ac_details['acname']; ?>");
		$('#ac_no').val("<?php echo $row_ac_details['acno']; ?>");
		$('#bank_name').val("<?php echo $row_ac_details['bank']; ?>");
		$('#ifsc').val("<?php echo $row_ac_details['ifsc']; ?>");
		<?php	
		}
		else
		{
		?>
		$('#ac_holder').attr('readonly',false);
		$('#ac_no').attr('readonly',false);
		$('#bank_name').attr('readonly',false);
		$('#ifsc').attr('readonly',false);
		<?php		
		}
		?>
	}
	else
	{
		$('#rtgs_div').hide();
		$('#rtgs_div1').hide();
		$('#rtgs_div2').hide();
		$('#rtgs_div3').hide();
		$('#rtgs_div4').hide();
		
		$('#chq_div').hide();
		$('#chq_div1').hide();
		
		$('#chq_bank').attr('required',false);
		$('#chq_no').attr('required',false);
		
		$('#ac_holder').attr('required',false);
		$('#ac_no').attr('required',false);
		$('#bank_name').attr('required',false);
		$('#ifsc').attr('required',false);
	}		
}		
</script>		
		
<script>
	document.getElementById('menu_button2').click();
	PaymentBy('<?php echo $row["mode"] ?>');
	$('#loadicon').hide();
</script>