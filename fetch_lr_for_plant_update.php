<?php
require_once("./connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$qry = Qry($conn,"SELECT id,plant,branch FROM lr_check WHERE lrno='$lrno'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script type='text/javascript'>
		alert('Invalid LR No entered !');
		window.location.href='./lr_entry_update_plant.php';
	</script>";	
	exit();
}

$row = fetchArray($qry);

if($row['branch']!='PUNE')
{
	echo "<script>
		alert('LR not belongs to PUNE branch !!');
		$('#plant').val('');
		$('#lr_id').val('');
		$('.plant_sel').attr('disabled',true);
		$('#lr_sub').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row['plant']=='')
{
	echo "<script>
		$('#plant').val('');
		$('#lr_id').val('$row[id]');
		$('.plant_sel').attr('disabled',false);
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}
else
{
	echo "<script>
		$('#lr_id').val('$row[id]');
		$('#plant').val('$row[plant]');
		$('.plant_sel').attr('disabled',false);
		$('#plant_$row[plant]').attr('style','background:yellow');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}
?>