<?php 
require_once './connect.php';

$get_data = Qry($conn,"SELECT * FROM add_party_temp_table ORDER BY name ASC");
					
if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

					if(numRows($get_data)>0)
					{
						echo "<table class='table table-bordered' style='font-size:13px'>
							<tr><td colspan='5'><button id='final_add_btn' type='button' class='pull-right btn btn-sm btn-success' onclick='SavePartyFinal()'>Add party</button></td></tr>
							<tr>
								<th>#</th>
								<th>Party name</th>
								<th>State</th>
								<th>Pincode</th>
								<th>#</th>
							</tr>";
						
						$sn=1;
						while($row2 = fetchArray($get_data))
						{
							echo "<tr>
								<td>$sn</td>
								<td>$row2[name]</td>
								<td>$row2[state]</td>
								<td>$row2[pincode]</td>
								<td><button type='button' class='btn btn-danger btn-sm' onclick='Remove($row2[id])'>Delete</button></td>
							</tr>";
						$sn++;	
						}
						
						echo "</table>";
					}
					else
					{
						echo "<font color='red'><center>No record found..</center></font>";
					}
					
echo "<script>
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled',false);
	</script>";
	
?>