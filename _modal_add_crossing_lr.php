<script type="text/javascript">
$(function() {
	$("#to_loc_crossing").autocomplete({
	source: 'autofill/get_location.php',
	// appendTo: '#appenddiv',
	select: function (event, ui) { 
		$('#to_loc_crossing').val(ui.item.value);   
		$('#edit_modal_from_id').val(ui.item.id);      
		return false;
	},
	change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to_loc_crossing').val("");   
			$('#edit_modal_from_id').val("");   
			alert('Location does not exists !');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(document).ready(function (e) {
$("#CrossingLRAddForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#add_btn_crossing_lr").attr("disabled", true);
	$.ajax({
	url: "./update_lr_by_id.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_CrossingLRForm").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<button type="button" style="display:none" data-toggle="modal" id="btn_modal_add_crossing_LR" data-target="#modal_add_crossing_LR"></button>

<form id="CrossingLRAddForm" autocomplete="off"> 
<div class="modal fade" id="modal_add_crossing_LR" style="background:#DDD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
	  
	  <div class="modal-header bg-primary">
		ADD Crossing LR
      </div>
	  
	<div class="modal-body">
	
<div class="row">

	<div class="col-md-7">
       <div class="form-group">
           <label class="control-label mb-1">LR No <font color="red"><sup>*</sup></font></label>
		   <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" id="crossing_modal_lrno" name="lrno" class="form-control" required>
      </div>
	</div>
	
	<div class="col-md-5" id="crossing_fetch_btn">
       <div class="form-group">
           <br>
		   <button style="margin-top:3px" type="button" onclick="GetCrossingInfo()" class="btn btn-danger">Check !</button>
      </div>
	</div>

	<div class="col-md-6">
       <div class="form-group">
          <label class="control-label mb-1">From Station <font color="red"><sup>*</sup></font></label>
		  <input type="text" readonly id="from_loc_crossing" name="from_loc" class="form-control" required />
      </div>
   </div>
	 
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">To Station <font color="red"><sup>*</sup></font></label>
		   <input type="text" readonly id="to_loc_crossing" name="to_loc" class="form-control" required />
      </div>
   </div>
   
	<div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Act. Weight <font color="red"><sup>*</sup></font></label>
			 <input type="number" readonly min="0.001" step="any" id="act_wt_crossing" name="act_weight" class="form-control" required />
		</div>
   </div>
   
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Chrg. Weight <font color="red"><sup>*</sup></font></label>
			<input type="number" readonly min="0.001" step="any" id="chrg_wt_crossing" name="chrg_weight" class="form-control" required />
		</div>
   </div>
   
    <div class="col-md-12">
       <div class="form-group">
           <label class="control-label mb-1">Eway bill no. <font color="red"><sup>*</sup></font></label>
		   <input type="text" maxlength="12" id="ewb_no_ip" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="ewb_no" class="form-control" required="required">
		</div>
   </div>
   
    <div class="col-md-12">
       <div class="form-group">
           <label class="control-label mb-1">Further crossing <font color="red"><sup>*</sup></font></label>
		   <select onchange="CrossingSelection(this.value)" id="further_crossing_crossing" name="crossing" class="form-control" required>
				<option value="">--select--</option>
				<option value="YES">Yes</option>
				<option value="NO">No</option>
		   </select>
		</div>
   </div>
   
   <script>
   function CrossingSelection(elem)
   {
	   var to_loc_db = $('#to_loc_crossing_db').val();
	   
	   if(to_loc_db=='')
	   {
		   $('#further_crossing_crossing').val('');
		   $('#crossing_modal_lrno').focus();
	   }
	   else
	   {
		   if(elem=='YES')
		   {
			   $('#to_loc_crossing').val('');
			   $('#to_loc_crossing').attr('readonly',false);
		   }
		   else
		   {
			   $('#to_loc_crossing').val(to_loc_db);
			   $('#to_loc_crossing').attr('readonly',true);
		   }
	   }
   }
   </script>
   
  </div>
  
</div>
		
	<input type="hidden" name="id" id="lr_edit_id">
	<input type="hidden" name="frno" id="edit_modal_frno">
	<input type="hidden" name="to_loc_db" id="to_loc_crossing_db">
	<input type="hidden" name="to_loc_id_db" id="to_loc_id_crossing_db">
	<input type="hidden" name="from_id" id="cross_loc_from_id">
	<input type="hidden" name="to_id" id="cross_loc_to_id">
	<input type="hidden" name="con1_id" id="cross_modal_con1_id">
	<input type="hidden" name="item_id" id="cross_modal_item_id">
			
 <div id="result_CrossingLRForm"></div>
 
	<div class="modal-footer">
		<button type="button" id="close_btn_crossing_lr_add" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="add_btn_crossing_lr" disabled class="btn btn-primary">Add LR</button>
	</div>
		</div>	
     </div>
 </div>
</form>
  
<script>  
// function EditLR(id)
// {
	// $('#lr_edit_id').val(id);
	// $("#loadicon").show();
	// jQuery.ajax({
	// url: "load_lr_by_id.php",
	// data: 'id=' + id,
	// type: "POST",
	// success: function(data) {
		// $("#edit_modal_lrs_record").html(data);
	// },
	// error: function() {}
	// });
// }  
</script>  