<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateLRForm").on('submit',(function(e) {
$("#loadicon").show();
$("#lr_sub").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./lr_update.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#lr_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
$(function() {  
      $("#item1").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/lr_items.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#item1').val(ui.item.value);   
               $('#item_id').val(ui.item.id);
				return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#item1").val('');
				$("#item_id").val('');
                alert('Item does not exist !'); 
              } 
              },
			}); 
}); 

$(function() {  
      $("#consignor").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignor.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignor').val(ui.item.value);   
               $('#consignor_id').val(ui.item.id);     
               $('#con1_gst').val(ui.item.gst);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignor").val('');
				$("#consignor_id").val('');
				$("#con1_gst").val('');
                alert('Consignor does not exist !'); 
              } 
              },
			}); 
});  

$(function() {  
      $("#consignee").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignee').val(ui.item.value);   
               $('#consignee_id').val(ui.item.id);     
               $('#con2_gst').val(ui.item.gst);     
               $('#consignee_pincode').val(ui.item.pincode);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignee").val('');
				$("#consignee_id").val('');
				$("#con2_gst").val('');
				$("#consignee_pincode").val('');
                alert('Consignee does not exist !'); 
              } 
              },
			}); 
});

$(function() {
		$("#tno_market").autocomplete({
		source: 'autofill/get_market_vehicle.php',
		select: function (event, ui) { 
            $('#tno_market').val(ui.item.value);   
            $('#wheeler_market_truck').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_market').val("");   
			$('#wheeler_market_truck').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#tno_own").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#tno_own').val(ui.item.value);   
            $('#wheeler_own_truck').val(ui.item.wheeler);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#tno_own').val("");   
			$('#wheeler_own_truck').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});	
</script>

<script type="text/javascript">
$(function() {
		$("#from").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
            $('#from').val(ui.item.value);   
            $('#from_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#from').val("");   
			$('#from_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#to").autocomplete({
		source: 'autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#to').val(ui.item.value);   
            $('#to_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#to').val("");   
			$('#to_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#dest_zone").autocomplete({
		source: 'autofill/get_location.php',
		select: function (event, ui) { 
            $('#dest_zone').val(ui.item.value);   
            $('#dest_zone_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#dest_zone').val("");   
			$('#dest_zone_id').val("");   
			alert('Location does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">LR - Entry</h3>
              </div>
              
<form role="form" id="UpdateLRForm" action="" method="POST" autocomplete="off">
			  
     <div class="card-body">
				
<script>		
function FetchLR(lrno)
{
	$("#lrno").attr('readonly',true);
	$('#loadicon').show();
	jQuery.ajax({
	url: "fetch_lr.php",
	data: 'lrno=' + lrno,
	type: "POST",
	success: function(data) {
		$("#lr_result").html(data);
	},
		error: function() {}
	});
}
</script>	

<div id="lr_result"></div>
		
<script type="text/javascript">			
function Lrtype(elem)
{
	if($('#vehile_type_db').val()=='MARKET')
	{
		$("#tno_own").val('');
		$("#tno_market").val($('#vehile_no_db').val());
		$("#wheeler_market_truck").val($('#wheeler_db').val());
	}
	else if($('#vehile_type_db').val()=='OWN')
	{
		$("#tno_own").val($('#vehile_no_db').val());
		$("#wheeler_own_truck").val($('#wheeler_db').val());
		$("#tno_market").val('');
	}
	else
	{
		$("#tno_own").val('');
		$("#tno_market").val('');
	}
	
	if(elem.value=="MARKET")
	{
		$("#div_market").show();
		$("#tno_market").attr('required',true);
		
		$("#div_own").hide();	
		$("#tno_own").attr('required',false);
	}
	else if(elem.value=="OWN")
	{
		$("#div_market").hide();
		$("#tno_market").attr('required',false);
		
		$("#div_own").show();	
		$("#tno_own").attr('required',true);
	}	
}
</script>

		<div class="row">	
		
			<div id="error_msg" class="alert alert-danger col-md-12" style="display:none"></div>
			<div id="error_msg_diesel" class="alert alert-warning col-md-12" style="display:none"></div>

			<div class="col-md-3">	
               <div class="form-group">
                  <label>LR No <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="FetchLR(this.value)" class="form-control" id="lrno" name="lrno" required>
              </div>
			</div>
			
			<div class="col-md-3" id="company_auto_div">	
               <div class="form-group">
                  <label>Company <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" id="company_auto" name="company_auto" required readonly>
              </div>
			</div>
			
			<div class="col-md-3" id="company_div" style="display:none">	
               <div class="form-group">
                  <label>Company <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" id="company" name="company" required>
					<option value="">Select an option</option>
					<option id="company_RAMAN_ROADWAYS" value="RRPL">RRPL</option>
					<option id="company_RRPL" value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Branch <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" name="branch" id="branch" required readonly>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Branch User <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" name="branch_user" id="branch_user" required readonly>
              </div>
			</div>
			
			<input type="hidden" id="open_lr" name="open_lr">
			<input type="hidden" id="vehile_type_db">
			<input type="hidden" id="vehile_no_db">
			<input type="hidden" id="wheeler_db">
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Vehicle Type <font color="red"><sup>*</sup></font></label>
                  <select onchange="Lrtype(this)" id="lr_type" name="lr_type" class="form-control" required="required">
					<option id="veh_type_OWN" value="MARKET">MARKET Vehicle</option>
					<option id="veh_type_MARKET" value="OWN">OWN Vehicle</option>
				  </select>
              </div>
			</div>
			
			<input type="hidden" id="wheeler_market_truck" name="wheeler_market">
			<input type="hidden" id="wheeler_own_truck" name="wheeler_own_truck">
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>LR Date <font color="red"><sup>*</sup></font></label>
                  <input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" name="lr_date" id="lr_date" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>System Date <font color="red"><sup>*</sup></font></label>
                  <input type="date" class="form-control" name="sys_date" id="sys_date" required readonly pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
              </div>
			</div>
			
			<div class="col-md-3" id="div_market" style="display:none1">	
               <div class="form-group">
                  <label>Market Truck No <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" class="form-control" name="tno_market" id="tno_market" required >
              </div>
			</div>
			
			<div class="col-md-3" id="div_own" style="display:none">	
               <div class="form-group">
                  <label>Own Truck No <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" class="form-control" name="tno_own" id="tno_own" required >
              </div>
			</div>
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>From Station <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" class="form-control" name="from" id="from" required >
              </div>
			</div>
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>To Station <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" class="form-control" name="to" id="to" required >
              </div>
			</div>
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>Destination Zone <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" required="required" class="form-control" name="dest_zone" id="dest_zone">
              </div>
			</div>
			
			<input type="hidden" id="from_id" name="from_id">
			<input type="hidden" id="to_id" name="to_id">
			<input type="hidden" id="dest_zone_id" name="dest_zone_id">
			
			<div class="col-md-8">	
               <div class="form-group">
                  <label>Consignor <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" name="consignor" id="consignor" required readonly>
              </div>
			</div>
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>Consignor GST <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" name="con1_gst" id="con1_gst" required readonly>
              </div>
			</div>
			
			<div class="col-md-8">	
               <div class="form-group">
                  <label>Consignee <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z 0-9A-Z.-]/,'')" class="form-control" name="consignee" id="consignee" required>
              </div>
			</div>
			
			<input type="hidden" id="consignor_id" name="consignor_id">
			<input type="hidden" id="consignee_id" name="consignee_id">
			<input type="hidden" id="consignee_pincode" name="consignee_pincode">
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>Consignee GST <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" name="con2_gst" id="con2_gst" required readonly>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>DO No <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z,A-Z-0-9/]/,'')" class="form-control" name="do_no" id="do_no" required >
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Shipment No <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z,A-Z-0-9/]/,'')" class="form-control" name="ship_no" id="ship_no" required >
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>BE/Invoice No <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-z, A-Z-0-9/]/,'')" class="form-control" name="inv_no" id="inv_no" required >
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>PO No <font color="red"><sup>*</sup></font></label>
                  <input required="required" type="text" oninput="this.value=this.value.replace(/[^a-z, A-Z-0-9/]/,'')" class="form-control" name="po_no" id="po_no" >
              </div>
			</div>
			
			<div class="col-md-2">	
               <div class="form-group">
                  <label>Act Weight <font color="red"><sup>*</sup></font></label>
                  <input type="number" step="any" min="0.010" max="90" class="form-control" name="act_wt" id="act_wt" required >
              </div>
			</div>
			
			<div class="col-md-2">	
               <div class="form-group">
                  <label>Gross Weight <font color="red"><sup>*</sup></font></label>
                  <input type="number" step="any" class="form-control" min="0.010" max="90" name="gross_wt" id="gross_wt" required >
              </div>
			</div>
			
			<div class="col-md-2">	
               <div class="form-group">
                  <label>Charge Weight <font color="red"><sup>*</sup></font></label>
                  <input type="number" step="any" class="form-control" min="0.010" max="90" oninput="ResetAll()" name="charge_wt" id="chrg_wt" required >
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Item Group <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');" name="item" id="item1" required>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Material/Item  <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');"
				  name="item_name" id="item_name" required>
              </div>
			</div>
			
			<input type="hidden" name="item_id" id="item_id">
			
			<div class="col-md-6">	
               <div class="form-group">
                  <label>Bank <font color="red"><sup>*</sup></font></label>
                  <input required="required" type="text" oninput="this.value=this.value.replace(/[^a-z 0-9A-Z.,/]/,'')" class="form-control" name="bank" id="bank">
              </div>
			</div>
			
			<div class="col-md-6">	
               <div class="form-group">
                  <label>Sold to Party <font color="red"><sup>*</sup></font></label>
                  <input required="required" type="text" id="sold_to_pay" oninput="this.value=this.value.replace(/[^a-z 0-9A-Z.,/]/,'')" name="sold_to_pay" class="form-control">
              </div>
			</div>
			
			<div class="col-md-6">	
               <div class="form-group">
                  <label>LR Name <font color="red"><sup>*</sup></font></label>
                  <input required="required" id="lr_name" type="text" oninput="this.value=this.value.replace(/[^a-z 0-9A-Z.,/]/,'')" name="lr_name" class="form-control" />
              </div>
			</div>
			
			<div class="col-md-6">	
               <div class="form-group">
                  <label>Consignee Address <font color="red"><sup>*</sup></font></label>
                  <textarea id="con2_addr" required="required" oninput="this.value=this.value.replace(/[^a-z A-Z0-9.,/-]/,'')" 
				  name="con2_addr" class="form-control"></textarea>
              </div>
			</div>
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>No of Articles</label>
                  <input id="articles" placeholder="No of Bags Etc." name="articles" type="text" oninput="this.value=this.value.replace(/[^a-z- A-Z0-9,/-]/,'')" class="form-control" />
              </div>
			</div>
			
			<div class="col-md-4">	
               <div class="form-group">
                  <label>Goods Value</label>
                  <input placeholder="Goods Value" min="0" step="any" id="goods_value" name="goods_value" type="number" class="form-control" />
              </div>
			</div>
			
			<div class="col-md-2">	
               <div class="form-group">
                  <label>Billing Rate</label>
                  <input oninput="sum1();" type="number" min="0" class="form-control" name="b_rate" id="b_rate">
              </div>
			</div>
			
			<div class="col-md-2">	
               <div class="form-group">
                  <label>Billing Amount</label>
                  <input oninput="sum2();" type="number" min="0" class="form-control" name="b_amt" id="b_amt">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Truck Type</label>
                  <select name="t_type" id="t_type" class="form-control">
					<option value="">--Select Option--</option>
					<option value="T1">T1</option>
					<option value="T2">T2</option>
					<option value="T3">T3</option>
					<option value="T4">T4</option>
					<option value="T5">T5</option>
					<option value="T6">T6</option>
					<option value="T7">T7</option>
					<option value="T8">T8</option>
					<option value="A2">A2</option>
					<option value="A5">A5</option>
					<option value="A8">A8</option>
					<option value="U2">U2</option>
					<option value="V2">V2</option>
					<option value="Y2">Y2</option>
				</select>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Freight Type <font color="red"><sup>*</sup></font></label>
                   <select name="freight_type" id="freight_type" class="form-control" required="required">
						<option value="">NULL</option>
						<option id="f_type1" value="PREPAID">PREPAID</option>
						<option id="f_type2" value="TOPAY">TOPAY</option>
					</select>
              </div>
			</div>
			
			<div class="col-md-6">	
               <div class="form-group">
                  <label>Description of Goods</label>
                 <textarea id="goods_desc" name="goods_desc" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9/-]/,'')" class="form-control"></textarea>
              </div>
			</div>
			
		</div>
	</div>
                
       <div class="card-footer">
			<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete_button" type="button" onclick="Delete_LR()" class="btn pull-right btn-danger" disabled>Delete LR</button>
       </div>
				
              </form>
            </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script> 
function Delete_LR()
{
	$('#lr_delete_button').attr('disabled',true);
	$('#lr_sub').attr('disabled',true);
	
	var lrno = $('#lrno').val();
	
	if(lrno=='')
	{
		alert('Unable to fetch LR No !');
		$('#lr_delete_button').attr('disabled',false);
		$('#lr_sub').attr('disabled',false);
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "delete_lr.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#lr_result").html(data);
		},
			error: function() {}
		});
	}
}
</script> 

</div> 

<?php include ("./footer.php"); ?>

</body>
</html>

<script type="text/javascript">
function sum1() 
{
	if($("#chrg_wt").val()=='')
	{
		$("#chrg_wt").focus();
		$("#b_rate").val('');
		$("#b_amt").val('');
	}	
	else
	{
		$("#b_amt").val(Math.round(Number($("#chrg_wt").val()) * Number($("#b_rate").val())).toFixed(2));
	}
}

function sum2() 
{
	if($("#chrg_wt").val()=='')
	{
		$("#chrg_wt").focus();
		$("#b_rate").val('');
		$("#b_amt").val('');
	}	
	else
	{
		$("#b_rate").val(Math.round(Number($("#b_amt").val()) / Number($("#chrg_wt").val())).toFixed(2));
	}	
}

function ResetAll() 
{
	$("#b_amt").val('');
	$("#b_rate").val('');
}
</script> 