<?php 
require_once("./connect.php");

$days_from = escapeString($conn,$_POST['days_from']);
$days_to = escapeString($conn,$_POST['days_to']);
$amount = escapeString($conn,$_POST['amount']);
$type_of = escapeString($conn,$_POST['type_of']);
$id = escapeString($conn,$_POST['id']);
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if($type_of!='DAY' AND $type_of!='FIX')
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid type defined !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($type_of=='FIX')
{
	$amount_from = escapeString($conn,$_POST['amount_from']);
	$amount_to = escapeString($conn,$_POST['amount_to']);
	
	if($amount_from=='' || $amount_from<0)
	{
		echo "<script type='text/javascript'>
				$('#update_btn_$id').attr('disabled',false);
				$('#loadicon').hide();
				swal({
				title: 'Invalid value of amount from !!',
				type: 'error',
				closeOnConfirm: true
				});
			</script>";
		exit();
	}
	
	if($amount_to=='' || $amount_to<0)
	{
		echo "<script type='text/javascript'>
				$('#update_btn_$id').attr('disabled',false);
				$('#loadicon').hide();
				swal({
				title: 'Invalid value of amount to !!',
				type: 'error',
				closeOnConfirm: true
				});
			</script>";
		exit();
	}
	
	if($amount_to<=$amount_from)
	{
		echo "<script type='text/javascript'>
				$('#update_btn_$id').attr('disabled',false);
				$('#loadicon').hide();
				swal({
				title: 'Check amount range value !!',
				type: 'error',
				closeOnConfirm: true
				});
			</script>";
		exit();
	}
}

if($id=='' || $id==0)
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Record not found !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($days_from=='' || $days_from==0)
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid value of days from !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($days_to=='' || $days_to==0)
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid value of till days !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($days_to<=$days_from)
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Check days value !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($amount=='' || $amount==0)
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid value of deduction amount !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($type_of=='FIX')
{
	$chk_record = Qry($conn,"SELECT from1,to1,amt_frm,amt_to,chrg FROM pod_late_chrg WHERE id='$id'");
}
else
{	
	$chk_record = Qry($conn,"SELECT days_from,days_to,charges FROM late_pod_rules WHERE id='$id'");
}

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
}

if(numRows($chk_record)==0)
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Record not found !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

$row_record = fetchArray($chk_record);

$update_log=array();
$update_Qry=array();

if($type_of=='FIX')
{
	$update_dates="NO";

	if($days_from!=$row_record['from1'])
	{
		$update_log[]="Days_From : $row_record[from1] to $days_from";
		$update_Qry[]="from1='$days_from'";
		$update_dates="YES";
	}

	if($days_to!=$row_record['to1'])
	{
		$update_log[]="Days_To : $row_record[to1] to $days_to";
		$update_Qry[]="to1='$days_to'";
		$update_dates="YES";
	}

	if(sprintf("%.2f",$amount_from)!=sprintf("%.2f",$row_record['charges']))
	{
		$update_log[]="amount_from : $row_record[amt_frm] to $amount_from";
		$update_Qry[]="amt_frm='$amount_from'";
	}
	
	if(sprintf("%.2f",$amount_to)!=sprintf("%.2f",$row_record['charges']))
	{
		$update_log[]="amount_to : $row_record[amt_to] to $amount_to";
		$update_Qry[]="amt_to='$amount_to'";
	}
	
	if(sprintf("%.2f",$amount)!=sprintf("%.2f",$row_record['chrg']))
	{
		$update_log[]="Amount : $row_record[chrg] to $amount";
		$update_Qry[]="chrg='$amount'";
	}
}
else
{
	$update_dates="NO";

	if($days_from!=$row_record['days_from'])
	{
		$update_log[]="Days_From : $row_record[days_from] to $days_from";
		$update_Qry[]="days_from='$days_from'";
		$update_dates="YES";
	}

	if($days_to!=$row_record['days_to'])
	{
		$update_log[]="Days_To : $row_record[days_to] to $days_to";
		$update_Qry[]="days_to='$days_to'";
		$update_dates="YES";
	}

	if(sprintf("%.2f",$amount)!=sprintf("%.2f",$row_record['charges']))
	{
		$update_log[]="Amount : $row_record[charges] to $amount";
		$update_Qry[]="charges='$amount'";
	}
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Nothing to update !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($type_of=='FIX')
{
	$chk_record1 = Qry($conn,"SELECT id FROM pod_late_chrg WHERE $days_from>=from1 AND $days_to<=to1");
	$update_table_name="pod_late_chrg";
	$rule_type="RULE_UPDATE_FIX";
	$rule_type2="FIX_AMOUNT";
}
else
{
	$chk_record1 = Qry($conn,"SELECT id FROM late_pod_rules WHERE $days_from>=days_from AND $days_to<=days_to");
	$update_table_name="late_pod_rules";
	$rule_type="RULE_UPDATE";
	$rule_type2="DAY_WISE";
}

if(!$chk_record1){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
	exit();
}

if(numRows($chk_record1)>0 AND $update_dates=='YES')
{
	echo "<script type='text/javascript'>
			$('#update_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Duplicate record found !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"UPDATE `$update_table_name` SET $update_Qry WHERE id='$id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$rule_type2','LATE_POD_RULE','$rule_type','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
			$('#loadicon').hide();
			$('#update_btn_$id').attr('disabled',false);
			swal({
			  title: 'Updated successfully !!',
			  type: 'success',
			  closeOnConfirm: true
			});
	</script>";
	exit();
}
else
{
	closeConnection($conn);
	echo "<script>
		alert('Error !!');
		$('#update_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
?>