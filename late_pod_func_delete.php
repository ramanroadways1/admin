<?php 
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$type_of = escapeString($conn,$_POST['type_of']);
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if($type_of!='DAY' AND $type_of!='FIX')
{
	echo "<script type='text/javascript'>
			$('#dlt_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Invalid type defined !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($id=='' || $id==0)
{
	echo "<script type='text/javascript'>
			$('#dlt_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Record not found !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

if($type_of=='FIX')
{
	$chk_record = Qry($conn,"SELECT from1,to1,amt_frm,amt_to,chrg,timestamp FROM pod_late_chrg WHERE id='$id'");
}
else
{
	$chk_record = Qry($conn,"SELECT days_from,days_to,charges,timestamp FROM late_pod_rules WHERE id='$id'");
}

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while Processing Request.","./");
}

if(numRows($chk_record)==0)
{
	echo "<script type='text/javascript'>
			$('#dlt_btn_$id').attr('disabled',false);
			$('#loadicon').hide();
			swal({
			title: 'Record not found !!',
			type: 'error',
			closeOnConfirm: true
			});
		</script>";
	exit();
}

$row_record = fetchArray($chk_record);

if($type_of=='FIX')
{
	$update_log="Days_From: $row_record[from1], Days_To: $row_record[to1], Amount_From: $row_record[amt_frm], Amount_To: $row_record[amt_to], Charges: $row_record[chrg], Timestamp: $row_record[timestamp]";
	$table_name="pod_late_chrg";
	$rule_type="RULE_DELETE_FIX";
	$rule_type2="FIX_AMOUNT";
}
else
{
	$update_log="Days_From: $row_record[days_from], Days_To: $row_record[days_to], Amount: $row_record[charges], Timestamp: $row_record[timestamp]";
	$table_name="late_pod_rules";
	$rule_type="RULE_DELETE";
	$rule_type2="DAY_WISE";
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"DELETE FROM `$table_name` WHERE id='$id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$rule_type2','LATE_POD_RULE','$rule_type','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
			$('#loadicon').hide();
			$('#dlt_btn_$id').attr('disabled',true);
			$('#update_btn_$id').attr('disabled',true);
			swal({
			  title: 'Deleted successfully !!',
			  type: 'success',
			  closeOnConfirm: true
			});
	</script>";
	exit();
}
else
{
	closeConnection($conn);
	echo "<script>
		alert('Error !!');
		$('#dlt_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
?>