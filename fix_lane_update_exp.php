<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$amount = sprintf("%.2f",round(escapeString($conn,$_POST['amount'])));
$timestamp = date("Y-m-d H:i:s");

if($id=='')
{
	echo "<script>
		alert('Expense not found !!');
		$('#update_exp_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($amount=='' || $amount<=0)
{
	echo "<script>
		alert('Invalid Expense amount !!');
		$('#update_exp_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_record = Qry($conn,"SELECT exp_amount FROM dairy.fix_lane_exp WHERE id='$id'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_record)==0)
{
	echo "<script>
		alert('No record found !!');
		$('#update_exp_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row = fetchArray($chk_record);

$exp_amount = $row['exp_amount'];

if($amount==sprintf("%.2f",$exp_amount))
{
	echo "<script>
		alert('nothing to update !!');
		$('#update_exp_btn_$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_exp_Qry = Qry($conn,"UPDATE dairy.fix_lane_exp SET exp_amount='$amount' WHERE id='$id'");

if(!$update_exp_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,edit_by,timestamp) VALUES 
('$id','By Expense Id','FIX_LANE','EDIT_EXP','Exp amount update $exp_amount to $amount.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	echo "<script>
		alert('Updated Successfully !!');
		$('#update_exp_btn_$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	closeConnection($conn);
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fix_lane.php");
	exit();
}
?>