<?php
require_once("./connect.php");
$type_of = escapeString($conn,$_POST['type_of']);
?>
<br />		
<table id="example" class="table table-bordered table-striped" style="font-size:12px;">
	<thead>
	<?php
	if($type_of=='DAY')
	{
	?>
	<tr>    
		<th>#</th>
		<th>From Days</th>
		<th>Till Days</th>
		<th>Amount/day</th>
		<th>Timestamp</th>
		<th>#</th>
		<th>#</th>
	</tr>
	<tr>    
		<td>#</td>
		<td><input type="number" placeholder="From days" id="days_from_new"></td>
		<td><input type="number" placeholder="Till days" id="days_till_new"></td>
		<td><input placeholder="Deduction per day" type="number" id="amount_new"></td>
		<td colspan="3"><button type="button" id="add_rule_btn" onclick="AddRule('DAY')" class="btn btn-sm btn-primary">Add New Rule</button></td>
	</tr>	
		<?php
		}
		else
		{
		?>
	<tr>    
		<th>#</th>	
		<th>From Days</th>
		<th>Till Days</th>
		<th>Amount From</th>
		<th>Amount To</th>
		<th>Charges</th>
		<th>Timestamp</th>
		<th>#</th>
		<th>#</th>
	</tr>
	<tr>    
		<td>#</td>
		<td><input type="number" placeholder="From days" id="days_from_new"></td>
		<td><input type="number" placeholder="Till days" id="days_till_new"></td>
		<td><input type="number" placeholder="From Amount" id="amt_from_new"></td>
		<td><input type="number" placeholder="To Amount" id="amt_to_new"></td>
		<td><input placeholder="Deduction Amount" type="number" id="amount_new"></td>
		<td colspan="3"><button type="button" id="add_rule_btn" onclick="AddRule('FIX')" class="btn btn-sm btn-primary">Add New Rule</button></td>
	</tr>	
		<?php
		}
		?>
	</thead>
	<tbody>
<?php
if($type_of=='DAY')
{
	$get_record = Qry($conn,"SELECT id,days_from,days_to,charges,timestamp FROM late_pod_rules");
}
else
{
	$get_record = Qry($conn,"SELECT id,from1,to1,amt_frm,amt_to,chrg,timestamp FROM pod_late_chrg");
}

if(!$get_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if($type_of=='DAY')
{
	$sn=1;
	while($row = fetchArray($get_record))
	{
		echo "<tr> 
			<td>$sn</td>
			<td><input id='days_from_$row[id]' type='number' value='$row[days_from]'></td>	
			<td><input id='days_to_$row[id]' type='number' value='$row[days_to]'></td>	
			<td><input id='charges_$row[id]' type='number' value='$row[charges]'></td>	
			<td>".date("d-m-y H:i A",strtotime($row['timestamp']))."</td>
			<td><button ";if($sn=="1") { echo "disabled"; }; echo " type='button' class='btn btn-success btn-sm' id='update_btn_$row[id]' onclick=Update('$row[id]','DAY')><i class='fa fa-pencil' aria-hidden='true'></i> Update</button></td>
			<td><button ";if($sn=="1") { echo "disabled"; }; echo " type='button' id='dlt_btn_$row[id]' onclick=DltFunc('$row[id]','DAY') class='btn btn-danger btn-sm'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Delete</button></td>
		 </tr>";
	$sn++; 
	}
}
else
{
	$sn=1;
	while($row = fetchArray($get_record))
	{
		echo "<tr> 
			<td>$sn</td>
			<td><input id='days_from_$row[id]' type='number' value='$row[from1]'></td>	
			<td><input id='days_to_$row[id]' type='number' value='$row[to1]'></td>	
			<td><input id='amt_from_$row[id]' type='number' value='$row[amt_frm]'></td>	
			<td><input id='amt_to_$row[id]' type='number' value='$row[amt_to]'></td>	
			<td><input id='charges_$row[id]' type='number' value='$row[chrg]'></td>	
			<td>".date("d-m-y H:i A",strtotime($row['timestamp']))."</td>
			<td><button ";if($sn=="1") { echo "disabled"; }; echo " type='button' class='btn btn-success btn-sm' id='update_btn_$row[id]' onclick=Update('$row[id]','FIX')><i class='fa fa-pencil' aria-hidden='true'></i> Update</button></td>
			<td><button ";if($sn=="1") { echo "disabled"; }; echo " type='button' id='dlt_btn_$row[id]' onclick=DltFunc('$row[id]','FIX') class='btn btn-danger btn-sm'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Delete</button></td>
		 </tr>";
	$sn++; 
	}
}
echo '
</tbody>
</table>';

$update_record = Qry($conn,"UPDATE _functions SET func_value='$type_of' WHERE func_type='LATE_POD'");
if(!$update_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo '
<script>
	$("#loadicon").hide();
</script>';
?>