<?php
require_once("./connect.php");
require_once("./connect2.php");

$id = mysqli_real_escape_string($conn,strtoupper($_POST['id']));

$qry=mysqli_query($conn,"SELECT id,to_branch,branch,company,amount,narr,date FROM debit WHERE id='$id'");

if(!$qry)
{
	echo mysqli_error($conn);echo "<script>$('#loadicon').hide();</script>";exit();
}

if(mysqli_num_rows($qry)==0)
{
	echo "<script>
		alert('No result found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row=mysqli_fetch_array($qry);

?>
<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="16">
	   <?php echo "Branch : ".$row['branch']; ?>
	   </th>
    </tr>
</table>	

<div class="container-fluid">
    <div class="row">
      
		<div class="form-group col-md-3"> 
			<label>Credit to Branch <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" value="<?php echo $row['to_branch']; ?>" readonly required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Company <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" value="<?php echo $row['company']; ?>" readonly required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Transaction Date <font color="red"><sup>*</sup></font></label>
			<input type="date" name="date" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?php echo $row['date']; ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Amount <font color="red"><sup>*</sup></font></label>
			<input type="number" name="amount" class="form-control" value="<?php echo $row['amount']; ?>" min="1" required>
		</div>
		
		<div class="form-group col-md-6"> 
			<label>Narration <font color="red"><sup>*</sup></font></label>
			<textarea class="form-control" name="narration" required><?php echo $row['narr']; ?></textarea>
		</div>
		
		<input type="hidden" value="<?php echo $row['id']; ?>" name="id">
	
	</div>
</div>

	<script>
		document.getElementById('menu_button2').click();
		$('#loadicon').hide();
	</script>