<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination2(event);" onkeydown = "return disableCtrlKeyCombination2(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Forfeit Freight Memo Balance (Advance NEFT)</h3>
              </div>
    
<script>	
function GetFm(id)
{
	var lrno_fm_no = $('#lrno_fm_no').val();
	var frno = $('#fm_no').val();
	var search_by = $('#search_by').val();
	
	if(search_by=='')
	{
		alert('Select search by option first !');
	}
	else
	{
		if(search_by=='LR' && lrno_fm_no=='')
		{
			alert('Select FM number first !');
		}
		else if(search_by=='FM' && frno=='')
		{
			alert('Enter FM number first !');
		}
		else
		{
			if(search_by=='LR')
			{
				var frno = lrno_fm_no;
			}
			
			if(frno=='')
			{
				alert('FM number not found !');
			}
			else
			{
				$('#button_chk').attr('disabled',true);
				$("#loadicon").show();
					jQuery.ajax({
					url: "get_fm_details_for_forfeit_fm.php",
					data: 'frno=' + frno + '&search_by=' + search_by,
					type: "POST",
					success: function(data) {
						$("#func_result").html(data);
					},
				error: function() {}
				});
			}
		}
	}
}

function FetchFMNumber(lrno)
{
	if(lrno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "get_fm_no_by_lrno.php",
			data: 'lrno=' + lrno,
			type: "POST",
			success: function(data) {
				$("#lrno_fm_no").html(data);
			},
			error: function() {}
		});
	}
}
</script>	
	
	<div class="card-body">
<form method="POST" autocomplete="off" id="Form1">			

	<div class="row">	
		
		<script>
		function SearchBy(elem)
		{
			$('#lrno_fm_no').val('');
			$('#lrno').val('');
			$('#fm_no').val('');
			
			if(elem=='FM')
			{
				$('#lrno_fm_no_div').hide();
				$('#lrno_fm_no').attr('required',false);
				
				$('#lrno_div').hide();
				$('#lrno').attr('required',false);
				
				$('#fm_no_div').show();
				$('#fm_no').attr('required',true);
				
			}
			else if(elem=='LR')
			{
				$('#lrno_fm_no_div').show();
				$('#lrno_fm_no').attr('required',true);
				
				$('#lrno_div').show();
				$('#lrno').attr('required',true);
				
				$('#fm_no_div').hide();
				$('#fm_no').attr('required',false);
			}
			else
			{
				$('#lrno_fm_no_div').show();
				$('#lrno_fm_no').attr('required',true);
				
				$('#lrno_div').show();
				$('#lrno').attr('required',true);
				
				$('#fm_no_div').hide();
				$('#fm_no').attr('required',false);
			}
		}
		</script>
		
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Search By <font color="red"><sup>*</sup></font></label>
                  <select name="search_by" onchange="SearchBy(this.value)" id="search_by" class="form-control" required="required">
						<option class="search_by_option" value="">--select--</option>
						<option id="LR_option" class="search_by_option" value="LR">LR Number</option>
						<option id="FM_option" class="search_by_option" value="FM">FM Number</option>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3" id="lrno_div">	
               <div class="form-group">
                  <label>LR Number <font color="red"><sup>*</sup></font></label>
                  <input onblur="FetchFMNumber(this.value)" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');" type="text" class="form-control" id="lrno" name="lrno" required="required">
              </div>
			</div>
			
			<div class="col-md-3" style="display:none" id="fm_no_div">	
               <div class="form-group">
                  <label>FM Number <font color="red"><sup>*</sup></font></label>
                  <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');" type="text" class="form-control" id="fm_no" name="fm_no" required="required">
              </div>
			</div>
			
			<div class="col-md-3" id="lrno_fm_no_div">	
               <div class="form-group">
                  <label>FM Number <font color="red"><sup>*</sup></font></label>
                  <select name="lrno_fm_no" id="lrno_fm_no" class="form-control" required="required">
						<option value="">--select an option--</option>
				  </select>
              </div>
			</div>
			
			<div id="button_div" class="col-md-3">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="button_chk" onclick="GetFm()" class="btn btn-danger">Get FM !</button>
			</div>

	</div>
	
	<div class="row">	
	
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Balance Left <font color="red"><sup>*</sup></font></label>
                  <input min="0" type="number" readonly class="form-control" id="balance_amount" name="balance_amount" required="required">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Others <font color="red"><sup>(-) *</sup></font></label>
                  <input min="0" type="number" class="form-control" id="other" name="other" required="required">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Unloading <font color="red"><sup>(+) *</sup></font></label>
                  <input min="0" type="number" class="form-control" id="unloading" name="unloading" required="required">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Detention <font color="red"><sup>(+) *</sup></font></label>
                  <input min="0" type="number" class="form-control" id="detention" name="detention" required="required">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Paid Amount <font color="red"><sup>*</sup></font></label>
                  <input readonly min="0" id="paid_amount" type="number" class="form-control" name="paid_amount" required="required">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>UTR Number <font color="red"><sup>*</sup></font></label>
                  <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-]/,'');" class="form-control" id="utr_no" name="utr_no" required="required">
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>UTR Date <font color="red"><sup>*</sup></font></label>
                  <input name="utr_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Narration <font color="red"><sup>*</sup></font></label>
                  <textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9@#_.-]/,'');" class="form-control" id="narration" name="narration" required="required"></textarea>
              </div>
			</div>
			
			<div class="col-md-12">
				 <div class="form-group">
					<button type="submit" id="button_submit" style="display:none" disabled class="btn btn-danger">Forfeit FM</button>
				</div>
			</div>
				
		</div>
	</form>	
		
		<div class="row">	
			<div class="col-md-12 table-responsive" id="main_div"></div>
		</div>
	
	</div>
	
	 </div>
</div>
        </div>
      </div>
    </section>
  </div>
</div>

<script>
$(function() {
$("#balance_amount,#other,#unloading,#detention").on("input keydown keyup blur change", sum);
function sum() {
	$("#paid_amount").val((Number($("#balance_amount").val()) + Number($("#unloading").val()) + Number($("#detention").val()) - Number($("#other").val())).toFixed(2));
	// $("#total_adv").val((Number($("#casmt").val()) + Number($("#chq_amt").val()) + Number($("#dsl_amt").val()) + Number($("#rtgs_amt").val())).toFixed(2));
	// $("#bamt").val((Number($("#total_freight").val()) - Number($("#total_adv").val())).toFixed(2));
}});
</script>

<?php include ("./footer.php"); ?>
</body>
</html>

<div id="func_result"></div>

<script>
$(document).ready(function (e) {
	$("#Form1").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#button_submit").attr("disabled", true);
		$.ajax({
        	url: "./save_forfeit_fm_balance_adv_neft.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data){
				$("#func_result").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>