<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Generate e-way bill token</h3>
              </div>
				
<div class="card-body">

	<div class="row"><div id="token_res" class="col-md-12"></div></div>
	
	<div class="row">	
		<div class="col-md-6 table-responsive" style="overflow:auto">
		
<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:14px" colspan="12">RRPL : <button id="rrpl_btn" type="button" onclick="GenToken('RRPL')" class="pull-right btn btn-sm btn-warning">Generate Token</button></th>
    </tr>
	
	<tr>    
		<th>Token</th>
		<th>Branch</th>
		<th>User</th>
		<th>Timestamp</th>
	</tr>
<?php
$rrpl = Qry($conn,"SELECT t.token,t.user,u.name,t.timestamp 
FROM ship.api_token as t 
LEFT JOIN emp_attendance AS u ON u.code=t.branch_user 
WHERE t.company='RRPL' ORDER BY t.id DESC LIMIT 2");

if(!$rrpl){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn=1;
while($row_rrpl = fetchArray($rrpl))
{
	echo '<tr>
		<td>'.$row_rrpl['token'].'</td>	
        <td>'.$row_rrpl['user'].'</td>
        <td>'.$row_rrpl['name'].'</td>
        <td>'.$row_rrpl['timestamp'].'</td>
     </tr>';
$sn++; 
}
echo '</table>';
?>			
		</div>
		
		
				<div class="col-md-6 table-responsive" style="overflow:auto">
		
<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:14px" colspan="12">RAMAN_ROADWAYS : <button id="rr_btn" type="button" onclick="GenToken('RR')" class="pull-right btn btn-sm btn-warning">Generate Token</button></th>
    </tr>
	
	<tr>    
		<th>Token</th>
		<th>Branch</th>
		<th>User</th>
		<th>Timestamp</th>
	</tr>
<?php
$rr = Qry($conn,"SELECT t.token,t.user,u.name,t.timestamp 
FROM ship.api_token as t 
LEFT JOIN emp_attendance AS u ON u.code=t.branch_user 
WHERE t.company='RAMAN_ROADWAYS' ORDER BY t.id DESC LIMIT 2");

if(!$rr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn2=1;
while($row_rr = fetchArray($rr))
{
	echo '<tr>
		<td>'.$row_rr['token'].'</td>	
        <td>'.$row_rr['user'].'</td>
        <td>'.$row_rr['name'].'</td>
        <td>'.$row_rr['timestamp'].'</td>
     </tr>';
$sn2++; 
}
echo '</table>';
?>			
		</div>
	</div>
</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	</div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script type="text/javascript">
function GenToken(elem)
{
	if(elem=='RRPL'){
		var page_url = "https://rrpl.online/b5aY6EZzK52NA8F/_API/gen_token.php";
		$('#rrpl_btn').attr('disabled',true);
	}
	else{
		var page_url = "https://rrpl.online/b5aY6EZzK52NA8F/_API/gen_token_rr.php";
		$('#rr_btn').attr('disabled',true);
	}
	
	jQuery.ajax({
	url: page_url,
	data: 'ok=' + 'ok',
	type: "POST",
	success: function(data) {
		$("#token_res").html(data);
	},
	error: function() {}
	});
}
</script>
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>