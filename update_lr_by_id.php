<?php
require_once './connect.php'; 

$lrno=escapeString($conn,strtoupper($_POST['lrno'])); // LR No.

$id=escapeString($conn,strtoupper($_POST['id'])); // LR id
$frno=escapeString($conn,strtoupper($_POST['frno'])); // Vou No

$from=escapeString($conn,strtoupper($_POST['from']));
$to=escapeString($conn,strtoupper($_POST['to']));
$from_id=escapeString($conn,strtoupper($_POST['from_id']));
$to_id=escapeString($conn,strtoupper($_POST['to_id']));
$consignor=escapeString($conn,strtoupper($_POST['consignor']));
$consignee=escapeString($conn,strtoupper($_POST['consignee']));
$con1_id=escapeString($conn,strtoupper($_POST['con1_id']));
$con2_id=escapeString($conn,strtoupper($_POST['con2_id']));
$con2_gst=escapeString($conn,strtoupper($_POST['con2_gst']));
$con2_pincode=escapeString($conn,strtoupper($_POST['con2_pincode']));
$act_weight=escapeString($conn,strtoupper($_POST['act_weight']));
$lr_date=escapeString($conn,($_POST['lr_date']));

$timestamp=date("Y-m-d H:i:s");

$get_ext_data = Qry($conn,"SELECT frno,date as lr_date,truck_no,lrno,fstation,tstation,consignor,consignee,wt12,con1_id,con2_id,done 
FROM freight_form_lr WHERE id='$id'");

if(!$get_ext_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_ext_data)==0){
	echo "<script>
		alert('LR not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_lr = fetchArray($get_ext_data);
	
if($row_lr['frno']!=$frno)
{
	echo "<script>
		alert('Voucher number not verified.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['lrno']!=$lrno)
{
	echo "<script>
		alert('LR number not verified.');
		$('#loadicon').hide();
		$('#lr_update_button').attr('disabled',false);
	</script>";
	exit();
}

if($row_lr['done']=="1")
{
	$check_diary_trip = Qry($conn,"SELECT id FROM dairy.trip WHERE tno='$row_lr[truck_no]' AND lr_type like '%$frno%'");

	if(!$check_diary_trip){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($check_diary_trip)==0){
		echo "<script>
			alert('Error : Running trip not found in e-diary !');
			$('#loadicon').hide();
			$('#lr_update_button').attr('disabled',false);
		</script>";
		exit();
	}

	$row_trip_id = fetchArray($check_diary_trip);
	
	$update_trip_this_trip = "1";
	
	$trip_id_this = $row_trip_id['id'];
	
	if($row_lr['fstation']!=$from)
	{
		$check_last_trip = Qry($conn,"SELECT id,from_station,to_station,lr_type FROM dairy.trip WHERE tno='$row_lr[truck_no]' AND id<'$row_trip_id[id]' 
		ORDER BY id DESC LIMIT 1");

		if(!$check_last_trip){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($check_last_trip)>0)
		{
			$row_last_loc = fetchArray($check_last_trip);
			
			if($row_last_loc['lr_type']!='ATLOADING' AND $row_last_loc['lr_type']!='EMPTY' AND $row_last_loc['lr_type']!='CON20' AND $row_last_loc['CON40']!='EMPTY' AND $row_last_loc['to_station']!=$from)
			{
				echo "<script>
					alert('Error : From Location : $from not matching with Last Trip\'s to Location : $row_last_loc[to_station] !');
					$('#loadicon').hide();
					$('#lr_update_button').attr('disabled',false);
				</script>";
				exit();
			}
			
			$trip_id_last = $row_last_loc['id'];
			$update_trip_from = "1";
		}
		else
		{
			$update_trip_from = "0";
		}
	}
	else
	{
		$update_trip_from = "0";
	}

	if($row_lr['tstation']!=$to)
	{
		$check_next_trip = Qry($conn,"SELECT id,from_station,to_station,lr_type FROM dairy.trip WHERE tno='$row_lr[truck_no]' AND id>'$row_trip_id[id]' 
		ORDER BY id ASC LIMIT 1");

		if(!$check_next_trip){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($check_next_trip)>0)
		{
			$row_next_loc = fetchArray($check_next_trip);
			
			if($row_next_loc['lr_type']!='ATLOADING' AND $row_next_loc['lr_type']!='EMPTY' AND $row_next_loc['lr_type']!='CON20' AND $row_next_loc['CON40']!='EMPTY' AND $row_next_loc['from_station']!=$to)
			{
				echo "<script>
					alert('Error : To Location : $to not matching with Next Trip\'s From Location : $row_next_loc[from_station] !');
					$('#loadicon').hide();
					$('#lr_update_button').attr('disabled',false);
				</script>";
				exit();
			}
			
			$trip_id_next = $row_next_loc['id'];
			$update_trip_to = "1";
		}
		else
		{
			$update_trip_to = "0";
		}
	}
	else
	{
		$update_trip_to = "0";
	}
	
}
else
{
	$update_trip_from = "0";
	$update_trip_to = "0";
	$update_trip_this_trip = "0";
}
	
$update_log=array();
$update_Qry=array();

if($from!=$row_lr['fstation'])
{
	$update_log[]="From_Loc : $row_lr[fstation] to $from";
	$update_Qry[]="fstation='$from'";
}

if($to!=$row_lr['tstation'])
{
	$update_log[]="To_Loc : $row_lr[tstation] to $to";
	$update_Qry[]="tstation='$to'";
}

if($con2_id!=$row_lr['con2_id'])
{
	$update_log[]="Consignee : $row_lr[consignee] to $consignee";
	$update_Qry[]="consignee='$consignee'";
	$update_Qry[]="con2_id='$con2_id'";
}

if($act_weight!=$row_lr['wt12'])
{
	$update_log[]="Act_Wt : $row_lr[wt12] to $act_weight";
	$update_Qry[]="wt12='$act_weight'";
}

$update_lr_date="NO";

if($lr_date!=$row_lr['lr_date'])
{
	$update_log[]="lr_date : $row_lr[lr_date] to $lr_date";
	$update_Qry[]="date='$lr_date'";
	$update_lr_date="YES";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#lr_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"UPDATE freight_form_lr SET $update_Qry WHERE id='$id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_total_vou = Qry($conn,"SELECT id FROM freight_form_lr WHERE lrno='$lrno'");

if(!$get_total_vou){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_total_vou)==1 AND $from!=$row_lr['fstation'])
{
	$update_lr_loc = Qry($conn,"UPDATE lr_sample SET fstation='$from',from_id='$from_id' WHERE lrno='$lrno'");
	
	if(!$update_lr_loc){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($act_weight!=$row_lr['wt12'])
{
	$update_lr_entry = Qry($conn,"UPDATE lr_sample SET wt12='$act_weight' WHERE lrno='$lrno'");
			
	if(!$update_lr_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($update_lr_date=='YES')
{
	$update_lr_date_1 = Qry($conn,"UPDATE lr_sample SET date='$lr_date' WHERE lrno='$lrno'");
			
	if(!$update_lr_date_1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($update_trip_this_trip=="1")
{
	$get_kms = Qry($conn,"SELECT km FROM dairy.master_km WHERE from_loc_id='$from_id' AND to_loc_id='$to_id'");
	if(!$get_kms){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_kms)>0)
	{
		$row_trip_km = fetchArray($get_kms);
		
		$update_trip = Qry($conn,"UPDATE dairy.trip SET from_station='$from',to_station='$to',from_id='$from_id',to_id='$to_id',
		km='$row_trip_km[km]',act_wt='$act_weight' WHERE id='$trip_id_this'");
	}
	else
	{
		$update_trip = Qry($conn,"UPDATE dairy.trip SET from_station='$from',to_station='$to',from_id='$from_id',to_id='$to_id',
		act_wt='$act_weight' WHERE id='$trip_id_this'");
	}
	
	if(!$update_trip){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($update_trip_from=="1")
	{
		$get_kms_last_trip = Qry($conn,"SELECT km FROM dairy.master_km WHERE from_loc='$row_last_loc[from_station]' AND 
		to_loc='$from'");
		
		if(!$get_kms_last_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_kms_last_trip)>0)
		{
			$row_last_trip_km = fetchArray($get_kms_last_trip);
			
			$update_last_trip_to_loc = Qry($conn,"UPDATE dairy.trip SET to_station='$from',to_id='$from_id',km='$row_last_trip_km[km]' 
			WHERE id='$trip_id_last'");
		}
		else
		{
			$update_last_trip_to_loc = Qry($conn,"UPDATE dairy.trip SET to_station='$from',to_id='$from_id' WHERE id='$trip_id_last'");
		}
		
		if(!$update_last_trip_to_loc){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
	if($update_trip_to=="1")
	{
		$get_kms_next_trip = Qry($conn,"SELECT km FROM dairy.master_km WHERE from_loc='$from' AND 
		to_loc='$row_next_loc[to_station]'");
		
		if(!$get_kms_next_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_kms_next_trip)>0)
		{
			$row_next_trip_km = fetchArray($get_kms_next_trip);
			
			$update_next_trip_to_loc = Qry($conn,"UPDATE dairy.trip SET from_station='$to',from_id='$to_id',km='$row_next_trip_km[km]' 
			WHERE id='$trip_id_next'");
		}
		else
		{
			$update_next_trip_to_loc = Qry($conn,"UPDATE dairy.trip SET from_station='$to',from_id='$to_id' WHERE id='$trip_id_next'");
		}
		
		if(!$update_next_trip_to_loc){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$lrno','FM_UPDATE','LR_UPDATE','Vou_no: $frno, $update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// if($consignee!=$row_lr['consignee'])
// {
	
// }

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('LR Update Success ! Note : Please Change master Lane if required and also verify the LR entry.');
		$('#get_button').attr('disabled',false);
		$('#lr_update_button').attr('disabled',false);
		$('#close_lr_update_button').click();
		$('#get_button').click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error while processing request !!');
		$('#lr_update_button').attr('disabled',false);
		$('#close_lr_update_button').click();
		$('#get_button').attr('disabled',false);
		$('#get_button').click();
	</script>";
	exit();
}	
?>