<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));
$frno = escapeString($conn,strtoupper($_POST['frno']));
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");


$qry = Qry($conn,"SELECT frno,baladv,totalbal,paidto FROM freight_form WHERE id='$id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "
	<script>
		alert('Freight memo found !!');
		$('#loadicon').hide();
		$('#Btn1$id').attr('disabled', true);
	</script>";
	exit();
}

$row = fetchArray($qry);

if($row['paidto']!='')
{
	echo "
	<script>
		alert('Warning : Balance paid !!');
		$('#loadicon').hide();
		$('#Btn1$id').attr('disabled', true);
	</script>";
	exit();
}

if($row['frno']!=$frno)
{
	echo "
	<script>
		alert('Warning : Freight memo number not verified !!');
		$('#loadicon').hide();
		$('#Btn1$id').attr('disabled', true);
	</script>";
	exit();
}

$check_total_lrs = Qry($conn,"SELECT id FROM freight_form_lr WHERE frno='$row[frno]'");

if(!$check_total_lrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$total_lrs = numRows($check_total_lrs);

$check_pod = Qry($conn,"SELECT lrno FROM rcv_pod WHERE frno='$frno'");

if(!$check_pod){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows_pod = numRows($check_pod);

if($numrows_pod==0)
{
	$pod_status="ALL_PENDING";
}
else if($numrows_pod!=$total_lrs)
{
	$lrnos = array();
	while($row_lrno=fetchArray($check_pod))
	{
		$lrnos[] = "'".$row_lrno['lrno']."'";
	}

	$lrnos=implode(',', $lrnos);
	$pod_status="PARTIAL_PENDING";
}
else
{
	$pod_status = "ALL_RCVD";
}

StartCommit($conn);
$flag = true;

if($pod_status=="ALL_PENDING")
{
	$copy_pod = Qry($conn,"INSERT INTO rcv_pod(frno,veh_type,lrno,lr_id,consignor_id,branch,fm_date,fm_amount,pod_branch,
	pod_copy,pod_date,del_date,ho_rcvd,timestamp,exdate) SELECT f.frno,'MARKET',f.lrno,l.id,l.con1_id,f.branch,f.create_date,
	f2.actualf,l.branch,'pod_copy/pod_auto_rcvd.jpg','$date','$date','1','$timestamp','$date' 
	FROM freight_form_lr AS f 
	LEFT OUTER JOIN freight_form AS f2 ON f2.id='$id' 
	LEFT OUTER JOIN lr_sample AS l ON l.lrno=f.lrno 
	WHERE f.frno='$frno'");
	
	if(!$copy_pod){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
	
	$update_pod_date_fm = Qry($conn,"UPDATE freight_form_lr SET market_pod_date='$date' WHERE frno='$frno'");
	
	if(!$update_pod_date_fm){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}
else if($pod_status=='PARTIAL_PENDING')
{
	$copy_pod = Qry($conn,"INSERT INTO rcv_pod(frno,veh_type,lrno,lr_id,consignor_id,branch,fm_date,fm_amount,pod_branch,
	pod_copy,pod_date,del_date,ho_rcvd,timestamp,exdate) SELECT f.frno,'MARKET',f.lrno,l.id,l.con1_id,f.branch,f.create_date,
	f2.actualf,l.branch,'pod_copy/pod_auto_rcvd.jpg','$date','$date','1','$timestamp','$date' 
	FROM freight_form_lr AS f 
	LEFT OUTER JOIN freight_form AS f2 ON f2.id='$id' 
	LEFT OUTER JOIN lr_sample AS l ON l.lrno=f.lrno 
	WHERE f.frno='$frno' AND f.lrno NOT IN($lrnos)");
	
	if(!$copy_pod){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
	
	$update_pod_date_fm = Qry($conn,"UPDATE freight_form_lr SET market_pod_date='$date' WHERE frno='$frno' AND lrno NOT IN($lrnos)");
	
	if(!$update_pod_date_fm){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}

	$update_fm_balance = Qry($conn,"UPDATE freight_form SET otherfr=baladv,totalbal='0.00',bal_branch_user=adv_branch_user,paidto=ptob,bal_date='$date',
	pto_bal_name=pto_adv_name,bal_pan=adv_pan,rtgs_bal='1',narra='Balance Forfeit.',pod='1',branch_bal=branch,forfeit_balance='1' 
	WHERE id='$id'");
	
	if(!$update_fm_balance){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Done !');
		$('#Form1').trigger('reset');
		$('#Btn1$id').attr('disabled', true);
		$('#loadicon').hide();
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./mark_pod_rcvd.php");
	exit();
}	
?>