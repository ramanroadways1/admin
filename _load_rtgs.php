<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$type = escapeString($conn,strtoupper($_POST['type']));
$total_adv = escapeString($conn,strtoupper($_POST['adv']));
$freight_amt = escapeString($conn,strtoupper($_POST['freight']));
$balance_left = escapeString($conn,strtoupper($_POST['balance']));

$qry=Qry($conn,"SELECT id,fno,amount,acname,acno,bank_name,ifsc,pan,pay_date,mobile,crn,colset_d,bank,utr_date,timestamp_approve,
time_download FROM rtgs_fm WHERE fno='$frno' AND type='$type'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('Rtgs not found. Type : $type.');
		window.location.href='fm_view.php';
	</script>";
	exit();
}

$row = fetchArray($qry);
?>

<table class="table table-bordered" style="font-size:12.5px;">
				 
	<tr>
       <th class="bg-<?php if($type=='ADVANCE'){ echo "warning"; } else { echo "success"; } ?>" style="font-size:13px;" colspan="9"><?php echo $type; ?> RTGS A/c Details :</th>
    </tr>
				 
	<tr>
		 <td colspan="3"><b>A/c Holder : </b><?php echo $row['acname']; ?></td>
		 <td colspan="2"><b>A/c No : </b><?php echo $row['acno']; ?></td>
		 <td colspan="2"><b>Bank & IFSC : </b><?php echo $row['bank_name']."(".$row['ifsc'].")"; ?></td>
		 <td colspan="2"><b>Amount : </b><?php echo $row['amount']; ?> 
		 <?php
		 if($type=='ADVANCE' AND $row['colset_d']!='1')
		 {
			echo "<button style='margin-left:10px' onclick=RtgsAdvDelete('$row[id]','$row[fno]') id='btn_rtgs_adv_del_$row[id]' type='button' class='pull-right btn btn-sm btn-danger'>Delete</button>";
			echo "<button onclick=RtgsAmtEdit('$row[id]') type='button' class='pull-right btn btn-sm btn-danger'>Edit</button>";
		 }
		 ?>
		 </td>
	</tr>
	
	<tr>
		 <td colspan="2"><b>Ref.No : </b><?php echo $row['crn']; ?></td>
		 <td colspan="3"><b>UTR No & Date : </b><?php if($row['bank']!="") { echo $row['bank']."(".$row['utr_date'].")"; } ?></td>
		 <td colspan="2"><b>Approved on : </b><?php echo $row['timestamp_approve']; ?></td>
		 <td colspan="2"><b>Downloaded on : </b><?php echo $row['time_download']; ?></td>
	</tr>

</table>

<script>
	$('#loadicon').hide();	
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#FormUpdateRtgsAdv").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#rtgs_amt_update_modal_btn").attr("disabled", true);
	$.ajax({
	url: "./update_rtgs_amount.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#rtgs_amt_update_modal_res").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="FormUpdateRtgsAdv" autocomplete="off"> 
<div class="modal fade" id="ModalRtgsAdvUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
	   
	  <div class="modal-header bg-primary">Update Rtgs amount : <?php echo $frno; ?></div>
	  
<div class="modal-body">
			
	<div class="row">
	
			<div class="col-md-6">
                <div class="form-group">
                     <label class="control-label mb-1">Freight <font color="red"><sup>*</sup></font></label>
					 <input class="form-control" value="<?php echo $freight_amt ?>" id="rtgs_modal_frt_amt" type="text" readonly required />
                  </div>
            </div>
			
			<div class="col-md-6">
                <div class="form-group">
                     <label class="control-label mb-1">Total Adv <font color="red"><sup>*</sup></font></label>
					 <input class="form-control" value="<?php echo $total_adv ?>" id="rtgs_modal_total_adv" type="text" readonly required />
                  </div>
            </div>
			
			<div class="col-md-6">
                <div class="form-group">
                     <label class="control-label mb-1">RTSG Adv <font color="red"><sup>*</sup></font></label>
					 <input name="rtgs_amt" class="form-control" oninput="CalculateNewRtgsAmt(this.value)" id="rtgs_modal_amt_input" type="text" value="<?php echo $row['amount']; ?>" required />
                  </div>
            </div>
			  
			<div class="col-md-6">
                <div class="form-group">
                     <label class="control-label mb-1">Balance left <font color="red"><sup>*</sup></font></label>
					 <input class="form-control" value="<?php echo $balance_left ?>" id="rtgs_modal_bal_left" type="text" required />
                  </div>
            </div>
	</div>
</div>

<script type="text/javascript"> 
function CalculateNewRtgsAmt(rtgs_amt){
	
	var freight = Number($("#rtgs_modal_frt_amt_db").val());
	var adv = Number($("#rtgs_modal_adv_amt_db").val());
	var balance = Number($("#rtgs_modal_bal_left_db").val());
	
	var rtgs_amt = Number(rtgs_amt);
	var rtgs_amt_db = Number($("#rtgs_modal_rtgs_amt_db").val());
	
	var rtgs_diff =  Number(rtgs_amt-rtgs_amt_db);
	
	$("#rtgs_modal_total_adv").val((Number(adv) + Number(rtgs_diff)).toFixed(2));
	$("#rtgs_modal_bal_left").val((Number(freight) - Number($("#rtgs_modal_total_adv").val())).toFixed(2));
}	
</script> 
		
		<div id="rtgs_amt_update_modal_res"></div>
		
		<input type="hidden" value="<?php echo $freight_amt; ?>" id="rtgs_modal_frt_amt_db">
		<input type="hidden" value="<?php echo $total_adv; ?>" id="rtgs_modal_adv_amt_db">
		<input type="hidden" value="<?php echo $balance_left; ?>" id="rtgs_modal_bal_left_db">
		<input type="hidden" value="<?php echo $row['amount']; ?>" id="rtgs_modal_rtgs_amt_db">
		
		<input type="hidden" value="<?php echo $row['id']; ?>" name="rtgs_id">
		<input type="hidden" value="<?php echo $row['fno']; ?>" name="vou_no">
		<input type="hidden" id="rtgs_amt_modal_vou_id" name="vou_id">
	
	<div class="modal-footer">
		<button type="button" onclick="$('#get_button').attr('disabled',false);$('#get_button')[0].click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="rtgs_amt_update_modal_btn" class="btn btn-primary">Update</button>
	</div>
		
		</div>	
     </div>
 </div>	
 </form>
