<?php
require_once("./connect.php");

$frno=escapeString($conn,strtoupper($_POST['vou_no']));
$id=escapeString($conn,strtoupper($_POST['vou_id_fm']));
$ewbNo=escapeString($conn,strtoupper($_POST['ewb_no']));
$consignor_id=escapeString($conn,strtoupper($_POST['con1_id']));
$item_id=escapeString($conn,strtoupper($_POST['item_id']));
$timestamp = date("Y-m-d H:i:s");

$get_fm = Qry($conn,"SELECT frno,truck_no,oid,ptob as adv_to,paidto as bal_to FROM freight_form WHERE id='$id'");
if(!$get_fm){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_fm)==0){
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_fm = fetchArray($get_fm);

if($row_fm['frno']!=$frno)
{
	echo "<script>
		alert('Freight memo not verified !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$ext_tno = $row_fm['truck_no'];
$adv_to = $row_fm['adv_to'];
$bal_to = $row_fm['bal_to'];

$tno=escapeString($conn,strtoupper($_POST['tno']));
$owner_name=escapeString($conn,strtoupper($_POST['owner_name']));
$owner_id=escapeString($conn,strtoupper($_POST['owner_id']));
$wheeler=escapeString($conn,strtoupper($_POST['wheeler']));

if($bal_to!='')
{
	echo "<script>
		alert('Balance Paid. Reset Balance First !');
		$('#loadicon').hide();
		$('#chanage_owner_button').attr('disabled',false);
	</script>";
	exit();
}

if($adv_to!='')
{
	echo "<script>
		alert('Advance Paid. Reset Advance First !');
		$('#loadicon').hide();
		$('#chanage_owner_button').attr('disabled',false);
	</script>";
	exit();
}

$update_log=array();
$update_Qry=array();

if($tno!=$ext_tno)
{
	$update_log[]="Truck_No : $ext_tno($row_fm[oid]) to $tno($owner_id)";
	$update_Qry[]="truck_no='$tno'";
	$update_Qry[]="oid='$owner_id'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#chanage_owner_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

// Check eway bill

if($consignor_id=='56' || $consignor_id=='675' || $item_id=='101')
{
	
}
else
{
	
$get_token=Qry($conn,"SELECT token FROM ship.api_token ORDER BY id DESC LIMIT 1");
if(!$get_token){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","../");
	exit();
}

$row_token=fetchArray($get_token);
$authToken=$row_token['token'];

		$Curl_EwbFetch = curl_init();
		curl_setopt_array($Curl_EwbFetch, array(
		  CURLOPT_URL => "https://api.taxprogsp.co.in/v1.03/dec/ewayapi?action=GetEwayBill&aspid=1613151119&password=RRPL@123&gstin=24AAGCR0742P1Z5&username=RRPL@AHD_API_111&authtoken=$authToken&ewbNo=$ewbNo",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 900,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"Accept: */*",
			"Accept-Encoding: gzip, deflate",
			"Cache-Control: no-cache",
			"Connection: keep-alive",
			"Host: api.taxprogsp.co.in",
			"Postman-Token: fbf12a45-9e77-4846-a07e-aa599957d3ed,cc3bfe12-9401-47f6-a573-e12a12761576",
			"User-Agent: PostmanRuntime/7.15.2",
			"cache-control: no-cache"
		  ),
		));

		$response_gen = curl_exec($Curl_EwbFetch);
		$err = curl_error($Curl_EwbFetch);
		curl_close($Curl_EwbFetch);
		
		if($err)
		{
			$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(error_desc,lrno,branch,timestamp) VALUES 
			('cURL Error : $err','RRPL.ONLINE','RRPL_ADMIN','$timestamp')");
			
			if(!$insert_error){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","../");
				exit();
			}
			echo "<script>
				alert('Error: While Fetching Eway-Bill.');
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		$response_decoded = json_decode($response_gen, true);
		
		if(@$response_decoded['error'])
		{
			if($response_decoded['error']['error_cd']=="GSP102")
			{
				  $curlToken = curl_init();
				  curl_setopt_array($curlToken, array(
				  CURLOPT_URL => "https://api.taxprogsp.co.in/v1.03/dec/authenticate?action=ACCESSTOKEN&aspid=1613151119&password=RRPL@123&gstin=24AAGCR0742P1Z5&username=RRPL@AHD_API_111&ewbpwd=RamanRoadways12",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 900,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "GET",
				  CURLOPT_HTTPHEADER => array(
					"Accept: */*",
					"Accept-Encoding: gzip, deflate",
					"Cache-Control: no-cache",
					"Connection: keep-alive",
					"Host: api.taxprogsp.co.in",
					"Postman-Token: fbf12a45-9e77-4846-a07e-aa599957d3ed,cc3bfe12-9401-47f6-a573-e12a12761576",
					"User-Agent: PostmanRuntime/7.15.2",
					"cache-control: no-cache"
				  ),
				));
				$response_Token = curl_exec($curlToken);
				$errToken = curl_error($curlToken);
				curl_close($curlToken);
				
				if($errToken)
				{
					$insert_error2 = Qry($conn,"INSERT INTO ship.eway_bill_error(error_desc,lrno,branch,timestamp) VALUES 
					('cURL Error : $errToken','RRPL.ONLINE','RRPL_ADMIN','$timestamp')");
					
					if(!$insert_error2){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
							Redirect("Error while processing Request","../");
							exit();
					}
						
					echo "<script>
						alert('Error: While Fetching Eway-Bill Token.');
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$response_decodedToken1 = json_decode($response_Token, true);
				$authToken_new = $response_decodedToken1['authtoken'];
				
				if($authToken_new=='')
				{
					echo "<script>
						alert('Error : Empty token. Please try once again..');
						$('#loadicon').hide();
					</script>";
					exit();
				}
				
				$insert_token=Qry($conn,"INSERT INTO ship.api_token (token,user,timestamp) VALUES 
				('$authToken_new','RRPL_ADMIN','$timestamp')");
				if(!$insert_token){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
				}
				
				echo "<script>
						alert('Please try again !!');
						$('#eway_bill_no').val('');
						$('#EwayBillFlag').val('');
						$('#loadicon').hide();
					</script>";
				exit();
				
			}
			else if($response_decoded['error']['error_cd']=="325")
			{
				echo "<script>
						alert('Error: Invalid Eway-Bill Number : $ewbNo.');
						$('#loadicon').hide();
					</script>";
				exit();
			}
			else
			{
				$errorMsg = $response_decoded['error']['message'];
				$errorMsgCode = $response_decoded['error']['error_cd'];
				
				$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(error_desc,lrno,msg,branch,timestamp) VALUES 
				('$response_gen','RRPL.ONLINE','$errorMsg','RRPL_ADMIN','$timestamp')");
				
				if(!$insert_error){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","../");
					exit();
				}
				
				echo "<script>
						alert('Error: While Fetching Eway-Bill. Code : $errorMsgCode.');
						$('#loadicon').hide();
					</script>";
				exit();
			}
		}
	
	$i_veh_no = 0;
	
	foreach($response_decoded['VehiclListDetails'] as $Data1){
		if($i_veh_no == 0) {
			$Veh_no=$Data1['vehicleNo'];
		}
	$i_veh_no++;
		// $Veh_no=$Data1['vehicleNo'];
	}
	
if($Veh_no!=$tno)	
{
	echo "<script>
		alert('Error : Vehicle Number : $tno not matching with eway bill vehicle no : $Veh_no.');
		$('#loadicon').hide();
	</script>";
}
	$billDate = $response_decoded['ewayBillDate'];	
	$Con1Gst = $response_decoded['userGstin'];	
	$Doc_No = $response_decoded['docNo'];	
	$Doc_date = $response_decoded['docDate'];	
	$fromGstin = $response_decoded['fromGstin'];	
	$fromTrdName = $response_decoded['fromTrdName'];	
	$fromPincode = $response_decoded['fromPincode'];	
	$toGstin = $response_decoded['toGstin'];	
	$toTrdName = $response_decoded['toTrdName'];	
	$totInvValue = $response_decoded['totInvValue'];	
	$totalValue = $response_decoded['totalValue'];	
	// $Veh_no = $response_decoded['VehiclListDetails']['vehicleNo'];	
	$FromLoc = $response_decoded['fromPlace'];	
	$ToLoc = $response_decoded['toPlace'];	
	$fromPincode = $response_decoded['fromPincode'];	
	$toPincode = $response_decoded['toPincode'];	
	
}
	
StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"UPDATE freight_form SET $update_Qry WHERE id='$id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_Qry_fm_lrs = Qry($conn,"UPDATE freight_form_lr SET truck_no='$tno' WHERE frno='$frno'");

if(!$update_Qry_fm_lrs){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$row_fm[frno]','FM_UPDATE','OWNER_UPDATE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Owner Updated Successfully ! Update Vehicle Number in LR if required.');
		$('#get_button').attr('disabled',false);
		$('#chanage_owner_button').attr('disabled',false);
		$('#close_owner_button').click();
		$('#get_button').click();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./fm_view.php");
	exit();
}
?>