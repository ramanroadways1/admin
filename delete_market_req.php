<?php
require_once('./connect.php');
	
$frno=escapeString($conn,strtoupper($_POST['frno']));
$timestamp = date("Y-m-d H:i:s");

if($frno=='')
{
	echo "<script>
		alert('Unable to fetch Token Number.');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_done = Qry($conn,"SELECT id FROM `diesel_fm` WHERE fno='$frno' AND IF(dsl_by='PUMP',colset_d,done)='1'");
if(!$chk_done){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_done)>0)
{
	echo "<script>
		alert('Error : Pump or Card Diesel marked as done !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$get_diesel_data = Qry($conn,"SELECT id,fno,token_no,branch,com,qty,rate,disamt,cash,dsl_by,tno,lrno,dcard,veh_no,dcom,pay_date,done,pre_req,colset_d,
crossing,pre_and_fm,timestamp FROM `diesel_fm` WHERE fno='$frno'");
if(!$get_diesel_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_diesel_data)==0)
{
	echo "<script>
		alert('Error : Diesel request not found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$update_log=array();
$is_fm_created = false;

while($row_diesel = fetchArray($get_diesel_data))
{
	if($row_diesel['cash']>0)
	{
		echo "<script>
			alert('Error : Cash entry found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}

	if($row_diesel['crossing']>0)
	{
		echo "<script>
			alert('Error : Crossing diesel found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	if($row_diesel['pre_and_fm']==1)
	{
		echo "<script>
			alert('Error : Freight Memo Created. You can\'t delete this diesel request !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	if($row_diesel['fno']!=$row_diesel['token_no'])
	{
		$is_fm_created = true;
	}
	
	$token_no = $row_diesel['fno'];
	$truck_no = $row_diesel['tno'];
	$lrno = $row_diesel['lrno'];
	$fm_branch = $row_diesel['branch'];
	
	$update_log[]="Diesel-Id: $row_diesel[id]-> Vou_No: $row_diesel[fno], Token_No: $row_diesel[token_no], Qty: $row_diesel[qty], 
	Rate: $row_diesel[rate], Amount: $row_diesel[disamt], Card/Pump: $row_diesel[dsl_by], Card/Pump Number: $row_diesel[dcard], 
	Vehicle_No: $row_diesel[veh_no], Company: $row_diesel[dcom], Date: $row_diesel[pay_date], Pre_REQ: $row_diesel[pre_req], 
	Timestamp: $row_diesel[timestamp]";
}

$update_log = implode(', ',$update_log); 
$update_log = "Token_No: $token_no -> Truck_No: $truck_no, LR Number: $lrno. ".$update_log; 

if($is_fm_created==true)
{
	echo "<script>
		alert('Freight memo created. You can\'t delete this diesel Request !');
		$('#loadicon').hide();
	</script>";
	exit();	
}

$get_requested_diesel = Qry($conn,"SELECT id,lrno FROM diesel_fm WHERE fno='$frno' AND pre_req='1'");
if(!$get_requested_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_requested_diesel)==0)
{
	$diesel_request="0";
	
	echo "<script>
		alert('This is not requested diesel !');
		$('#loadicon').hide();
	</script>";
	exit();	
}
else
{
	$diesel_request="1";
	$diesel_lrno = array();
	$diesel_ids= array();
			
	while($row_req_diesel=fetchArray($get_requested_diesel))
	{
		$diesel_lrno[] = "'".$row_req_diesel['lrno']."'";
		$diesel_ids[] = "'".$row_req_diesel['id']."'";
	}
	
	$diesel_lrno=implode(',', $diesel_lrno);
	$diesel_ids=implode(',', $diesel_ids);
	
	$check_pending_diesel = Qry($conn,"SELECT id FROM _pending_diesel WHERE diesel_id in($diesel_ids)");
	if(!$check_pending_diesel){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($check_pending_diesel)==0)
	{
		errorLog("Pending diesel not found in _pending_diesel table. $token_no",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Diese request not found !');
			$('#loadicon').hide();
		</script>";
		exit();	
	}
}

StartCommit($conn);
$flag = true;

$delete_diesel = Qry($conn,"DELETE FROM diesel_fm WHERE fno='$frno'");

if(!$delete_diesel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Diesel not deleted: $frno.",$conn,$page_name,__LINE__);
}
	
if($diesel_request=="1")
{
	$delete_pending_diesel = Qry($conn,"DELETE FROM _pending_diesel WHERE diesel_id in($diesel_ids)");
	
	if(!$delete_pending_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Pending diesel not deleted _pending_diesel table: $frno.",$conn,$page_name,__LINE__);
	}
	
	$delete_lr_pending_diesel = Qry($conn,"DELETE FROM diesel_lr_pending WHERE diesel_id in($diesel_ids)");
	
	if(!$delete_lr_pending_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_lr_sample = Qry($conn,"UPDATE lr_sample SET diesel_req=diesel_req-1 WHERE lrno in($diesel_lrno)");
	
	if(!$update_lr_sample){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_lr_sample_pending = Qry($conn,"UPDATE lr_sample_pending SET diesel_req=diesel_req-1 WHERE lrno in($diesel_lrno)");
	
	if(!$update_lr_sample_pending){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}	

$update_log_Qry = Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$frno','MARKET_DIESEL_REQ','REQ_DELETED','$update_log','$fm_branch','ADMIN','$timestamp')");
	
if(!$update_log_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Deleted Successfully !');
		document.getElementById('button2').click();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./request_market_truck.php");
	exit();
}
?>