<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination2(event);" onkeydown = "return disableCtrlKeyCombination2(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Forfeit Freight Memo Balance</h3>
              </div>
              
	<div class="card-body">
		<form action="#0" method="POST" autocomplete="off" id="Form1">			
	<div class="row" id="row_id_form">	
		
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Record By <font color="red"><sup>*</sup></font></label>
                  <select onchange="RecordType(this.value)" name="record_type" id="record_type" required class="form-control">
					<option value="FM">FM Number</option>
					<option value="Excel">Excel Upload (.csv)</option>
				  </select>
              </div>
			</div>
			
			<script>
			function RecordType(elem)
			{
				if(elem=='FM')
				{
					$('#file_div').hide();
					$('#file_input').attr('required',false);
					
					$('#fm_no_div').show();
					$('#fm_no').attr('required',true);
				}
				else
				{
					$('#file_div').show();
					$('#file_input').attr('required',true);
					
					$('#fm_no_div').hide();
					$('#fm_no').attr('required',false);
				}
			}
			</script>
			
			<div id="fm_no_div" class="col-md-3">	
               <div class="form-group">
                  <label>FM Number <font color="red"><sup>*</sup></font></label>
                  <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');" type="text" class="form-control" id="fm_no" name="fm_no" required="required">
              </div>
			</div>
			
			<div id="file_div" style="display:none" class="col-md-3">	
               <div class="form-group">
                  <label>Select Excel <font color="red"><sup>* (only .csv)</sup></font></label>
                  <input style="height:38px;" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" type="file" class="form-control" id="file_input" name="file" />
              </div>
			</div>
			
			<div id="button_div" class="col-md-2">
				<label>&nbsp;</label>
				<br />
				<button type="submit" id="button_chk" class="btn pull-right btn-danger">Check !</button>
			</div>
				
		</div>
		</form>	
		
		<div class="row">	
			<div class="col-md-12 table-responsive" id="main_div"></div>
		</div>
	
	</div>
	
	 </div>
</div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php include ("./footer.php"); ?>
</body>
</html>

<div id="func_result"></div>

<script>
$(document).ready(function (e) {
	$("#Form1").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	// $("#button_chk").attr("disabled", true);
		$.ajax({
        	url: "./check_file_forfeit_fm.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data) 
		    {
				$("#main_div").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function RemoveRecord(id)
{
	$('#RemoveBtn'+id).attr('disabled',true);
	$("#loadicon").show();
		jQuery.ajax({
		url: "remove_forfeit_fm_records.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
	error: function() {}
	});
}

function ClearPOD()
{
	$('#btn_clear_pod').attr('disabled',true);
	$("#loadicon").show();
		jQuery.ajax({
		url: "save_forfeit_fm_excel.php",
		data: 'id=' + 'ok',
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
	error: function() {}
	});
}

function Forfeit(id,frno)
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "save_forfeit_fm.php",
		data: 'id=' + id + '&frno=' + frno,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
	error: function() {}
	});
}
</script>