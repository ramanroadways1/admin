<?php
session_start();

if(!isset($_SESSION['rrpl_super_user']))
{
	echo "<script type='text/javascript'>
		window.location.href='../';
	</script>";
	exit();
}

include($_SERVER['DOCUMENT_ROOT']."/_connect.php");

date_default_timezone_set('Asia/Kolkata');

$menu = basename($_SERVER['PHP_SELF']);
$timestamp=date("Y-m-d H:i:s");
?>