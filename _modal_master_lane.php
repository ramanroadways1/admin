<script type="text/javascript">
  $(function() {  
      $("#master_lane_from").autocomplete({
		source: function(request, response) { 
		$.ajax({
                  url: 'autofill/get_master_lane_from_loc.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   vou_no: $('#vou_number_block').val()
                  },
                  success: function(data) {
                    response(data);
              }
           });
         },
           select: function (event, ui) { 
               $('#master_lane_from').val(ui.item.value);   
               $('#master_lane_from_id').val(ui.item.id);     
			  return false;
         },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#master_lane_from").val('');
				$("#master_lane_from_id").val('');
                alert('Location does not exist !'); 
      }},});}); 

$(function() {  
      $("#master_lane_to").autocomplete({
		source: function(request, response) { 
		$.ajax({
                  url: 'autofill/get_master_lane_to_loc.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   vou_no: $('#vou_number_block').val()
                  },
                  success: function(data) {
                    response(data);
              }
           });
         },
           select: function (event, ui) { 
               $('#master_lane_to').val(ui.item.value);   
               $('#master_lane_to_id').val(ui.item.id);     
			  return false;
         },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#master_lane_to").val('');
				$("#master_lane_to_id").val('');
                alert('Location does not exist !'); 
      }},});}); 

$(document).ready(function (e) {
$("#UpdateMasterLane").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#master_lane_modal_update").attr("disabled", true);
	$.ajax({
	url: "./update_master_lane.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="UpdateMasterLane" autocomplete="off"> 
<div class="modal fade" id="modal_Master_Lane" style="background:#DDD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
	  
	    <div class="modal-header bg-primary">
		Change Master Lane :
      </div>
	  
		<div class="modal-body">
	
<div class="row">

	<div class="col-md-6">
       <div class="form-group">
          <label class="control-label mb-1">Dispatch Location <font color="red"><sup>*</sup></font></label>
		  <input type="text" id="master_lane_from" name="from" class="form-control" required />
      </div>
   </div>
	 
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Last Delivery Location <font color="red"><sup>*</sup></font></label>
		   <input type="text" id="master_lane_to" name="to" class="form-control" required />
      </div>
   </div>
   
   <input type="hidden" id="master_lane_from_id" name="from_id">
   <input type="hidden" id="master_lane_to_id" name="to_id">
   <input type="hidden" id="master_lane_vou_id" name="vou_id">
	 
</div>
</div>
		
	<div class="modal-footer">
		<button type="button" id="master_lane_modal_close" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="master_lane_modal_update" class="btn btn-primary">Update Locations</button>
	</div>
		</div>	
     </div>
 </div>
</form>