<?php
require_once("./connect.php");
// echo md5("RJiten123#");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Market Truck Diesel Request</h3>
              </div>
              
	<div class="card-body">
				
<script>		
function SearchBy(select)
{
	$('#result_main').html('');
	$('#frno').val('');
	$('#lrno').val('');
	$('#tno').val('');
	$('#date_range').val('-0 days');
	
	if(select=='FM')
	{
		$('#div_frno').show();
		$('#div_lrno').hide();
		$('#div_tno').hide();
		$('#div_range').hide();
	}
	else if(select=='LR')
	{
		$('#div_frno').hide();
		$('#div_lrno').show();
		$('#div_tno').hide();
		$('#div_range').hide();
	}
	else if(select=='TRUCK')
	{
		$('#div_frno').hide();
		$('#div_lrno').hide();
		$('#div_tno').show();
		$('#div_range').show();
	}
	else
	{
		alert('Invalid option selected !');
		$('#result_main').html('');

		$('#div_frno').hide();
		$('#div_lrno').hide();
		$('#div_tno').hide();
		$('#div_range').hide();
	}
}
		
function Fetch(option)
{
	if(option=='FM')
	{
		if($('#frno').val()=='')
		{
			alert('Enter Freight Memo No First !');
		}
		else
		{
			var search_value = $('#frno').val();
			var date_range = "";
		}
	}
	else if(option=='LR')
	{
		if($('#lrno').val()=='')
		{
			alert('Enter LR No First !');
		}
		else
		{
			var search_value = $('#lrno').val();
			var date_range = "";
		}
	}
	else if(option=='TRUCK')
	{
		if($('#tno').val()=='' || $('#date_range').val()=='')
		{
			alert('Enter Truck No and Date Range First !');
		}
		else
		{
			var search_value = $('#tno').val();
			var date_range = $('#date_range').val();
		}
	}
	else
	{
		alert('Invalid option selected !');
		$('#div_frno').show();
		$('#div_lrno').hide();
		$('#div_tno').hide();
		$('#div_range').hide();
	}
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "fetch_market_request.php",
	data: 'option=' + option + '&search_value=' + search_value + '&date_range=' + date_range,
	type: "POST",
	success: function(data) {
		$("#result_main").html(data);
	},
	error: function() {}
	});
}		
</script>	

<script type="text/javascript">
$(function() {
    $("#tno").autocomplete({
      source: 'autofill/get_market_vehicle.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			 $(event.target).focus();
			 $("#button2").attr("disabled",true);
			alert('Truck Number doest not exists.');
        }
    }, 
    focus: function (event, ui) {
        $("#button2").attr("disabled",false);
		return false;
    }
    });
  });
</script>

	<div class="row">	
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Search By <font color="red"><sup>*</sup></font></label>
                  <select id="SearchBy" onchange="SearchBy(this.value)" class="form-control" required>
					<option value="FM">FM No</option>
					<option value="LR">LR No</option>
					<option value="TRUCK">Truck No</option>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3" id="div_frno">	
               <div class="form-group">
                  <label>FM No <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" id="frno" name="frno">
              </div>
			</div>
			
			<div class="col-md-3" id="div_lrno" style="display:none">	
               <div class="form-group">
                  <label>LR No <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" id="lrno" name="lrno">
              </div>
			</div>
			
			<div class="col-md-3" id="div_tno" style="display:none">	
               <div class="form-group">
                  <label>Truck No <font color="red"><sup>*</sup></font></label>
                  <input type="text" class="form-control" id="tno" name="tno">
              </div>
			</div>
			
			<div class="col-md-3" id="div_range" style="display:none">	
               <div class="form-group">
                  <label>Days Ago <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" name="range" id="date_range">
					<option value="-0 days">Today's</option>
					<option value="-1 days">Last 2 days</option>
					<option value="-4 days">Last 5 days</option>
					<option value="-6 days">Last 7 days</option>
					<option value="-9 days">Last 10 days</option>
					<option value="-14 days">Last 15 days</option>
					<option value="-29 days">Last 30 days</option>
					<option value="-59 days">Last 60 days</option>
					<option value="-89 days">Last 90 days</option>
					<option value="-119 days">Last 120 days</option>
					<option value="FULL">FULL REPORT</option>
				</select>
              </div>
			</div>
			
			<div id="button_div" class="col-md-2">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="button2" onclick="Fetch($('#SearchBy').val())" class="btn pull-right btn-danger">Check !</button>
			</div>
	</div>
		
		<div class="row">	
			<div class="col-md-12 table-responsive" style="overflow:auto">	
			
				<div id="result_main"></div>
			
			</div>
		</div>
	</div>
	
	<!--
	<div class="card-footer">
			<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>
    </div>-->
	
	<div id="result_main2"></div>
				
           </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script>  
function EditReq(frno,id)
{
	$('#modal_frno').val(frno);
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "load_market_req_for_edit.php",
	data: 'id=' + id + '&frno=' + frno,
	type: "POST",
	success: function(data) {
		$("#result_modal_data").html(data);
		document.getElementById("req_modal_button").click();
	},
	error: function() {}
	});
}

function DeleteReq(frno,id)
{
	$("#deletereqBtn"+id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
	url: "delete_market_req.php",
	data: 'frno=' + frno,
	type: "POST",
	success: function(data) {
		$("#result_main2").html(data);
	},
	error: function() {}
	});
}  

function SaveSingle(id,dsl_by_func,diesel_id)
{
	var dsl_by = $('#dsl_by'+id).val();
		
	if(dsl_by!=dsl_by_func)	
	{
		alert('Diesel Type not found !');
	}
	else
	{
		var qty = $('#qty'+id).val();
		var rate = $('#rate'+id).val();
		var amt = $('#amt'+id).val();
		
		if(dsl_by=='CARD')
		{
			var card_no = $('#card'+id).val();
			var comp = $('#fuelcomp'+id).val();
			var card_id = $('#cardno2'+id).val();
			var phy_card_no = $('#phy_card_no'+id).val();
			var mobile_no = "";
		}	
		else if(dsl_by=='OTP')
		{
			var card_no = $('#mobile_no'+id).val();
			var comp = $('#fuelcomp'+id).val();
			var card_id = "0";
			var phy_card_no = card_no;
			var mobile_no = card_no;
		}	
		else if(dsl_by=='PUMP')
		{
			var card_no = $('#pumpvalue'+id).val();
			var comp = '';
			var card_id = "0";
			var phy_card_no = card_no;
			var mobile_no = "";
		}
		else
		{
			window.location.href='request_market_truck.php';	
		}
		
		if(dsl_by=='PUMP' && (qty==0 || qty=='' || rate==0 || rate==''))
		{
			alert('Check diesel rate or qty !');
		}
		else if(card_no=='')
		{
			alert('Select pump or card first !');
		}
		else
		{
			if(qty>400 || rate>90)
			{
				alert('Max value for Qty is : 400 and Rate is : 90');
			}
			else
			{
				$("#loadicon").show();
				jQuery.ajax({
				url: "save_market_req_diesel_cache.php",
				data: 'id=' + id + '&dsl_by=' + dsl_by + '&qty=' + qty + '&rate=' + rate + '&amt=' + amt + '&card_no=' 
				+ card_no + '&comp=' + comp + '&diesel_id=' + diesel_id + '&card_id=' + card_id + '&phy_card_no='
				+ phy_card_no + '&mobile_no=' + mobile_no,
				type: "POST",
				success: function(data){
					$("#result_two").html(data);
					// $("#save_result"+id).html(data);
				},
				error: function() {}
				});
			}
		}
	}
} 

function Edit(id,dsl_by)
{
	$("#loadicon").show();
	$("#req_update_button").attr("disabled", false);
	
		$('.qtyclass').attr('readonly',true);
		$('.rateclass').attr('readonly',true);
		$('.amtclass').attr('readonly',true);
		$('.cashclass').attr('readonly',true);
		$('.cardclass').attr('readonly',true);
		$('.savebtnclass').attr('disabled',true);
		$('.pumpvalueclass').attr('disabled',true);
		$('.fuelcompclass').attr('disabled',true);
		
		$('.qtyclass').attr('oninput','');
		$('.rateclass').attr('oninput','');
		$('.amtclass').attr('oninput','');
		
		$('#amt'+id).attr('readonly',false);
		$('#cash'+id).attr('readonly',true);
		$('#card'+id).attr('readonly',false);
		$('#mobile_no'+id).attr('readonly',false);
		
		$('#savebtn'+id).attr('disabled',false);
		$('#pumpvalue'+id).attr('disabled',false);
		$('#fuelcomp'+id).attr('disabled',false);
		
	if(dsl_by=='CARD')
	{
		
	}
	else if(dsl_by=='OTP')
	{
		
	}
	else if(dsl_by=='PUMP')
	{
		// $('#qty'+id).attr("oninput","sum2('"+id+"')");
		// $('#rate'+id).attr("oninput","sum1('"+id+"')");
		$('#amt'+id).attr("oninput","AmountCall('"+id+"')");
		$('#qty'+id).attr('readonly',false);
		$('#rate'+id).attr('readonly',false);
	}
	else
	{
		window.location.href='request_market_truck.php';	
	}
	
	jQuery.ajax({
	url: "mark_as_edit.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data){
		$("#result_main2").html(data);
	},
	error: function() {}
	});
}
</script>  

<script>
// function AmountCall(id)
// {
	// if($("#qty"+id).val()=='' && $("#rate"+id).val()=='' || $("#qty"+id).val()==0 && $("#rate"+id).val()==0){
		// $("#qty"+id).focus()
		// $("#amt"+id).val('');
	// }
	// else
	// {
		// if($("#rate"+id).val()=='' || $("#rate"+id).val()<60)
		// {
			// $("#rate"+id).val((Number($("#amt"+id).val()) / Number($("#qty"+id).val())).toFixed(2));
		// }
		// else
		// {
			// $("#qty"+id).val((Number($("#amt"+id).val()) / Number($("#rate"+id).val())).toFixed(2));
		// }
	// }	
// }
					function sum1(id) 
					 {
						if($("#qty"+id).val()=='')
						{
							$("#qty"+id).val('0');
						}
						
						if($("#rate"+id).val()=='')
						{
							$("#rate"+id).val('0');
						}
							
						$("#amt"+id).val(Math.round(Number($("#qty"+id).val()) * Number($("#rate"+id).val())));
					}
					
					function sum2(id) 
					{
						if($("#qty"+id).val()=='')
						{
							$("#qty"+id).val('0');
						}
						
						if($("#rate"+id).val()=='')
						{
							$("#rate"+id).val('0');
						}
							
						$("#amt"+id).val(Math.round(Number($("#qty"+id).val()) * Number($("#rate"+id).val())));
					}
					 
					 function sum3(id) 
					 {
						if($("#qty"+id).val()=='')
						{
							$("#qty"+id).val('0');
						}
						
						if($("#rate"+id).val()=='')
						{
							$("#rate"+id).val('0');
						}
						
						if($("#amt"+id).val()=='')
						{
							$("#amt"+id).val('0');
						}
						
						$("#rate"+id).val((Number($("#amt"+id).val()) / Number($("#qty"+id).val())).toFixed(2));
					}
</script>

<a style="display:none1" id="req_modal_button" data-toggle="modal" data-target="#ReqModal"></a>

<script type="text/javascript">
$(document).ready(function (e) {
$("#ReqUpdateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#req_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_market_req.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_req_modal").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<div style="background:#EEE" class="modal fade" id="ReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
		
	<div class="modal-header bg-primary">
		Update Diesel Request :
     </div>
	  
		<form id="ReqUpdateForm" autocomplete="off"> 
			<div class="modal-body">
			
				<div id="result_req_modal"></div>
				
			<input type="hidden" name="frno" id="modal_frno">
				
			<div class="row">
			
				<div class="col-md-12 table-responsive" style="overflow:auto">
					<div id="result_modal_data"></div>
				</div>
				
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" id="req_close_button" onclick="document.getElementById('menu_button2').click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="req_update_button" disabled class="btn btn-primary">Update Final</button>
	</div>
	
		</form>
		</div>	
     </div>
</div>	
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>