<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$vou_id = escapeString($conn,strtoupper($_POST['vou_id']));
$timestamp=date("Y-m-d H:i:s");

$get_vou_details = Qry($conn,"SELECT frno,branch,branch_user,fstation,tstation,wt12,weight,done,timestamp 
FROM freight_form_lr WHERE id='$vou_id'");
if(!$get_vou_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_vou_details)==0)
{
	echo "<script>
		alert('Voucher not found !');
		window.location.href='./own_truck_form.php';
	</script>";
	exit();
}

$row_voucher = fetchArray($get_vou_details);

$voucher_branch = $row_voucher['branch'];

if($row_voucher['frno']!=$frno)
{
	echo "<script>
		alert('Voucher number not verified !');
		window.location.href='./own_truck_form.php';
	</script>";
	exit();
}

if($row_voucher['done']=="1")
{
	echo "<script>
		alert('e-Diary Trip Created. Delete Trip First !');
		$('#loadicon').hide();
		$('#delete_form_button').attr('disabled',false);
	</script>";
	exit();
}

$fetch_lr_count = Qry($conn,"SELECT id FROM freight_form_lr WHERE lrno IN(SELECT lrno FROM freight_form_lr WHERE frno='$frno') 
AND frno!='$frno'");
if(!$fetch_lr_count){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}	
		
if(numRows($fetch_lr_count)>0)
{
	echo "<script>
		alert('More than One Freight Memo or Own Truck Form Found. Contact Sytem-Admin !');
		$('#loadicon').hide();
		$('#delete_form_button').attr('disabled',false);
	</script>";
	exit();
}
	
$select_lrno = Qry($conn,"SELECT lrno FROM freight_form_lr WHERE frno='$frno' AND branch='$voucher_branch'");
if(!$select_lrno){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$lrnos = array();
$lrnos_store = array();
		
while($row_lrno=fetchArray($select_lrno))
{
	$lrnos[] = "'".$row_lrno['lrno']."'";
	$lrnos_store[] = $row_lrno['lrno'];
}
		
$lrnos=implode(',', $lrnos);
$lrnos_store=implode(',', $lrnos_store);
	
$update_log = "Truck_no: $row_voucher[truck_no], Branch: $row_voucher[branch], Branch_User: $row_voucher[branch_user],
From-To: $row_voucher[fstation]-$row_voucher[tstation], Act_Weight: $row_voucher[wt12], Chrg_Weight: $row_voucher[weight], 
Timestamp: $row_voucher[timestamp], LR_Nos: $lrnos_store.";

		
StartCommit($conn);
$flag = true;
	
$update_lr = Qry($conn,"UPDATE lr_sample SET crossing='' WHERE lrno IN($lrnos) AND branch='$voucher_branch'");
if(!$update_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Crossing Update failed. $lrnos_store.",$conn,$page_name,__LINE__);
}

$copy_to_pending = Qry($conn,"INSERT INTO lr_sample_pending(lr_id,company,branch,branch_user,lrno,lr_type,wheeler,date,create_date,
truck_no,fstation,tstation,dest_zone,consignor,consignor_before,bill_to,bill_to_id,consignee,con2_addr,from_id,to_id,con1_id,con2_id,
zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,weight2,bill_rate,bill_amt,t_type,do_no,shipno,invno,item,
item_id,item_name,plant,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing,cancel,break,diesel_req,nrr,
downloadid,downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus) SELECT id,company,branch,branch_user,
lrno,lr_type,wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignor,consignor_before,bill_to,bill_to_id,consignee,
con2_addr,from_id,to_id,con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,weight2,bill_rate,
bill_amt,t_type,do_no,shipno,invno,item,item_id,item_name,plant,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,
crossing,cancel,break,diesel_req,nrr,downloadid,downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus 
FROM lr_sample WHERE lrno IN($lrnos) AND branch='$voucher_branch'");

if(!$copy_to_pending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("LR copy failed. $lrnos_store.",$conn,$page_name,__LINE__);
}
	
$delete_fm_lr=Qry($conn,"DELETE FROM freight_form_lr WHERE frno='$frno'");
if(!$delete_fm_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
	('$vou_id','$frno','OWN_TRUCK_FORM','DELETE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
		
	echo "<script>
		alert('Voucher Deleted Successfully !');
		window.location.href='./own_truck_form.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./own_truck_form.php");
	exit();
}
?>