<?php
require_once("./connect.php");

$option=escapeString($conn,$_POST['option']);

if($option=='FM')
{
	$frno = escapeString($conn,strtoupper($_POST['search_value']));
	$search_var="fno='$frno'";
}
else if($option=='LR')
{
	$lrno = escapeString($conn,strtoupper($_POST['search_value']));
	$search_var="lrno='$lrno'";
}
else if($option=='TRUCK')
{
	$tno = escapeString($conn,strtoupper($_POST['search_value']));
	if($_POST['date_range']=='FULL'){
		$from_date="2017-01-01";
		$to_date=date("Y-m-d");
	}
	else{
		$from_date=date('Y-m-d', strtotime($_POST['date_range'], strtotime(date("Y-m-d"))));
		$to_date=date("Y-m-d");
	}
	$search_var="pay_date BETWEEN '$from_date' AND '$to_date' AND tno='$tno'";
}
else
{
	echo "<script>
		alert('ERROR : Diesel Request data not found !');
		window.location.href='request_market_truck.php';
	</script>";
	exit();
}

$qry=Qry($conn,"SELECT id,branch,fno,token_no,com,dsl_by,SUM(disamt) as diesel,cash,tno,lrno,
GROUP_CONCAT(dsl_nrr separator ', ') as dsl_nrr FROM diesel_fm WHERE $search_var GROUP by fno");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)>0)
{
	?>
	<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:13px;" colspan="16">Market Truck Diesel Requests :</th>
    </tr>
		<tr>    
			<th>Vou_Id</th>
			<th>Company</th>
			<th>Amount</th>
			<th>Truck_No</th>
			<th>LR_No</th>
			<th>Diesel_Narr.</th>
			<th></th>
			<th></th>
		</tr>	
	<?php
	$cash_amount = 0;
	while($row=fetchArray($qry))
	{
		$cash_amount = $cash_amount+$row['cash'];
		
		echo "<tr>
				<td>$row[fno]</td>
				<td>$row[com]</td>
				<td>$row[diesel]</td>
				<td>$row[tno]</td>
				<td>$row[lrno]</td>
				<td>$row[dsl_nrr]</td>
				<td>
					<button type='button' onclick=EditReq('$row[fno]','$row[id]') class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
				</td>
				<td>
					<button type='button' id='deletereqBtn$row[id]' onclick=DeleteReq('$row[fno]','$row[id]') class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button>
				</td>
			</tr>
			";
	}
	
	if($cash_amount>0)
	{
		echo "<script>
			alert('Cash entry found.');
			window.location.href='./request_market_truck.php';
		</script>";
		exit();
	}
	
	echo "</table>";
	
	echo "<script>
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<font color='red'>No record found !<font>
	<script>
		$('#loadicon').hide();	
	</script>";
	exit();
}
?>