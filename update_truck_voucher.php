<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));
$frno = escapeString($conn,strtoupper($_POST['frno']));
$vou_date = escapeString($conn,($_POST['vou_date']));
$amount = escapeString($conn,($_POST['amount']));
$pay_mode = escapeString($conn,strtoupper($_POST['pay_mode']));
$chq_bank = escapeString($conn,strtoupper($_POST['chq_bank']));
$chq_no = escapeString($conn,strtoupper($_POST['chq_no']));
$pan_no = escapeString($conn,strtoupper($_POST['pan_no']));
$narration = escapeString($conn,strtoupper($_POST['narration']));
$vou_type = escapeString($conn,strtoupper($_POST['vou_type']));
$random_var = escapeString($conn,strtoupper($_POST['random_var']));
$driver_name = escapeString($conn,strtoupper($_POST['driver_name']));
$tno = escapeString($conn,strtoupper($_POST['tno']));
$driver_code = escapeString($conn,strtoupper($_POST['driver_code']));
$trip_id = escapeString($conn,strtoupper($_POST['trip_id']));

$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$fetch_vou = Qry($conn,"SELECT id,tdvid,user,date,newdate,company,truckno,dname,amt,mode,hisab_vou,dest,colset_d,timestamp FROM 
mk_tdv WHERE id='$id'");.

if(!$fetch_vou){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_vou)==0)
{
	echo "<script>
		alert('Voucher not found !');
		$('#req_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row=fetchArray($fetch_vou);

if($row['truckno']!=$tno)
{
	echo "<script>
		alert('Vehicle number not verified !');
		$('#req_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row['tdvid']!=$frno)
{
	echo "<script>
		alert('Voucher number not verified !');
		$('#req_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$update_log=array();
$update_Qry=array();

if($pay_mode!=$row['mode'])
{
	$update_log[]="Payment_Mode : $row[mode] to $pay_mode";
	$update_Qry[]="mode='$pay_mode'";
}

if($amount!=$row['amt'])
{
	$update_log[]="Amount : $row[amt] to $amount";
	$update_Qry[]="amt='$amount'";
}

if($vou_date!=$row['newdate'])
{
	$update_log[]="Vou_Date : $row[newdate] to $vou_date";
	$update_Qry[]="newdate='$vou_date'";
}

if($narration!=$row['dest'])
{
	$update_log[]="Narration : $row[dest] to $narration";
	$update_Qry[]="dest='$narration'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#req_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$company = $row['company'];
$branch = $row['user'];
$old_amount = $row['amt'];
$sys_date = $row['date'];
$downloaded = $row['colset_d'];

if($row['mode']=='HAPPAY')
{
	echo "<script>
		alert('You can\'t edit happay\'s voucher !');
		$('#req_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($pay_mode=='CASH')
{
	$fetch_branch_balance= Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
	if(!$fetch_branch_balance){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($fetch_branch_balance)==0)
	{
		Redirect("Branch balance not found.","./");
		exit();
	}

	$row_branch_balance=fetchArray($fetch_branch_balance);

	$rrpl_cash_balance = $row_branch_balance['balance'];
	$rr_cash_balance = $row_branch_balance['balance2'];
		
	if($company=='RRPL' && $rrpl_cash_balance>=$amount)
	{
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_cash_balance>=$amount)
	{
	}
	else
	{
		echo "<script>
			alert('Insufficient Balance in $company !');
			$('#req_update_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
		
if($row['mode']=='CASH' || $row['mode']=='HAPPAY')
{
	$type11="dairy.cash";
}
else if($row['mode']=='CHEQUE')
{
	$type11="dairy.cheque";
}
else if($row['mode']=='NEFT')
{
	$type11="dairy.rtgs";
}
else
{
	echo "<script>
		alert('Invalid option selected !');
		window.location.href='truck_voucher.php';
	</script>";
	exit();
}
		
if($vou_type=='MAIN')
{
	$v_type="M";
	
	if($trip_id!="")
	{
		echo "<script>
			alert('Invalid Voucher Type Defined !');
			window.location.href='truck_voucher.php';
		</script>";
		exit();
	}
}
else if($vou_type=='DIARY')
{
	$v_type="D";
	
	if($trip_id=="")
	{
		echo "<script>
			alert('Invalid Voucher Type Defined !');
			window.location.href='truck_voucher.php';
		</script>";
		exit();
	}
}
else
{
	echo "<script>
			alert('Invalid Voucher Type Defined !');
			window.location.href='truck_voucher.php';
		</script>";
		exit();
}

	if($random_var==1)
	{
		if($v_type!="D")
		{
			echo "<script>
				alert('Invalid Voucher Type Defined !');
				window.location.href='truck_voucher.php';
			</script>";
			exit();
		}
	
		$fetch_trans_id = Qry($conn,"SELECT trip_id,trans_id FROM $type11 WHERE vou_no='$frno'");
		if(!$fetch_trans_id){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($fetch_trans_id)==0)
		{
			echo "<script>
				alert('Unable to fetch Transaction Id : $frno !');
				$('#req_update_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		$row_trans_id22 = fetchArray($fetch_trans_id);
		
		$trans_id_db=$row_trans_id22['trans_id'];
		$trip_id_db=$row_trans_id22['trip_id'];
		
		if($trip_id!=$trip_id_db)
		{
			echo "<script>
				alert('Trip id not verified !');
				window.location.href='truck_voucher.php';
			</script>";
			exit();
		}
		
		$chk_driver_book = Qry($conn,"SELECT id,driver_code,tno FROM dairy.driver_book WHERE trans_id='$trans_id_db'");
		if(!$chk_driver_book){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($chk_driver_book)==0)
		{
			echo "<script>
				alert('Unable to fetch Driver Code !');
				window.location.href='truck_voucher.php';
			</script>";
			exit();
		}
		
		$row_driver_code = fetchArray($chk_driver_book);
		
		$tno_db = $row_driver_code['tno'];
		$driver_code_db = $row_driver_code['driver_code'];
		$driver_book_id = $row_driver_code['id'];
		
		if($tno!=$tno_db)
		{
			echo "<script>
				alert('Truck Number not verified !');
				window.location.href='truck_voucher.php';
			</script>";
			exit();
		}
		
		if($driver_code_db!=$driver_code)
		{
			echo "<script>
				alert('Driver Code not verified !');
				window.location.href='truck_voucher.php';
			</script>";
			exit();
		}
	}
	
	if($pay_mode=='NEFT')
	{
		if($random_var==1)
		{
			$chk_ac_details = Qry($conn,"SELECT acname,acno,bank,ifsc FROM dairy.driver_ac WHERE code='$driver_code_db'");
			if(!$chk_ac_details){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
			
			if(numRows($chk_ac_details)==0)
			{
				echo "<script>
					alert('Unable to fetch Driver Account details !');
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$row_ac_details = fetchArray($chk_ac_details);
			
			$ac_holder = escapeString($conn,strtoupper($row_ac_details['acname']));
			$ac_no = escapeString($conn,strtoupper($row_ac_details['acno']));
			$bank_name = escapeString($conn,strtoupper($row_ac_details['bank']));
			$ifsc = escapeString($conn,strtoupper($row_ac_details['ifsc']));
		}
		else
		{
			$ac_holder = escapeString($conn,strtoupper($_POST['ac_holder']));
			$ac_no = escapeString($conn,strtoupper($_POST['ac_no']));
			$bank_name = escapeString($conn,strtoupper($_POST['bank_name']));
			$ifsc = escapeString($conn,strtoupper($_POST['ifsc']));
		}
		
		if($ac_holder=='' || $ac_no=='' || $bank_name=='' || $ifsc=='')
		{
			echo "<script>
				alert('Incomplete account details !');
				$('#req_update_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	else
	{
			$ac_holder = "";
			$ac_no = "";
			$bank_name = "";
			$ifsc = "";
	}	

	if(count(explode(' ', $pan_no)) > 1)
	{
		echo "<script>
			alert('PAN Card Contains Whitespaces !');
			$('#req_update_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

	if(count(explode(' ', $ac_no)) > 1)
	{
		echo "<script>
			alert('Account Number Contains Whitespaces !');
			$('#req_update_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

	if(count(explode(' ', $ifsc)) > 1)
	{
		echo "<script>
			alert('IFSC Code Contains Whitespaces !');
			$('#req_update_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

if($amount==0 || $amount=='' || $pay_mode=='')
{
	echo "<script>
		alert('Unable to fetch transaction details !');
		$('#req_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row['mode']=='NEFT')
{
		$fetch_rtgs_req = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
		if(!$fetch_rtgs_req){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
			
		if(numRows($fetch_rtgs_req)>0)
		{
			echo "<script>
				alert('Rtgs Payment Done. You Can\'t Edit !');
				$('#req_update_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
}
else if($row['mode']=='CASH')
{
	$fetch_cash_id = Qry($conn,"SELECT id,user FROM cashbook WHERE vou_no='$frno'");
	if(!$fetch_cash_id){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
		if(numRows($fetch_cash_id)==0)
		{
			echo "<script>
				alert('Voucher not found in cashbook !');
				$('#loadicon').hide();
			</script>";
			exit();
		}
			
		$row_cash_id = fetchArray($fetch_cash_id);
		
		if($row_cash_id['user']!=$branch)	
		{
			echo "<script>
				alert('Voucher branch not matching with cashbook branch !');
				$('#loadicon').hide();
			</script>";
			exit();
		}			
		
		$cash_id = $row_cash_id['id'];
}

$fetch_email = Qry($conn,"SELECT email FROM user WHERE username='$branch'");
if(!$fetch_email){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_email)==0)
{
	echo "<script>
		alert('Branch not found !');
		$('#req_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_email = fetchArray($fetch_email);
$email=$row_email['email'];

if($company=='RRPL')
{
	$debit="debit";
	$balance="balance";
}
else
{
	$debit="debit2";
	$balance="balance2";
}

$diff = $row['amt']-$amount;

StartCommit($conn);
$flag = true;

if($row['mode']==$pay_mode)
{
	if($pay_mode=='NEFT')
	{
		$fetch_rtgs_req = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
		if(!$fetch_rtgs_req){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($fetch_rtgs_req)>0)
		{
			$flag = false;
			errorLog("Rtgs Payment Done: $frno.",$conn,$page_name,__LINE__);
		}
			
		$update_rtgs = Qry($conn,"UPDATE rtgs_fm SET totalamt='$amount',amount='$amount',acname='$ac_holder',acno='$ac_no',
		bank_name='$bank_name',ifsc='$ifsc',pan='$pan_no' WHERE fno='$frno' AND colset_d!='1'");
		if(!$fetch_rtgs_req){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Unable to Update RTGS Voucher : $frno.",$conn,$page_name,__LINE__);
		}
			
		if($row['amt']!=$amount)
		{			
			$update_passbook = Qry($conn,"UPDATE passbook SET `$debit`='$amount' WHERE vou_no='$frno'");
			if(!$update_passbook){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	else if($pay_mode=='CHEQUE')
	{
		$update_passbook = Qry($conn,"UPDATE passbook SET chq_no='$chq_no',`$debit`='$amount' WHERE vou_no='$frno'");
		if(!$update_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_chequebook = Qry($conn,"UPDATE cheque_book SET amount='$amount',cheq_no='$chq_no' WHERE vou_no='$frno'");
		if(!$update_chequebook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else if($pay_mode=='CASH')
	{
		if(sprintf("%.2f",$row['amt'])!=sprintf("%.2f",$amount))
		{
			$min_cash_id = $cash_id-1;
			
			$update_cashbook = Qry($conn,"UPDATE cashbook SET `$debit`='$amount' WHERE id='$cash_id'");
			if(!$update_cashbook){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Unable to update cashbook: $frno.",$conn,$page_name,__LINE__);	
			}
			
			$update_main_balance = Qry($conn,"UPDATE user SET `$balance`=`$balance`+'$diff' WHERE username='$branch'");
			if(!$update_main_balance){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Unable to update Main Balance: $frno.",$conn,$page_name,__LINE__);	
			}
			
			$update_cashbook_amount = Qry($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$diff' WHERE id>'$min_cash_id' AND 
			user='$branch' AND comp='$company'");
			if(!$update_cashbook_amount){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}	
	else
	{
		$flag = false;
		errorLog("Invalid option selected: $frno.",$conn,$page_name,__LINE__);
	}
	
		if($v_type=='D' && $random_var==1)
		{
			if($pay_mode=='CASH')
			{				
				$update_diary_vou = Qry($conn,"UPDATE dairy.cash SET amount='$amount',date='$vou_date',narration='$narration' WHERE 
				trans_id='$trans_id_db' AND vou_no='$frno'");
				if(!$update_diary_vou){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$v_type1="CASH";
				$trip_col="cash";
			}
			else if($pay_mode=='CHEQUE')
			{
				$update_diary_vou = Qry($conn,"UPDATE dairy.cheque SET amount='$amount',date='$vou_date',cheq_no='$chq_no',
				bank='$chq_bank',narration='$narration' WHERE trans_id='$trans_id_db' AND vou_no='$frno'");
				if(!$update_diary_vou){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$v_type1="CHEQUE";
				$trip_col="cheque";
			}
			else
			{
				$update_diary_vou = Qry($conn,"UPDATE dairy.rtgs SET amount='$amount',date='$vou_date',narration='$narration' WHERE 
				trans_id='$trans_id_db' AND vou_no='$frno'");
				if(!$update_diary_vou){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$v_type1="RTGS";
				$trip_col="rtgs";
			}

			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Unable to update dairy voucher: $frno.",$conn,$page_name,__LINE__);
			}
			
			if(sprintf("%.2f",$row['amt'])!=sprintf("%.2f",$amount))
			{
				$sel_id_driver_book = Qry($conn,"SELECT id,credit,balance FROM dairy.driver_book WHERE trans_id='$trans_id_db'");
				if(!$sel_id_driver_book){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(numRows($sel_id_driver_book)==0)
				{
					$flag = false;
					errorLog("Record not found in driver book.",$conn,$page_name,__LINE__);
				}
				
				$row_driver_book_id=fetchArray($sel_id_driver_book);
				
				$d_book_id=$row_driver_book_id['id'];
				$new_bal_driver_book = $row_driver_book_id['balance'] + ($diff);
				
				$update_driver_book = Qry($conn,"UPDATE dairy.driver_book SET credit='$amount',balance=balance+'$diff' WHERE id='$d_book_id'");
				if(!$update_driver_book){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(AffectedRows($conn)==0)
				{
					$flag = false;
					errorLog("Amount not updated Driver book id : $d_book_id.",$conn,$page_name,__LINE__);
				}
				
				$update_driver_book_all = Qry($conn,"UPDATE dairy.driver_book SET balance=balance+'$diff' WHERE 
				id>'$d_book_id' AND driver_code='$driver_code' AND tno='$tno'");
				if(!$update_driver_book_all){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+'$diff' WHERE 
				code='$driver_code' AND down=0 ORDER BY id DESC LIMIT 1");
				if(!$update_driver_balance){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(AffectedRows($conn)==0)
				{
					$flag = false;
					errorLog("Driver balance not updated Driver_code : $driver_code.",$conn,$page_name,__LINE__);
				}
				
				$update_trip_amount = Qry($conn,"UPDATE dairy.trip SET `$trip_col`=`$trip_col`+'$diff' WHERE id='$trip_id'");
				if(!$update_trip_amount){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(AffectedRows($conn)==0)
				{
					$flag = false;
					errorLog("Trip not updated. Trip_id : $trip_id.",$conn,$page_name,__LINE__);
				}
			}
		}
	
	$update_voucher = Qry($conn,"UPDATE mk_tdv SET newdate='$vou_date',amt='$amount',mode='$pay_mode',chq_no='$chq_no',
	chq_bank='$chq_bank',ac_name='$ac_holder',ac_no='$ac_no',bank='$bank_name',ifsc='$ifsc',pan='$pan_no',
	dest='$narration',colset_d='',colset='' WHERE id='$id'");
	if(!$update_voucher){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
				
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Voucher not updated. Id : $id, Voucher_no: $frno.",$conn,$page_name,__LINE__);
	}
}
else
{
	if($row['mode']=='NEFT')
	{
		// $fetch_rtgs_req = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
		// if(!$fetch_rtgs_req){
			// $flag = false;
			// errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		// }
		
		// if(numRows($fetch_rtgs_req)>0)
		// {
			// $flag = false;
			// errorLog("Rtgs Payment Done. Id : $id, Voucher_no: $frno.",$conn,$page_name,__LINE__);
		// }
		
		$delete_rtgs = Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$frno' AND colset_d!='1'");
		if(!$delete_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		if(AffectedRows($conn)==0)
		{
			$flag = false;
			errorLog("Unable to delete RTGS Payment: $frno.",$conn,$page_name,__LINE__);
		}
			
		$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
		if(!$delete_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($sys_date==$today)
		{
			$update_today_data = Qry($conn,"UPDATE today_data SET neft_amount=neft_amount-'$old_amount' WHERE branch='$branch' AND date='$today'");
			if(!$update_today_data){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}	
	}
	else if($row['mode']=='CHEQUE')
	{
		$delete_passbook = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno'");
		if(!$delete_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$delete_chequebook = Qry($conn,"DELETE FROM cheque_book WHERE vou_no='$frno'");
		if(!$delete_chequebook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$update_main_balance = Qry($conn,"UPDATE user SET `$balance`=`$balance`+'$diff' WHERE username='$branch'");
		if(!$update_main_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0)
		{
			$flag=false;
			errorLog("Balance not updated.",$conn,$page_name,__LINE__);
		}
		
		$update_cashbook_amount = Qry($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$old_amount' WHERE id>'$cash_id' AND user='$branch' AND comp='$company'");
		if(!$update_cashbook_amount){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$delete_cashbook = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
		if(!$delete_cashbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	
		if($sys_date==$today)
		{
			$update_today_data1=Qry($conn,"UPDATE today_data SET truck_vou=truck_vou-1,truck_vou_amount=truck_vou_amount-'$old_amount' WHERE 
			branch='$branch' AND date='$today'");
			if(!$update_today_data1){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
		
		if($v_type=='D' && $random_var==1)
		{
			if($row['mode']=='CASH')
			{				
				$delete_diary_vou = Qry($conn,"DELETE FROM dairy.cash WHERE trans_id='$trans_id_db' AND vou_no='$frno'");
				
				$v_type1="CASH";
				$trip_col_del="cash";
			}
			else if($row['mode']=='CHEQUE')
			{
				$delete_diary_vou = Qry($conn,"DELETE FROM dairy.cheque WHERE trans_id='$trans_id_db' AND vou_no='$frno'");
				
				$v_type1="CHEQUE";
				$trip_col_del="cheque";
			}
			else
			{
				$delete_diary_vou = Qry($conn,"DELETE FROM dairy.rtgs WHERE trans_id='$trans_id_db' AND vou_no='$frno'");
				
				$v_type1="RTGS";
				$trip_col_del="rtgs";
			}

			if(!$delete_diary_vou){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Unable to Update Diary Voucher. Trans_id: $trans_id_db.",$conn,$page_name,__LINE__);
			}
			
			$update_trip_amount_del = Qry($conn,"UPDATE dairy.trip SET `$trip_col_del`=`$trip_col_del`-'$old_amount' WHERE 
			id='$trip_id'");
			if(!$update_trip_amount_del){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Trip not updated. trip_id: $trip_id.",$conn,$page_name,__LINE__);
			}
		}	
	
	if($pay_mode=='NEFT')
	{
		if($company=='RRPL')
		{
			$crn_string="RRPL-T";
		}
		else
		{
			$crn_string="RR-T";
		}
		
		$get_crn_qry = GetCRN($crn_string,$conn);
		
		if(!$get_crn_qry || $get_crn_qry==0 || $get_crn_qry=="")
		{
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$crnnew = $get_crn_qry;
		
		$insert_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,`$debit`,timestamp) VALUES 
		('$branch','$frno','$today','$vou_date','$company','Truck_Voucher','$narration','$amount','$timestamp')");
		if(!$insert_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,
		tno,type,email,crn,timestamp) VALUES ('$frno','$branch','$company','$amount','$amount','$ac_holder','$ac_no','$bank_name','$ifsc',
		'$pan_no','$today','$vou_date','$tno','TRUCK_ADVANCE','$email','$crnnew','$timestamp')");			
		if(!$qry_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$update_today_data_rtgs=Qry($conn,"UPDATE today_data SET neft=neft+1,neft_amount=neft_amount+'$amount' WHERE branch='$branch' 
		AND date='$today'");
		if(!$update_today_data_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else if($pay_mode=='CHEQUE')
	{
		$insert_passbook=Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,chq_no,`$debit`,timestamp) 
		VALUES ('$branch','$frno','$today','$vou_date','$company','Truck_Voucher','$narration','$chq_no','$amount','$timestamp')");
		if(!$insert_passbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$insert_chequebook=Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,vou_date,amount,cheq_no,date,branch,timestamp) 
		VALUES ('$frno','Truck_Voucher','$vou_date','$amount','$chq_no','$today','$branch','$timestamp')");
		if(!$insert_chequebook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else if($pay_mode=='CASH')
	{
		$fetch_balance= Qry($conn,"SELECT balance,balance2 FROM user WHERE username='$branch'");
		if(!$fetch_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($fetch_balance)==0){
			$flag = false;
			errorLog("Branch Balance not found. $branch",$conn,$page_name,__LINE__);
		}

		$row_balance=fetchArray($fetch_balance);

		$rrpl_cash=$row_balance['balance'];
		$rr_cash=$row_balance['balance2'];
		
		if($company=='RRPL' && $rrpl_cash>=$amount)
		{
			$newbal=$rrpl_cash - $amount;
			
		}
		else if($company=='RAMAN_ROADWAYS' && $rr_cash>=$amount)
		{
			$newbal=$rr_cash - $amount;
		}
		else
		{
			$flag = false;
			errorLog("INSUFFICIENT Balance in $company. $branch",$conn,$page_name,__LINE__);
		}
		
		$query_update_cash = Qry($conn,"update user set `$balance`='$newbal' where username='$branch'");
		if(!$query_update_cash){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$cash_entry=Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit`,`$balance`,timestamp) 
		VALUES ('$branch','$today','$vou_date','$company','$frno','Truck_Voucher','$narration','$amount','$newbal','$timestamp')");
		if(!$cash_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$flag = false;
		errorLog("Invalid payment mode.",$conn,$page_name,__LINE__);
	}
	
		if($v_type=='D' && $random_var==1)
		{
			if($pay_mode=='CASH')
			{	
				$trans_id_Qry = GetTransId("CASH",$conn,"cash");
				if(!$trans_id_Qry || $trans_id_Qry==0 || $trans_id_Qry=="")
				{
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
		
				$trans_id_new = $trans_id_Qry;
				
				$insert_diary_vou = Qry($conn,"INSERT INTO dairy.cash(trip_id,trans_id,tno,vou_no,amount,date,narration,branch,
				timestamp) VALUES ('$trip_id','$trans_id_new','$tno','$frno','$amount','$today','$narration','$branch','$timestamp')");
				if(!$insert_diary_vou){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$v_type1="CASH";
				$trip_col="cash";
				$dest_col="CASH_ADV";
			}
			else if($pay_mode=='CHEQUE')
			{
				$trans_id_Qry = GetTransId("CHQ",$conn,"cheque");
				if(!$trans_id_Qry || $trans_id_Qry==0 || $trans_id_Qry=="")
				{
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
		
				$trans_id_new = $trans_id_Qry;
				
				$insert_diary_vou = Qry($conn,"INSERT INTO dairy.cheque (trip_id,trans_id,tno,vou_no,amount,cheq_no,bank,date,
				narration,branch,timestamp) VALUES ('$trip_id','$trans_id_new','$tno','$frno','$amount','$chq_no','$chq_bank','$today',
				'$narration','$branch','$timestamp')");
				
				if(!$insert_diary_vou){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$v_type1="CHEQUE";
				$trip_col="cheque";
				$dest_col="CHEQUE_ADV";
			}
			else
			{
				$trans_id_Qry = GetTransId("CHQ",$conn,"cheque");
				if(!$trans_id_Qry || $trans_id_Qry==0 || $trans_id_Qry=="")
				{
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
		
				$trans_id_new = $trans_id_Qry;

				$insert_diary_vou = Qry($conn,"INSERT INTO dairy.rtgs (trip_id,trans_id,tno,vou_no,amount,driver_code,acname,acno,
				bank,ifsc,date,narration,branch,timestamp) VALUES ('$trip_id','$trans_id_new','$tno','$frno','$amount','$driver_code',
				'$ac_holder','$ac_no','$bank_name','$ifsc','$today','$narration','$branch','$timestamp')");
				
				if(!$insert_diary_vou){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$v_type1="RTGS";
				$trip_col="rtgs";
				$dest_col="RTGS_ADV";
			}
			
			if(AffectedRows($conn)==0)
			{
				$flag = false;
				errorLog("Unable to insert voucher.",$conn,$page_name,__LINE__);
			}
			
			if(sprintf("%.2f",$row['amt'])!=sprintf("%.2f",$amount))
			{
				$sel_id_driver_book = Qry($conn,"SELECT id,credit,balance FROM dairy.driver_book WHERE trans_id='$trans_id_db'");
				if(!$sel_id_driver_book){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(numRows($sel_id_driver_book)==0)
				{
					$flag = false;
					errorLog("Unable to Fetch Driver Book Data !",$conn,$page_name,__LINE__);
				}
				
				$row_driver_book_id=fetchArray($sel_id_driver_book);
				$d_book_id=$row_driver_book_id['id'];
				$new_bal_driver_book = ($row_driver_book_id['balance'] - ($diff));
				
				$update_driver_book = Qry($conn,"UPDATE dairy.driver_book SET trans_id='$trans_id_new',desct='$dest_col',
				credit='$amount',balance='$new_bal_driver_book' WHERE id='$d_book_id'");
				
				if(!$update_driver_book){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(AffectedRows($conn)==0)
				{
					$flag = false;
					errorLog("Unable to update new vouchers trans id.",$conn,$page_name,__LINE__);
				}
				
				$update_driver_book_all = Qry($conn,"UPDATE dairy.driver_book SET balance=balance+'$diff' WHERE 
				id>'$d_book_id' AND driver_code='$driver_code' AND tno='$tno'");
				if(!$update_driver_book_all){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				$update_driver_balance = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+'$diff' WHERE 
				code='$driver_code' AND down=0 ORDER BY id DESC LIMIT 1");
				if(!$update_driver_balance){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(AffectedRows($conn)==0)
				{
					$flag = false;
					errorLog("UNABLE TO UPDATE DRIVER NEW BALANCE",$conn,$page_name,__LINE__);
				}
			}
			else
			{
				$update_driver_book = Qry($conn,"UPDATE dairy.driver_book SET trans_id='$trans_id_new',desct='$dest_col' WHERE 
				trans_id='$trans_id_db'");
				if(!$update_driver_book){
					$flag = false;
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
				
				if(AffectedRows($conn)==0)
				{
					$flag = false;
					errorLog("UNABLE TO INSERT NEW TRANS ID",$conn,$page_name,__LINE__);
				}
			}
			
			$update_trip_amount_add = Qry($conn,"UPDATE dairy.trip SET `$trip_col`=`$trip_col`+'$amount' WHERE 
			id='$trip_id'");
			if(!$update_trip_amount_add){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	
	$update_today_data2=Qry($conn,"UPDATE today_data SET truck_vou=truck_vou+1,truck_vou_amount=truck_vou_amount+'$amount' WHERE 
	branch='$branch' AND date='$today'");
	if(!$update_today_data2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_voucher_new = Qry($conn,"UPDATE mk_tdv SET newdate='$vou_date',amt='$amount',mode='$pay_mode',chq_no='$chq_no',
	chq_bank='$chq_bank',ac_name='$ac_holder',ac_no='$ac_no',bank='$bank_name',ifsc='$ifsc',pan='$pan_no',
	dest='$narration',colset_d='' WHERE id='$id'");
	
	if(!$update_voucher_new){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

// $new_data="NEW DATA :- Amount : $amount, PaymentMode : $pay_mode, 
// VouDate : $vou_date, AcDetails : $ac_holder, $ac_no, $bank_name, $ifsc, $pan_no, Narration : $narration";

$update_log=Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) 
VALUES ('$frno','Truck_Voucher','Vou_update','$update_log','$branch','ADMIN','$timestamp')");

if(!$update_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Updated Successfully !');
		document.getElementById('button2').click();
		document.getElementById('req_close_button').click();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request !');
		$('#loadicon').hide();
	</script>";
	// Redirect("Error While Processing Request.","./request_market_truck.php");
	exit();
}
?>