<?php
require_once("./connect.php");

$frno=escapeString($conn,strtoupper($_POST['frno']));
$lrno=escapeString($conn,strtoupper($_POST['lrno']));
$type=escapeString($conn,strtoupper($_POST['type']));
$pod_id=escapeString($conn,strtoupper($_POST['pod_id']));
$timestamp =  date("Y-m-d H:i:s");

echo "<script>
	$('#btn_remove_$frno_$lrno').attr('disabled',true);
</script>";

if($type=='MARKET')
{
	$chk_fm_balance = Qry($conn,"SELECT paidto,branch FROM freight_form WHERE frno = '$frno'");
	if(!$chk_fm_balance){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}

	$row_fm = fetchArray($chk_fm_balance);

	$branch = $row_fm['branch'];
	
	if($row_fm['paidto']!='')
	{
		echo "<script>
			alert('Freight memo balance paid. Can\'t reset pod copy !');
			$('#loadicon').hide();
		</script>";
		exit();
	}

StartCommit($conn);
$flag = true;		
	
$error_msg="";
	
	$delete_pod_record = Qry($conn,"DELETE FROM rcv_pod WHERE frno='$frno' AND lrno='$lrno'");
	
	if(!$delete_pod_record){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(AffectedRows($conn)==0)
	{
		$flag = false;
		$error_msg="POD not found..";
	}
	
	$delete_pod_date = Qry($conn,"UPDATE freight_form_lr SET market_pod_date='' WHERE frno='$frno' AND lrno='$lrno'");
	
	if(!$delete_pod_date){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_pending_lr_list = Qry($conn,"INSERT INTO _pending_lr_list(vou_id,vou_no,bid,oid,branch,lr_date,create_date,
	lrno,timestamp) SELECT f.id,f.frno,f2.bid,f2.oid,f.branch,f.date,f.create_date,f.lrno,f.timestamp FROM freight_form_lr AS f 
	LEFT OUTER JOIN freight_form AS f2 ON f2.frno=f.frno 
    WHERE f.frno='$frno' AND f.lrno='$lrno'");
	
	if(!$insert_pending_lr_list){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_count = Qry($conn,"UPDATE _pending_lr SET lr_pod_pending=lr_pod_pending+1 WHERE branch='$branch'");
	
	if(!$update_count){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp,branch_user) VALUES 
	('$frno','$lrno','POD_REMOVED','','$branch','ADMIN','$timestamp','ADMIN')");

	if(!$insert_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		echo "<script> 	
				alert('POD removed successfully !');
				$('#loadicon').hide();	
			</script>";
		exit();
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		
		if($error_msg!='')
		{
			echo "<script> 	
				alert('$error_msg !');
				$('#loadicon').hide();	
			</script>";
		}
		else
		{
			echo "<script> 	
				alert('Error While Processing Request !');
				$('#loadicon').hide();	
			</script>";
		}
		exit();
		
	}
}	
else if($type=='OWN')
{
	echo "<script>
			alert('Inactive !');
			$('#loadicon').hide();
		</script>";
	exit();
	
	$chk_trip_no = Qry($conn,"SELECT trip_no FROM rcv_pod WHERE id='$pod_id'");
	if(!$chk_trip_no){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}

	$row_trip = fetchArray($chk_trip_no);

	$chk_running_trip = Qry($conn,"SELECT trip_no FROM dairy.trip WHERE trip_no='$row_trip[trip_no]");
	if(!$chk_running_trip){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($chk_running_trip)==0)
	{
		echo "<script>
			alert('Hisab taken. Can\'t reset pod copy !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
else
{
	echo "<script>
			alert('Invalid vehicle type !');
			$('#loadicon').hide();
		</script>";
	exit();
}

?>