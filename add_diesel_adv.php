<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s"); 

$frno=mysqli_real_escape_string($conn,strtoupper($_POST['frno']));
$freight=mysqli_real_escape_string($conn,strtoupper($_POST['freight']));
$advance_to=mysqli_real_escape_string($conn,strtoupper($_POST['advance_to']));
$total_adv=mysqli_real_escape_string($conn,strtoupper($_POST['total_adv']));
$diesel_adv=mysqli_real_escape_string($conn,strtoupper($_POST['diesel_adv']));
$balance=mysqli_real_escape_string($conn,strtoupper($_POST['balance']));
$balance_to=mysqli_real_escape_string($conn,strtoupper($_POST['balance_to']));
$branch_diesel=mysqli_real_escape_string($conn,strtoupper($_POST['branch_diesel']));

if($frno=='')
{
	echo "<script type='text/javascript'>
			alert('Unable to fetch Data.');
			$('#loadicon').hide();
		</script>";
		exit();
}

if($freight<=0)
{
	echo "<script type='text/javascript'>
			alert('Freight Amount is Zero !');
			$('#loadicon').hide();
		</script>";
		exit();
}

if($advance_to=='')
{
	echo "<script type='text/javascript'>
			alert('Freight Memo Advance Pending !');
			$('#loadicon').hide();
		</script>";
		exit();
}

if($advance_to=='NULL' || $total_adv==0)
{
	echo "<script type='text/javascript'>
			alert('Zero Advance Paid. Reset Freight Memo Advance !');
			$('#loadicon').hide();
		</script>";
		exit();
}

if($balance_to!='')
{
	echo "<script type='text/javascript'>
			alert('Freight Memo Balance Paid. Reset Freight Memo Balance First !');
			$('#loadicon').hide();
		</script>";
		exit();
}

$amount=mysqli_real_escape_string($conn,$_POST['amount']);

if(($balance-$amount)<0)
{
	echo "<script type='text/javascript'>
			alert('Diesel Amount is grater than Balance Amount !');
			$('#loadicon').hide();
		</script>";
	exit();
}

if(($amount+$total_adv)>$freight)
{
	echo "<script type='text/javascript'>
			alert('Total Advance is grater than Freight Amount !');
			$('#loadicon').hide();
		</script>";
	exit();
}

if($balance-($amount+$total_adv)<0)
{
	echo "<script type='text/javascript'>
			alert('Balance becomes Less than Zero !');
			$('#loadicon').hide();
		</script>";
	exit();
}

$pump_card=mysqli_real_escape_string($conn,strtoupper($_POST['pump_card']));

$card_type=mysqli_real_escape_string($conn,strtoupper($_POST['card_type']));

if($pump_card=='CARD' || $pump_card=='OTP')
{
$card=mysqli_real_escape_string($conn,strtoupper($_POST['card_no']));
$type=$pump_card;
$rate=0;
$qty=0;
}
else
{
$card=mysqli_real_escape_string($conn,strtoupper($_POST['pump_name']));	
$card=strtok($card,'-');
$type='PUMP';
$rate=$_POST['rate'];
$qty=$_POST['qty'];

$chk_pump=mysqli_query($conn,"SELECT id FROM diesel_pump WHERE code='$card' AND branch='$branch_diesel'");
if(mysqli_num_rows($chk_pump)==0)
{
	echo "<script type='text/javascript'>
			alert('ERROR : Selected PUMP not belongs to Branch : $branch_diesel !');
			$('#loadicon').hide();
		</script>";
	exit();
}
}

$fuel_company=mysqli_real_escape_string($conn,strtoupper($_POST['fuel_company']));
$date=date("Y-m-d");

$delete = mysqli_query($conn,"DELETE FROM diesel_sample WHERE date!='$date'");
if(!$delete)
{
	echo mysqli_error($conn);
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

if(($type=='CARD' || $type=='OTP') AND $fuel_company=='BPCL')
{
	if($card_type=='')
	{
		echo "<script type='text/javascript'>
			alert('Something went wrong Please Try Again !');
			document.getElementById('AddDieselForm').reset();
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else if($card_type=='RRPL')
	{
		$card=$card;
	}
	else
	{
		$card=$card_type."".$card;
	}
}
else
{
	$card=$card;
}

$select_lrno=mysqli_query($conn,"SELECT id,lrno,actualf,totalf,weight,advamt,cash,cheque,rtgs FROM freight_form_lr WHERE frno='$frno'");
if(!$select_lrno)
{
	echo mysqli_error($conn);
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

$num_of_lr=mysqli_num_rows($select_lrno);

if($num_of_lr==0)
{
	echo "<script type='text/javascript'>
			alert('NO LR found !');
			$('#loadicon').hide();
		</script>";
		exit();
}

$select_data=mysqli_query($conn,"SELECT branch,company,truck_no,newdate,totalf,cashadv,chqadv,rtgsneftamt FROM freight_form 
WHERE frno='$frno'");

if(!$select_data)
{
	echo mysqli_error($conn);
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_fm = mysqli_fetch_array($select_data);

$new_diesel=$amount+$diesel_adv;

if($num_of_lr==1)
{
$data_row=mysqli_fetch_array($select_lrno);
$db_lr=$data_row['lrno'];
	
$db_advance=$row_fm['cashadv']+($amount+$diesel_adv)+$row_fm['chqadv']+$row_fm['rtgsneftamt'];
$db_balance=$row_fm['totalf']-$db_advance;

$update_data=mysqli_query($conn,"UPDATE freight_form_lr SET advamt='$db_advance',diesel='$new_diesel',balamt='$db_balance' 
WHERE frno='$frno'");

if(!$update_data)
{
	echo mysqli_error($conn);
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}
}
else
{
	while($data_row=mysqli_fetch_array($select_lrno))
	{
		$db_lr=$data_row['lrno'];
		$lr_actualf=$data_row['actualf'];
		$lr_id=$data_row['id'];
	//id,lrno,actualf,weight			
	
	$x=($lr_actualf*100)/$freight;
	
	$cash_db=$data_row['cash'];
	$diesel_db=sprintf("%.2f",((($amount+$diesel_adv)*$x)/100));
	$cheq_db=$data_row['cheque'];
	$rtgs_db=$data_row['rtgs'];
	
	$advance_db=sprintf("%.2f",($cash_db+$cheq_db+$diesel_db+$rtgs_db));
	$balance_db=sprintf("%.2f",($data_row['totalf']-$advance_db));
//balance
	$update_data=mysqli_query($conn,"UPDATE freight_form_lr SET advamt='$advance_db',cash='$cash_db',cheque='$cheq_db',diesel='$diesel_db',
	rtgs='$rtgs_db',balamt='$balance_db' WHERE id='$lr_id' AND frno='$frno'");
	}

	$check_sum=mysqli_query($conn,"SELECT SUM(diesel) as diesel_lr FROM freight_form_lr WHERE frno='$frno'");

	if(!$check_sum)
	{
		echo mysqli_error($conn);
		exit();
	}

	$row_sum=mysqli_fetch_array($check_sum);

	if(($amount+$diesel_adv)!=$row_sum['diesel_lr'])
	{
		$diesel_diff=sprintf("%.2f",(($amount+$diesel_adv)-$row_sum['diesel_lr']));
	}
	else
	{
		$diesel_diff=0;
	}

	$update_data_lr=mysqli_query($conn,"UPDATE freight_form_lr SET diesel=diesel+'$diesel_diff' WHERE frno='$frno' ORDER BY 
	actualf DESC, id DESC LIMIT 1");

	if(!$update_data_lr)
	{
		echo mysqli_error($conn);
		echo "<script>
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$select_last=mysqli_query($conn,"SELECT totalf,cash,cheque,diesel,rtgs FROM freight_form_lr WHERE frno='$frno' ORDER 
	BY actualf DESC, id DESC LIMIT 1");

	if(!$select_last)
	{
		echo mysqli_error($conn);
		echo "<script>
			$('#loadicon').hide();
		</script>";
		exit();
	}

	$row_select=mysqli_fetch_array($select_last);

	$advance_new=sprintf("%.2f",($row_select['cash']+$row_select['cheque']+$row_select['diesel']+$row_select['rtgs']));
	$balance_new=sprintf("%.2f",($row_select['totalf']-$advance_new));

	$update_data_lr2=mysqli_query($conn,"UPDATE freight_form_lr SET advamt='$advance_new',balamt='$balance_new' WHERE frno='$frno' 
	ORDER BY actualf DESC, id DESC LIMIT 1");

	if(!$update_data_lr2)
	{
		echo mysqli_error($conn);
		echo "<script>
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

	$update_fm = mysqli_query($conn,"UPDATE freight_form SET totaladv=totaladv+'$amount',disadv=disadv+'$amount',baladv=baladv-'$amount',
	colset_d_adv=''	WHERE frno='$frno'");
	
	if(!$update_fm)
	{
		echo mysqli_error($conn);
		echo "<script>
			$('#loadicon').hide();
		</script>";
		exit();
	}

$insert_diesel=mysqli_query($conn,"INSERT INTO diesel_fm (branch,fno,com,qty,rate,disamt,tno,lrno,type,dsl_by,dcard,dcom,dsl_nrr,pay_date,fm_date,
timestamp) VALUES ('$row_fm[branch]','$frno','$row_fm[company]','$qty','$rate','$amount','$row_fm[truck_no]','$db_lr','ADVANCE','$type',
'$card','$fuel_company','$fuel_company-$card Rs: $amount/-','$date','$row_fm[newdate]','$timestamp')");
	
if(!$insert_diesel)
{
	echo mysqli_error($conn);
	echo "<script>
		$('#loadicon').hide();
	</script>";
	exit();
}

$update_log=mysqli_query($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
	('$frno','FREIGHT_MEMO','DIESEL_ADDED','NEW DIESEL REQ GENERATED $fuel_company-$card Rs: $amount/-','','ADMIN','$timestamp')");
	
echo "<script>
		alert('Diesel Entry Success.');
		document.getElementById('close_diesel_button').click();
		$('#AddDieselForm')[0].reset();
		Fetch('$frno');
	</script>";
	exit();
?>