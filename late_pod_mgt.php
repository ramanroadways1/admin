<?php
require_once("./connect.php");

$chk_active_func = Qry($conn,"SELECT func_value FROM _functions WHERE func_type='LATE_POD'");

if(!$chk_active_func){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_func = fetchArray($chk_active_func);
$func_value=$row_func['func_value'];
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<style>
  .slow .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Late POD Functions:</h3>
              </div>
 
<div class="card-body">
	<div class="row">		
		<div class="col-md-12">
		<input type="checkbox" id="button_check" checked data-toggle="toggle" 
		<?php if($func_value=='DAY') { echo "data-on='Day wise' data-off='Fix Charges' data-onstyle='success' data-offstyle='danger'"; } else { echo "data-on='Fix Charges' data-off='Day wise' data-onstyle='danger' data-offstyle='success'"; }?> data-style="slow" />
		</div>
	</div>
	
	<div class="row">	
		<div id="load_tab_result" class="col-md-12 table-responsive" style="overflow:auto">
		</div>
	</div>
</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	</div>
			
		</div>
		
		  <div id="func_result2"></div>
		  <div id="func_result33"></div>
		  
        </div>
      </div>
    </section>
  </div>
    

<script>
$("#button_check").change(function(){
    if($(this).prop("checked") == true){
		<?php
		if($func_value=='DAY'){
		?>
		LoadData('DAY');
		<?php		
		}
		else
		{
		?>
		LoadData('FIX');
		<?php			
		}
		?>
	}
	else{
		<?php
		if($func_value=='DAY'){
		?>
		LoadData('FIX');
		<?php		
		}
		else
		{
		?>
		LoadData('DAY');
		<?php			
		}
		?>
	}
});

function AddRule(type_of)
{
	var days_from = $('#days_from_new').val();
	var days_to = $('#days_till_new').val();
	var amount = $('#amount_new').val();
	
	if(days_from==0 || days_from=="")
	{
		alert('Invalid value for days from !!');
	}
	else if(days_to==0 || days_to=="")
	{
		alert('Invalid value for days till !!');
	}
	else if(amount==0 || amount=="")
	{
		alert('Invalid deduction amount !!');
	}
	else
	{
		if(type_of=='FIX')
		{
			var amt_from_new = $('#amt_from_new').val();
			var amt_to_new = $('#amt_to_new').val();
	
			if(amt_from_new==0 || amt_from_new=="")
			{
				alert('Invalid value for amount from !!');
			}
			else if(amt_to_new==0 || amt_to_new=="")
			{
				alert('Invalid value for amount to !!');
			}
			else
			{
				$('#add_rule_btn').attr('disabled',true);
				$('#loadicon').show();
				jQuery.ajax({
					url: "late_pod_add_new_rule.php",
					data: 'days_from=' + days_from + '&days_to=' + days_to + '&amount=' + amount + '&type_of=' + type_of + '&amt_from=' + amt_from_new + '&amt_to=' + amt_to_new,
					type: "POST",
					success: function(data) {
						$("#func_result33").html(data);
					},
					error: function() {}
				});
			}
		}
		else
		{
			$('#add_rule_btn').attr('disabled',true);
			$('#loadicon').show();
			jQuery.ajax({
				url: "late_pod_add_new_rule.php",
				data: 'days_from=' + days_from + '&days_to=' + days_to + '&amount=' + amount + '&type_of=' + type_of,
				type: "POST",
				success: function(data) {
					$("#func_result33").html(data);
				},
				error: function() {}
			});
		}
	}
}	

function Update(id,type_of)
{
	$('#update_btn_'+id).attr('disabled',true);
	var days_from = $('#days_from_'+id).val();
	var days_to = $('#days_to_'+id).val();
	var amount = $('#charges_'+id).val();
	
	if(type_of=='FIX')
	{
		var amount_from = $('#amt_from_'+id).val();
		var amount_to = $('#amt_to_'+id).val();
	}
	else
	{
		var amount_from = "";
		var amount_to = "";
	}

	$('#loadicon').show();
	jQuery.ajax({
		url: "late_pod_func_update.php",
		data: 'days_from=' + days_from + '&days_to=' + days_to + '&amount=' + amount + '&id=' + id + '&amount_from=' + amount_from + '&amount_to=' + amount_to + '&type_of=' + type_of,
		type: "POST",
		success: function(data) {
			$("#func_result33").html(data);
		},
		error: function() {}
	});
}	
 
function DltFunc(id,type_of)
{
	$('#dlt_btn_'+id).attr('disabled',true);
	$('#loadicon').show();
	jQuery.ajax({
		url: "late_pod_func_delete.php",
		data: 'id=' + id + '&type_of=' + type_of,
		type: "POST",
		success: function(data) {
			$("#func_result33").html(data);
		},
		error: function() {}
	});
}	

</script> 
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>

<script>
$('#example').dataTable( {
    paging: true,
    searching: true,
	"autoWidth": true,
} );

function LoadData(type_of)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "late_pod_change_function.php",
			data: 'type_of=' + type_of,
			type: "POST",
			success: function(data) {
				$("#load_tab_result").html(data);
			},
			error: function() {}
		}); 
}
LoadData('<?php echo $func_value; ?>');
</script>