<?php
require_once("./connect.php");

$date = date("Y-m-d"); 
$timestamp=date("Y-m-d H:i:s");
 
$lrno=escapeString($conn,strtoupper($_POST['lrno']));
$plant=escapeString($conn,strtoupper($_POST['plant'])); 
$lr_id=escapeString($conn,strtoupper($_POST['lr_id'])); 

$get_lr = Qry($conn,"SELECT lrno,plant FROM lr_check WHERE id='$lr_id'");

if(!$get_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_lr)==0){
	echo "<script>
		alert('LR not found !');
		window.location.href='./lr_entry_update_plant.php';
	</script>";
	exit();
}

$row_lr = fetchArray($get_lr);
	
if($row_lr['lrno']!=$lrno)
{
	echo "<script type='text/javascript'>
		alert('LR number not verified !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";	
	exit();
}

$update_log=array();
$update_Qry=array();

if($plant!=$row_lr['plant'])
{
	$update_log[]="plant : $row_lr[plant] to $plant";
	$update_Qry[]="plant='$plant'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry_temp = Qry($conn,"UPDATE lr_check SET $update_Qry WHERE id='$lr_id'");

if(!$update_Qry_temp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Record not updated. LR no: $lrno.",$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$lr_id','$lrno','LR_UPDATE','PLANT_UPDATE_PUNE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Plant Updated Successfully !');
		window.location.href='./lr_entry_update_plant.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request.!');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}	
?>