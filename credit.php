<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Credits :</h3>
              </div>
              
	<div class="card-body">
	
<div id="lr_result"></div>
		
<script type="text/javascript">
$(function() {
    $("#tno").autocomplete({
      source: 'autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			 $(event.target).focus();
			 $("#button2").attr("disabled",true);
		alert('Truck Number doest not exists.');
        }
    }, 
    focus: function (event, ui) {
        $("#button2").attr("disabled",false);
		return false;
    }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#CreditForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
// $("#button2").attr("disabled", true);
	$.ajax({
	url: "./fetch_credit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_main").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="CreditForm">

	<div class="row">	
		
		<div class="col-md-3">	
               <div class="form-group">
                  <label>Credit Type <font color="red"><sup>*</sup></font></label>
                  <select name="SearchBy" class="form-control" required>
					<option value="">---Select---</option>
					<option value="CLAIM">Claim</option>
					<option value="FREIGHT">Freight</option>
					<option value="HO">Head-Office</option>
					<option value="BANK_WDL">Bank Withdrawal</option>
					<option value="OTHER">Others</option>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Select Branch <font color="red"><sup>*</sup></font></label>
                  <select name="branch" class="form-control" required>
					<option value="">---Select---</option>
					<?php
					$fetch_branch = Qry($conn,"SELECT username from user WHERE role='2' order by username asc");
					if(!$fetch_branch){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while processing Request","./");
						exit();
					}
					if(numRows($fetch_branch)>0)
					{
						while($row_branch=fetchArray($fetch_branch))
						{
							echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
						}
					}
					?>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Days Ago <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" name="range" id="date_range">
					<option value="-0 days">Today's</option>
					<option value="-1 days">Last 2 days</option>
					<option value="-4 days">Last 5 days</option>
					<option value="-6 days">Last 7 days</option>
					<option value="-9 days">Last 10 days</option>
					<option value="-14 days">Last 15 days</option>
					<option value="-29 days">Last 30 days</option>
					<option value="-59 days">Last 60 days</option>
					<option value="-89 days">Last 90 days</option>
					<option value="-119 days">Last 120 days</option>
					<option value="FULL">FULL REPORT</option>
				</select>
              </div>
			</div>
			
			<div id="button_div" class="col-md-2">
				<label></label>
				<br />
				<button type="submit" id="button2" class="btn pull-right btn-danger">Check !</button>
			</div>
			
		</div>
		
</form>	
		
		<div class="row">	
			<div class="col-md-12 table-responsive" style="overflow:auto">	
			
				<div id="result_main"></div>
			
			</div>
		</div>
	</div>
	
	<div id="result_main2"></div>
				
           </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script>  
function Edit(id,section)
{
	$('#modal_frno').val(id);
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "load_credit.php",
	data: 'id=' + id + '&section=' + section,
	type: "POST",
	success: function(data) {
		$("#result_modal_data").html(data);
		document.getElementById("req_modal_button").click();
	},
	error: function() {}
	});
}

function Delete(id)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "delete_credit.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data) {
	$("#result_main2").html(data);
	$("#loadicon").hide();
	},
	error: function() {}
	});
}  
</script>  

<a style="display:none1" id="req_modal_button" data-toggle="modal" data-target="#ReqModal"></a>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#req_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_credit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_req_modal").html(data);
		$("#req_update_button").attr("disabled", false);
		$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script> 

<div class="modal fade" id="ReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
		
	<div class="modal-header bg-primary">
		Update Credit :
    </div>
	
		<form id="UpdateForm" autocomplete="off"> 
			<div class="modal-body">
			
				<div id="result_req_modal"></div>
				
			<input type="hidden" name="frno" id="modal_frno">
				
			<div class="row">
			
				<div class="col-md-12 table-responsive" style="overflow:auto">
					<div id="result_modal_data"></div>
				</div>
				
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" id="req_close_button" onclick="document.getElementById('menu_button2').click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="req_update_button" class="btn btn-primary">Update Final</button>
	</div>
		</form>
		</div>	
     </div>
 </div>	
  <!-- MODAL -->
 
<?php include ("./footer.php"); ?>

</div>
</body>
</html>