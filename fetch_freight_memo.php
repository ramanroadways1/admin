<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));

$qry=Qry($conn,"SELECT f.id as fm_id,f.frno,f.truck_no,f.branch,f.bid,f.did,f.oid,f.from1,f.to1,f.from_id,f.to_id,
f.weight,f.company,f.actualf,f.newtds,f.dsl_inc,f.gps,f.adv_claim,f.newother,f.tds,f.totalf,f.totaladv,f.ptob,
f.adv_date,f.pto_adv_name,f.adv_pan,f.cashadv,f.chqadv,f.chqno,f.disadv,f.rtgsneftamt,f.rtgs_adv,f.narre,f.baladv,f.unloadd,
f.detention,f.gps_rent,f.gps_device_charge,f.bal_tds,f.otherfr,f.claim,f.late_pod,f.totalbal,f.paidto,
f.bal_date,f.pto_bal_name,f.bal_pan,f.paycash,f.paycheq,f.paycheqno,f.paydsl,f.newrtgsamt,f.rtgs_bal,f.narra,f.timestamp,
f.colset_d_adv,f.colset_d_bal,f.branch_bal,b.name as broker,b.pan as broker_pan,b.mo1 as broker_mobile,b.full as broker_addr,
o.name as owner,o.pan as owner_pan,o.wheeler as wheeler,o.mo1 as owner_mobile,o.full as owner_addr,d.name as driver,
d.pan as driver_lic,d.mo1 as driver_mobile,d.full as driver_addr,e1.name as fm_user,e2.name as adv_branch_user,e2.name as bal_branch_user 
FROM freight_form as f
LEFT OUTER JOIN mk_broker as b ON b.id=f.bid
LEFT OUTER JOIN mk_driver as d ON d.id=f.did
LEFT OUTER JOIN mk_truck as o ON o.id=f.oid
LEFT OUTER JOIN emp_attendance as e1 ON e1.code=f.branch_user
LEFT OUTER JOIN emp_attendance as e2 ON e2.code=f.adv_branch_user
LEFT OUTER JOIN emp_attendance as e3 ON e3.code=f.bal_branch_user
WHERE f.frno='$frno'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows=numRows($qry);

if($numrows==0)
{
	echo "<script>
		alert('Invalid Vou No entered !');
		$('#loadicon').hide();
		$('#frno').attr('disabled',false);
	</script>";	
	exit();
}

$qry_LR = Qry($conn,"SELECT l.id,l.lrno,l.date,l.fstation,l.tstation,l.consignor,l.con1_id,l.consignee,l.wt12,l.weight,l.fix_rate,
l.ratepmt,l.actualf,lr.item_id 
FROM freight_form_lr AS l 
LEFT OUTER JOIN lr_sample AS lr ON lr.lrno=l.lrno 
WHERE l.frno='$frno'");

if(!$qry_LR){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row=fetchArray($qry);
?>
<script>
function AddMoreLRModal(fm_id,frno)
{
	$('#btn_modal_add_more_LR')[0].click();
	$('#add_more_lr_fm_id').val(fm_id);
	$('#add_more_lr_vou_no').val(frno);
}

function AddMoreLRModalCross(fm_id,frno)
{
	$('#btn_modal_add_crossing_LR')[0].click();
}

function GetCrossingInfo()
{
	var lrno = $('#lrno_crossing_1').val();
	
	if(lrno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./get_crossing_info_by_lrno.php",
			data: 'lrno=' + lrno,
			type: "POST",
			success: function(data){
				$("#result_CrossingLRForm").html(data);
			},
		error: function() {}
	 });
	}
	else
	{
		$('#add_btn_crossing_lr').atrr('disabled',true);
	}
}

function GetLRinfoToAdd()
{
	var lrno = $('#add_lr_modal_lrno').val();
	var truck_no = '<?php echo $row["truck_no"]; ?>';
	var branch = '<?php echo $row["branch"]; ?>';
	var fm_id = '<?php echo $row["fm_id"]; ?>';
	var rate = $('#rate_amount_'+fm_id).val();
	var fix_rate = $('#fix_rate_'+fm_id).val();
	
	if(lrno!='')
	{
		$("#loadicon").show();
		jQuery.ajax({
			url: "./get_lr_info_by_lrno.php",
			data: 'lrno=' + lrno + '&truck_no=' + truck_no + '&branch=' + branch + '&rate=' + rate + '&fix_rate=' + fix_rate,
			type: "POST",
			success: function(data){
				$("#result_AddmoreLRForm").html(data);
			},
		error: function() {}
	 });
	}
	else
	{
		$('#add_btn_add_more_lr').atrr('disabled',true);
	}
}
</script>

<table id="bal_diesel_table" class="table table-bordered" style="font-size:12px;">
	<tr>
       <th class="bg-info" style="font-size:13px;" colspan="12">Freight Memo LRs : <?php echo $frno; ?>
		<button style="display:none" type="button" class="pull-right btn btn-sm btn-warning" onclick="AddMoreLRModal('<?php echo $row['fm_id']; ?>','<?php echo $row['frno']; ?>')">Add more LRs</button>
		<button style="margin-right:10px;display:none" type="button" class="pull-right btn btn-sm btn-warning" onclick="AddMoreLRModalCross('<?php echo $row['fm_id']; ?>','<?php echo $row['frno']; ?>')">Add Crossing LR</button>
		</th>
	</tr>
	
	<tr>    
		<th>LR_No</th>
		<th>LR_Date</th>
		<th>From </th>
		<th>To</th>
		<th>Consignor & Consignee</th>
		<th>Act_Wt</th>
		<th>Chrg_Wt</th>
		<th>Rate/Fix</th>
		<th>Rate</th>
		<th>Freight</th>
		<th></th>
	</tr>
	
<?php
$act_weight_sum = 0;
$weight_sum = 0;
$freight_sum = 0;
$fix_rate_var_db = "";
$rate_var_db = 0;
$lrnos = array();
while($row_lr = fetchArray($qry_LR))
{
	$lrnos[] = "'".$row_lr['lrno']."'";
	$con1_id = $row_lr['con1_id'];
	$item_id = $row_lr['item_id'];
	$act_weight_sum = $act_weight_sum+$row_lr['wt12'];
	$weight_sum = $weight_sum+$row_lr['weight'];
	$freight_sum = $freight_sum+$row_lr['actualf'];
	
	$fix_rate_var_db = $row_lr['fix_rate'];
	$rate_var_db = $row_lr['ratepmt'];
	
	echo '<tr>
		<td>'.$row_lr['lrno'].'</td>
		<td>'.date("d/m/y",strtotime($row_lr['date'])).'</td>
		<td>'.$row_lr['fstation'].'</td>
		<td>'.$row_lr['tstation'].'</td>
		<td>'.$row_lr['consignor'].'<br>'.$row_lr['consignee'].'</td>
		<td>'.$row_lr['wt12'].'</td>
		<td>'.$row_lr['weight'].'</td>
		<td>'.$row_lr['fix_rate'].'</td>
		<td>'.$row_lr['ratepmt'].'</td>	
        <td>'.$row_lr['actualf'].'</td>
        <td>
			<button onclick="EditLR('.$row_lr['id'].')" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
		</td>
	</tr>';
}

$lrnos=implode(',', $lrnos);
echo '</table>';

echo "<input type='hidden' value='$rate_var_db' id='rate_amount_$row[fm_id]'>
	<input type='hidden' value='$fix_rate_var_db' id='fix_rate_$row[fm_id]'>";
	
// check eway bill free starts

$chkEwayBillFree = Qry($conn,"SELECT id FROM _eway_bill_free WHERE (branch='$row[branch]' || consignor='$con1_id' || lrno IN($lrnos)) 
AND status='1'");

if(!$chkEwayBillFree){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

if(numRows($chkEwayBillFree)>0 || $row['company']=='RAMAN_ROADWAYS')
{
	echo "<script>
		$('#EwayBillFlag').val('2');
		$('#ewb_no').attr('readonly',true);
		// $('#ewb_no').attr('onblur','');
	</script>";
}
else
{
	echo "<script>
		$('#EwayBillFlag').val('');
		$('#ewb_no').attr('readonly',false);
		// $('#ewb_no').attr('onblur','FetchEwayBill(this.value)');
	</script>";
}

// check eway bill free ends

echo "<script>
		$('#item_id_tno_change').val('$item_id');
		$('#con1_id_tno_change').val('$con1_id');
		$('#tno_fm').html('$row[truck_no]');
		$('#company_fm').html('$row[company]');
		$('#branch_fm').html('$row[branch]-$row[fm_user]');
		$('#owner_fm').html('$row[owner]');
		$('#owner_pan_fm').html('$row[owner_pan]');
		$('#weight_fm').html('$row[weight]');
		$('#broker_fm').html('$row[broker]');
		$('#broker_pan_fm').html('$row[broker_pan]');
		
		$('#broker_modal_id').val('$row[bid]');
		$('#broker_modal_name').val('$row[broker]');
		$('#broker_modal_pan').val('$row[broker_pan]');
		$('#broker_modal_mobile').val('$row[broker_mobile]');
		$('#broker_modal_addr').val('$row[broker_addr]');
		
		$('#owner_modal_tno').val('$row[truck_no]');
		$('#owner_modal_id').val('$row[oid]');
		$('#owner_modal_name').val('$row[owner]');
		$('#owner_modal_pan').val('$row[owner_pan]');
		$('#owner_modal_mobile').val('$row[owner_mobile]');
		$('#owner_modal_addr').val('$row[owner_addr]');
		$('#owner_modal_wheeler').val('$row[wheeler]');
		
		$('#driver_modal_id').val('$row[did]');
		$('#driver_modal_name').val('$row[driver]');
		$('#driver_modal_lic').val('$row[driver_lic]');
		$('#driver_modal_mobile').val('$row[driver_lic]');
		$('#driver_modal_addr').val('$row[driver_addr]');
		
		$('#from_fm').html('$row[from1]');
		$('#driver_fm').html('$row[driver]');
		$('#driver_lic_fm').html('$row[driver_lic]');
		$('#to_fm').html('$row[to1]');
		
		$('#actual_freight_fm').html('$row[actualf]');
		$('#loading_fm').html('$row[newtds]');
		$('#dsl_inc_fm').html('$row[dsl_inc]');
		$('#gps_fm').html('$row[gps]');
		$('#claim_fm').html('$row[adv_claim]');
		$('#other_fm').html('$row[newother]');
		$('#tds_fm').html('$row[tds]');
		$('#total_freight_fm').html('$row[totalf]');
		
		$('#cash_adv_fm').html('$row[cashadv]');
		$('#chq_adv_fm').html('$row[chqadv]');
		$('#chq_no_fm').html('$row[chqno]');
		$('#diesel_adv_fm').html('$row[disadv]');
		$('#rtgs_fm').html('$row[rtgsneftamt]');
		$('#adv_user_fm').html('$row[branch]-$row[adv_branch_user]');
		
		$('#total_adv_fm').html('$row[totaladv]');
		$('#balance_left_fm').html('$row[baladv]');
		$('#adv_to_fm').html('$row[ptob]');
		$('#adv_date_fm').html('$row[adv_date]');
		$('#adv_narration_fm').html('$row[narre]');
		
		$('#balance_fm').html('$row[baladv]');
		$('#unloading_fm').html('$row[unloadd]');
		$('#detention_fm').html('$row[detention]');
		$('#gps_rent_fm').html('$row[gps_rent]');
		$('#gps_device_fm').html('$row[gps_device_charge]');
		$('#tds_bal_fm').html('$row[bal_tds]');
		$('#other_bal_fm').html('$row[otherfr]');
		$('#claim_bal_fm').html('$row[claim]');
		$('#late_pod_fm').html('$row[late_pod]');
		
		$('#cash_bal_fm').html('$row[paycash]');
		$('#chq_bal_fm').html('$row[paycheq]');
		$('#chq_no_bal_fm').html('$row[paycheqno]');
		$('#diesel_bal_fm').html('$row[paydsl]');
		$('#rtgs_bal_fm').html('$row[newrtgsamt]');
		$('#bal_user_fm').html('$row[branch_bal]-$row[bal_branch_user]');
		
		$('#total_bal_fm').html('$row[totalbal]');
		$('#bal_to_fm').html('$row[paidto]');
		$('#bal_date_fm').html('$row[bal_date]');
		$('#bal_narration_fm').html('$row[narra]');
		
		$('#vou_number_block').val('$frno');
		$('#vou_id_block').val('$row[fm_id]');
		$('#weight_g').val('$weight_sum');
		$('#actual2').val('$freight_sum');
		$('#rate2').val(Math.round(Number($('#actual2').val())/Number($('#weight_g').val())).toFixed(2));
		$('#rate_freight_db_freight').val('$freight_sum');
		$('#rate_freight_db_weight').val('$weight_sum');
		$('#rate_freight_db_rate').val(Math.round(Number($('#actual2').val())/Number($('#weight_g').val())).toFixed(2));
		
		$('#fm_no_head').html('$frno');
		$('#main_div').show();
		$('#all_button_div').show();
		$('#div_lr_input').hide();
		$('#loadicon').hide();
	</script>";
	
if($row['ptob']!="")
{
	echo "<script>
		$('#adv_div').show();
	</script>";
}
else
{
	echo "<script>
		$('#adv_div').hide();
	</script>";
}

if($row['paidto']!="")
{
	echo "<script>
		$('#bal_div').show();
	</script>";
}
else
{
	echo "<script>
		$('#bal_div').hide();
	</script>";
}

if($row['disadv']>0)
{
	echo "<script>
		AdvDieselLoad('$frno');
	</script>";
}

if($row['paydsl']>0)
{
	echo "<script>
		BalDieselLoad('$frno');
	</script>";
}

if($row['rtgsneftamt']>0)
{
	echo "<script>
		AdvRtgsLoad('$frno','$row[totalf]','$row[totaladv]','$row[rtgsneftamt]','$row[baladv]');
	</script>";
}

if($row['newrtgsamt']>0)
{
	echo "<script>
		BalRtgsLoad('$frno','$row[totalf]','$row[totaladv]','$row[rtgsneftamt]','$row[baladv]');
	</script>";
}

if($row['totaladv']<=0 || $row['paidto']!='')
{
	echo "<script>
		$('#add_adv_dsl_modal_btn').attr('disabled',true);
	</script>";
}
?>

<?php include ("./_modal_add_more_lr.php"); ?>
<?php include ("./_modal_add_crossing_lr.php"); ?>
<?php include ("./_modal_diesel_adv.php"); ?>