<?php
require_once("./connect.php");

$date = date("Y-m-d"); 
$timestamp=date("Y-m-d H:i:s");
 
$lrno=escapeString($conn,strtoupper($_POST['lrno']));

$get_lr = Qry($conn,"SELECT id,lr_type,branch,con1_id,lr_id,crossing,cancel,break,diesel_req,download FROM lr_sample_pending 
WHERE lrno='$lrno'");

if(!$get_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_lr)==0){
	echo "<script>
		alert('LR not found !');
		window.location.href='./lr_entry.php';
	</script>";
	exit();
}

$row_lr = fetchArray($get_lr);

$lr_type = $row_lr['lr_type'];
$lr_branch = $row_lr['branch'];
$lr_id = $row_lr['id'];
$lr_id_main = $row_lr['lr_id'];

$chk_coal = Qry($conn,"SELECT z FROM user WHERE username='$lr_branch'");

if(!$chk_coal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numrows($chk_coal)==0)
{
	errorLog("Branch not found !",$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Branch not found !');
		window.location.href='./lr_entry.php';
	</script>";	
	exit();
}

$row_branch_chk = fetchArray($chk_coal);

if($row_branch_chk['z']=="1")
{
	$is_coal_branch = "YES";
	
	$chkEwayBill = Qry($conn,"SELECT eway_bill,done FROM ship.lr_entry WHERE lrno='$lrno'");
	if(!$chkEwayBill){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}

	if(numRows($chkEwayBill)==0)
	{
		echo "<script>
			alert('Invalid LR No');
			$('#loadicon').hide();
			$('#delete_button_$id').attr('disabled',false);
		</script>";
		exit();
	}
		
	$rowChk=fetchArray($chkEwayBill);
		
	if($rowChk['done']==1)
	{
		echo "<script>
			alert('LR Attached with Voucher.');
			$('#loadicon').hide();
		</script>";
		exit();
	}
		
	if($rowChk['eway_bill']==1)
	{
		echo "<script>
			alert('Eway Bill Generated for this LR. Cancel it First');
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
else
{
	$is_coal_branch = "NO";
}

$chk_open_lr = Qry($conn,"SELECT id FROM open_lr WHERE party_id='$row_lr[con1_id]'");

if(!$chk_open_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_open_lr)>0)
{
	$open_lr="1";
}
else
{
	$open_lr="0";
	
	$getBilty_Id = Qry($conn,"SELECT bilty_id FROM lr_check WHERE lrno='$lrno'");
	
	if(!$getBilty_Id){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($getBilty_Id)==0)
	{
		errorLog("LR not found. LR Number : $lrno.",$conn,$page_name,__LINE__);
		Redirect("LR not found.","./lr_entry.php");
		exit();
	}
	
	$bilty_id = fetchArray($getBilty_Id)['bilty_id'];
	
	if($bilty_id=="" || $bilty_id==0)
	{
		errorLog("Bilty Id not found. LR Number : $lrno.",$conn,$page_name,__LINE__);
		Redirect("Bilty Id not found.","./lr_entry.php");
		exit();
	}
}

if($row_lr['diesel_req']>0)
{
	echo "<script>
		alert('You can\'t Delete. Diesel request found. Delete it first !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_lr['break']>0)
{
	echo "<script>
		alert('You can\'t Delete. This is breaking LR. Delete all LR\'s pieces first !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_lr['download']>0)
{
	echo "<script>
		alert('You can\'t Delete. LR downloaded by head-office !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_lr['cancel']==1)
{
	echo "<script>
		alert('You can\'t Delete. LR marked as cancelled !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_lr['crossing']!='')
{
	echo "<script>
		alert('You can\'t Delete. LR Mapped with Freight memo or OLR !');
		$('#lr_sub').attr('disabled',false);
	</script>";
	exit();
}

$get_values = Qry($conn,"SELECT * FROM lr_sample_pending WHERE id='$row_lr[id]'");

if(!$get_values){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_values)==0){
	echo "<script>
		alert('LR not found !');
		window.location.href='./lr_entry.php';
	</script>";
	exit();
}

$log_record = array();

while($row_data = fetchArray($get_values))
{
    foreach($row_data as $key => $value)
    {
		$log_record[]=$key."->".$value;
    }
}

$log_record = implode(', ',$log_record); 

StartCommit($conn);
$flag = true;

if($open_lr=="0")
{
	$update_stock = Qry($conn,"UPDATE lr_bank SET stock_left=stock_left+1 WHERE id='$bilty_id'");
	if(!$update_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$get_ewb_no = Qry($conn,"SELECT id,ewb_no FROM _eway_bill_lr_wise WHERE lr_id='$lr_id_main'");
if(!$get_ewb_no){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_ewb_no)==0)
{
	$ewb_no = "";
}
else
{
	$row_ewb = fetchArray($get_ewb_no);
	$ewb_no = $row_ewb['ewb_no'];
}

if($ewb_no!='')
{
	$dlt_ewb_lr_wise=Qry($conn,"DELETE FROM _eway_bill_lr_wise WHERE lr_id='$lr_id_main'");
	if(!$dlt_ewb_lr_wise){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$dlt_ewb_val=Qry($conn,"DELETE FROM _eway_bill_validity WHERE lr_id='$lr_id_main'");
	if(!$dlt_ewb_val){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$copy_lr_data = Qry($conn,"INSERT INTO lr_sample_deleted(dlt_timestamp,deleted_branch,deleted_branch_user,lr_id,company,branch,branch_user,
lrno,ewb_no,lr_type,wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignor,consignor_before,consignee,con2_addr,
from_id,to_id,con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,
t_type,do_no,shipno,invno,item,item_id,item_name,plant,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing,
cancel,break,diesel_req,nrr,downloadid,downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus) 
SELECT '$timestamp','ADMIN','ADMIN',lr_id,company,branch,branch_user,lrno,'$ewb_no',lr_type,wheeler,date,create_date,truck_no,fstation,tstation,
dest_zone,consignor,consignor_before,consignee,con2_addr,from_id,to_id,con1_id,con2_id,zone_id,po_no,bank,freight_type,
sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,t_type,do_no,shipno,invno,item,item_id,item_name,plant,articles,goods_desc,
goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing,cancel,break,diesel_req,nrr,downloadid,downtime,billrate,billparty,
partyname,pod_rcv_date,download,billfreight,lrstatus FROM lr_sample_pending WHERE id='$lr_id'");

if(!$copy_lr_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_lr = Qry($conn,"DELETE FROM lr_sample_pending WHERE id='$lr_id'");
	
if(!$delete_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
}

$delete_lr_main = Qry($conn,"DELETE FROM lr_sample WHERE id='$lr_id_main'");
	
if(!$delete_lr_main){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$delete_lr_check = Qry($conn,"DELETE FROM lr_check WHERE lrno='$lrno'");
	
if(!$delete_lr_check){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($lr_type=='MARKET')
{
	$update_lr_count = Qry($conn,"UPDATE _pending_lr SET market_lr=market_lr-1 WHERE branch='$lr_branch'");
}
else
{
	$update_lr_count = Qry($conn,"UPDATE _pending_lr SET own_lr=own_lr-1 WHERE branch='$lr_branch'");
}

if(!$update_lr_count){
	$flag = false; 
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($is_coal_branch=='YES')
{
	$dlt_lr_entry = Qry($conn,"DELETE FROM ship.lr_entry WHERE lrno='$lrno' AND done!='1'");
	if(!$dlt_lr_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(AffectedRows($conn)==0)
	{
		$flag = false;
		errorLog("Unable to delete LR from lr_entry(shipLR). LRNo: $lrno.",$conn,$page_name,__LINE__);
	}
}	

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$lr_id_main','$lrno','LR_ENTRY','LR_DELETE','$log_record','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('LR : $lrno. Deleted Successfully !');
		window.location.href='./lr_entry.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./lr_entry.php");
	exit();
}	

?>