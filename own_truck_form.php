<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Own Truck Form : <span id="fm_no_head"></span></h3>
              </div>
              
	<div class="card-body">
	
<script>		
function FetchFm(frno)
{
	var search_by = $('#search_by').val();
	
	if(search_by=='')
	{
		alert('select search_by option first !');
	}
	else
	{
		var frno = frno.toUpperCase();
				
		if(frno=='')
		{
			alert('Enter Voucher number first !');
			$("#all_button_div").hide();
			$("#search_button_div").show();
		}
		else
		{
			$("#loadicon").show();
			$("#frno").attr('disabled',true);
			$("#lrno_find").attr('readonly',true);
			$("#search_by").attr('disabled',true);
			jQuery.ajax({
			url: "fetch_own_truck_form.php",
			data: 'frno=' + frno,
			type: "POST",
			success: function(data) {
				$("#load_lr_result").html(data);
			},
				error: function() {}
			});
		}	
	}	
}
</script>
	
<div class="row" id="div_lr_input">	
	
	<div class="col-md-3">	
        <div class="form-group">
            <label>Search By <font color="red"><sup>*</sup></font></label>
			<select name="search_by" id="search_by" class="form-control" required onchange="SearchBy(this.value)">
				<option value="">--select option--</option>
				<option value="LR">LR Number</option>
				<option value="FM">Vou Number</option>
			</select>
		</div>
	</div>
	
<script>
function SearchBy(elem)
{
	if(elem=='LR')
	{
		$('#lr_no_div').show();
		$('#fm_no_div').hide();
		$('#get_button').attr('disabled',true);
	}
	else if(elem=='FM')
	{
		$('#lr_no_div').hide();
		$('#fm_no_div').show();
		$('#get_button').attr('disabled',false);
	}
	else
	{
		$('#lr_no_div').hide();
		$('#fm_no_div').hide();
		$('#get_button').attr('disabled',true);
	}
}

function SearchLRNumber(lrno)
{
	if(lrno=='')
	{
		alert('enter LR number first !');
		$('#get_button').attr('disabled',true);
		$('#fm_no_div').hide();
		$('#lr_no_div').show();
	}
	else
	{
		$("#lrno_find").attr('readonly',true);
		$("#get_button").attr('readonly',true);
		$("#search_by").attr('disabled',true);
		$('#loadicon').show();
		jQuery.ajax({
		url: "fm_view_find_lrno.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#fm_no_div").html(data);
		},
			error: function() {}
		});
	}
}
</script>

	<div class="col-md-3" style="display:none" id="lr_no_div">	
        <div class="form-group">
           <label>LR Number <font color="red"><sup>*</sup></font></label>
           <input onblur="SearchLRNumber(this.value)" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" id="lrno_find">
        </div>
	</div>
			
	<div class="col-md-3" id="fm_no_div" style="display:none">	
        <div class="form-group">
            <label>Voucher No <font color="red"><sup>*</sup></font></label>
            <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" class="form-control" id="frno" name="frno">
        </div>
	</div>
			
	<div id="search_button_div" class="col-md-2">
		<label>&nbsp;</label>
		<br />
		<button type="button" id="get_button" disabled onclick="FetchFm($('#frno').val())" class="btn pull-right btn-danger">Get details !</button>
	</div>	
			
</div>

<div class="row" id="all_button_div" style="display:none">
	
	<div class="col-md-12">	
		<div class="form-group">
			<button type="button" onclick="AddMoreLR();" class="btn btn-sm btn-primary">Add More LR</button>
			<button type="button" onclick="ChangeTruckNo();" class="btn btn-sm btn-warning">Change Truck No</button>
			<button type="button" onclick="ChangeCreateDate();" class="btn btn-sm btn-success">Change Create Date</button>
			<button type="button" id="delete_form_button" onclick="DeleteForm();" class="btn btn-sm pull-right btn-danger">Delete OWN Truck Form</button>
		</div>	   
	</div>
		
	<div id="main_div" class="col-md-12 table-responsive" style="display:none;overflow:auto">	
			<div id="load_lr_result"></div>
	</div>
	
	<input type="hidden" id="truck_no_db">
	<input type="hidden" id="fm_id_find">
	
</div>
	<!--
	<div class="card-footer">
			<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>
    </div>
	-->
           </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script>  
function DeleteForm()
{
	var frno = $("#frno").val().toUpperCase();
	var vou_id = $("#fm_id_find").val();
	
	if(frno=='')
	{
		alert('Value is Empty');
		$("#main_div").hide();
		$("#main_div1").hide();
		$("#button_div").show();
	}
	else
	{
		$("#loadicon").show();
		$("#delete_form_button").attr('disabled',true);
		jQuery.ajax({
		url: "delete_own_truck_form.php",
		data: 'frno=' + frno + '&vou_id=' + vou_id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
			error: function() {}
		});
	}	
}
		
function ChangeTruckNo()
{
	var vou_no = $('#vou_id_block').val();
	var tno = $('#truck_no_db').val();
	
	if(vou_no=='' || tno=='')
	{
		alert('Unable to fetch Voucher Number or Truck Number !');
	}
	else
	{
		$('#frno_modal').val(vou_no);
		$('#tno_modal').val(tno);
		$('#TruckModal').modal();
	}
}  

function ChangeCreateDate()
{
	var vou_no = $('#vou_id_block').val();
	
	if(vou_no=='')
	{
		alert('Unable to fetch Voucher Number !');
	}
	else
	{
		$('#edit_date_vou_no').val(vou_no);
		$('#CreateDateModal').modal();
	}
}
</script>  

<input type="hidden" id="vou_id_block" name="fm_id"> 
 
</div>
<?php include ("./_modal_edit_lr.php"); ?>
<?php include ("./_modal_olr_create_date.php"); ?>
<?php include ("./_modal_olr_change_tno.php"); ?>
<?php include ("./footer.php"); ?>
</body>
</html>