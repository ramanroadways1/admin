<?php
require_once("./connect.php");

$date = date("Y-m-d"); 
$timestamp=date("Y-m-d H:i:s");
 
$lrno=escapeString($conn,strtoupper($_POST['lrno']));
$lr_type=escapeString($conn,strtoupper($_POST['lr_type'])); 

$get_lr = Qry($conn,"SELECT id,lr_id,company,branch,lr_type,date,truck_no,fstation,tstation,dest_zone,consignor,consignee,
con2_addr,from_id,to_id,con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,
bill_amt,t_type,do_no,shipno,invno,item,item_id,item_name,articles,goods_desc,goods_value,con1_gst,con2_gst,crossing,cancel,
break,diesel_req,download FROM lr_sample_pending WHERE lrno='$lrno'");

if(!$get_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_lr)==0){
	echo "<script>
		alert('LR not found !');
		window.location.href='./lr_entry.php';
	</script>";
	exit();
}

$row_lr = fetchArray($get_lr);

$lr_id = $row_lr['lr_id'];
$lr_temp_id = $row_lr['id'];
$lr_branch = $row_lr['branch'];

$chk_coal = Qry($conn,"SELECT z FROM user WHERE username='$lr_branch'");

if(!$chk_coal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numrows($chk_coal)==0)
{
	errorLog("Branch not found !",$conn,$page_name,__LINE__);
	echo "<script type='text/javascript'>
		alert('Branch not found !');
		window.location.href='./lr_entry.php';
	</script>";	
	exit();
}

$row_branch_chk = fetchArray($chk_coal);

if($row_branch_chk['z']=="1")
{
	echo "<script>
		alert('You can\'t edit COAL\'s LR !');
		$('#lr_sub').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_lr['crossing']!='')
{
	echo "<script>
		alert('You can\'t edit. LR Mapped with Freight memo or OLR !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_lr['cancel']==1)
{
	echo "<script>
		alert('You can\'t edit. LR marked as cancelled !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_lr['break']>0)
{
	echo "<script>
		alert('You can\'t edit. This is breaking LR. Delete all LR\'s pieces first !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_lr['download']>0)
{
	echo "<script>
		alert('You can\'t edit. LR downloaded by head-office !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
		
$chk_open_lr = Qry($conn,"SELECT id FROM open_lr WHERE party_id='$row_lr[con1_id]'");

if(!$chk_open_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_open_lr)>0)
{
	$open_lr="1";
}
else
{
	$open_lr="0";
}

if($open_lr==1)
{
	$company=escapeString($conn,strtoupper($_POST['company']));
}
else if($open_lr==0)
{
	$company=escapeString($conn,strtoupper($_POST['company_auto']));
}
else
{
	echo "<script>
		alert('Something went wrong !');
		window.location.href='./lr_entry.php';
	</script>";
	exit();
}

if($lr_type=='MARKET')
{
	$tno=escapeString($conn,strtoupper($_POST['tno_market']));
	$wheeler=escapeString($conn,strtoupper($_POST['wheeler_market']));
}
else if($lr_type=='OWN')
{
	$tno=escapeString($conn,strtoupper($_POST['tno_own']));
	$wheeler=escapeString($conn,strtoupper($_POST['wheeler_own_truck']));
}
else
{
	echo "<script>
		alert('Something went wrong !');
		window.location.href='./lr_entry.php';
	</script>";
	exit();
}
	
if($company=='' || $tno=='')
{
	echo "<script>
		alert('Error : Company or Vehicle Number not found !');
		window.location.href='./lr_entry.php';
	</script>";
	exit();
}	

// if($wheeler=='')
// {
	// echo "<script>
		// alert('Error : Wheeler not found !');
		// $('#loadicon').hide();
		// $('#lr_sub').attr('disabled',false);
	// </script>";
	// exit();
// }	

if($row_lr['diesel_req']>0)
{
	if($row_lr['diesel_req']==2)
	{
		echo "<script>
			alert('You Can not Edit This LR ! 2 Diesel Request Found From This LR !');
			$('#loadicon').hide();
			$('#lr_sub').attr('disabled',false);
		</script>";
		exit();
	}

$fetch_req_diesel = Qry($conn,"SELECT diesel_id,fno,tno FROM _pending_diesel WHERE lrno='$lrno'");

if(!$fetch_req_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_req_diesel)==0)
{
	echo "<script>
		alert('Error : Diesel request not found !');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled',false);
	</script>";
	exit();
}	
	
$row_diesel_req = fetchArray($fetch_req_diesel);
		
$fetch_diesel_comp = Qry($conn,"SELECT com FROM diesel_fm WHERE id='$row_diesel_req[diesel_id]'");	
if(!$fetch_diesel_comp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_diesel_comp)==0)
{
	echo "<script>
		alert('Error : Diesel request not found !');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled',false);
	</script>";
	exit();
}		

$row_diesel_comp = fetchArray($fetch_diesel_comp);

if($row_diesel_comp['com']!=$company)
{
	echo "<script>
		alert('You can not change Company. Update company in diesel request first.');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled',false);
	</script>";
	exit();
}
		
if($row_diesel_req['tno']!=$tno)
{
	echo "<script>
		alert('You can not change Vehicle No. Update vehicle number in diesel request first.');
		$('#loadicon').hide();
		$('#lr_sub').attr('disabled',false);
	</script>";
	exit();
}
	
}
		
$lr_date = escapeString($conn,($_POST['lr_date']));
$from = escapeString($conn,strtoupper($_POST['from']));
$to = escapeString($conn,strtoupper($_POST['to']));
$dest_zone = escapeString($conn,strtoupper($_POST['dest_zone']));
$from_id = escapeString($conn,strtoupper($_POST['from_id']));
$to_id = escapeString($conn,strtoupper($_POST['to_id']));
$dest_zone_id = escapeString($conn,strtoupper($_POST['dest_zone_id']));
$consignee = escapeString($conn,strtoupper($_POST['consignee']));
$consignee_id = escapeString($conn,strtoupper($_POST['consignee_id']));
$con2_gst = escapeString($conn,strtoupper($_POST['con2_gst']));
$do_no = escapeString($conn,strtoupper($_POST['do_no'])); 
$ship_no = escapeString($conn,strtoupper($_POST['ship_no']));
$inv_no = escapeString($conn,strtoupper($_POST['inv_no']));
$po_no = escapeString($conn,strtoupper($_POST['po_no'])); 
$act_wt = escapeString($conn,strtoupper($_POST['act_wt'])); 
$gross_wt = escapeString($conn,strtoupper($_POST['gross_wt'])); 
$charge_wt = escapeString($conn,strtoupper($_POST['charge_wt'])); 
$item = escapeString($conn,strtoupper($_POST['item'])); 
$item_id = escapeString($conn,strtoupper($_POST['item_id'])); 
$item_name = escapeString($conn,strtoupper($_POST['item_name'])); 
$bank = escapeString($conn,strtoupper($_POST['bank'])); 
$sold_to_party = escapeString($conn,strtoupper($_POST['sold_to_pay'])); 
$lr_name = escapeString($conn,strtoupper($_POST['lr_name'])); 
$con2_addr = escapeString($conn,strtoupper($_POST['con2_addr'])); 
$articles = escapeString($conn,($_POST['articles'])); 
$goods_value = escapeString($conn,strtoupper($_POST['goods_value'])); 
$b_rate = escapeString($conn,strtoupper($_POST['b_rate'])); 
$b_amt = escapeString($conn,strtoupper($_POST['b_amt'])); 
$t_type = escapeString($conn,strtoupper($_POST['t_type'])); 
$freight_type = escapeString($conn,strtoupper($_POST['freight_type'])); 
$goods_desc = escapeString($conn,strtoupper($_POST['goods_desc'])); 

$update_log=array();
$update_Qry=array();

if($company!=$row_lr['company'])
{
	$update_log[]="Company : $row_lr[company] to $company";
	$update_Qry[]="company='$company'";
}

if($lr_type!=$row_lr['lr_type'])
{
	$update_Qry[]="lr_type='$lr_type'";
}

if($tno!=$row_lr['truck_no'])
{
	$update_log[]="Vehicle_No : $row_lr[truck_no] to $tno";
	$update_Qry[]="truck_no='$tno'";
	$update_Qry[]="wheeler='$wheeler'";
}

if($lr_date!=$row_lr['date'])
{
	$update_log[]="LR Date : $row_lr[lr_date] to $lr_date";
	$update_Qry[]="date='$lr_date'";
}

if($from_id!=$row_lr['from_id'])
{
	$update_log[]="From_Loc : $row_lr[fstation] to $from";
	$update_Qry[]="fstation='$from'";
	$update_Qry[]="from_id='$from_id'";
}

if($to_id!=$row_lr['to_id'])
{
	$update_log[]="To_Loc : $row_lr[tstation] to $to";
	$update_Qry[]="tstation='$to'";
	$update_Qry[]="to_id='$to_id'";
}

if($dest_zone_id!=$row_lr['zone_id'])
{
	$update_log[]="Dest.Zone : $row_lr[dest_zone] to $dest_zone";
	$update_Qry[]="dest_zone='$dest_zone'";
	$update_Qry[]="zone_id='$dest_zone_id'";
}

if($consignee_id!=$row_lr['con2_id'])
{
	$update_log[]="Consignee : $row_lr[consignee] to $consignee";
	$update_Qry[]="consignee='$consignee'";
	$update_Qry[]="con1_id='$consignee_id'";
	$update_Qry[]="con2_gst='$con2_gst'";
}

if($do_no!=$row_lr['do_no'])
{
	$update_log[]="DO_no : $row_lr[do_no] to $do_no";
	$update_Qry[]="do_no='$do_no'";
}

if($ship_no!=$row_lr['shipno'])
{
	$update_log[]="Shipment_No : $row_lr[shipno] to $ship_no";
	$update_Qry[]="shipno='$ship_no'";
}

if($inv_no!=$row_lr['invno'])
{
	$update_log[]="Inv.No : $row_lr[invno] to $inv_no";
	$update_Qry[]="invno='$inv_no'";
}

if($po_no!=$row_lr['po_no'])
{
	$update_log[]="Po_No : $row_lr[po_no] to $po_no";
	$update_Qry[]="po_no='$po_no'";
}

if($act_wt!=$row_lr['wt12'])
{
	$update_log[]="Act_Wt : $row_lr[wt12] to $act_wt";
	$update_Qry[]="wt12='$act_wt'";
}

if($gross_wt!=$row_lr['gross_wt'])
{
	$update_log[]="Gross_Wt : $row_lr[gross_wt] to $gross_wt";
	$update_Qry[]="gross_wt='$gross_wt'";
}

if($charge_wt!=$row_lr['weight'])
{
	$update_log[]="charge_wt : $row_lr[weight] to $charge_wt";
	$update_Qry[]="weight='$charge_wt'";
	$update_Qry[]="weight2='$charge_wt'";
}

if($item_id!=$row_lr['item_id'])
{
	$update_log[]="Item_Group : $row_lr[item] to $item";
	$update_Qry[]="item='$item'";
	$update_Qry[]="item_id='$item_id'";
}

if($item_name!=$row_lr['item_name'])
{
	$update_log[]="item_name : $row_lr[item_name] to $item_name";
	$update_Qry[]="item_name='$item_name'";
}

if($bank!=$row_lr['bank'])
{
	$update_log[]="Bank : $row_lr[bank] to $bank";
	$update_Qry[]="bank='$bank'";
}

if($sold_to_party!=$row_lr['sold_to_pay'])
{
	$update_log[]="Sold_To_Party : $row_lr[sold_to_pay] to $sold_to_party";
	$update_Qry[]="sold_to_pay='$sold_to_party'";
}

if($lr_name!=$row_lr['lr_name'])
{
	$update_log[]="LR_NAME : $row_lr[lr_name] to $lr_name";
	$update_Qry[]="lr_name='$lr_name'";
}

if($con2_addr!=$row_lr['con2_addr'])
{
	$update_log[]="Consignee_Addr : $row_lr[con2_addr] to $con2_addr";
	$update_Qry[]="con2_addr='$con2_addr'";
}

if($articles!=$row_lr['articles'])
{
	$update_log[]="Articles : $row_lr[articles] to $articles";
	$update_Qry[]="articles='$articles'";
}

if($goods_value!=$row_lr['goods_value'])
{
	$update_log[]="Goods_Value : $row_lr[goods_value] to $goods_value";
	$update_Qry[]="goods_value='$goods_value'";
}

if($b_rate!=$row_lr['bill_rate'])
{
	$update_log[]="Billing_Rate : $row_lr[bill_rate] to $b_rate";
	$update_Qry[]="bill_rate='$b_rate'";
}

if($b_amt!=$row_lr['bill_amt'])
{
	$update_log[]="Billing_Amount : $row_lr[bill_amt] to $b_amt";
	$update_Qry[]="bill_amt='$b_amt'";
}

if($t_type!=$row_lr['t_type'])
{
	$update_log[]="Truck_Type : $row_lr[t_type] to $t_type";
	$update_Qry[]="t_type='$t_type'";
}

if($freight_type!=$row_lr['freight_type'])
{
	$update_log[]="Freight_Type : $row_lr[freight_type] to $freight_type";
	$update_Qry[]="freight_type='$freight_type'";
}

if($goods_desc!=$row_lr['goods_desc'])
{
	$update_log[]="Goods_Desc. : $row_lr[goods_desc] to $goods_desc";
	$update_Qry[]="goods_desc='$goods_desc'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#lr_sub').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry_temp = Qry($conn,"UPDATE lr_sample_pending SET $update_Qry WHERE id='$lr_temp_id'");

if(!$update_Qry_temp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0){
	$flag = false;
	errorLog("Temp lr not updated.",$conn,$page_name,__LINE__);
}

$update_Qry = Qry($conn,"UPDATE lr_sample SET $update_Qry WHERE id='$lr_id'");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$lr_id','$lrno','LR_ENTRY','LR_UPDATE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('LR : $lrno. Updated Successfully !');
		window.location.href='./lr_entry.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./lr_entry.php");
	exit();
}	
?>