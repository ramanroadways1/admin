<?php
require_once("./connect.php");
exit();
$frno = mysqli_real_escape_string($conn,strtoupper($_POST['frno']));
$new_company = mysqli_real_escape_string($conn,strtoupper($_POST['company']));

$timestamp=date("Y-m-d H:i:s");
$today=date("Y-m-d");

$fetch_vou = mysqli_query($conn,"SELECT user,comp,amt,chq,colset_d FROM mk_venf WHERE vno='$frno'");
if(!$fetch_vou)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($fetch_vou)==0)
{
	echo "<script>
		alert('Voucher not found !');
	</script>";
	exit();
}

$row=mysqli_fetch_array($fetch_vou);

$company = $row['comp'];
$branch = $row['user'];
$vou_amount = $row['amt'];
$pay_mode = $row['chq'];

if($company==$new_company)
{
	echo "<script>
		alert('Nothing to update !');
	</script>";
	exit();
}

if($pay_mode=='NEFT')
{
		$fetch_rtgs_req = mysqli_query($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND colset_d='1'");
		if(mysqli_num_rows($fetch_rtgs_req)>0)
		{
			echo "<script>
				alert('Rtgs Payment Done. You Can\'t Edit !');
			</script>";
			exit();
		}
		else
		{
			if($new_company=='RRPL')
			{
				$crn="RRPL-E".date("y");
			
				$ramanc=mysqli_query($conn,"SELECT DISTINCT crn FROM rtgs_fm where crn like '$crn%' ORDER BY id DESC LIMIT 1");
				if(mysqli_num_rows($ramanc)!=0)
				{
					$rowcrn=mysqli_fetch_array($ramanc);
					$count=$rowcrn['crn'];	
					$newstring=substr($count,8,15);
					$newstr = ++$newstring;
				}
				else
				{
					$newstr=1;
				}	
			
				$crnnew=$crn.sprintf("%'.06d",$newstr);
			}
			else
			{
				$crn="RR-E".date("y");
			
				$ramanc=mysqli_query($conn,"SELECT DISTINCT crn FROM rtgs_fm where crn like '$crn%' ORDER BY id DESC LIMIT 1");
				if(mysqli_num_rows($ramanc)!=0)
				{
					$rowcrn=mysqli_fetch_array($ramanc);
					$count=$rowcrn['crn'];	
					$newstring=substr($count,6);
					$newstr = ++$newstring;
				}
				else
				{
					$newstr=1;
				}	
				
				$crnnew=$crn.sprintf("%'.06d",$newstr);
			}
			
			$update_rtgs = mysqli_query($conn,"UPDATE rtgs_fm SET com='$new_company',crn='$crnnew' WHERE fno='$frno' AND colset_d!='1'");
			if(!$update_rtgs)
			{
				echo mysqli_error($conn);
				exit();
			}
			
			if(mysqli_affected_rows($conn)==0)
			{
				echo "<script>
					alert('Unable to update Company !');
				</script>";
				exit();
			}
			
			$update_passbook = mysqli_query($conn,"UPDATE passbook SET comp='$new_company' WHERE vou_no='$frno'");
			if(!$update_passbook)
			{
				echo mysqli_error($conn);
				exit();
			}
		}
	
}
else if($pay_mode=='CHEQUE')
{
	$update_passbook = mysqli_query($conn,"UPDATE passbook SET comp='$new_company' WHERE vou_no='$frno'");
	if(!$update_passbook)
	{
		echo mysqli_error($conn);
		exit();
	}
}
else
{
	if($company=='RRPL')
	{
		$debit="debit";	
		$balance="balance";	
	}
	else
	{
		$debit="debit2";
		$balance="balance2";
	}
	
	if($new_company=='RRPL')
	{
		$debit_new="debit";	
		$balance_new="balance";	
	}
	else
	{
		$debit_new="debit2";
		$balance_new="balance2";
	}
	
	$fetch_cash_id = mysqli_query($conn,"SELECT id,`$debit` as cash,`$balance` as balance FROM cashbook WHERE vou_no='$frno'");
	if(!$fetch_cash_id)
	{
		echo mysqli_error($conn);
		exit();
	}
	
	if(mysqli_num_rows($fetch_cash_id)==0)
	{
		echo "<script>
				alert('Unable to fetch Cashbook Data !');
			</script>";
			exit();
	}
	
	$row1=mysqli_fetch_array($fetch_cash_id);
	
	$cash_id = $row1['id'];
	$cash_amount = $row1['cash'];
	$cash_balance = $row1['balance'];
	
	$last_debit = mysqli_query($conn,"SELECT `$balance_new` as new_bal FROM cashbook WHERE id<'$cash_id' AND user='$branch' AND 
	comp='$new_company' AND (`$balance_new`!='' || `$balance_new`>0) ORDER BY id DESC LIMIT 1");
	
	if(!$last_debit)
	{
		echo mysqli_error($conn);
		exit();
	}
	
	$row_last = mysqli_fetch_array($last_debit);
	
	$last_amount = $row_last['new_bal'];
	
	if(empty($last_amount))
	{
		echo "<script>
				alert('Last cash debit is empty !');
			</script>";
		exit();
	}
	
	if($last_amount=='' || $last_amount==0)
	{
		echo "<script>
				alert('Last cash debit is empty or zero !');
			</script>";
		exit();
	}
	
	$new_bal1 = $last_amount-$cash_amount;
	
	$update_cashbook = mysqli_query($conn,"UPDATE cashbook SET comp='$new_company',`$debit`='',`$balance`='',`$debit_new`='$cash_amount',
	`$balance_new`='$new_bal1' WHERE id='$cash_id'");
	if(!$fetch_cash_id)
	{
		echo mysqli_error($conn);
		exit();
	}
	
	if(mysqli_affected_rows($conn)==0)
	{
		echo "<script>
			alert('Unable to Fetch Cash entry !');
		</script>";
		exit();
	}
			
	$update_cash_balance = mysqli_query($conn,"UPDATE cashbook SET `$balance`=`$balance`+'$cash_amount' WHERE id>'$cash_id' AND 
	comp='$company' AND user='$branch'");
	if(!$update_cash_balance)
	{
		echo mysqli_error($conn);
		exit();
	}
	
	$update_cash_balance2 = mysqli_query($conn,"UPDATE cashbook SET `$balance_new`=`$balance_new`-'$cash_amount' WHERE id>'$cash_id' AND 
	comp='$new_company' AND user='$branch'");
	if(!$update_cash_balance2)
	{
		echo mysqli_error($conn);
		exit();
	}
	
	$update_main_balance = mysqli_query($conn,"UPDATE user SET `$balance`=`$balance`+'$cash_amount',
	`$balance_new`=`$balance_new`-'$cash_amount' WHERE username='$branch'");
	
	if(!$update_main_balance)
	{
		echo mysqli_error($conn);
		exit();
	}
	
}	

$update_voucher = mysqli_query($conn,"UPDATE mk_venf SET comp='$new_company',colset_d='' WHERE vno='$frno'");
if(!$update_voucher)
	{
		echo mysqli_error($conn);
		exit();
	}		

echo "<script>
	alert('Company Successfully Updated !');
	document.getElementById('button2').click();
	document.getElementById('req_close_button').click();
</script>";
?>
