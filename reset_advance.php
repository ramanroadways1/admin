<?php 
require_once("./connect.php");

$frno=escapeString($conn,strtoupper($_POST['frno']));
$id=escapeString($conn,strtoupper($_POST['id']));
$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$get_fm = Qry($conn,"SELECT frno,company,branch,branch_user,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,ptob,totaladv,ptob,adv_branch_user,
adv_date,cashadv,chqadv,disadv,rtgsneftamt,paidto,gps_id FROM freight_form WHERE id='$id'");

if(!$get_fm){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./fm_view.php");
	exit();
}

if(numRows($get_fm)==0)
{
	echo "<script>
		alert('Freight memo not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$row_fm = fetchArray($get_fm);

if($row_fm['gps_id']!="0")
{
	echo "<script>
		alert('Can\'t reset voucher mapped with gps device !');
		$('#loadicon').hide();
		$('#adv_reset_id').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['frno']!=$frno)
{
	echo "<script>
		alert('Freight memo not verified !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

if($row_fm['ptob']=='')
{
	echo "<script>
		alert('Freight_Memo Advance Pending !');
		$('#loadicon').hide();
		$('#adv_reset_id').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['paidto']!='')
{
	echo "<script>
		alert('Balance Paid. Reset Balance First !');
		$('#loadicon').hide();
		$('#adv_reset_id').attr('disabled',false);
	</script>";
	exit();
}

if($row_fm['gps']>0)
{
	echo "<script>
		alert('GPS charges deducted. You can\'t reset advance !');
		$('#loadicon').hide();
		$('#adv_reset_id').attr('disabled',false);
	</script>";
	exit();
}

$fm_branch=$row_fm['branch'];
$company=$row_fm['company'];

$get_lrs = Qry($conn,"SELECT lrno FROM freight_form_lr WHERE frno='$frno'");

if(!$get_lrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./fm_view.php");
	exit();
}

if(numRows($get_lrs)==0)
{
	echo "<script>
		alert('LR not found !');
		window.location.href='./fm_view.php';
	</script>";
	exit();
}

$lrnos = array();
$lrnos_store = array();
		
while($row_lrno=fetchArray($get_lrs))
{
	$lrnos[] = "'".$row_lrno['lrno']."'";
	$lrnos_store[] = $row_lrno['lrno'];
}
		
$lrnos=implode(',', $lrnos);
$lrnos_store=implode(',', $lrnos_store);


$update_log = "Branch : $fm_branch($row_fm[branch_user]), Company: $company, O/B: $row_fm[ptob], Loading(+): $row_fm[newtds], 
Diesel_Inc(+): $row_fm[dsl_inc], GPS(-): $row_fm[gps], Adv_Claim(-): $row_fm[adv_claim], Others(-): $row_fm[newother], 
TDS(-): $row_fm[tds], Total_Freight: $row_fm[totalf], Total_Adv: $row_fm[totaladv], Adv_date: $row_fm[adv_date], 
Adv_User: $row_fm[adv_branch_user], Cash_adv: $row_fm[cashadv], Cheq_adv: $row_fm[chqadv], Diesel_adv: $row_fm[disadv], 
Rtgs_adv: $row_fm[rtgsneftamt].";

if($row_fm['ptob']=='NULL')
{
	if($row_fm['totaladv']>0)
	{
		errorLog("Invalid advance amount. FM_Id: $id.",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Invalid advance amount.');
			$('#loadicon').hide();
			$('#adv_reset_id').attr('disabled',false);
		</script>";
		exit();
	}
}

	$cash=$row_fm['cashadv'];
	$cheque=$row_fm['chqadv'];
	$diesel=$row_fm['disadv'];
	$neft=$row_fm['rtgsneftamt'];
		
	$total_advance=$cash+$cheque+$diesel+$neft;
		
	if($neft>0)
	{
		$chk_neft=Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$frno' AND type='ADVANCE' AND colset_d='1'");
		if(!$chk_neft){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		if(numRows($chk_neft)>0)
		{
			echo "<script>
				alert('NEFT Request Download by Accounts !');
				$('#loadicon').hide();
				// $('#adv_reset_id').attr('disabled',false);
			</script>";
			exit();	
		}
	}
		
	if($diesel>0)
	{
		$chk_diesel=Qry($conn,"SELECT id FROM diesel_fm WHERE fno='$frno' AND type='ADVANCE' 
		AND IF(dsl_by='PUMP',colset_d,done)='1'");
		
		if(!$chk_diesel){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		if(numRows($chk_diesel)>0)
		{
			echo "<script>
				alert('Diesel Request done by Diesel Department. Delete Diesel First !');
				$('#loadicon').hide();
				// $('#adv_reset_id').attr('disabled',false);
			</script>";
			exit();	
		}
		
		$chk_diesel_sum = Qry($conn,"SELECT SUM(qty) as total_qty FROM diesel_fm WHERE fno='$frno' AND type='ADVANCE'");
		if(!$chk_diesel_sum){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		$row_diesel_qty = fetchArray($chk_diesel_sum);
		$diesel_Qty = $row_diesel_qty['total_qty'];
	}
	else
	{
		$diesel_Qty = 0;
	}
	
	$chk_diesel_pre_req = Qry($conn,"SELECT lrno FROM lr_sample WHERE lrno IN($lrnos) AND diesel_req>0");
	
	if(!$chk_diesel_pre_req){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./fm_view.php");
		exit();
	}
	
	if(numRows($chk_diesel_pre_req)==1)
	{
		$chk_diesel_req_entry = Qry($conn,"SELECT id FROM _pending_diesel WHERE lrno IN($lrnos)");
	
		if(!$chk_diesel_req_entry){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./fm_view.php");
			exit();
		}
		
		if(numRows($chk_diesel_req_entry)>0)
		{
			errorLog("Pending diesel found. $lrnos_store.",$conn,$page_name,__LINE__);
			Redirect("Pending diesel found.","./fm_view.php");
			exit();
		}
		
		$requested_diesel = "1";
		
	}
	else if(numRows($chk_diesel_pre_req)>1)
	{
		errorLog("Diesel requested multiple time. $lrnos_store.",$conn,$page_name,__LINE__);
		Redirect("Diesel requested multiple time.","./fm_view.php");
		exit();
	}
	else
	{
		$requested_diesel = "0";
	}
	
	if($requested_diesel==1 AND $diesel==0)
	{
		errorLog("Diesel requested found but diesel amount is zero. $lrnos_store.",$conn,$page_name,__LINE__);
		Redirect("Diesel Error.","./fm_view.php");
		exit();
	}

StartCommit($conn);
$flag = true;
			
	if($requested_diesel=="1")
	{
		$copy_diesel = Qry($conn,"INSERT INTO _pending_diesel(diesel_id,fno,lrno,tno,branch) SELECT id,token_no,lrno,tno,branch FROM 
		diesel_fm WHERE fno='$frno' AND pre_req='1' AND branch='$fm_branch'");
			
		if(!$copy_diesel){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0){
			$flag = false;
			errorLog("Diesel request copy failed. LRnos: $lrnos_store.",$conn,$page_name,__LINE__);
		}
		
		$change_diesel_status = Qry($conn,"UPDATE diesel_fm SET fno='',pre_and_fm='0' WHERE fno='$frno' AND pre_req='1' AND
		branch='$fm_branch'");
			
		if(!$change_diesel_status){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0){
			$flag = false;
			errorLog("Diesel status modify failed. LRnos: $lrnos_store.",$conn,$page_name,__LINE__);
		}
	}	
	
	if($neft>0)
	{
		if($row_fm['adv_date']==$date)
		{	
			$update_today_data_rtgs=Qry($conn,"UPDATE today_data SET neft=neft-1,neft_amount=neft_amount-'$neft' WHERE 
			branch='$fm_branch' AND date='$date'");
			
			if(!$update_today_data_rtgs){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
			
		$neft_delete=Qry($conn,"DELETE FROM rtgs_fm WHERE fno='$frno' AND type='ADVANCE' AND colset_d!='1'");
		if(!$neft_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		if(AffectedRows($conn)==0){
			$flag = false;
			errorLog("UNABLE TO DELETE RTGS PAYMENT WHEN RTGS AMOUNT IS GRETER THAN 0. Vou_No: $frno.",$conn,$page_name,__LINE__);
		}
				
		$passbook_delete=Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno' AND desct='Advance RTGS/NEFT'");
		if(!$passbook_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	if($diesel>0)
	{
		if($row_fm['adv_date']==$date)
		{	
			$update_today_data_dsl=Qry($conn,"UPDATE today_data SET diesel_qty_market=diesel_qty_market-'$diesel_Qty',
			diesel_amount_market=diesel_amount_market-'$diesel' WHERE branch='$fm_branch' AND date='$date'");	
			
			if(!$update_today_data_dsl){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
			
		$diesel_delete=Qry($conn,"DELETE FROM diesel_fm WHERE fno='$frno' AND type='ADVANCE' 
		AND IF(dsl_by='PUMP',colset_d,done)!='1'");
		
		if(!$diesel_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(AffectedRows($conn)==0 AND $requested_diesel==0){
			$flag = false;
			errorLog("UNABLE TO DELETE Diesel PAYMENT WHEN Diesel AMOUNT IS GRETER THAN 0. Vou_No: $frno.",$conn,$page_name,__LINE__);
		}
	}
		
	if($cheque>0)
	{
		$cheque_delete = Qry($conn,"DELETE FROM passbook WHERE vou_no='$frno' AND desct='Advance Cheque'");
		if(!$cheque_delete){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
			
		$cheque_delete2=Qry($conn,"DELETE FROM cheque_book WHERE vou_no='$frno' AND pay_type='ADV'");
		if(!$cheque_delete2){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	if($cash>0)
	{
		$get_cash_id=Qry($conn,"SELECT id,debit,debit2 FROM cashbook WHERE vou_no='$frno' AND desct='Advance Cash'");
		if(!$get_cash_id){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if(numRows($get_cash_id)==0)
		{
			$flag = false;
			errorLog("UNABLE TO FETCH CASH ENTRY WHILE CASH AMOUNT IS GREATER THAN 0. Vou_No : $frno.",$conn,$page_name,__LINE__);
		}
				
		$row_cash = fetchArray($get_cash_id);
				
		$cash_id = $row_cash['id'];
			
		$delete_cash_entry = Qry($conn,"DELETE FROM cashbook WHERE id='$cash_id'");
		if(!$delete_cash_entry){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		if($company=='RRPL'){
			$balance_col="balance";
		}
		else{
			$balance_col="balance2";
		}
		
		$update_balance = Qry($conn,"UPDATE user SET `$balance_col`=`$balance_col`+'$cash' WHERE username='$fm_branch'");
		if(!$update_balance){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_cashbook=Qry($conn,"UPDATE cashbook SET `$balance_col`=`$balance_col`+'$cash' WHERE id>$cash_id AND comp='$company' 
		AND user='$fm_branch'");
		if(!$update_cashbook){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
		
	if($row_fm['adv_date']==$date)
	{
		$update_today_data=Qry($conn,"UPDATE today_data SET fm_adv=fm_adv-1,fm_adv_amount=fm_adv_amount-'$total_advance' 
		WHERE branch='$fm_branch' AND date='$date'");
		if(!$update_today_data){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}

	
	$adv_reset_fm = Qry($conn,"UPDATE freight_form SET actualf=0,newtds=0,dsl_inc=0,gps=0,adv_claim=0,newother=0,tds=0,totalf=0,
	totaladv=0,adv_branch_user='',ptob='',adv_date=0,pto_adv_name='',adv_pan='',cashadv=0,chqadv=0,chqno='',disadv=0,rtgsneftamt=0,
	rtgs_adv='0',narre='',baladv=0,colset_adv=0,colset_d_adv=0,adv_download='' WHERE id='$id'");
		
	if(!$adv_reset_fm){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}		
	
	$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
	('$id','$row_fm[frno]','FM_UPDATE','ADVANCE_RESET','$update_log','','ADMIN','$timestamp')");

	if(!$insertLog){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($flag)
	{
		MySQLCommit($conn);
		closeConnection($conn);
		
		echo "<script>
			alert('Freight Memo Advance Reset Success !');
			$('#get_button').attr('disabled',false);
			// $('#adv_reset_id').attr('disabled',false);
			$('#get_button').click();
		</script>";
	}
	else
	{
		MySQLRollBack($conn);
		closeConnection($conn);
		// echo "<script>
			// alert('Error While Processing Request !!');
			// $('#loadicon').hide();
		// </script>";
		Redirect("Error While Processing Request.","./fm_view.php");
		exit();
	}				
?>