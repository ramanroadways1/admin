<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Search Employee</h3>
              </div>
              
<div class="card-body">
				
<script>		
function Fetch(option)
{
	var branch = $('#emp_branch').val();
	
	if(branch=='')
	{
		alert('Select branch first !');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "load_branch_employee.php",
		data: 'branch=' + branch,
		type: "POST",
			success: function(data) {
			$("#result_main").html(data);
		},
		error: function() {}
		});
	}
}
</script>	

	<div class="row">	
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Select Branch <font color="red"><sup>*</sup></font></label>
                  <select id="emp_branch" class="form-control" required>
					<option value="">-select branch-</option>
					<?php
	$qry = Qry($conn,"SELECT username FROM user WHERE role='2'");
	if(!$qry){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($qry)>0)
	{
		while($row_branch = fetchArray($qry))
		{
			echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
		}	
	}	
					?>
				  </select>
              </div>
			</div>
			
			<div id="button_div" class="col-md-2">
				<label>&nbsp;</label>
				<br />
				<button type="button" onclick="Fetch()" id="button2" class="btn pull-right btn-danger">Check !</button>
			</div>
			
		</div>
		
		<div class="row">	
			<div class="col-md-12 table-responsive" style="overflow:auto">	
			
				<div id="result_main"></div>
			
			</div>
		</div>
	</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	</div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script>  
function Edit(id)
{
	var parts = id.split('_', 2);
	var id = parts[0];
	var type  = parts[1];
	
	$("#req_update_button").attr("disabled", false);
	
		$("#loadicon").show();
			jQuery.ajax({
			url: "mark_as_edit.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data)
			{
				$("#func_result").html(data);
				$("#loadicon").hide();
			},
			error: function() {}
			});
}
</script>  

<script type="text/javascript">
$(document).ready(function (e) {
$("#ReqUpdateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#req_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_market_req.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="ReqUpdateForm" autocomplete="off"> 
<div class="modal fade" id="ReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-xl" role="document">
       <div class="modal-content">
		<div class="modal-body">
			
		<div class="row">
			
				<div class="col-md-12">
                   <div class="form-group">
                      <h5>Update Diesel Req. : </h5>
                  </div>
               </div>
			   
				<div class="col-md-12 table-responsive" style="overflow:auto">
					<div id="result_modal_data"></div>
				</div>
				
			</div>
		</div>
	<input type="hidden" name="frno" id="modal_frno">	
	<div class="modal-footer">
		<button type="button" id="req_close_button" onclick="document.getElementById('menu_button2').click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="req_update_button" disabled class="btn btn-primary">Update Final</button>
	</div>
		</div>	
     </div>
 </div>	
</form>
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>