<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Coal Dispatch Data</h3>
              </div>
				
<div class="card-body">
	
	<div class="row">	
		<div class="col-md-12 table-responsive" style="overflow:auto">
		
<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="12">BL/BE Wise :</th>
    </tr>
	
	<tr>    
		<th>#</th>
		<th>Vessel_name</th>
		<th>BL_No.</th>
		<th>BE_No.</th>
		<th>BE_Date</th>
		<th>BL_Weight</th>
	</tr>
<?php
$get_data = Qry($conn,"SELECT be.be_no,be.be_date,be.branch,bl.bl_no,bl.bl_weight,bl.bl_date,ship.vessel_name,vessel.name as ship_name 
FROM ship.be_data AS be 
LEFT OUTER JOIN ship.bl_data AS bl ON bl.id = be.bl_id 
LEFT OUTER JOIN ship.shipment AS ship ON ship.unq_id = be.unq_id 
LEFT OUTER JOIN ship.vessel_name AS vessel ON vessel.id = ship.vessel_name");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn=1;
while($row = fetchArray($get_data))
{
	// $timestamp = date("d/m/y h:i A",strtotime($row['timestamp']));
	echo "<tr>
		<td>$sn</td>
		<td>$row[ship_name]</td>
		<td>$row[bl_no]</td>
		<td>$row[be_no]</td>
		<td>$row[be_date]</td>
		<td>$row[bl_weight]</td>
     </tr>";
$sn++; 
}
echo '</table>';
?>			
		</div>
		
		<div class="col-md-12 table-responsive" style="overflow:auto">
		
<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="12">Destination Wise :</th>
    </tr>
	
	<tr>    
		<th>#</th>
		<th>Branch</th>
		<th>Vessel_name</th>
		<th>Destination</th>
		<th>BL_No.</th>
		<th>BE_No.</th>
		<th>BE_Date</th>
		<th>BL_Weight</th>
		<th>By_Road_Dispatch</th>
	</tr>
<?php
$get_data2 = Qry($conn,"SELECT ROUND(SUM(l.actual_wt),2) as dispatch_weight,l.to_loc,s.name as destination,
be.be_no,be.be_date,be.branch,bl.bl_no,bl.bl_weight,bl.bl_date,ship.vessel_name,vessel.name as ship_name 
FROM ship.lr_entry AS l 
LEFT OUTER JOIN ship.be_data AS be ON be.id = l.boe_id 
LEFT OUTER JOIN ship.bl_data AS bl ON bl.id = be.bl_id 
LEFT OUTER JOIN ship.shipment AS ship ON ship.unq_id = be.unq_id 
LEFT OUTER JOIN station AS s ON s.id = l.to_loc 
LEFT OUTER JOIN ship.vessel_name AS vessel ON vessel.id = ship.vessel_name 
WHERE l.boe_id in(SELECT id FROM ship.be_data) GROUP BY l.boe_id,l.to_loc");

if(!$get_data2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn=1;
while($row2 = fetchArray($get_data2))
{
	// $timestamp = date("d/m/y h:i A",strtotime($row['timestamp']));
	echo "<tr>
		<td>$sn</td>
		<td>$row2[branch]</td>
		<td>$row2[ship_name]</td>
		<td>$row2[destination]</td>
		<td>$row2[bl_no]</td>
		<td>$row2[be_no]</td>
		<td>$row2[be_date]</td>
		<td>$row2[bl_weight]</td>
		<td>$row2[dispatch_weight]</td>
     </tr>";
$sn++; 
}
echo '</table>';
?>			
		</div>

<div class="col-md-12 table-responsive" style="overflow:auto">
		
<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="12">Multiple BE :</th>
    </tr>
	
	<tr>    
		<th>#</th>
		<th>Destination</th>
		<th>BE_No.</th>
		<th>By_Road_Dispatch</th>
	</tr>
<?php
$get_data2 = Qry($conn,"SELECT ROUND(SUM(l.actual_wt),2) as dispatch_weight,l.to_loc,l.do_no,s.name as destination 
FROM ship.lr_entry AS l 
LEFT OUTER JOIN station AS s ON s.id = l.to_loc 
WHERE (l.do_no like '%,%' OR l.do_no like '%/%') GROUP BY l.do_no");

if(!$get_data2){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$sn=1;
while($row2 = fetchArray($get_data2))
{
	// $timestamp = date("d/m/y h:i A",strtotime($row['timestamp']));
	echo "<tr>
		<td>$sn</td>
		<td>$row2[destination]</td>
		<td>'$row2[do_no]</td>
		<td>$row2[dispatch_weight]</td>
     </tr>";
$sn++; 
}
echo '</table>';
?>			
		</div>

		
	</div>
</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	</div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php include ("./footer.php"); ?>
</body>
</html>