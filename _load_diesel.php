<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$type = escapeString($conn,strtoupper($_POST['type']));

$qry=Qry($conn,"SELECT d.qty,d.rate,d.disamt,d.dsl_by,d.dcard,d.done_time,d.timestamp_download,p.name as pump_name 
FROM diesel_fm as d 
LEFT OUTER JOIN diesel_pump AS p ON p.code=d.dcard AND p.branch=d.branch
WHERE d.fno='$frno' AND d.type='$type'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('Diesel not found. Type : $type.');
		window.location.href='fm_view.php';
	</script>";
	exit();
}
?>

<table class="table table-bordered" style="font-size:12.5px;">
				 
	<tr>
       <th class="bg-<?php if($type=='ADVANCE'){ echo "warning"; } else { echo "success"; } ?>" style="font-size:13px;" colspan="9"><?php echo $type; ?> Diesel Details :</th>
    </tr>
	
	<tr>
		 <th>Qty</th>
		 <th>Rate</th>
		 <th>Amount</th>
		 <th>Card/Pump</th>
		 <th>Card_No/Pump_Name</th>
		 <th>Done_Time</th>
		 <th>Download_Time</th>
	</tr>
	
	<?php
	while($row = fetchArray($qry))
	{
		if($row['dsl_by']=='PUMP'){
			$card_no = $row['pump_name'];
		}
		else{
			$card_no = $row['dcard'];
		}
		
		echo "<tr>
			<td>$row[qty]</td>
			<td>$row[rate]</td>
			<td>$row[disamt]</td>
			<td>$row[dsl_by]</td>
			<td>$card_no</td>
			<td>$row[done_time]</td>
			<td>$row[timestamp_download]</td>
		</tr>";
	}
	?>
	
</table>

<script>
	$('#loadicon').hide();	
</script>