<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$id = escapeString($conn,strtoupper($_POST['id']));

$get_exp_vou = Qry($conn,"SELECT id,vno,user,newdate,comp,des,desid,amt,chq,chq_no,chq_bnk_n,neft_bank,neft_acname,neft_acno,neft_ifsc,
pan,narrat,empcode,vehno,colset_d FROM mk_venf WHERE id='$id'");

if(!$get_exp_vou){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_exp_vou)==0)
{
	echo "<script>
		alert('No result found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row=fetchArray($get_exp_vou);

$get_exp_comment = Qry($conn,"SELECT comm FROM expenses WHERE id='$row[desid]'");
$row_comm = fetchArray($get_exp_comment);
$vou_branch = $row['user'];
?>

<script>
function MyFunc1()
{
	$("#req_update_button").attr("disabled", true);
	$('#main_div_main').hide();
	$('#change_company_div').show();
}
</script>

<script>
  $(function() {  
      $("#pan_no").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: './autofill/exp_ac.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
             },
              select: function (event, ui) { 
			   $('#pan_no').val();
			   $('#pan_no').attr('readonly',true);
               $('#pan_no').val(ui.item.value);   
               $('#party_id').val(ui.item.id);     
               $('#ac_holder').val(ui.item.name);     
			   $('#ac_no').val(ui.item.acno);
			   $('#bank_name').val(ui.item.bank);
			   $('#ifsc').val(ui.item.ifsc);
			   $('#req_update_button').attr('disabled', false);
               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#pan_no").val('');
				$("#party_id").val('');
				$("#ac_holder").val('');
				$("#ac_no").val('');
				$("#bank_name").val('');
				$("#ifsc").val('');
                alert('PAN number does not exist !'); 
				$('#req_update_button').attr('disabled', true);
              } 
              },
			}); 
      }); 
</script> 


<script type="text/javascript">
// $(function() {
    // $("#pan_no").autocomplete({
      // source: './autofill/exp_party_pan_no.php',
	  // change: function (event, ui) {
        // if(!ui.item){
            // $(event.target).val("");
			 // $(event.target).focus();
			 // $("#req_update_button").attr("disabled",true);
		// alert('PAN Number doest not exists.');
        // }
    // }, 
    // focus: function (event, ui) {
        // $("#req_update_button").attr("disabled",false);
		// return false;
    // }
    // });
  // });
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#CompanyForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_change_company").attr("disabled", true);
	$.ajax({
	url: "./update_company_exp_vou.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_two").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<div id="result_two"></div>

<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th style="font-size:12.5px;" colspan="16">
	   <?php echo "Vou No : ".$frno.", Company : ".$row['comp'].
	   "&nbsp; <a href='#' style='color:blue' onclick='MyFunc1();'>Change</a>"; ?>
	   </th>
    </tr>
	
</table>	

<div class="container-fluid" id="change_company_div" style="display:none">
        <div class="row">
          <div class="col-md-12">
	
	<form id="CompanyForm">
	
		<div class="form-group col-md-4"> 
			<label>Select Company <font color="red"><sup>*</sup></font></label>
			<select name="company" class="form-control" required>
				<option value="">Select Company</option>
				<option <?php if ($row['comp'] == 'RRPL') echo ' selected="selected"'; ?> value="RRPL">RRPL</option>
				<option <?php if ($row['comp'] == 'RAMAN_ROADWAYS') echo ' selected="selected"'; ?> value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
			</select>
		</div>
		
		<input type="hidden" value="<?php echo $frno; ?>" name="frno">
		<input type="hidden" value="<?php echo $id; ?>" name="id">
		
		<div class="form-group col-md-4"> 
			<button type="submit" id="button_change_company" class="btn btn-success">Change Company</button>
		</div>
		
	</form>	
	
		</div>
	</div>
</div>

<div class="container-fluid" id="main_div_main">
    <div class="row">
      
		<div class="form-group col-md-3"> 
			<label>Vou Date <font color="red"><sup>*</sup></font></label>
			<input type="date" name="vou_date" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?php echo $row['newdate']; ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Expense Heading <font color="red"><sup>*</sup></font></label>
			<select class="form-control" onchange="GetExpDetails(this.value)" id="exp_head_id" name="exp_head" required>
				<option value="">Select an option</option>
				<?php
				$fetch_head = Qry($conn,"SELECT * FROM expenses ORDER BY exp ASC");
				if(numRows($fetch_head)>0)
				{
					while($row_exp = fetchArray($fetch_head))
					{
					?>
					<option <?php if ($row['desid'] == $row_exp['id']) echo ' selected="selected"'; ?> value="<?php echo $row_exp['id'].'_'.$row_exp['cash_limit'].'_'.$row_exp['comm'].'_'.$row_exp['exp']; ?>"><?php echo $row_exp["exp"];?></option>
					<?php
					}
				}						
				?>
			</select>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Amount <font color="red"><sup>*</sup></font></label>
			<input type="number" min="1" name="amount" class="form-control" value="<?php echo $row['amt']; ?>" required>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Payment Mode <font color="red"><sup>*</sup></font></label>
			<select class="form-control" id="payment_mode_id" onchange="PaymentBy(this.value);" name="pay_mode" required>
				<option value="">Select an option</option>
				<option <?php if ($row['chq'] == 'CASH') echo ' selected="selected"'; ?> value="CASH">CASH</option>
				<option <?php if ($row['chq'] == 'CHEQUE') echo ' selected="selected"'; ?> value="CHEQUE">CHEQUE</option>
				<option <?php if ($row['chq'] == 'NEFT') echo ' selected="selected"'; ?> value="NEFT">NEFT</option>
			</select>
		</div>
			
		<div id="chq_div" style="display:none" class="form-group col-md-3"> 
			<label>Bank Name (Cheque Bank) <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" id="chq_bank" name="chq_bank" value="<?php echo $row['chq_bnk_n']; ?>" required>
		</div>	
		
		<div id="chq_div1" style="display:none" class="form-group col-md-3"> 
			<label>Cheque No <font color="red"><sup>*</sup></font></label>
			<input type="number" min="1" id="chq_no" name="chq_no" class="form-control" value="<?php echo $row['chq_no']; ?>" required>
		</div>
		
		<div id="rtgs_div" style="display:none" class="form-group col-md-3"> 
			<label>PAN No <font color="red"><sup>*</sup></font> <a href="./add_exp_vou_party.php" target="_blank">(Add Party)</a></label>
			<input type="text" name="pan_no" id="pan_no" class="form-control" value="<?php echo $row['pan']; ?>" required>
		</div>
		
		<div id="rtgs_div1" style="display:none" class="form-group col-md-3"> 
			<label>Ac Holder <font color="red"><sup>*</sup></font></label>
			<input type="text" name="ac_holder" id="ac_holder" class="form-control" readonly value="<?php echo $row['neft_acname']; ?>" required>
		</div>
		
		<div id="rtgs_div2" style="display:none" class="form-group col-md-3"> 
			<label>A/c No <font color="red"><sup>*</sup></font></label>
			<input type="text" name="ac_no" id="ac_no" class="form-control" readonly value="<?php echo $row['neft_acno']; ?>" required>
		</div>
		
		<div id="rtgs_div3" style="display:none" class="form-group col-md-3"> 
			<label>Bank Name <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" name="bank_name" id="bank_name" readonly value="<?php echo $row['neft_bank']; ?>" required>
		</div>
		
		<div id="rtgs_div4" style="display:none" class="form-group col-md-3"> 
			<label>IFSC Code <font color="red"><sup>*</sup></font></label>
			<input type="text" class="form-control" name="ifsc" id="ifsc" readonly value="<?php echo $row['neft_ifsc']; ?>" required>
		</div>

	<div id="employee_div" style="display:none" class="form-group col-md-3"> 
			<label>Employee/Person <font color="red">*</font></label>
			<select id="empcode" name="empcode" class="form-control" required="required">
				<option value="">-- Select Employee --</option>
				<option <?php if ($row['empcode'] == 'KKG') echo ' selected="selected"'; ?> value="KKG">KISHORE GANDHI</option>
				<option <?php if ($row['empcode'] == 'RKG') echo ' selected="selected"'; ?> value="RKG">RISHAB GANDHI</option>
				<option <?php if ($row['empcode'] == 'JKG') echo ' selected="selected"'; ?> value="JKG">JAYANT GANDHI</option>
				<option <?php if ($row['empcode'] == 'SKG') echo ' selected="selected"'; ?> value="SKG">SANKET GANDHI</option>
				<option <?php if ($row['empcode'] == 'MKJAIN') echo ' selected="selected"'; ?> value="MKJAIN">MAHENDRA JAIN</option>
				<option <?php if ($row['empcode'] == 'TEJAS_SHAH') echo ' selected="selected"'; ?> value="TEJAS_SHAH">TEJAS SHAH</option>
				<?php 
				$emp_list = Qry($conn,"SELECT code,name FROM emp_attendance where branch='$vou_branch' AND status='3' ORDER BY code ASC ");
				if(!$emp_list){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","./");
					exit();
				}
				
				while($fetchemp = fetchArray($emp_list))
				{
				?>
	<option <?php if ($row['empcode'] == $fetchemp['code']) echo ' selected="selected"'; ?> value="<?php echo $fetchemp['code']; ?>"><?php echo $fetchemp['name']." (".$fetchemp['code'].")";?></option>
				<?php
				}
				?>
		</select>
	</div>
	
	<div class="form-group col-md-3" id="vehicle_div" style="display:none;">
		<label>Vehicle Number <font color="red">*</font></label>
			<select id="vehno" name="vehno" class="form-control" required="required">
			<option value="">-- Select Vehicle --</option>
			<?php 
			$veh_list = Qry($conn,"SELECT reg_no,owner_name FROM asset_vehicle where branch='$vou_branch' AND active='1' ORDER BY 
			`reg_no` ASC ");
			
			if(!$veh_list){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
			
			while($fetchveh = fetchArray($veh_list))
			{
			?>
<option <?php if ($row['vehno'] == $fetchveh['reg_no']) echo ' selected="selected"'; ?> value="<?php echo $fetchveh['reg_no']; ?>"><?php echo $fetchveh['reg_no']." (".$fetchveh['owner_name'].")";?></option>				
			<?php
			}
				
			$veh_listReq = Qry($conn,"SELECT req_code,veh_type,model_name FROM asset_vehicle_req where branch='$vou_branch' AND 
			ho_approval='1' AND asset_added=0 ORDER BY id ASC ");
			
			if(!$veh_listReq){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
				while($fetchvehReq = fetchArray($veh_listReq))
				{
				?>
<option <?php if ($row['vehno'] == $fetchvehReq['req_code']) echo ' selected="selected"'; ?> value="<?php echo $fetchvehReq['req_code']; ?>"><?php echo $fetchvehReq['req_code']." (".$fetchvehReq['model_name'].")";?></option>				
				<?php
				}	
			?>
		</select>
	</div>
	
		<div class="form-group col-md-3">
			<label>Expense Description <font color="red">*</font></label>
			<textarea value="<?php echo $row_comm['comm']; ?>" style="font-size:12px;" readonly class="form-control" id="comments"></textarea>
		</div>
		
		<div class="form-group col-md-3"> 
			<label>Narration <font color="red"><sup>*</sup></font></label>
			<textarea class="form-control" name="narration" required><?php echo $row['narrat']; ?></textarea>
		</div>
		
	</div>
</div>

<script>
function GetExpDetails(sel) {

	var exp_code = sel.split("_")[0];
	var cash_limit = sel.split("_")[1];
	var comment = sel.split("_")[2].toLowerCase();
		
	$('#comments').html(comment);
	// $('#cash_limit').val(cash_limit);

	// 7 = CONVEYANCE , 36= TOURS & TRAVELLING EXPS.
 		
		if(exp_code == 36 || exp_code == 7)
		{
			$('#employee_div').show();
			$('#empcode').attr('required',true);
		}
		else
		{
			$('#employee_div').hide();
			$('#empcode').attr('required',false);
		}
		
		// 43= VEHICLE FUEL , 44=VEHICLE REPAIR & MAINTENANCE
		
		if(exp_code == 43 || exp_code == 44)
		{
			$('#vehicle_div').show();
			$('#vehno').attr('required',true);
		}
		else
		{
			$('#vehicle_div').hide();
			$('#vehno').attr('required',false);
		}
} 

function PaymentBy(elem)
{
	if($('#exp_head_id').val()=='')
	{
		$('#exp_head_id').focus();
		$('#payment_mode_id').val('');
	}
	else
	{
		$("#req_update_button").attr("disabled", false);
		
		if(elem=='CHEQUE')
		{	
			$('#rtgs_div').hide();
			$('#rtgs_div1').hide();
			$('#rtgs_div2').hide();
			$('#rtgs_div3').hide();
			$('#rtgs_div4').hide();
			
			$('#chq_div').show();
			$('#chq_div1').show();
			
			$('#chq_bank').attr('required',true);
			$('#chq_no').attr('required',true);
			
			$('#pan_no').attr('required',false);
			$('#ac_holder').attr('required',false);
			$('#ac_no').attr('required',false);
			$('#bank_name').attr('required',false);
			$('#ifsc').attr('required',false);
		}
		else if(elem=='NEFT')
		{
			$('#rtgs_div').show();
			$('#rtgs_div1').show();
			$('#rtgs_div2').show();
			$('#rtgs_div3').show();
			$('#rtgs_div4').show();
			
			$('#chq_div').hide();
			$('#chq_div1').hide();
			
			$('#chq_bank').attr('required',false);
			$('#chq_no').attr('required',false);
			
			$('#pan_no').attr('required',true);
			$('#ac_holder').attr('required',true);
			$('#ac_no').attr('required',true);
			$('#bank_name').attr('required',true);
			$('#ifsc').attr('required',true);
		}
		else
		{
			$('#rtgs_div').hide();
			$('#rtgs_div1').hide();
			$('#rtgs_div2').hide();
			$('#rtgs_div3').hide();
			$('#rtgs_div4').hide();
			
			$('#chq_div').hide();
			$('#chq_div1').hide();
			
			$('#chq_bank').attr('required',false);
			$('#chq_no').attr('required',false);
			
			$('#pan_no').attr('required',false);
			$('#ac_holder').attr('required',false);
			$('#ac_no').attr('required',false);
			$('#bank_name').attr('required',false);
			$('#ifsc').attr('required',false);
		}		
	}		
}		
</script>		
		
<script>
	document.getElementById('menu_button2').click();
	PaymentBy('<?php echo $row["chq"] ?>');
	var desid = '<?php echo $row["desid"] ?>';
		
		if(desid == 36 || desid == 7)
		{
			$('#employee_div').show();
			$('#empcode').attr('required',true);
			$('#vehicle_div').hide();
			$('#vehno').attr('required',false);
		}
		else if(desid == 43 || desid == 44)
		{
			$('#employee_div').hide();
			$('#empcode').attr('required',false);
			$('#vehicle_div').show();
			$('#vehno').attr('required',true);
		}
		else
		{
			$('#employee_div').hide();
			$('#empcode').attr('required',false);
			$('#vehicle_div').hide();
			$('#vehno').attr('required',false);
		}
	
	$('#loadicon').hide();
</script>