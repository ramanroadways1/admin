<?php
require_once("./connect.php");

$id=escapeString($conn,$_POST['id']);
$section=escapeString($conn,$_POST['section']);
$party_name=escapeString($conn,strtoupper($_POST['party_name']));
$ac_name=escapeString($conn,strtoupper($_POST['ac_holder']));
$ac_no=escapeString($conn,strtoupper($_POST['ac_no']));
$bank=escapeString($conn,strtoupper($_POST['bank_name']));
$ifsc=escapeString($conn,strtoupper($_POST['ifsc']));
$otp=escapeString($conn,$_POST['otp']);
$branch=escapeString($conn,$_POST['branch']);
$user_code=escapeString($conn,$_POST['user_code']);

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if($section=='OWNER')
{
	$chk_ac_details = Qry($conn,"SELECT acname,accno,bank,ifsc FROM mk_truck WHERE id='$id'");
}
else if($section=='BROKER')
{
	$chk_ac_details = Qry($conn,"SELECT acname,accno,bank,ifsc FROM mk_broker WHERE id='$id'");
}
else if($section=='DRIVER')
{
	$chk_ac_details = Qry($conn,"SELECT acname,acno as accno,bank,ifsc FROM dairy.driver_ac WHERE code='$id'");
}
else
{
	echo "<script>
		alert('Invalid account type !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!$chk_ac_details){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_ac_details)==0)
{
	echo "<script>
		alert('Invalid account !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_ac = fetchArray($chk_ac_details);
	
$update_log=array();
$update_Qry=array();

if($ac_name!=$row_ac['acname'])
{
	$update_log[]="Ac_holder : $row_ac[acname] to $ac_name";
	$update_Qry[]="acname='$ac_name'";
}

if($ac_no!=$row_ac['accno'])
{
	if($section=='DRIVER')
	{
		$update_log[]="Ac_no : $row_ac[accno] to $ac_no";
		$update_Qry[]="acno='$ac_no'";
	}
	else
	{
		$update_log[]="Ac_no : $row_ac[accno] to $ac_no";
		$update_Qry[]="accno='$ac_no'";
	}
}

if($bank!=$row_ac['bank'])
{
	$update_log[]="bank : $row_ac[bank] to $bank";
	$update_Qry[]="bank='$bank'";
}

if($ifsc!=$row_ac['ifsc'])
{
	$update_log[]="ifsc : $row_ac[ifsc] to $ifsc";
	$update_Qry[]="ifsc='$ifsc'";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#req_update_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

// echo "<script>
		// $('#loadicon').hide();
		// </script>";
		// exit();
		
if($otp!='')
{
	// For Verify otp
	$url="https://www.smsschool.in/api/verifyRequestOTP.php";
	$postData = array(
		'authkey' => "242484ARRP024r65bc082c9",
		'mobile' => "9024281599",
		'otp' => "$otp"
	);

	$ch = curl_init();
	curl_setopt_array($ch, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_POST => true,
	CURLOPT_POSTFIELDS => $postData
	));

	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	$output = curl_exec($ch);

	if(curl_errno($ch))
	{
		echo "<script>
			alert('API Failure !');
			$('#req_update_button').attr('disabled',false);
				$('#loadicon').hide();
		</script>";
		exit();	
	}

	$result = json_decode($output, true);
	curl_close($ch);

	if($result['type']=='error')
	{
		echo "<script>
				alert('Error : $result[message].');
				$('#req_update_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
		exit();	
	}
	else
	{
		if($result['message']!='otp_verified')
		{
			echo "<script>
				alert('Error : $result[message].');
				$('#req_update_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();	
		}
	}
}

if($otp=='')
{
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");

	if(!in_array($_FILES['ac_proof']['type'], $valid_types))
	{
		echo "<script>
			alert('Error : Only Image Upload Allowed.');
			$('#req_update_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
	
StartCommit($conn);
$flag = true;

if($otp=='')
{
	$sourcePath = $_FILES['ac_proof']['tmp_name'];
	$fix_name = mt_rand().date('dmYHis');
	$targetPath="../34geXmnqK8pxJJN_RTGS/manage_ac/copy/".$fix_name.".".pathinfo($_FILES['ac_proof']['name'],PATHINFO_EXTENSION);
	$targetPath2="copy/".$fix_name.".".pathinfo($_FILES['ac_proof']['name'],PATHINFO_EXTENSION);

	ImageUpload(1000,1000,$sourcePath);

	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		$flag = false;
		errorLog("Unable to Upload Copy.",$conn,$page_name,__LINE__);	
	}
}
else
{
	$targetPath2="";
}

if($section=='OWNER')
{
	$update_ac = Qry($conn,"UPDATE mk_truck SET $update_Qry WHERE id='$id'");
	
	if(!$update_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}
else if($section=='BROKER')
{
	$update_ac = Qry($conn,"UPDATE mk_broker SET $update_Qry WHERE id='$id'");
	
	if(!$update_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}
else if($section=='DRIVER')
{
	$update_ac = Qry($conn,"UPDATE dairy.driver_ac SET $update_Qry WHERE code='$id'");
	
	if(!$update_ac){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	}
}

$update_ac2 = Qry($conn,"INSERT INTO ac_update(o_b,ac_for,at_the_time_of,acname,acno,bank,ifsc,copy,branch,
	branch_user,timestamp) VALUES ('$section','$id','ADMIN','$ac_name','$ac_no','$bank','$ifsc','$targetPath2',
	'$branch','$user_code','$timestamp')");
	
	if(!$update_ac2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	} 
	
$insertLog = Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$section','AC_UPDATE','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Account updated Successfully !!');
		window.location.href='./change_ac.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
			alert('Error While Processing Request.');
			$('#req_update_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}