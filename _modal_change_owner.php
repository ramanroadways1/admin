<script type="text/javascript">
$(document).ready(function (e) {
$("#ChanageOwnerForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#chanage_owner_button").attr("disabled", true);
	$.ajax({
	url: "./update_owner_fm.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="ChanageOwnerForm" autocomplete="off"> 
<div class="modal fade" id="OwnerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
		
	<div class="modal-header bg-primary">
		Change Owner :
      </div>
	  
		<div class="modal-body">
			<div class="row">
			 <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Vehicle Number <font color="red"><sup>*</sup></font></label>
					   <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="tno" id="owner_modal_tno" class="form-control" required>
                  </div>
              </div>
			  
			  <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Owner's Name <font color="red"><sup>*</sup></font></label>
					   <input type="text" id="owner_modal_name" name="owner_name" class="form-control" readonly required>
                  </div>
              </div>
			  
			  <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Owner's PAN <font color="red"><sup>*</sup></font></label>
					   <input id="owner_modal_pan" name="owner_pan_no" class="form-control" required readonly />
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Owner's Mobile <font color="red"><sup>*</sup></font></label>
					   <input id="owner_modal_mobile" class="form-control" required readonly />
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Owner's Address <font color="red"><sup>*</sup></font></label>
					   <textarea id="owner_modal_addr" class="form-control" required readonly></textarea>
                  </div>
               </div>
			   
			   <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">e-way Bill Number <font color="red"><sup>*</sup></font></label>
					   <input type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" id="ewb_no" name="ewb_no" class="form-control" required />
                  </div>
               </div>
			   
			   <input type="hidden" id="EwayBillFlag" name="EwayBillFlag">
			   <input type="hidden" id="item_id_tno_change" name="item_id">
			   <input type="hidden" id="con1_id_tno_change" name="con1_id">
			   <input type="hidden" id="owner_modal_id" name="owner_id">
			   <input type="hidden" id="owner_modal_wheeler" name="wheeler">
			   <input type="hidden" id="owner_vou_no" name="vou_no">
			   <input type="hidden" id="owner_vou_id" name="vou_id_fm">
			  
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" disabled id="close_owner_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="chanage_owner_button" class="btn btn-primary">Change Owner</button>
	</div>
		</div>	
     </div>
 </div>
 </form>