<?php
require_once("./connect.php");

$party_name = escapeString($conn,$_POST['party_name']);
$party_id = escapeString($conn,$_POST['party_id']);
$timestamp = date("Y-m-d H:i:s");

if($party_id=="" || $party_name=="")
{
	echo "<script>
		alert('Party not found. Please check !');
		$('#add_new_button').attr('disabled',true);
		$('#loadicon').hide();	
	</script>";
	exit();
}	

$qry = Qry($conn,"SELECT name FROM consignor WHERE id='$party_id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('Party not Found !');
		$('#add_new_button').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}
	
$row=fetchArray($qry);

if($row['name']!=$party_name)
{
	echo "<script>
		alert('Party name not verified !');
		$('#add_new_button').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}	

$check_qry = Qry($conn,"SELECT id FROM open_lr WHERE party_id='$party_id'");
if(!$check_qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($check_qry)>0)
{
	echo "<script>
		alert('Duplicate Record Found !');
		$('#add_new_button').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update_Qry = Qry($conn,"INSERT INTO open_lr(party,party_id,branch,timestamp) VALUES ('$party_name','$party_id','ALL','$timestamp')");

if(!$update_Qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_id = getInsertID($conn);

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$insert_id','','OPEN_LR_PARTY','OPEN_LR_PARTY_ADDED','$party_name($party_id)','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Party Added Successfully !');
		window.location.href='./open_lr_party.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./open_lr_party.php");
	exit();
}	
?>