<?php
require_once("./connect.php");

$id = escapeString($conn,$_POST['id']);
$exp_name = escapeString($conn,$_POST['exp_name']);
$amount = round(escapeString($conn,$_POST['amount']));
$timestamp = date("Y-m-d H:i:s"); 

if($id=='')
{
	echo "<script>
		alert('Lane not found !!');
		$('#add_exp_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($exp_name=='')
{
	echo "<script>
		alert('Expense not found !!');
		$('#add_exp_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($amount=='' || $amount<=0)
{
	echo "<script>
		alert('Invalid expense amount !!');
		$('#add_exp_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$exp_code = explode("_",$exp_name)[0];
$exp_name = explode("_",$exp_name)[1];

$chk_record = Qry($conn,"SELECT id FROM dairy.fix_lane_exp WHERE lane_id='$id' AND exp_code='$exp_code'");

if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_record)>0)
{
	echo "<script>
		alert('Duplicate record found !!');
		$('#add_exp_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$insert_record = Qry($conn,"INSERT INTO dairy.fix_lane_exp(lane_id,exp_code,exp_name,exp_amount,timestamp) VALUES ('$id','$exp_code',
'$exp_name','$amount','$timestamp')");

if(!$insert_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	echo "<script>
		alert('Inserted Successfully !!');
		$('#add_exp_btn').attr('disabled',false);
		$('#exp_head_name').val('');
		$('#exp_amount_new').val('');
		$('#loadicon').hide();
	</script>";
	exit();

?>