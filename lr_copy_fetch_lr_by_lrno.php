<?php
require_once("./connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));

$qry = Qry($conn,"SELECT branch FROM lr_sample_pending WHERE lrno = '$lrno'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows=numRows($qry);

if($numrows==0)
{
	echo "<script>
		alert('Invalid LR number entered !');
		$('#loadicon').fadeOut('slow');
		$('.lrno_copy_div').hide();
		$('#create_button').attr('disabled',true);
		$('#get_button').attr('disabled',false);
		$('#lrno').attr('readonly',false); 
	</script>";	
	exit();
}

$row = fetchArray($qry);

if($row['branch'] != 'JHARSUGUDA')
{
	echo "<script>
		alert('LR does not belongs to jharsuguda !');
		$('#loadicon').fadeOut('slow');
		$('.lrno_copy_div').hide();
		$('#create_button').attr('disabled',true);
		$('#get_button').attr('disabled',false);
		$('#lrno').attr('readonly',false);
	</script>";	
	exit();
}

echo "<option value=''>--select postfix--</option>
<option value='".$lrno."A'>".$lrno."A</option>
<option value='".$lrno."B'>".$lrno."B</option>
";

	echo "<script>
		$('#loadicon').fadeOut('slow');
		$('.lrno_copy_div').show();
		$('#create_button').attr('disabled',false);
		$('#get_button').attr('disabled',true);
		$('#lrno').attr('readonly',true);
	</script>";	
	exit();
?>