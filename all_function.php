<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

<div id="func_result"></div>

    <section class="content">
      <div class="container-fluid">
        
		
	<div class="row">
	
          <div class="col-12 col-sm-6 col-md-3" data-toggle="modal" data-target="#AddBrokerModal">
            <div class="info-box">
             <img class="img-responsive" style="background:#FFF;width:60px;height:100%" src="svg/b_to_b.svg">
			 
              <div class="info-box-content">
                <span class="info-box-text">&nbsp; Add new branch</span>
               </div>
            </div>
          </div>
		  
          <div class="col-12 col-sm-6 col-md-3" data-toggle="modal" data-target="#AddBrokerModal">
            <div class="info-box mb-3">
             
			 <img class="img-responsive" style="background:#FFF;width:60px;height:100%" src="svg/party.svg">
			 
              <div class="info-box-content">
                <span class="info-box-text">&nbsp;  Add Consignor</span>
              </div>
            </div>
         </div>
         
	<div class="clearfix hidden-md-up"></div>
		
		<div class="col-12 col-sm-6 col-md-3" data-toggle="modal" data-target="#AddBrokerModal">
            <div class="info-box mb-3">
             
			 <img class="img-responsive" style="background:#FFF;width:60px;height:100%" src="svg/party.svg">
			 
              <div class="info-box-content">
                <span class="info-box-text">&nbsp;  Add Consignee</span>
              </div>
            </div>
         </div>
		
		<div class="col-12 col-sm-6 col-md-3" data-toggle="modal" data-target="#AddBrokerModal">
            <div class="info-box mb-3">
             
			 <img class="img-responsive" style="background:#FFF;width:60px;height:100%" src="svg/truck_vou.svg">
			 
              <div class="info-box-content">
                <span class="info-box-text">&nbsp;  Add Market Truck</span>
              </div>
            </div>
         </div>
      </div>
	  

	<div class="row">
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
             
			 <img class="img-responsive" style="background:#FFF;width:60px;height:100%" src="svg/broker1.svg">
			 
              <div class="info-box-content">
                <span class="info-box-text">&nbsp;  Add Broker</span>
              </div>
            </div>
         </div>
		  
       <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
             
			 <img class="img-responsive" style="background:#FFF;width:60px;height:100%" src="svg/driver.svg">
			 
              <div class="info-box-content">
                <span class="info-box-text">&nbsp;  Add Driver</span>
              </div>
            </div>
         </div>
         
	<div class="clearfix hidden-md-up"></div>
		<div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
             
			 <img class="img-responsive" style="background:#FFF;width:60px;height:100%" src="svg/broker1.svg">
			 
              <div class="info-box-content">
                <span class="info-box-text">&nbsp;  Add Consignor</span>
              </div>
            </div>
         </div>
		
		<div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
             
			 <img class="img-responsive" style="background:#FFF;width:60px;height:100%" src="svg/b_to_b.svg">
			 
              <div class="info-box-content">
                <span class="info-box-text">&nbsp;  Add Consignor</span>
              </div>
            </div>
         </div>
    </div>
	  
        </div>
      </div>
    </section>
  </div>
  
<form id="AddForm1" autocomplete="off"> 
<div class="modal fade" id="AddForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
	   
	  <div class="modal-header">
			<h5 class="modal-title">Add new Open LR Party :</h5>
      </div>
	  
		<div class="modal-body">
			
		<div class="row">
			
			<div class="col-md-12">
				<div class="form-group">
					<label>Party Name <font color="red"><sup>*</sup></font></label>
                     <input type="text" id="party_name" name="party_name" class="form-control" required>
                 </div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label>Party Name <font color="red"><sup>*</sup></font></label>
                     <input type="text" id="party_gst" name="party_gst" class="form-control" required readonly>
                 </div>
			</div>
				
			</div>
		</div>
	<input type="hidden" name="party_id" id="party_id">	
	<div class="modal-footer">
		<button type="button" id="add_close_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="add_new_button" class="btn btn-primary">ADD Party</button>
	</div>
		</div>	
     </div>
 </div>	
</form>
 
</div>
<?php include ("./footer.php"); ?>
</body>
</html>