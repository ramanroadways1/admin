<?php
require_once './connect.php'; 

$id=escapeString($conn,strtoupper($_POST['id']));

$qry = Qry($conn,"SELECT frno,lrno,date,fstation,tstation,consignor,consignee,from_id,to_id,con1_id,con2_id,wt12,weight,done,crossing FROM 
freight_form_lr WHERE id='$id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('No result found !');
		window.location.href='fm_view.php';
	</script>";
	exit();	
}

$row = fetchArray($qry);
	
// if($row['done']==1)
// {
	// echo "<script>
		// alert('Own Truck Form Attached To E-Diary Trip. Delete Trip First !');
		// $('#trip_done_alert').show();
		// $('#loadicon').hide();
		// $('#lr_update_button').attr('disabled',true);
	// </script>";
	// exit();
// }

$qry_lr_locations = Qry($conn,"SELECT fstation,tstation FROM lr_sample WHERE lrno='$row[lrno]'");

if(!$qry_lr_locations){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry_lr_locations)==0)
{
	echo "<script>
		alert('LR not found !');
		window.location.href='fm_view.php';
	</script>";
	exit();	
}

$row_lr_loc = fetchArray($qry_lr_locations);

$qry_get_consignee = Qry($conn,"SELECT gst,pincode FROM consignee WHERE id='$row[con2_id]'");

if(!$qry_get_consignee){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_con2 = fetchArray($qry_get_consignee);

$check_Other_Vou = Qry($conn,"SELECT id,frno,truck_no,fstation,tstation FROM freight_form_lr WHERE lrno='$row[lrno]' ORDER by id ASC");

if(!$check_Other_Vou){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$get_last_to_loc = Qry($conn,"SELECT tstation FROM freight_form_lr WHERE lrno='$row[lrno]' AND id<'$id' ORDER BY id DESC LIMIT 1"); 

if(!$get_last_to_loc){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_last_to_loc)==0)
{
	$last_loc_verify = "";
}
else
{
	$row_last_loc = fetchArray($get_last_to_loc);
	$last_loc_verify = $row_last_loc['tstation'];
}

$get_next_loc = Qry($conn,"SELECT fstation FROM freight_form_lr WHERE lrno='$row[lrno]' AND id>'$id' ORDER BY id ASC LIMIT 1"); 

if(!$get_next_loc){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_next_loc)==0)
{
	$next_loc_verify = "";
}
else
{
	$row_next_loc = fetchArray($get_next_loc);
	$next_loc_verify = $row_next_loc['tstation'];
}
?>

<table class="table table-bordered" style="font-size:12px;">
	<tr>
       <th style="font-size:12.4px;" colspan="4">Vouchers Created from LR :</th>
    </tr>
<?php
	while($row_other_vou = fetchArray($check_Other_Vou))
	{
		if($row_other_vou['id']==$id)
		{
			echo '<tr style="background:lightgreen">';
		}
		else
		{
			echo '<tr>';
		}
		
		echo '<td>'.$row_other_vou["frno"].'</td>
			<td>'.$row_other_vou["truck_no"].'</td>
			<td>'.$row_other_vou["fstation"].' to '.$row_other_vou["tstation"].'</td>
		</tr>'; 
	}

echo "</table>";
	
if(numRows($check_Other_Vou)==1)
{
	// echo "<script>
		// $('#change_from_lr').prop('checked','checked');
	// </script>";
}
else
{
	// echo "<script>
		// $('#change_from_lr').prop('checked',false);
		// $('#change_from_lr').attr('disabled',true);
	// </script>";
}

	echo "<script>
			$('#lr_edit_id').val('$id');
			$('#edit_modal_crossing_var').val('$row[crossing]');
			$('#edit_modal_frno').val('$row[frno]');
			$('#edit_modal_lrno').val('$row[lrno]');
			$('#edit_modal_lr_date').val('$row[date]');
			$('#edit_modal_from').val('$row[fstation]');
			$('#edit_modal_to').val('$row[tstation]');
			$('#edit_modal_consignor').val('$row[consignor]');
			$('#edit_modal_consignee').val('$row[consignee]');
			$('#edit_modal_act_weight').val('$row[wt12]');
			
			$('#edit_modal_from_id').val('$row[from_id]');
			$('#edit_modal_to_id').val('$row[to_id]');
			$('#edit_modal_con1_id').val('$row[con1_id]');
			$('#edit_modal_con2_id').val('$row[con2_id]');
			
			$('#edit_from_loc_lr').val('$row_lr_loc[fstation]');
			$('#edit_to_loc_lr').val('$row_lr_loc[tstation]');
			$('#edit_modal_con2_gst').val('$row_con2[gst]');
			$('#edit_modal_consignee_gst').val('$row_con2[gst]');
			$('#edit_modal_con2_pincode').val('$row_con2[pincode]');
			
			$('#last_loc_verify').val('$last_loc_verify');
			$('#next_loc_verify').val('$next_loc_verify');
			
			$('#modal_edit_LR').modal();
			$('#lr_update_button').attr('disabled',false);
			$('#loadicon').hide();
	</script>";
?>