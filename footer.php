<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="#"> RRPL</a>.</strong> All rights
    reserved.
</footer>

<!--<aside class="control-sidebar control-sidebar-dark"></aside>-->

<!--<script src="./plugins/jquery/jquery.min.js"></script>-->
<script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="./plugins/datatables/jquery.dataTables.min.js"></script>
<script src="./plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="./plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="./js/fastclick.js"></script>
<script src="./js/adminlte.min.js"></script>
<script src="./js/demo.js"></script>

<script>
  $(function () {
    $("#data_table").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>