<?php
require_once("./connect.php");

$frno = escapeString($conn,strtoupper($_POST['frno']));
$timestamp=date("Y-m-d H:i:s");

if($frno=='')
{
	echo "<script>
		alert('No result found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_diesel_done = Qry($conn,"SELECT diesel_id FROM dairy.diesel_cache_own WHERE token_no='$frno' AND save='1'");
if(!$chk_diesel_done){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$total_updated_rows = numRows($chk_diesel_done);

if($total_updated_rows==0)
{
	echo "<script>
		alert('Please Save All Records First !');
		$('#req_update_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$diesel_ids = array();
		
while($row_lrno=fetchArray($chk_diesel_done))
{
	$diesel_ids[] = "'".$row_lrno['diesel_id']."'";
}
		
$diesel_ids=implode(',', $diesel_ids);

$chk_done = Qry($conn,"SELECT id FROM dairy.diesel_entry WHERE id IN($diesel_ids) AND IF(card_pump='PUMP',download,done)='1'");
if(!$chk_done){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_done)>0)
{
	echo "<script>
		alert('Error : Pump or Card request marked as done !');
		window.location.href='./request_own_truck.php';
	</script>";
	exit();
}

$get_diesel_update = Qry($conn,"SELECT d1.diesel_id,d1.amount as diesel1,d1.veh_no as card1,d1.dcom as company1,d2.veh_no as card2,
d2.diesel as diesel2,d2.dsl_company as company2 
FROM dairy.diesel_cache_own AS d1 
LEFT OUTER JOIN dairy.diesel_entry AS d2 ON d2.id=d1.diesel_id 
WHERE d1.token_no='$frno'");

if(!$get_diesel_update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_diesel_update)==0)
{
	echo "<script>
		alert('No record found to update !');
		$('#req_update_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$update_log=array();
// $is_fm_created = false;

while($row_update = fetchArray($get_diesel_update))
{
	// if($row_update['fno']!=$row_update['token_no'])
	// {
		// $is_fm_created = true;
	// }
	
	if($row_update['card1']!=$row_update['card2'])
	{
		$update_log[]="Diesel-Id: $row_update[diesel_id]-> Card/Pump : $row_update[card1] to $row_update[card2]";
	}
	
	if($row_update['company1']!=$row_update['company2'])
	{
		$update_log[]="Diesel-Id: $row_update[diesel_id]-> Company : $row_update[company1] to $row_update[company2]";
	}
	
	if($row_update['diesel1']!=$row_update['diesel2'])
	{
		$update_log[]="Diesel-Id: $row_update[diesel_id]-> Amount : $row_update[diesel1] to $row_update[diesel2]";
	}
}

$update_log = implode(', ',$update_log); 

if($update_log=="")
{
	echo "<script>
		alert('Nothing to update !');
		$('#req_update_button').attr('disabled', false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$update_log = "Token_No: ".$frno.": ".$update_log;

StartCommit($conn);
$flag = true;

$update_diesel_table = Qry($conn,"UPDATE dairy.diesel t1 
INNER JOIN dairy.diesel_cache_own AS t3
ON t1.id = t3.diesel_id
SET 
t1.qty = t3.qty,
t1.rate = t3.rate,
t1.amount = t3.amount,
t1.narration = t3.dsl_narr
WHERE t3.token_no='$frno' AND t3.save='1' AND t1.id = t3.diesel_id");

if(!$update_diesel_table){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_diesel_entry = Qry($conn,"UPDATE dairy.diesel_entry t2 
INNER JOIN dairy.diesel_cache_own AS t3
ON t2.id = t3.diesel_id
SET 
t2.diesel = t3.amount,
t2.card_pump = t3.card_pump,
t2.card = t3.card_no,
t2.veh_no = t3.veh_no,
t2.dsl_company = t3.dcom,
t2.narration = t3.dsl_narr,
t2.dsl_mobileno = t3.mobile_no
WHERE t3.token_no='$frno' AND t3.save='1' AND t2.id = t3.diesel_id 
AND IF(t2.card_pump='PUMP',t2.download,t2.done)!='1'");

if(!$update_diesel_entry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$updated_diesel_rows = AffectedRows($conn);

if($updated_diesel_rows!=$total_updated_rows)
{
	$flag = false;
	errorLog("Total Diesel Rows to update in cache is: $total_updated_rows and updated in diesel_fm: $updated_diesel_rows.",$conn,$page_name,__LINE__);
}

$delete_cache_data=Qry($conn,"DELETE FROM diesel_cache WHERE frno='$frno'");

if(!$delete_cache_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_log_qry=Qry($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$frno','OWN_DIESEL_REQ','UPDATE_REQ','$update_log','','ADMIN','$timestamp')");

if(!$update_log_qry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}


if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Updated Successfully !');
		// window.location.href='./request_own_truck.php';
		document.getElementById('button2').click();
		document.getElementById('req_close_button').click();
		// $('#req_update_button').attr('disabled', false);
		// $('#loadicon').hide();
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	// echo "<script>
		// alert('Error While Processing Request !');
		// $('#req_update_button').attr('disabled', false);
		// $('#loadicon').hide();
	// </script>";
	Redirect("Error While Processing Request.","./request_own_truck.php");
	exit();
}

?>
