<?php
require_once("./connect.php");

$trip_no=escapeString($conn,$_POST['trip_no']);

$qry=Qry($conn,"SELECT ");
}
else
{
	echo "<script>
		alert('ERROR : Diesel Request data not found !');
		window.location.href='request_market_truck.php';
	</script>";
	exit();
}

if(!$qry)
{
	echo mysqli_error($conn);
}

if(mysqli_num_rows($qry)>0)
{
	?>
	<table class="table table-bordered" style="font-family:Verdana;font-size:12px;">
	<tr>
       <th class="bg-info" style="font-family:Century Gothic;font-size:14px;letter-spacing:1px;" colspan="16">Market Truck Diesel Requests :</th>
    </tr>
		<tr>    
			<th>Branch</th>
			<th>VouId</th>
			<th>Company</th>
			<th>Amount</th>
			<th>Cash</th>
			<th>TruckNo</th>
			<th>LRNo</th>
			<th>DieselNarr.</th>
			<th>Date</th>
			<th>Done</th>
			<th></th>
			<th></th>
		</tr>	
	<?php
	while($row=mysqli_fetch_array($qry))
	{
		if($row['done']==1)
		{
			$done="<font color='green'>OK</font>";
		}
		else
		{
		  $done="";	
		}
		
		echo "<tr>
				<td>$row[branch]</td>
				<td>$row[fno]</td>
				<td>$row[com]</td>
				<td>$row[diesel]</td>
				<td>$row[cash]</td>
				<td>$row[tno]</td>
				<td>$row[lrno]</td>
				<td>$row[dsl_nrr]</td>
				<td>".date('d-m-y',strtotime($row['pay_date']))."</td>
				<td>$done</td>
				<td>
					<button type='button' onclick=EditReq('$row[fno]') class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
				</td>
				<td>
					<button type='button' onclick=DeleteReq('$row[fno]') class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button>
				</td>
			</tr>
			";
	}
	echo "</table>";
	
	echo "<script>
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<script>
		alert('No record Found !');
		$('#loadicon').hide();	
	</script>";
	exit();
}
?>