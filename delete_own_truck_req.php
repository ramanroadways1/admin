<?php
require_once('./connect2.php');
	
$frno=mysqli_real_escape_string($conn2,strtoupper($_POST['frno']));

$sel_frno = mysqli_query($conn2,"SELECT unq_id FROM diesel WHERE id='$frno'");

$row_22=mysqli_fetch_array($sel_frno);

$frno = $row_22['unq_id'];

if($frno=='')
{
	echo "<script>
		alert('Unable to fetch Token Number.');
	</script>";
	exit();
}

$chk_for_cash_diesel_entry = mysqli_query($conn2,"SELECT id FROM diesel WHERE unq_id='$frno' AND cash_diesel=1");

if(mysqli_num_rows($chk_for_cash_diesel_entry)>0)
{
	echo "<script>
		alert('You Can\'t edit Cash Diesel Entry !');
		window.location.href='./request_own_truck.php';
	</script>";
	exit();
}

$fetch_data = mysqli_query($conn2,"SELECT trip_id,trans_id,SUM(amount) as amount,narration,tno,date,branch FROM diesel WHERE unq_id='$frno'");
if(!$fetch_data)
{
	echo mysqli_error($conn2);
	exit();
}

if(mysqli_num_rows($fetch_data)==0)
{
	echo "<script>
		alert('No result found.');
	</script>";
	exit();
}

$row=mysqli_fetch_array($fetch_data);

$trip_id=$row['trip_id'];
$amount=$row['amount'];

$data_old="VouId : $frno, TruckNo : $row[tno], TransId : $row[trans_id], TripId : $row[trip_id], Amount : $row[amount], 
Narration : :$row[narration]";

if($amount==0)
{
	echo "<script>
		alert('Amount is zero $amount $frno');
	</script>";
	exit();
}

if($row['trip_id']=='')
{
	$delete=mysqli_query($conn2,"DELETE FROM diesel WHERE unq_id='$frno'");
	if(!$delete)
	{
		echo mysqli_error($conn2);
		exit();
	}
	
	if(mysqli_affected_rows($conn2)==0)
	{
		echo "<script>
			alert('Unable to delete Request.');
			window.location.href='request_own_truck.php';
		</script>";
		exit();
	}
	else
	{
		$delete2=mysqli_query($conn2,"DELETE FROM diesel_entry WHERE unq_id='$frno'");
		if(!$delete2)
		{
			echo mysqli_error($conn2);
			exit();
		}
	}
}
else
{
	$update_trip = mysqli_query($conn2,"UPDATE trip SET diesel=diesel-'$amount' WHERE id='$trip_id'");
	if(!$update_trip)
		{
			echo mysqli_error($conn2);
			exit();
		}
		
	if(mysqli_affected_rows($conn2)==0)
	{
		$update_trip_after_hisab = mysqli_query($conn2,"UPDATE trip_final SET diesel=diesel-'$amount' WHERE trip_id='$trip_id'");
		if(!$update_trip_after_hisab)
		{
			echo mysqli_error($conn2);
			exit();
		}
		
		if(mysqli_affected_rows($conn2)==0)
		{
			echo "<script>
				alert('Unable to delete Request.');
				window.location.href='request_own_truck.php';
			</script>";
			exit();
		}
	}

	$delete=mysqli_query($conn2,"DELETE FROM diesel WHERE unq_id='$frno'");
	if(!$delete)
	{
		echo mysqli_error($conn2);
		exit();
	}
	
	if(mysqli_affected_rows($conn2)==0)
	{
		echo "<script>
			alert('Unable to delete Request.');
			window.location.href='request_own_truck.php';
		</script>";
		exit();
	}
	else
	{
		$delete2=mysqli_query($conn2,"DELETE FROM diesel_entry WHERE unq_id='$frno'");
		if(!$delete2)
		{
			echo mysqli_error($conn2);
			exit();
		}
	}	
		
}	

$update_log=mysqli_query($conn,"INSERT INTO edit_log_admin(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
		('$frno','OWN_DIESEL_REQ','DELETE_REQ','$data_old','$row[branch]','ADMIN','$timestamp')");
		
		echo "<script>
			alert('Request Deleted Successfully !');
			window.location.href='./request_own_truck.php';
		</script>";
		exit();
?>