<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">e-LR Format :</h3>
              </div>
              
	<div class="card-body">
	
<form autocomplete="off" action="e_lr2.php" method="POST">

	<div class="row">	
		
		<div class="col-md-3">	
              <div class="form-group">
                  <label>LR number <font color="red"><sup>*</sup></font></label>
                  <input name="lrno" type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" required />
              </div>
		</div>
		<input type="hidden" value="LR" name="by" />
		<div id="button_div" class="col-md-2">
			<label>&nbsp;</label>
			<br />
			<button type="submit" id="button2" class="btn pull-right btn-danger">Submit !</button>
		</div>
	</div>
		
</form>	
		
</div>
	
	 </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
 
<?php include ("./footer.php"); ?>

</div>
</body>
</html>