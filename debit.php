<?php
require_once("./connect.php");
?>
<!DOCTYPE html>
<html>

<?php include("head_files.php"); ?>

<body class="hold-transition sidebar-mini" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">
<div class="wrapper">
  
  <?php include "header.php"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Debits :</h3>
              </div>
              
	<div class="card-body">
	
<div id="lr_result"></div>
		
<script type="text/javascript">
$(function() {
    $("#tno").autocomplete({
      source: 'autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			 $(event.target).focus();
			 $("#button2").attr("disabled",true);
		alert('Truck Number doest not exists.');
        }
    }, 
    focus: function (event, ui) {
        $("#button2").attr("disabled",false);
		return false;
    }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#DebitForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button2").attr("disabled", true);
	$.ajax({
	url: "./fetch_debit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_main").html(data);
		$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script> 

<form id="DebitForm">

	<div class="row">	
		<div class="col-md-3">	
               <div class="form-group">
                  <label>Debit Type <font color="red"><sup>*</sup></font></label>
                  <select name="SearchBy" onchange="SearchBy(this.value)" class="form-control" required>
					<option value="">---Select---</option>
					<option value="ITR">ITR</option>
					<option value="DEBIT-BRANCH">Branch to Branch</option>
					<option value="HO">Head-Office</option>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Select Branch <font color="red"><sup>*</sup></font></label>
                  <select name="branch" class="form-control" required>
					<option value="">---Select---</option>
					<?php
					$fetch_branch = mysqli_query($conn,"SELECT username from user WHERE role='2' order by username asc");
					if(mysqli_num_rows($fetch_branch)>0)
					{
						while($row_branch=mysqli_fetch_array($fetch_branch))
						{
							echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
						}
					}
					?>
				  </select>
              </div>
			</div>
			
			<div class="col-md-3">	
               <div class="form-group">
                  <label>Days Ago <font color="red"><sup>*</sup></font></label>
                  <select class="form-control" name="range" id="date_range">
					<option value="-0 days">Today's</option>
					<option value="-1 days">Last 2 days</option>
					<option value="-4 days">Last 5 days</option>
					<option value="-6 days">Last 7 days</option>
					<option value="-9 days">Last 10 days</option>
					<option value="-14 days">Last 15 days</option>
					<option value="-29 days">Last 30 days</option>
					<option value="-59 days">Last 60 days</option>
					<option value="-89 days">Last 90 days</option>
					<option value="-119 days">Last 120 days</option>
					<option value="FULL">FULL REPORT</option>
				</select>
              </div>
			</div>
			
			<div id="button_div" class="col-md-2">
				<label></label>
				<br />
				<button type="submit" id="button2" class="btn pull-right btn-danger">Check !</button>
			</div>
			
		</div>
		
</form>	
		
		<div class="row">	
			<div class="col-md-12 table-responsive" style="overflow:auto">	
			
				<div id="result_main"></div>
			
			</div>
		</div>
	</div>
	
	<div class="card-footer">
			<!--<button id="lr_sub" type="submit" class="btn btn-primary" disabled>Update LR</button>
			<button id="lr_delete" type="button" onclick="Delete()" class="btn pull-right btn-danger" disabled>Delete LR</button>-->
    </div>
	
	<div id="result_main2"></div>
				
           </div>
			
		</div>
        </div>
      </div>
    </section>
  </div>
  
<script>  
function Edit(id)
{
	$('#modal_frno').val(id);
	
	$("#loadicon").show();
	jQuery.ajax({
	url: "load_debit.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data) {
		$("#result_modal_data").html(data);
		document.getElementById("req_modal_button").click();
	},
	error: function() {}
	});
}

function Delete(id)
{
	$("#loadicon").show();
	jQuery.ajax({
	url: "delete_debit.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data) {
	$("#result_main2").html(data);
	$("#loadicon").hide();
	},
	error: function() {}
	});
}  
</script>  

<a style="display:none1" id="req_modal_button" data-toggle="modal" data-target="#ReqModal"></a>

<script type="text/javascript">
$(document).ready(function (e) {
$("#UpdateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#req_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_debit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result_req_modal").html(data);
		$("#req_update_button").attr("disabled", false);
		$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script> 

<div class="modal fade" id="ReqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
		
		<form id="UpdateForm" autocomplete="off"> 
			<div class="modal-body">
			
				<div id="result_req_modal"></div>
				
			<input type="hidden" name="frno" id="modal_frno">
				
			<div class="row">
			
				<div class="col-md-12">
                   <div class="form-group">
                      <h4>Update Debits : </h4>
                  </div>
               </div>
			   
				<div class="col-md-12 table-responsive" style="overflow:auto">
					<div id="result_modal_data"></div>
				</div>
				
			</div>
		</div>
		
	<div class="modal-footer">
		<button type="button" id="req_close_button" onclick="document.getElementById('menu_button2').click();" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="req_update_button" class="btn btn-primary">Update Final</button>
	</div>
		</form>
		</div>	
     </div>
 </div>	
  <!-- MODAL -->
 
<?php include ("./footer.php"); ?>

</div>
</body>
</html>