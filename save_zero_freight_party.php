<?php
require_once("./connect.php");

$search_by = escapeString($conn,$_POST['search_by']);
$broker_owner = escapeString($conn,$_POST['broker_owner']);
$party_id = escapeString($conn,$_POST['party_id']);
$timestamp = date("Y-m-d H:i:s");

if($search_by=="" || $broker_owner=="")
{
	echo "<script>
		alert('Party not found. Please check !');
		$('#add_new_button').attr('disabled',true);
		$('#loadicon').hide();	
	</script>";
	exit();
}	

if($search_by=='BROKER')
{
	$col_name="broker_id";
}
else if($search_by=='OWNER')
{
	$col_name="veh_id";
}
else
{
	echo "<script>
		alert('Invalid option selected !');
		$('#add_new_button').attr('disabled',true);
		$('#loadicon').hide();	
	</script>";
	exit();
}


$chk_duplicate = Qry($conn,"SELECT id FROM _zero_freight_party WHERE pan_no='$broker_owner'");

if(!$chk_duplicate){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_duplicate)>0)
{
	echo "<script>
		alert('Duplicate Record Found !');
		$('#add_new_button').attr('disabled',false);
		$('#loadicon').hide();	
	</script>";
	exit();
}
	
StartCommit($conn);
$flag = true;

$insert_record = Qry($conn,"INSERT INTO _zero_freight_party(`$col_name`,pan_no,timestamp) VALUES 
('$party_id','$broker_owner','$timestamp')");

if(!$insert_record){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Record Added Successfully !');
		window.location.href='./zero_freight_parties.php';
	</script>";
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./zero_freight_parties.php");
	exit();
}	
?>