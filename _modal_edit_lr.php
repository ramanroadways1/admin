<script type="text/javascript">
$(function() {
	$("#edit_modal_from").autocomplete({
	source: 'autofill/get_location.php',
	// appendTo: '#appenddiv',
	select: function (event, ui) { 
		
		// if(ui.item.value!=$('#edit_from_loc_lr').val()){
			// $('#from_loc_alert').html('not matching with LR.');
		// }
		// else{
			// $('#from_loc_alert').html('');
		// }
		
		if(ui.item.value!=$('#last_loc_verify').val() && $('#last_loc_verify').val()!=''){
			$('#from_loc_alert').html('not matching with last to station.');
		}
		else if(ui.item.value!=$('#edit_from_loc_lr').val()){
			$('#from_loc_alert').html('not matching with LR.');
		}
		else{
			$('#from_loc_alert').html('');
		}
		
		$('#edit_modal_from').val(ui.item.value);   
		$('#edit_modal_from_id').val(ui.item.id);      
		return false;
	},
	change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#edit_modal_from').val("");   
			$('#edit_modal_from_id').val("");   
			alert('Location does not exists !');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
	$("#edit_modal_to").autocomplete({
	source: 'autofill/get_location.php',
	// appendTo: '#appenddiv',
	select: function (event, ui) { 
	
		// if(ui.item.value!=$('#edit_to_loc_lr').val()){
			// $('#to_loc_alert').html('not matching with LR.');
		// }
		// else{
			// $('#to_loc_alert').html('');
		// }
		
		if(ui.item.value!=$('#next_loc_verify').val() && $('#next_loc_verify').val()!=''){
			$('#to_loc_alert').html('not matching with next from station.');
		}
		else if(ui.item.value!=$('#edit_to_loc_lr').val()){
			$('#to_loc_alert').html('not matching with LR.');
		}
		else{
			$('#to_loc_alert').html('');
		}
		
		$('#edit_modal_to').val(ui.item.value);   
		$('#edit_modal_to_id').val(ui.item.id);      
		return false;
	},
	change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#edit_modal_to').val("");   
			$('#edit_modal_to_id').val("");   
			alert('Location does not exists !');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
	$("#edit_modal_consignee").autocomplete({
	source: 'autofill/get_consignee.php',
	// appendTo: '#appenddiv',
	select: function (event, ui) { 
		$('#edit_modal_consignee').val(ui.item.value);   
		$('#edit_modal_con2_id').val(ui.item.id);      
		$('#edit_modal_con2_gst').val(ui.item.gst);      
		$('#edit_modal_consignee_gst').val(ui.item.gst);      
		$('#edit_modal_con2_pincode').val(ui.item.pincode);      
		return false;
	},
	change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#edit_modal_consignee').val("");   
			$('#edit_modal_con2_id').val("");   
			$('#edit_modal_con2_gst').val("");   
			$('#edit_modal_consignee_gst').val("");   
			$('#edit_modal_con2_pincode').val("");   
			alert('Consignee does not exists !');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(document).ready(function (e) {
$("#UpdateLrForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#lr_update_button").attr("disabled", true);
	$.ajax({
	url: "./update_lr_by_id.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script> 

<form id="UpdateLrForm" autocomplete="off"> 
<div class="modal fade" id="modal_edit_LR" style="background:#DDD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
		<div class="modal-body">
	
<div class="row">

	<div class="col-md-12" style="display:none" id="trip_done_alert">
		<div class="alert alert-danger" role="alert">
		  Voucher attached to Trip. You can't edit this LR.
		</div>
	</div>
	
	<div class="col-md-4">
       <div class="form-group">
           <label class="control-label mb-1">LR No <font color="red"><sup>*</sup></font></label>
			 <input type="text" id="edit_modal_lrno" name="lrno" class="form-control" readonly required>
      </div>
	</div>
	
	<!--
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label mb-1">LR Date <font color="red"><sup>*</sup></font></label>
			<input type="date" max="<?php echo date('Y-m-d'); ?>" readonly id="edit_modal_lr_date" name="lr_date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" required />
		 </div>
	</div> -->
	
	<div class="col-md-4">
       <div class="form-group">
          <label class="control-label mb-1">From Station (LR) <font color="red"><sup>*</sup></font></label>
		  <input type="text" readonly id="edit_from_loc_lr" name="from_loc_lr" class="form-control" required />
      </div>
   </div>
	 
   <div class="col-md-4">
       <div class="form-group">
           <label class="control-label mb-1">To Station (LR) <font color="red"><sup>*</sup></font></label>
		   <input type="text" readonly id="edit_to_loc_lr" name="to_loc_lr" class="form-control" required />
      </div>
   </div>
   
   <div class="col-md-12" id="edit_modal_lrs_record">
   
   </div>
   
	<div class="col-md-6">
       <div class="form-group">
          <label class="control-label mb-1">From Station <font color="red"><sup>*</sup> <span id="from_loc_alert"></span></font></label>
		  <input type="text" id="edit_modal_from" name="from" class="form-control" required />
      </div>
   </div>
	 
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">To Station <font color="red"><sup>*</sup> <span id="to_loc_alert"></span></font></label>
		   <input type="text" id="edit_modal_to" name="to" class="form-control" required />
      </div>
   </div>
   
   <!--
   <div class="col-md-6">
       <div class="form-group">
          <label class="control-label mb-1">Change From Loc. in LR Entry &nbsp; <input style="margin-top:11px;" id="change_from_lr" type="checkbox" name="change_from_lr" /></label>
	  </div>
   </div>
   
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Change To Loc. in LR Entry &nbsp; <input style="margin-top:11px;" id="change_to_lr" type="checkbox" name="change_to_lr" /></label>
	  </div>
   </div>
	 -->
	 
	<div class="col-md-12">
       <div class="form-group">
           <label class="control-label mb-1">Consignor <font color="red"><sup>*</sup></font></label>
			 <input type="text" id="edit_modal_consignor" name="consignor" readonly class="form-control" required />
		</div>
   </div>
   
  <div class="col-md-12">
       <div class="form-group">
           <label class="control-label mb-1">Consignee <font color="red"><sup>*</sup></font></label>
			 <input type="text" id="edit_modal_consignee" readonly name="consignee" class="form-control" required />
		</div>
   </div>
   
   <!--
   <div class="col-md-5">
       <div class="form-group">
           <label class="control-label mb-1">Consignee GST <font color="red"><sup>*</sup></font></label>
			 <input type="text" id="edit_modal_consignee_gst" name="consignee_gst" class="form-control" required />
		</div>
   </div>-->
   
   <div class="col-md-6">
       <div class="form-group">
           <label class="control-label mb-1">Act. Weight <font color="red"><sup>*</sup></font></label>
			 <input type="number" min="0.001" step="any" id="edit_modal_act_weight" name="act_weight" class="form-control" required />
		</div>
   </div>
   
   <div class="col-md-6">
		<div class="form-group">
			<label class="control-label mb-1">LR Date <font color="red"><sup>*</sup></font></label>
			<input type="date" max="<?php echo date('Y-m-d'); ?>" id="edit_modal_lr_date" name="lr_date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" required />
		 </div>
	</div>
   
	</div>
</div>
		
	<input type="hidden" name="edit_modal_consignee_gst" id="edit_modal_consignee_gst">
	<input type="hidden" name="is_crossing" id="edit_modal_crossing_var">
	<input type="hidden" name="last_loc_verify" id="last_loc_verify">
	<input type="hidden" name="next_loc_verify" id="next_loc_verify">
	
	<input type="hidden" name="from_id" id="edit_modal_from_id">
	<input type="hidden" name="to_id" id="edit_modal_to_id">
	<input type="hidden" name="con1_id" id="edit_modal_con1_id">
	<input type="hidden" name="con2_id" id="edit_modal_con2_id">
	<input type="hidden" name="con2_gst" id="edit_modal_con2_gst">
	<input type="hidden" name="con2_pincode" id="edit_modal_con2_pincode">
	
	<input type="hidden" name="id" id="lr_edit_id">
	<input type="hidden" name="frno" id="edit_modal_frno">
				
	<div class="modal-footer">
		<button type="button" id="close_lr_update_button" class="btn btn-danger" data-dismiss="modal">Close</button>
		<button type="submit" id="lr_update_button" class="btn btn-primary">Update LR</button>
	</div>
		</div>	
     </div>
 </div>
</form>
  
<script>  
function EditLR(id)
{
	$('#lr_edit_id').val(id);
	$("#loadicon").show();
	jQuery.ajax({
	url: "load_lr_by_id.php",
	data: 'id=' + id,
	type: "POST",
	success: function(data) {
		$("#edit_modal_lrs_record").html(data);
	},
	error: function() {}
	});
}  
</script>  